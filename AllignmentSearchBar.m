//
//  AllignmentSearchBar.m
//  PQ
//
//  Created by Rahul Patil on 28/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AllignmentSearchBar.h"
#import "LanguageManager.h"


@implementation AllignmentSearchBar

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForLeftAlignedTextView];
}


- (void)setTextAlignmentForLeftAlignedTextView {
    UITextField *searchTextField = [self valueForKey:@"_searchField"];
    [self setValue:NSLocalizedString(@"Cancel",@"Cancel") forKey:@"_cancelButtonText"];
    [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search",@"Search") attributes:@{NSForegroundColorAttributeName: UIColorFromRGB(0x019F6E)}]];
    if ([Helper isCurrentLanguageRTL]) {
        [searchTextField setTextAlignment:NSTextAlignmentRight];
    } else {
        [searchTextField setTextAlignment:NSTextAlignmentLeft];
    }
}

@end
