//
//  AmazonTransfer.h
//  
//
//  Created by rahul Sharma on 04/09/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSCore.h"
#import "AWSS3.h"
#import "AWSS3TransferManager.h"

typedef void(^AmazonTransferCompletionBlock)(BOOL success, id result , NSError *error);
typedef void(^AmazonTransferProgressBlock)(int64_t progressSize, int64_t expectedSize);

#define AmazonAccessKey           @"AKIAJJIQHHJ6QQDIJQ2A"
#define AmazonSecretKey           @"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"
		
//Live
#define BucketForProfile          @"zirideimages"
// Dev
//#define BucketForProfile          @"pqdevtest"
//Staging
//#define BucketForProfile          @"pq.stage"
//Security
//#define BucketForProfile          @"pq.security"

#define BaseURL                   @"https://s3-us-west-1.amazonaws.com"

@interface AmazonTransfer : NSObject

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                         accessKey:(NSString*)accessKey
                         secretKey:(NSString*)secretKey;

+ (void) upload:(NSString*)localFilePath
         fileKey:(NSString*)fileKey
        toBucket:(NSString*)bucket
       mimeType:(NSString *) mimeType
 completionBlock:(AWSS3TransferUtilityUploadCompletionHandlerBlock)completionBlock;

+ (void) download:(NSString*)fileKey
       fromBucket:(NSString*)bucket
 completionBlock:(AWSS3TransferUtilityDownloadCompletionHandlerBlock) completionBlock;


@end


