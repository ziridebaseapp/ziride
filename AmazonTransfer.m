//
//  AmazonTransfer.m
//  
//
//  Created by rahul Sharma on 04/09/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AmazonTransfer.h"


static AmazonTransfer *shared = nil;

@implementation AmazonTransfer

+(instancetype)sharedInstance
{
    if (!shared) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    
    return shared;
}


/*
 List of AmazonRegion
 
 AWSRegionUnknown,
 AWSRegionUSEast1,
 AWSRegionUSWest1,
 AWSRegionUSWest2,
 AWSRegionEUWest1,
 AWSRegionEUCentral1,
 AWSRegionAPSoutheast1,
 AWSRegionAPNortheast1,
 AWSRegionAPSoutheast2,
 AWSRegionSAEast1,
 AWSRegionCNNorth1,

 */

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                         accessKey:(NSString*)accessKey
                         secretKey:(NSString*)secretKey {
    
    AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:accessKey secretKey:secretKey];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:regionType credentialsProvider:credentialsProvider];

    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
}


+ (void) upload:(NSString *) localFilePath
         fileKey:(NSString *) fileKey
        toBucket:(NSString *) bucket
        mimeType:(NSString *) mimeType
 completionBlock:(AWSS3TransferUtilityUploadCompletionHandlerBlock) completionBlock
{
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    [[transferUtility uploadFile:[NSURL fileURLWithPath:localFilePath]
                          bucket:bucket
                             key:fileKey
                     contentType:mimeType
                      expression:nil
                completionHander:completionBlock]
               continueWithBlock:^id(AWSTask *task) {

        if (task.error) {
            
            NSLog(@"Error: %@", task.error);
        }
        
        if (task.result) {
            // Do something with uploadTask.
        }
      
        return nil;
    }];
    
 
}

+ (void) download:(NSString *) fileKey
       fromBucket:(NSString *) bucket
  completionBlock:(AWSS3TransferUtilityDownloadCompletionHandlerBlock) completionBlock {
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    [[transferUtility downloadDataFromBucket:bucket
                                         key:fileKey
                                  expression:nil
                            completionHander:completionBlock]
                            continueWithBlock:^id(AWSTask *task) {
         
             if (task.error) {
                 
                 NSLog(@"Error: %@", task.error);
             }
             
             if (task.result) {
                 // Do something with uploadTask.
             }
         
            return nil;
     }];

}

@end
