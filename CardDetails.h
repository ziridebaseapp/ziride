//
//  CardDetails.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>


@interface CardDetails : NSManagedObject

@property (nonatomic, retain) NSString * expMonth;
@property (nonatomic, retain) NSString * expYear;
@property (nonatomic, retain) NSString * idCard;
@property (nonatomic, retain) NSString * last4;
@property (nonatomic, retain) NSString * cardtype;
@property (nonatomic, retain) NSString * isDefault;

@end
