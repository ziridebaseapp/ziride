//
//  CardDetails.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CardDetails.h"

@implementation CardDetails

@dynamic expMonth;
@dynamic expYear;
@dynamic idCard;
@dynamic last4;
@dynamic cardtype;
@dynamic isDefault;

@end
