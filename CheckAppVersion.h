

//
//  checkAppVersion.h
//  iServe_Customer
//
//  Created by Apple on 17/11/16.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckAppVersion : NSObject
+(instancetype) sharedInstance;
-(void)sendRequestToUpdateVersion;
-(void)checkAppHasUpdatedVersion;

@end
