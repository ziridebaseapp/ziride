//
//  checkAppVersion.m
//  iServe_Customer
//
//  Created by Apple on 17/11/16.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CheckAppVersion.h"

static CheckAppVersion *checkAppVersion = nil;
@implementation CheckAppVersion

+(instancetype)sharedInstance {
    if (!checkAppVersion) {
        checkAppVersion = [[self alloc] init];
    }
    return checkAppVersion;
}


-(void)checkAppHasUpdatedVersion {
    NSString* appID = [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSMutableURLRequest *request =  [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:100];
    NSURLSession *delegateFreeSession;
    delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                        delegate:nil
                                                   delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *task;
    task = [delegateFreeSession dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if (!data || !response || error) {
                                          [Helper showAlertWithTitle:NSLocalizedString(@"Error!", @"Error!") Message:[error localizedDescription]];
                                          return;
                                      }
                                      NSDictionary *appDetails = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                      if ([appDetails[@"resultCount"] integerValue] == 1) {
                                          NSString* appStoreVersion = appDetails[@"results"][0][@"version"];
                                          NSString* currentVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
                                          if([appStoreVersion doubleValue] > [currentVersion doubleValue]){
                                              UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Update", @"Update") Message:NSLocalizedString(@"Your application version is outdated. Please update the application from the store.",@"Your application version is outdated. Please update the application from the store.")];
                                              RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
                                              [alertView setContainerView:containerView];
                                              [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Update", @"Update"), NSLocalizedString(@"Later", @"Later"), nil]];
                                              [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
                                                  if (buttonIndex == 0) {
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://%@", itunesURL]]];
                                                  }
                                                  [alertView close];
                                              }];
                                              [alertView setUseMotionEffects:true];
                                              [alertView show];
                                          }
                                      }
                                  }];
    [task resume];
}

-(void)sendRequestToUpdateVersion {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable]) {
        NSDictionary *params = @{
                                 @"ent_sess_token":[Utility sessionToken],
                                 @"ent_dev_id":[Utility deviceID],
                                 @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                 @"ent_user_type":@"2",
                                };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"UpdateAppVersion"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self sendRequestToUpdateVersionResponse:response];
                                   }
                               }];
    }
}


-(void)sendRequestToUpdateVersionResponse:(NSDictionary *)dictionary {
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    if(!dictionary) {
        return;
    }
    if ([dictionary[@"Error"] length] != 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:dictionary[@"errMSg"]];
    } else {
        if ([[dictionary objectForKey:@"errFlag"]integerValue] == 0) {
            [[NSUserDefaults standardUserDefaults]setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"appVersion"];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:dictionary[@"errMSg"]];
        }
    }
}


@end
