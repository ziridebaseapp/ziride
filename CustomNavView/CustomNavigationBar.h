//
//  CustomNavigationBar.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomNavigationBarDelegate;
@interface CustomNavigationBar : UIView
@property(nonatomic,weak)id <CustomNavigationBarDelegate> delegate;
@property(nonatomic,strong)UILabel *labelTitle;
@property(nonatomic,strong)UIButton *rightbarButton;
@property(nonatomic,strong)UIButton *rightbarButton2;
@property(nonatomic,strong)UIButton *leftbarButton;

-(void)setTitle:(NSString*)title;
-(void)setTitle:(NSString*)title withColor:(UIColor *) color;
-(void)setRightBarButtonTitle:(NSString*)title;
-(void)setLeftBarButtonTitle:(NSString*)title;
-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff;
-(void)createRightBarButton;
-(void)hideTitleButton:(BOOL)toHide;
-(void)hideRightBarButton:(BOOL)hide;
-(void)addTitleButton;
-(void)hideLeftMenuButton:(BOOL)toHide;
//-(void)hideLeftMenuButtonInMapVC:(BOOL)toHide;
-(void)removeRightBarButton;
-(void)removeTitleButton;
-(void)addRightButton:(NSString *)imageName;

@end

@protocol CustomNavigationBarDelegate <NSObject>

@optional
-(void)rightBarButtonClicked:(UIButton*)sender;
-(void)leftBarButtonClicked:(UIButton*)sender;

@end
