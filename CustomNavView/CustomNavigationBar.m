//
//  CustomNavigationBar.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"

@interface CustomNavigationBar()
@end
@implementation CustomNavigationBar
@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize leftbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:UIColorFromRGB(0x019F6E)];
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *imageLeftbarButton = [UIImage imageNamed:@"map_menu"];
        leftbarButton.titleLabel.font = [UIFont fontWithName:fontNormal size:11];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu"] forState:UIControlStateHighlighted];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu_on"] forState:UIControlStateNormal];
        [leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        if ([Helper isCurrentLanguageRTL]) {
            labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 25, screenWidth-90, 40)];
            leftbarButton.frame = CGRectMake(screenWidth-imageLeftbarButton.size.height-10, 20, imageLeftbarButton.size.width, imageLeftbarButton.size.height);
        } else {
            labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 25, screenWidth-90, 40)];
            leftbarButton.frame = CGRectMake(10, 20, imageLeftbarButton.size.width, imageLeftbarButton.size.height);
        }
        labelTitle.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:labelTitle Text:@"ZIRIDE" WithFont:fontBold FSize:16 Color:UIColorFromRGB(0xFFFFFF)];
        [self addSubview:labelTitle];
        [self addSubview:leftbarButton];
    }
    return self;
}

-(void)createRightBarButton {
    UIImage *buttonImage = [UIImage imageNamed:@"map_menu"];
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        rightbarButton.frame = CGRectMake(10, 20, buttonImage.size.width, buttonImage.size.height);
    } else {
        rightbarButton.frame = CGRectMake(screenWidth-buttonImage.size.height-10, 20, buttonImage.size.width, buttonImage.size.height);
    }
    rightbarButton.titleLabel.font = [UIFont fontWithName:fontNormal size:11];
    [rightbarButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    [rightbarButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateSelected];
    [rightbarButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];
}

-(void)addTitleButton {
    if(_rightbarButton2) {
        [self removeTitleButton];
    }
    UIImage *buttonImage = [UIImage imageNamed:@"Navigationbar_logo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: buttonImage];
    CGFloat height = imageView.frame.size.height;
    CGFloat width = imageView.frame.size.width;
    _rightbarButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightbarButton2 setUserInteractionEnabled:NO];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    _rightbarButton2.frame = CGRectMake((screenWidth-width)/2,(64-height)/2+10, width, height);
    [_rightbarButton2 setBackgroundImage:buttonImage forState:UIControlStateNormal];
    _rightbarButton2.tag = 100;
    [self addSubview:_rightbarButton2];
    [_rightbarButton2 setHidden:NO];
    [labelTitle setHidden:YES];
    [self setBackgroundColor:UIColorFromRGB(0x019F6E)];
}

-(void)addRightButton:(NSString *)imageName {
    if(rightbarButton) {
        [rightbarButton removeFromSuperview];
        rightbarButton = nil;
    }
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIImage *imageRightBarButton = [UIImage imageNamed:@"home_btn_menu"];
    if ([Helper isCurrentLanguageRTL]) {
        rightbarButton.frame = CGRectMake(10, 20, imageRightBarButton.size.width, imageRightBarButton.size.height);
    } else {
        rightbarButton.frame = CGRectMake(screenWidth - imageRightBarButton.size.width - 10, 20, imageRightBarButton.size.width, imageRightBarButton.size.height);
    }
    [rightbarButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [Helper setButton:rightbarButton Text:@"" WithFont:fontNormal FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [rightbarButton setTitleColor:[UIColor colorWithWhite:0.384 alpha:1.000] forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    if (imageName.length) {
        NSString* offImageName = [NSString stringWithFormat:@"%@_off",imageName];
        NSString* onImageName = [NSString stringWithFormat:@"%@_on",imageName];
        imageRightBarButton = [UIImage imageNamed:offImageName];
        if ([Helper isCurrentLanguageRTL]) {
            rightbarButton.frame = CGRectMake(10, 20, imageRightBarButton.size.width, imageRightBarButton.size.height);
            rightbarButton.transform = CGAffineTransformMakeScale(-1, 1);
            rightbarButton.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            rightbarButton.frame = CGRectMake(screenWidth - imageRightBarButton.size.width - 10, 23, imageRightBarButton.size.width, imageRightBarButton.size.height);
            rightbarButton.transform = CGAffineTransformMakeScale(1, 1);
        }
        [rightbarButton setBackgroundImage:[UIImage imageNamed:offImageName] forState:UIControlStateNormal];
        [rightbarButton setBackgroundImage:[UIImage imageNamed:onImageName] forState:UIControlStateHighlighted];
        [rightbarButton setTitle:NSLocalizedString(@"Promo Code", @"Promo Code") forState:UIControlStateNormal];
        [rightbarButton setTitleColor:UIColorFromRGB(0x019F6E) forState:UIControlStateNormal];
        rightbarButton.titleLabel.font = [UIFont fontWithName:fontNormal size:10];
        [rightbarButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 9, 40)];
    }
    [self addSubview:rightbarButton];
}

-(void)removeRightBarButton {
    [rightbarButton setHidden:YES];
    [rightbarButton removeFromSuperview];
}

-(void)removeTitleButton {
    [_rightbarButton2 setHidden:YES];
    [_rightbarButton2 removeFromSuperview];
}


-(void)hideTitleButton:(BOOL)toHide {
    if (toHide) {
        [_rightbarButton2 setHidden:YES];
        [labelTitle setHidden:NO];
    } else {
        [_rightbarButton2 setHidden:NO];
        [labelTitle setHidden:YES];
    }
}

-(void)hideLeftMenuButton:(BOOL)toHide {
    if (toHide) {
        [leftbarButton setBackgroundImage:nil forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:nil forState:UIControlStateHighlighted];;
    } else {
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu_on"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu"] forState:UIControlStateHighlighted];
    }
}


//-(void)hideLeftMenuButtonInMapVC:(BOOL)toHide {
//    if (toHide) {
//        [leftbarButton setBackgroundImage:nil forState:UIControlStateNormal];
//        [leftbarButton setBackgroundImage:nil forState:UIControlStateHighlighted];;
//    } else {
//        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu"] forState:UIControlStateNormal];
//        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"map_menu_on"] forState:UIControlStateHighlighted];
//    }
//}

-(void)hideRightBarButton:(BOOL)hide; {
    [rightbarButton setHidden:hide];
}

-(void)setTitle:(NSString*)title {
    [Helper setToLabel:labelTitle Text:title WithFont:fontBold FSize:16 Color:UIColorFromRGB(0xffffff)];
    labelTitle.numberOfLines = 0;
}

-(void)setTitle:(NSString*)title withColor:(UIColor *) color {
    [Helper setToLabel:labelTitle Text:title WithFont:fontBold FSize:16 Color:color];
    labelTitle.numberOfLines = 0;
    labelTitle.adjustsFontSizeToFitWidth = YES;
    [_rightbarButton2 setHidden:YES];
    [labelTitle setHidden:NO];
}

-(void)setRightBarButtonTitle:(NSString*)title {
    [rightbarButton setTitle:title forState:UIControlStateNormal];
    rightbarButton.titleLabel.numberOfLines = 0;
    rightbarButton.titleLabel.adjustsFontSizeToFitWidth = true;
    if (title.length && title.length > 3 && [title containsString:NSLocalizedString(@"TIP", @"TIP")] ) {
        rightbarButton.titleLabel.font = [UIFont fontWithName:fontNormal size:10];
    } else {
        rightbarButton.titleLabel.font = [UIFont fontWithName:fontNormal size:12];
    }
}

-(void)setLeftBarButtonTitle:(NSString*)title {
    if([title isEqualToString:NSLocalizedString(@"BACK", @"BACK")]) {
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
        if ([Helper isCurrentLanguageRTL]) {
            leftbarButton.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            leftbarButton.transform = CGAffineTransformMakeScale(1, 1);
        }
    } else if([title isEqualToString:NSLocalizedString(@"BACK", @"BACK")]) {
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
        [leftbarButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
        if ([Helper isCurrentLanguageRTL]) {
            leftbarButton.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            leftbarButton.transform = CGAffineTransformMakeScale(1, 1);
        }
    } else {
        [leftbarButton setTitle:title forState:UIControlStateNormal];
    }
}

-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        leftbarButton.frame = CGRectMake(screenWidth-imageOn.size.height-10, 20, imageOn.size.width, imageOn.size.height);
    } else {
        leftbarButton.frame = CGRectMake(10, 20, imageOn.size.width, imageOn.size.height);
    }
    [leftbarButton setBackgroundImage:imageOn forState:UIControlStateNormal];
    [leftbarButton setBackgroundImage:imageOff forState:UIControlStateHighlighted];
}

-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}

-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}
@end
