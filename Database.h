//
//  Database.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Database : NSObject

#pragma mark - Card Details -
+ (NSArray *)getCardDetails;
+ (void)makeDataBaseEntry:(NSDictionary *)dictionary;
+ (BOOL)DeleteCard:(NSString*)Campaign_id;
+ (void)DeleteAllCard;
+ (BOOL)updateCardDetailsForCardId:(NSString *)cardID andStatus:(NSString *)isDefault;


#pragma mark - Address -
+(void)addSourceAddressInDataBase:(NSDictionary *)dictionary;
+(void)addFavouriteSourceAddress:(NSDictionary *)dictionary;
+(NSArray *)getSourceAddressFromDataBase;
+ (void)deleteAllSourceAddress;

#pragma mark - Common Address methods -
+(NSString *) getAddressIDForRemoveFromFavouriteAddress:(NSString *)address1 and:(NSString *)address2;
+(BOOL)updateAddressWithAddressID:(NSString *)addressID asFavouriteTag:(int)isFavourite;
+(BOOL)checkAddressIsFavouriteInDatabase:(NSString *)address1 and:(NSString *)address2;
+(NSArray *)getFavouriteAddressFromDataBase:(NSString *)address1 and:(NSString *)address2;
@end
