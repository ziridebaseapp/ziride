//
//  Database.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "Database.h"
#import "AppDelegate.h"
#import "CardDetails.h"
#import "SourceAddress.h"

@implementation Database

+(void)makeDataBaseEntry:(NSDictionary *)dictionary {
    NSError *error;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    CardDetails *entity = [NSEntityDescription insertNewObjectForEntityForName:@"CardDetails" inManagedObjectContext:context];
    [entity setExpMonth:flStrForObj(dictionary [@"exp_month"])];
    [entity setExpYear:flStrForObj(dictionary [@"exp_year"])];
    [entity setIdCard:flStrForObj(dictionary [@"id"])];
    [entity setCardtype:flStrForObj(dictionary [@"type"])];
    [entity setLast4:flStrForObj(dictionary [@"last4"])];
    [entity setIsDefault:flStrForStr(dictionary[@"isDefault"])];
    [context save:&error];
}

+(NSArray *)getCardDetails {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    return result;
}

+(BOOL)DeleteCard:(NSString*)Campaign_id {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    if ([context save:&error]) {
        return YES;
    } else {
        return NO;
    }
}


/**
 *  On logout it will delete all cards
 */

+(void)DeleteAllCard {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


+(BOOL)updateCardDetailsForCardId:(NSString *)cardID andStatus:(NSString *)isDefault {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *error;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", cardID];
    [fetch setPredicate:pred];
    CardDetails *card = [[context executeFetchRequest:fetch error:nil] lastObject];
    [card setIsDefault:isDefault];
    BOOL isSaved = [context save:&error];
    return isSaved;
}


#pragma mark - Source Address -
/**
 *  Adding Address to the Database and managing that
 */

+(void)addSourceAddressInDataBase:(NSDictionary *)dictionary {
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    if(addressArr.count) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyId CONTAINS[cd] %@", dictionary[@"place_id"]];
        NSArray *filterdArray = [[addressArr filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            return;
        }
    }
    NSDictionary *location = dictionary[@"geometry"][@"location"];
    NSError *error;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SourceAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"SourceAddress" inManagedObjectContext:context];
    NSString *add1 = flStrForStr(dictionary[@"name"]);
    NSString *add2 = flStrForStr(dictionary[@"formatted_address"]);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"types CONTAINS[cd] %@",@"postal_code"];
    NSArray *filterdArray = [[[dictionary[@"address_components"] mutableCopy] filteredArrayUsingPredicate:predicate] mutableCopy];
    NSString *zipCode = @"";
    if(filterdArray.count > 0) {
        zipCode = filterdArray[0][@"long_name"];
    }
    NSString *addressID = @"";
    if ([[dictionary allKeys] containsObject:@"addressID"]) {
        addressID = flStrForStr(dictionary[@"addressID"]);
    }
    [entity setSrcAddress:flStrForStr(add1)];
    [entity setSrcAddress2:flStrForStr(add2)];
    [entity setSrcLatitude:[NSNumber numberWithDouble:[location[@"lat"] doubleValue]]];
    [entity setSrcLongitude:[NSNumber numberWithDouble:[location[@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[(@"place_id")])];
    [entity setAddressID:addressID];
    [entity setLocationType:flStrForStr(dictionary[@"locationType"])];
    [entity setIsFavourite:[NSNumber numberWithInt:[dictionary[@"isFavourite"] intValue]]];
    [entity setAddressType:[NSNumber numberWithInt:[dictionary[@"addressType"] intValue]]];
    [entity setZipCode:flStrForStr(zipCode)];
    [context save:&error];
}

+(void)addFavouriteSourceAddress:(NSDictionary *)dictionary {
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    NSString *placeID = @"";
    if(addressArr.count) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"srcAddress CONTAINS[cd] %@ || srcAddress2 CONTAINS[cd] %@", dictionary[@"address1"], dictionary[@"address2"]];
        NSArray *filterdArray = [[addressArr filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            SourceAddress *add = (SourceAddress*)filterdArray[0];
            placeID = add.keyId;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            for (NSManagedObject *product in filterdArray) {
                [context deleteObject:product];
            }
        }
    }
    NSError *error;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SourceAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"SourceAddress" inManagedObjectContext:context];    
    NSString *add1 = flStrForStr(dictionary[@"address1"]);
    NSString *add2 = flStrForStr(dictionary[@"address2"]);
    if (!add2.length) {
        NSArray *arr = [add1 componentsSeparatedByString:@", "];
        if (arr.count >= 2) {
            add1 = arr[0];
            add2 = [arr componentsJoinedByString:@", "];
        }
    }
    
    [entity setSrcAddress:flStrForStr(add1)];
    [entity setSrcAddress2:flStrForStr(add2)];
    [entity setSrcLatitude:[NSNumber numberWithDouble:[dictionary[@"lat"] doubleValue]]];
    [entity setSrcLongitude:[NSNumber numberWithDouble:[dictionary[@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[@"place_id"])];
    [entity setAddressID:flStrForStr(dictionary[@"addressID"])];
    [entity setLocationType:flStrForStr(dictionary[@"locationType"])];
    [entity setIsFavourite:[NSNumber numberWithInt:[dictionary[@"isFavourite"] intValue]]];
    [entity setZipCode:flStrForStr(dictionary[@"zipCode"])];
    [entity setAddressType:[NSNumber numberWithInt:[dictionary[@"addressType"] intValue]]];
    [context save:&error];
}

+(NSArray *)getSourceAddressFromDataBase {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    NSArray* reversedResultArray = [[result reverseObjectEnumerator] allObjects];
    return reversedResultArray;
}

+ (void)deleteAllSourceAddress {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


#pragma mark - Common Address methods -

+(NSString *)getAddressIDForRemoveFromFavouriteAddress:(NSString *)address1 and:(NSString *)address2 {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"SourceAddress"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"srcAddress CONTAINS[cd] %@ || srcAddress2 CONTAINS[cd] %@", address1, address2];
    fetchRequest.predicate=predicate;
    SourceAddress *addr = [[context executeFetchRequest:fetchRequest error:nil] lastObject];
    if ([addr.addressID length]) {
        return addr.addressID;
    } else {
        return @"";
    }
}

+(BOOL)updateAddressWithAddressID:(NSString *)addressID asFavouriteTag:(int)isFavourite {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"SourceAddress"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"addressID CONTAINS[cd] %@", addressID];
    fetchRequest.predicate=predicate;
    NSError *error;
    SourceAddress *addr = [[context executeFetchRequest:fetchRequest error:nil] lastObject];
    [addr setIsFavourite:[NSNumber numberWithInt:isFavourite]];
    if (isFavourite == 0) {
        [addr setLocationType:@""];
    }
    BOOL isSaved = [context save:&error];
    return isSaved;
}


+(BOOL)checkAddressIsFavouriteInDatabase:(NSString *)address1 and:(NSString *)address2 {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity;
    entity = [NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"(srcAddress CONTAINS[cd] %@ || srcAddress2 CONTAINS[cd] %@) && isFavourite CONTAINS[cd] %d", address1, address2, 1];
    [fetch setPredicate:predicate];
    NSError *fetchError;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    if (fetchedProducts.count) {
        return YES;
    }
    return NO;
}


+(NSArray *)getFavouriteAddressFromDataBase :(NSString *)address1 and:(NSString *)address2 {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity;
    entity = [NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"(srcAddress CONTAINS[cd] %@ || srcAddress2 CONTAINS[cd] %@) && isFavourite CONTAINS[cd] %d", address1, address2, 1];
    [fetch setPredicate:predicate];
    NSError *fetchError;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    return fetchedProducts;
}

@end
