
//
//  FBLoginHandler.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "FBLoginHandler.h"
#import <Accounts/Accounts.h>


@interface FBLoginHandler ()

@property (strong, nonatomic) NSArray *readPermission;
@property (strong, nonatomic) NSDictionary *parameters;

@end

@implementation FBLoginHandler

static FBLoginHandler *share;

+ (id)sharedInstance{
    if (!share) {
        share  = [[self alloc] init];
    }
    return share;
}

- (instancetype)init {
    if (self = [super init]) {
        self.readPermission = @[@"public_profile", @"email", @"user_birthday", @"user_friends"];
        self.parameters = @{@"fields": @"picture, email, name, gender, birthday, first_name, last_name"};
    }
    return self;
}

/**
 *  Login with facebook
 */
- (void)loginWithFacebook:(UIViewController *)viewController {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login setLoginBehavior:FBSDKLoginBehaviorNative];
    [login logInWithReadPermissions:self.readPermission
                 fromViewController:viewController
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                
                                if (error) {
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(didFailWithError:)]) {
                                        [self.delegate didFailWithError:error];
                                    }
                                } else if (result.isCancelled) {
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(didUserCancelLogin)]) {
                                        [self.delegate didUserCancelLogin];
                                    }
                                } else {
                                    [self getDetailsFromFacebook];
                                }
                            }];
}

/**
 *  Get User details
 */
- (void)getDetailsFromFacebook {
    if ([FBSDKAccessToken currentAccessToken])  {
        
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                                       parameters:self.parameters];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (error) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(didFailWithError:)]) {
                    [self.delegate didFailWithError:error];
                }
            } else {
                if (self.delegate && [self.delegate respondsToSelector:@selector(didFacebookUserLoginWithDetails:)]) {
                    [self.delegate didFacebookUserLoginWithDetails:result];
                }
            }
        }];
    }
}

@end
