//
//  Helper.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol HelperDelegate <NSObject>

-(void)rightBarButtonClicked:(UIButton*)sender;
-(void)leftBarButtonClicked:(UIButton*)sender;
    
@end


@interface Helper : NSObject

@property(nonatomic,weak)id <HelperDelegate> delegate;



@property(nonatomic,assign)float _latitude;
@property(nonatomic,assign)float _longitude;
@property(nonatomic,assign)int menu_valueChanged;
@property(nonatomic,assign)int locate_ValueChanged;
@property(nonatomic,strong)NSString *location;

@property(nonatomic, strong) NSMutableArray *helperContry;
@property(nonatomic, strong) NSMutableArray *helperCity;


+ (id)sharedInstance;

+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color;
+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color;
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message;

+ (void) showToastWithMessage:(NSString *)message onView:(UIView *)view;
+ (void) removeToastMessageOnView:(UIView *)view;

+(UIImage *)setPlaceholderToCardType:(NSString *)mycardType;

+(NSString *)getModelName;
+(NSString *)removeWhiteSpaceFromURL:(NSString *)url;
+(NSString *)stripExtraSpacesFromString:(NSString *)string;
+(NSString*)getDateandTime:(NSString *)aDatefromServer;
+(NSString *)getCurrentDateTime;
+(NSString *)getCurrentNetworkDateTime;
+(NSDate *)getCurrentNetworkDateTimeForTimeStamp;
+(NSString*)getCurrentDate;
+(NSString*)getCurrentTime;
+(NSString*)getDayDate;
+(UIColor *)getColorFromHexString:(NSString *)hexString;
+(NSDate *)convertGMTtoLocal:(NSString *)gmtDateStr;
+(NSDate *)convertLocalToGMT:(NSString *)localDateStr;
+(NSString *)getDateInString:(NSString *)changableDate;
+(NSString *)getDateInStringForInvoice:(NSString *)changableDate;
+(NSString *) getDateInStringForCancelledPopUp:(NSString *)changableDate;
+(NSString *) getDateInStringForBookingDetails:(NSString *)changableDate;
+(NSDate *)getDateAndTimeFromString:(NSString *)aDatefromServer;
+ (NSString *)relativeDateStringForDate:(NSDate *)date;

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize;

+ (NSString *)timeFormatted:(int)totalSeconds;
//check EmailValidation

+(BOOL)emailValidationCheck:(NSString *)emailToValidate;
+(BOOL)mobileNumberValidate:(NSString*)number;
+ (NSString *)extractNumberFromText:(NSString *)text;
+(int)checkPasswordStrength:(NSString *)password;

+(BOOL)isIphone5;

+(NSString*)getDay:(NSDate *)date;

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (void) addShadowToView:(UIView *)view;
+ (void) addShadowToView:(UIView *)view withShadowOpacity:(float)opacityValue;
+(void) addTopShadow:(UIView *) view;
+(void) addBottomShadow:(UIView *) view;
+(void) addBottomShadow:(UIView *) view withOpacity:(float) opacityValue;
+(void) addBottomShadowOnButton:(UIButton *) button;
+(void) addBottomShadowOnButtonForBottomView:(UIButton *) button;
+(void) addShadowForAllSide:(UIView *) view;
+(void) addDotedBorder:(UIView *)view;

+ (UIView *)createViewWithTitle:(NSString*)title Message:(NSString*)message;

+(void) pushLikePresentVC:(UIView *)view;
+(void) popLikeDismissVC:(UIView *) view;
+(void) pushFromLeft:(UIView *)view;
+(void) pushFromRight:(UIView *)view;
+(void) checkForPush:(UIView *)nextView;
+(void) popFromLeft:(UIView *)view;
+(void) popFromRight:(UIView *)view;
+(void) checkForPop:(UIView *)nextView;

+(NSString *)getCurrencyLocal :(float)amount;

+(BOOL)checkForUpdateAppVersion:(NSString *)mandatoryVersion;

+ (BOOL)isCurrentLanguageRTL;


@end
