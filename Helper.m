//
//  Helper.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "Helper.h"
#import "NHNetworkTime.h"
#import <sys/utsname.h>
#import "RPCustomIOSAlertView.h"
#import "UILabel+DynamicHeight.h"



#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


static NSDictionary* deviceNamesByCode = nil;

@implementation Helper

static Helper *helper;
@synthesize _latitude;
@synthesize _longitude;
@synthesize menu_valueChanged;
@synthesize location;
@synthesize locate_ValueChanged;
@synthesize delegate;

+ (id)sharedInstance {
	if (!helper) {
		helper  = [[self alloc] init];
	}
	return helper;
}


+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color {
    lbl.textColor = color;
    if (txt != nil) {
        lbl.text = txt;
    }
    if (font != nil) {
        lbl.font = [UIFont fontWithName:font size:_size];
    }
//    lbl.backgroundColor = [UIColor clearColor];
}

+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color
{
    [btn setTitle:txt forState:UIControlStateNormal];
    [btn setTitleColor:t_color forState:UIControlStateNormal];
    if (s_color != nil) {
        [btn setTitleShadowColor:s_color forState:UIControlStateNormal];
    }
    if (font != nil) {
        btn.titleLabel.font = [UIFont fontWithName:font size:_size];
    } else {
        btn.titleLabel.font = [UIFont systemFontOfSize:_size];
    }
}



+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message {
    RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
    [alertView setContainerView:[Helper createViewWithTitle:title Message:message]];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Ok", @"Ok"), nil]];
    [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
        [alertView close];
    }];
    [alertView setUseMotionEffects:true];
    [alertView show];
}

+ (UIView *)createViewWithTitle:(NSString*)title Message:(NSString*)message {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, screenWidth - 60, 50)];
    titleLabel.text = title;
    titleLabel.numberOfLines = 1;
    titleLabel.font = [UIFont fontWithName:fontBold size:15];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, screenWidth - 60, 0)];
    messageLabel.text = message;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont fontWithName:fontNormal size:13];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.adjustsFontSizeToFitWidth = YES;
    CGRect frame = messageLabel.frame;
    frame.size.height = [messageLabel measureHeightLabel] + 10;
    messageLabel.frame = frame;
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth - 40, CGRectGetMaxY(messageLabel.frame) + 10)];
    [containerView addSubview:titleLabel];
    [containerView addSubview:messageLabel];
    return containerView;
}

+ (void) showToastWithMessage:(NSString *)message onView:(UIView *)view {
    UIView *toastView = [view viewWithTag:122];
    UILabel *messageLabel = [toastView viewWithTag:123];
    if (![messageLabel.text isEqualToString:message]) {
        messageLabel = [[UILabel alloc] init];
        [Helper setToLabel:messageLabel Text:message WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0xFFFFFF)];
        CGFloat messageHeight = [messageLabel measureHeightLabel];
        if (messageHeight < 50) {
            messageHeight = 50;
        } else {
            messageHeight += 10;
        }
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        toastView = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight, screenWidth, messageHeight)];
        toastView.tag = 122;
        [view addSubview:toastView];
        
        messageLabel.frame = CGRectMake(0, 0, screenWidth, messageHeight);
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.backgroundColor = UIColorFromRGB(0x15B8EC);
        messageLabel.numberOfLines = 0;
        messageLabel.tag = 123;
        [toastView addSubview:messageLabel];
        
        [UIView animateWithDuration:1 animations:^{
            CGRect frame = toastView.frame;
            frame.origin.y -= messageHeight;
            toastView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
    }
}

+ (void) removeToastMessageOnView:(UIView *)view {
    UIView *toastView = [view viewWithTag:122];
    if (toastView != nil) {
        int duration = 1; // duration in seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:1 animations:^{
                UILabel *messageLabel = (UILabel *)[toastView viewWithTag:123];
                CGFloat messageHeight = [messageLabel measureHeightLabel];
                if (messageHeight < 50) {
                    messageHeight = 50;
                } else {
                    messageHeight += 10;
                }
                CGRect frame = toastView.frame;
                frame.origin.y += messageHeight;
                toastView.frame = frame;
            } completion:^(BOOL finished) {
                [toastView removeFromSuperview];
            }];
        });
    }
}

+ (NSString *)getModelName {
    NSString *platform;
    struct utsname systemInfo;
    uname(&systemInfo);
    platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if (!deviceNamesByCode) {
        deviceNamesByCode = @{
                              @"i386"      :@"iPhone Simulator",
                              @"x86_64"    :@"iPhone Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPhone9,1" :@"iPhone 7",          //
                              @"iPhone9,3" :@"iPhone 7",          //
                              @"iPhone9,2" :@"iPhone 7 Plus",     //
                              @"iPhone9,4" :@"iPhone 7 Plus",     //
                              
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    NSString* deviceName = [deviceNamesByCode objectForKey:platform];
    if (!deviceName) {
        if ([platform rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        } else if([platform rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        } else if([platform rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        } else {
            deviceName = @"Unknown";
        }
    }
    return deviceName;
}

+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url {
	NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
	[string replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
	return string;
}


+ (NSString *)stripExtraSpacesFromString:(NSString *)string {
	NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
	NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
	NSArray *parts = [string componentsSeparatedByCharactersInSet:whitespaces];
	NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
	return [filteredArray componentsJoinedByString:@" "];
}




+(NSString*)getCurrentDate {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    return dateInStringFormated;
}

+(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:MM:SS"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    return dateInStringFormated;
}



+(NSString*)getDayDate {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    return dateInStringFormated;
}

+(NSString*)getDay:(NSDate *)date {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    return dateInStringFormated;
}

+(NSString *)getCurrentDateTime {
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    return dateInStringFormated;
}

+(NSString *)getCurrentNetworkDateTime {
    NSDate *now = [NSDate networkDate];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    return dateInStringFormated;
}

+(NSDate *) getCurrentNetworkDateTimeForTimeStamp {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *currentDateTime = [dateFormatter dateFromString:[self getCurrentNetworkDateTime]];
    return currentDateTime;
}


+(NSString*)getDateandTime:(NSString *)aDatefromServer{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

+ (UIColor *)getColorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+(NSDate *)convertGMTtoLocal:(NSString *)gmtDateStr {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *gmt = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:gmt];
    NSDate *localDate = [formatter dateFromString:gmtDateStr];
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT];
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] + timeZoneOffset;
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    return localCurrentDate;
}



+(NSDate *)convertLocalToGMT:(NSString *)localDateStr {
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *localDate = [formatter dateFromString: localDateStr];
    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT];
    NSTimeInterval gmtTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];
    return gmtDate;
}


/*
 *  Validates an Email Address.
 */
+ (BOOL) emailValidationCheck: (NSString *) emailToValidate {
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

+ (NSString *)extractNumberFromText:(NSString *)text {
    NSString *str = @"";
    NSRange rangeOfLastSpace = [text rangeOfString:@"[" options:NSBackwardsSearch];
    if (rangeOfLastSpace.location != NSNotFound) {
        str = [text substringWithRange:NSMakeRange(rangeOfLastSpace.location +1, 4)];
    }
    return str;
}

/*
 *  Validates an Phone Number.
 */
+ (BOOL)mobileNumberValidate:(NSString*)number {
    NSString *numberRegEx = @"[0-9]{6,14}$";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}


+(UIImage *)setPlaceholderToCardType:(NSString *)mycardType{
    NSString* cardTypeName;
    if([mycardType isEqualToString:@"amex"])
        cardTypeName = @"stp_card_amex";
    else if([mycardType isEqualToString:@"diners"])
        cardTypeName = @"stp_card_diners";
    else if([mycardType isEqualToString:@"discover"])
        cardTypeName = @"stp_card_discover";
    else if([mycardType isEqualToString:@"jcb"])
        cardTypeName = @"stp_card_jcb";
    else if([mycardType isEqualToString:@"masterCard"])
        cardTypeName = @"stp_card_mastercard";
    else if([mycardType isEqualToString:@"Visa"])
        cardTypeName = @"stp_card_visa";
    else
        cardTypeName = @"stp_card_placeholder_template";
    return [UIImage imageNamed:cardTypeName];
}

+(BOOL)isIphone5 {
    if([[UIScreen mainScreen] bounds].size.height >= 568)
        return YES;
    else
        return NO;
}

+ (NSString *)timeFormatted:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02dH:%02dM:%02dS",hours, minutes, seconds];
}

+(NSString*)getDateAndTimeForV1:(NSString *)aDatefromServer{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

+(NSString*)getDateAndTimeForV2:(NSString *)aDatefromServer{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

+(NSString*)getDateAndTimeForV3:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

+(NSString*)getDateAndTimeForV4:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

+(NSString *) getDateInString:(NSString *)changableDate {
    NSString *returnDate;
    NSString *date = [self getDateAndTimeForV1:changableDate];
    NSString *time = [self getDateAndTimeForV2:changableDate];
    NSString *at =  NSLocalizedString(@"at", @"at");
    returnDate = [NSString stringWithFormat:@"%@ %@ %@", date, at, time];
    return returnDate;
}

+(NSString *) getDateInStringForInvoice:(NSString *)changableDate {
    NSString *returnDate;
    NSString *date = [self getDateAndTimeForV3:changableDate];
    NSString *time = [self getDateAndTimeForV4:changableDate];
    NSString *at =  NSLocalizedString(@"at", @"at");
    returnDate = [NSString stringWithFormat:@"%@ %@ %@", date, at, time];
    return returnDate;
}

+(NSString *) getDateInStringForCancelledPopUp:(NSString *)changableDate {
    NSString *returnDate;
    NSString *date = [self getDateAndTimeForV1:changableDate];
    NSString *time = [self getDateAndTimeForV2:changableDate];
    NSString *at =  NSLocalizedString(@"|", @"|");
    returnDate = [NSString stringWithFormat:@"%@ %@ %@", date, at, time];
    return returnDate;
}


+(NSString *) getDateInStringForBookingDetails:(NSString *)changableDate {
    NSString *returnDate;
    NSString *date = [self getDateAndTimeForV1:changableDate];
    NSString *time = [self getDateAndTimeForV2:changableDate];
    returnDate = [NSString stringWithFormat:@"%@ | %@", date, time];
    return returnDate;
}

+(NSDate *)getDateAndTimeFromString:(NSString *)aDatefromServer{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//    [dateFormatter setLocale:locale];
    NSString *retTime = [dateFormatter stringFromDate:date];
    NSDate *retDate = [dateFormatter dateFromString:retTime];
    return retDate;
}


+ (NSString *)relativeDateStringForDate:(NSDate *)date {
    NSCalendarUnit units = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour | NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[Helper getCurrentNetworkDateTimeForTimeStamp]
                                                                    options:0];
    
    if (components.year > 1) {
        NSString *str = NSLocalizedString(@"yrs ago", @"yrs ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.year, str];
    } else if (components.year == 1) {
        NSString *str = NSLocalizedString(@"yr ago", @"yr ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.year, str];
    } else if (components.month > 1) {
        NSString *str = NSLocalizedString(@"months ago", @"months ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.month, str];
    } else if (components.month == 1) {
        NSString *str = NSLocalizedString(@"month ago", @"month ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.month, str];
    } else if (components.weekOfYear > 1) {
        NSString *str = NSLocalizedString(@"wks ago", @"wks ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.weekOfYear, str];
    } else if (components.weekOfYear == 1) {
        NSString *str = NSLocalizedString(@"wk ago", @"wk ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.weekOfYear, str];
    } else if (components.day > 1) {
        NSString *str = NSLocalizedString(@"days ago", @"days ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.day, str];
    } else if (components.day == 1) {
        NSString *str = NSLocalizedString(@"day ago", @"day ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.day, str];
    } else if (components.hour > 1) {
        NSString *str = NSLocalizedString(@"hrs ago", @"hrs ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.hour, str];
    } else if (components.hour == 1) {
        NSString *str = NSLocalizedString(@"hr ago", @"hr ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.hour, str];
    } else if (components.minute > 0) {
        NSString *str = NSLocalizedString(@"min ago", @"min ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.minute, str];
    } else if (components.second > 0) {
        NSString *str = NSLocalizedString(@"sec ago", @"sec ago");
        return [NSString stringWithFormat:@"%ld %@", (long)components.minute, str];
    } else {
        return NSLocalizedString(@"Now", @"Now");
    }
}

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize {
    float widthScale = 0;
    float heightScale = 0;
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    float scale = MIN(widthScale, heightScale);
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    return newSize;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (void) addShadowToView:(UIView *)view {
///    [view.layer setCornerRadius:5.0f];
//    [view.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [view.layer setBorderWidth:0.5f];
//    [view.layer setShadowColor:[UIColor blackColor].CGColor];
//    [view.layer setShadowOpacity:0.7];
//    [view.layer setShadowRadius:5.0];
//    [view.layer setShadowOffset:CGSizeMake(3.0, 2.0)];
    
//    float shadowSize = 5;
//    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(view.frame.origin.x - shadowSize / 2,
//                                                                           view.frame.origin.y - shadowSize / 2,
//                                                                           view.frame.size.width + shadowSize,
//                                                                           view.frame.size.height + shadowSize)];
//    view.layer.masksToBounds = NO;
//    view.layer.shadowColor = [UIColor blackColor].CGColor;
//    view.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//    view.layer.shadowOpacity = 0.4f;
//    view.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.25;
    view.layer.shadowRadius = 1;
    view.layer.shadowPath = shadowPath.CGPath;
}

+ (void) addShadowToView:(UIView *)view withShadowOpacity:(float)opacityValue {
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = opacityValue;
    view.layer.shadowRadius = 1;
    view.layer.shadowPath = shadowPath.CGPath;
}

+(void) addTopShadow:(UIView *) view {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
    CGRect shadowPath = UIEdgeInsetsInsetRect(view.bounds, contentInsets);
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.1f;
    view.layer.masksToBounds = NO;
}

+(void) addBottomShadow:(UIView *) view {
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 1.0;
    view.layer.shadowRadius = 0.5;
    view.layer.shadowOffset = CGSizeMake(0, 1.0f);
    view.layer.masksToBounds = NO;
}

+(void) addBottomShadow:(UIView *) view withOpacity:(float) opacityValue {
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = opacityValue;
    view.layer.shadowRadius = 0.5;
    view.layer.shadowOffset = CGSizeMake(0, 1.0f);
    view.layer.masksToBounds = NO;
}



+(void) addBottomShadowOnButton:(UIButton *) button {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(5, 5, -5, 5);
    CGRect shadowPath = UIEdgeInsetsInsetRect(button.bounds, contentInsets);
    button.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    button.layer.shadowColor = [[UIColor blackColor] CGColor];
    button.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    button.layer.shadowOpacity = 0.4f;
    button.layer.masksToBounds = NO;
}

+(void) addBottomShadowOnButtonForBottomView:(UIButton *) button {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(2, 2, -2, 2);
    CGRect shadowPath = UIEdgeInsetsInsetRect(button.bounds, contentInsets);
    button.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    button.layer.shadowColor = [[UIColor blackColor] CGColor];
    button.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    button.layer.shadowOpacity = 0.4f;
    button.layer.masksToBounds = NO;
}

+(void) addShadowForAllSide:(UIView *) view {
    UIView* shadowView = [[UIView alloc] initWithFrame:view.frame];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
    shadowView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    shadowView.layer.shadowOpacity = 0.5;
    shadowView.layer.shadowRadius = 4.0f;
    [view.superview insertSubview:shadowView belowSubview:view];
    [shadowView addSubview:view];
}

+(void) addDotedBorder:(UIView *)view {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = [UIColor colorWithRed:67/255.0f green:37/255.0f blue:83/255.0f alpha:1].CGColor;
    shapeLayer.fillColor = nil;
    shapeLayer.lineDashPattern = @[@10, @10];
    [view.layer addSublayer:shapeLayer];
    shapeLayer.path = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
    shapeLayer.frame = view.bounds;
    view.backgroundColor = UIColorFromRGB(0xF8F9FB);
}

+(void) pushLikePresentVC:(UIView *)view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionMoveIn;//For the destination VC
    transition.subtype = kCATransitionFromTop;
    [view.layer addAnimation:transition forKey:kCATransition];
}

+(void) popLikeDismissVC:(UIView *) view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.4f;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFromBottom;
    [view.layer addAnimation:transition forKey:kCATransition];
}

    
+(void) pushFromLeft:(UIView *)view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.25;
    transition.type = kCATransitionMoveIn;//For the destination VC
    transition.subtype = kCATransitionFromRight;
    [view.layer addAnimation:transition forKey:kCATransition];
}

+(void) pushFromRight:(UIView *)view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.25;
    transition.type = kCATransitionMoveIn;//For the destination VC
    transition.subtype = kCATransitionFromLeft;
    [view.layer addAnimation:transition forKey:kCATransition];
}
    
+(void)checkForPush:(UIView *)nextView {
    if ([Helper isCurrentLanguageRTL]) {
        [self pushFromRight:nextView];
    } else {
        [self pushFromLeft:nextView];
    }
}
    
+(void)popFromLeft:(UIView *)view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionReveal;//For the destination VC
    transition.subtype = kCATransitionFromRight;
    [view.layer addAnimation:transition forKey:kCATransition];
}
    
+(void)popFromRight:(UIView *)view {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionReveal;//For the destination VC
    transition.subtype = kCATransitionFromLeft;
    [view.layer addAnimation:transition forKey:kCATransition];
}

    
+(void)checkForPop:(UIView *)nextView {
    if ([Helper isCurrentLanguageRTL]) {
        [self popFromLeft:nextView];
    } else {
        [self popFromRight:nextView];
    }
}

+(NSString *)getCurrencyLocal:(float)amount {
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setMaximumFractionDigits:2];
    [currencyFormatter setMinimumFractionDigits:2];
    [currencyFormatter setAlwaysShowsDecimalSeparator:YES];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *someAmount = [NSNumber numberWithFloat:amount];
//    NSString *str = NSLocalizedString(@"EGP", @"EGP");
    NSString *Currencystring = [currencyFormatter stringFromNumber:someAmount];
    return Currencystring;
}


#pragma mark - Validation Methods -

+(int)checkPasswordStrength:(NSString *)password{
    unsigned long int  len = password.length;
    int strength = 0;
    if (len == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter a password", @"Please enter a password")];
        return 0;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password must contain atleast one uppercase letter", @"Password must contain atleast one uppercase letter")];
        return 0;
    } else {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password must contain atleast one lowercase letter", @"Password must contain one lowercase letter")];
        return 0;
    } else {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast one numerical", @"Please enter atleast one numerical")];
        return 0;
    }
    return 1;
}

+ (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    NSAssert(regex, @"Unable to create regular expression");
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    BOOL didValidate = 0;
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    return didValidate;
}


-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}
    
-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
    
+(BOOL)checkForUpdateAppVersion:(NSString *)mandatoryVersion {
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"Version:%@",currentVersion);
    if([mandatoryVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Update", @"Update") Message:NSLocalizedString(@"Your application version is outdated. Please update the application from the store.",@"Your application version is outdated. Please update the application from the store.")];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Update", @"Update"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://%@", itunesURL]]];
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
        return YES;
    }
    return NO;
}


+ (BOOL)isCurrentLanguageRTL {
    return ([NSLocale characterDirectionForLanguage:[NSLocale preferredLanguages][0]] == NSLocaleLanguageDirectionRightToLeft);
}

    
@end
