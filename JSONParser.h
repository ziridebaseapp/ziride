//
//  JSONParser.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
@interface JSONParser : NSObject

- (NSArray *)dictionaryWithContentsOfJSONURLString:(NSData*)data;


// to get location

-(NSArray *)parseGoogleReverseGoecodingForDataForDirection1:(NSData *)data;

@end
