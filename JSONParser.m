//
//  JSONParser.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) 


#import "JSONParser.h"

@implementation JSONParser

- (NSArray *)dictionaryWithContentsOfJSONURLString:(NSData*)data{
    NSArray *result;
    NSString *strResponse = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding: NSASCIIStringEncoding];
    NSDictionary *dict =
    [NSJSONSerialization JSONObjectWithData: [strResponse dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: nil];
    
    result = [[NSArray alloc] initWithObjects:dict, nil];
    return result;
}

/*
 To get google Direction Response
 */

- (NSArray *)parseGoogleReverseGoecodingForDataForDirection1:(NSData *)data {
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	NSError *error;
	SBJsonParser *json = [SBJsonParser new] ;
	NSDictionary *googleDirectionsResponse = (NSDictionary *)[json objectWithString:responseString error:&error];
	NSMutableArray *array = [[NSMutableArray alloc] init] ;
	if ([[googleDirectionsResponse objectForKey:@"status"] isEqualToString:@"OK"]) {
		NSArray *results = (NSArray *)[googleDirectionsResponse objectForKey:@"results"];
		for (NSDictionary *addressComponents in results) {
            NSDictionary *dictAddressComponents = [addressComponents objectForKey:@"address_components"];
			NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
			for (NSDictionary *dictComponent in dictAddressComponents) {
				NSArray *listType = [dictComponent objectForKey:@"types"];
                if (listType.count > 0) {
                    if ([[listType objectAtIndex:0] isEqualToString:@"administrative_area_level_1"]
                        || [[listType objectAtIndex:0] isEqualToString:@"administrative_area_level_2"]
                        || [[listType objectAtIndex:0] isEqualToString:@"locality"]
                        || [[listType objectAtIndex:0] isEqualToString:@"country"]) {
                        [dictionary setObject:[dictComponent objectForKey:@"long_name"] forKey:[listType objectAtIndex:0]];
                    }
                }
			}
			[array addObject:dictionary];
			break;
		}
	}
	return array;
}


@end
