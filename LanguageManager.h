//
//  LanguageManager.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
//  Singleton that manages the language selection and translations for strings in the app.
//

#import <Foundation/Foundation.h>

@class Locale;

@interface LanguageManager : NSObject

@property (nonatomic, copy) NSArray *availableLocales;

+ (LanguageManager *)sharedLanguageManager;
- (void)setLanguageWithLocale:(Locale *)locale;
- (Locale *)getSelectedLocale;
- (NSString *)getTranslationForKey:(NSString *)key;

@end
