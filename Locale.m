//
//  Localisation.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
//  Object representing a locale, containing a language code, country code and name.
//

#import "Locale.h"

@implementation Locale

- (id)initWithLanguageCode:(NSString *)languageCode countryCode:(NSString *)countryCode name:(NSString *)name {
    if (self = [super init]) {
        self.languageCode = languageCode;
        self.countryCode = countryCode;
        self.name = name;
    }
    return self;
}

@end
