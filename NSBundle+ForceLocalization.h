//
//  NSBundle+ForceLocalization.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (ForceLocalization)

+(void)setLanguage:(NSString*)language;


@end
