//
//  ProgressIndicator.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface ProgressIndicator : NSObject<MBProgressHUDDelegate>
{
    UIView *onView;
    MBProgressHUD *HUD;
}
@property(nonatomic,strong)UIView *onView;
@property(nonatomic,strong)NSString *displayMessage;


+ (id)sharedInstance;
-(void)showPIOnView:(UIView*)view withMessage:(NSString*)message;
-(void)showMessage:(NSString*)message On:(UIView*)view;
-(void)hideProgressIndicator;
-(MBProgressHUD*)getSharedInstace;
-(void)changePIMessage:(NSString*)_newMessage;
-(void)showPIOnWindow:(UIWindow*)window withMessge:(NSString*)message;

@end
