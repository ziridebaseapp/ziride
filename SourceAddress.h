//
//  SourceAddress.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

@interface SourceAddress : NSManagedObject

@property (nullable, nonatomic, retain) NSString *keyId;
@property (nullable, nonatomic, retain) NSString *srcAddress;
@property (nullable, nonatomic, retain) NSString *srcAddress2;
@property (nullable, nonatomic, retain) NSNumber *srcLatitude;
@property (nullable, nonatomic, retain) NSNumber *srcLongitude;
@property (nullable, nonatomic, retain) NSString *locationType;
@property (nullable, nonatomic, retain) NSNumber *isFavourite;
@property (nullable, nonatomic, retain) NSString *zipCode;
@property (nullable, nonatomic, retain) NSNumber *addressType;
@property (nullable, nonatomic, retain) NSString *addressID;


@end
