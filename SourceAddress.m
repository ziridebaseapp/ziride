//
//  SourceAddress.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SourceAddress.h"

@implementation SourceAddress

@dynamic srcAddress;
@dynamic srcAddress2;
@dynamic srcLatitude;
@dynamic srcLongitude;
@dynamic keyId;
@dynamic locationType;
@dynamic isFavourite;
@dynamic zipCode;
@dynamic addressType;
@dynamic addressID;

@end
