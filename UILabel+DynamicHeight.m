//
//  UILabel+DynamicHeight.m
//  InsightApp
//
//  Created by Surender Rathore on 15/03/14.
//
//

#import "UILabel+DynamicHeight.h"

@implementation UILabel (DynamicHeight)

-(CGSize)sizeOfMultiLineLabel{
    NSAssert(self, @"UILabel was nil");
    //Label text
    NSString *aLabelTextString = [self text];
    //Label font
    UIFont *aLabelFont = [self font];
    //Width of the Label
    CGFloat aLabelSizeWidth = self.frame.size.width;
    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
        //version < 7.0
        return [aLabelTextString sizeWithFont:aLabelFont
                            constrainedToSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0)) {
        //version >= 7.0
        //Return the calculated size of the Label
        return [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aLabelFont
                                                        }
                                              context:nil].size;
    }
    return [self bounds].size;
}

/**
 *  To get Label height Dynamically
 *
 *  @param label label whose height wiil be measure(1st set the text in label before call this method)
 *
 *  @return Label height in float
 */
-(CGFloat)measureHeightLabel {
    if(!self.text.length) {
        return 0;
    }
    CGSize constrainedSize = CGSizeMake(self.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:self.font.fontName size:self.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:self.text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > self.frame.size.width) {
        requiredHeight = CGRectMake(0,0, self.frame.size.width, requiredHeight.size.height);
    }
    CGRect newFrame = self.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}




@end
