//
//  UploadFile.m
//  PQ
//
//  Created by Rahul Patil on 07/09/12.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "UploadFiles.h"
#import "UploadProgress.h"

@interface UploadFile() {
    NSUInteger chunkSize;
	NSUInteger offset;
	NSUInteger thisChunkSize;
	NSUInteger length;
	NSData* myBlob;
    NSString *imageName;
}

@property(nonatomic,strong)NSMutableArray *imagesToUpload;
@property(nonatomic,strong)NSMutableArray *imagesUploadedUrls;
@property(nonatomic,assign)BOOL isUploadingMultipleImages;
@end

@implementation UploadFile
@synthesize imagesToUpload;
@synthesize imagesUploadedUrls;
@synthesize delegate;
@synthesize isUploadingMultipleImages;

-(void)uploadMultipleImages:(NSArray*)images {
    isUploadingMultipleImages = YES;
    imagesToUpload = [[NSMutableArray alloc] initWithArray:images];
    [self selectImageForUpload];
}

-(void)uploadImageFile:(UIImage*)image{
    [self calcImagelength:image];
}

-(void)selectImageForUpload {
    if (imagesToUpload.count > 0) {
        [self uploadImageFile:imagesToUpload[0]];
    }
}

-(void)uploadData:(NSData*)data {
    myBlob =  data;
	length = [myBlob length];
	chunkSize = 1024 * 1024;
	offset = 0;
    imageName = [NSString stringWithFormat:@"%@.xml",[self getCurrentTime]];
    [self uploadImage];
}

-(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    return dateInStringFormated;
}

-(void)calcImagelength:(UIImage*)image {
    myBlob =  UIImageJPEGRepresentation(image,1.0);
	length = [myBlob length];
	chunkSize = 1024 * 1024;
	offset = 0;
    imageName = [NSString stringWithFormat:@"%@%@.jpeg", @"image", [self getCurrentTime]];
    [self uploadImage];
}

-(void)uploadImage {
    thisChunkSize = length - offset > chunkSize ? chunkSize : length - offset;
    NSData* chunk = [NSData dataWithBytesNoCopy:(char *)[myBlob bytes] + offset
                                         length:thisChunkSize
                                   freeWhenDone:NO];
	NSString *binaryString = [chunk base64EncodedStringWithOptions:0];
    [self sendRequestToUploadImageWithChunk:binaryString];
}

-(void)sendRequestToUploadImageWithChunk:(NSString*)binaryString {
    static int valueNumberofChunks = 1;
    NSString *inStr = [NSString stringWithFormat: @"%d",valueNumberofChunks];
    NSDictionary *params = @{
                             @"ent_sess_token" :[Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_snap_name": flStrForStr(imageName),
                             @"ent_snap_chunk": flStrForStr(binaryString),
                             @"ent_upld_from": [NSNumber numberWithInt:2],
                             @"ent_snap_type": [NSNumber numberWithInt:1],
                             @"ent_offset": flStrForStr(inStr),
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_date_time": [Helper getCurrentNetworkDateTime]
                             };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithURL:BASE_URL_UPLOADIMAGE paramas:params onComplition:^(BOOL succeeded, NSDictionary *response) {
        if (succeeded && [response[@"errFlag"] integerValue] == 0) {
            offset += thisChunkSize;
            if(offset < length) {
                valueNumberofChunks++;
                [self uploadImage];
            } else {
                if (!imagesUploadedUrls) {
                    imagesUploadedUrls = [[NSMutableArray alloc] init];
                }
                if ([imagesUploadedUrls indexOfObject:response[@"picURL"]] == NSNotFound) {
                    [imagesUploadedUrls addObject:response[@"picURL"]];
                }
                if (isUploadingMultipleImages) {
                    [imagesToUpload removeObjectAtIndex:0];
                    if (imagesToUpload.count > 0) {
                        [self selectImageForUpload];
                        myBlob = nil;
                    } else {
                        [self notifyForSuccessfullUpload];
                    }
                } else {
                    [self notifyForSuccessfullUpload];
                }
            }
        } else {
            if (delegate && [delegate respondsToSelector:@selector(uploadFile:didFailedWithError:)]) {
                [delegate uploadFile:self didFailedWithError:nil];
            }
        }
    }];
}

-(void)notifyForSuccessfullUpload {
    if (delegate && [delegate respondsToSelector:@selector(uploadFile:didUploadSuccessfullyWithUrl:)]) {
        [delegate uploadFile:self didUploadSuccessfullyWithUrl:imagesUploadedUrls];
    }
}

@end
