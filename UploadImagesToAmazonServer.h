//
//  UploadImagesToAmazonServer.h
//  iServe_AutoLayout
//
//  Created by Rahul Sharma on 7/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UploadImageOnAWSDelegate <NSObject>
-(void)uploadFileDidUploadSuccessfullyWithUrl:(NSString*) imageURL;
-(void)uploadFileDidFailedWithError:(NSError*)error;
@end

@interface UploadImagesToAmazonServer : NSObject
@property(nonatomic,assign)id <UploadImageOnAWSDelegate> delegate;

+(instancetype) sharedInstance;
-(void)uploadProfileImageToServer:(UIImage *)profileImage andMobile:(NSString *)mobileNumber andType:(NSInteger)type;
@end
