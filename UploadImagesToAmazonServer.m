//
//  UploadImagesToAmazonServer.m
//  iServe_AutoLayout
//
//  Created by Rahul Sharma on 7/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UploadImagesToAmazonServer.h"
#import "AWSCore.h"
#import "AWSS3.h"
#import "AmazonTransfer.h"
#import "AppConstants.h"

static UploadImagesToAmazonServer *uploadImageToServer = nil;

@implementation UploadImagesToAmazonServer
@synthesize delegate;

+(instancetype)sharedInstance {
    if (!uploadImageToServer) {
        uploadImageToServer = [[self alloc] init];
    }
    return uploadImageToServer;
}

-(void)uploadProfileImageToServer:(UIImage *)profileImage andMobile:(NSString *)mobileNumber andType:(NSInteger)type {
    NSRange range = [mobileNumber rangeOfString:@"+"];
    if (range.location != NSNotFound) {
        mobileNumber = [mobileNumber substringFromIndex:range.location+1];
    }
    NSString *fullImageName = [NSString stringWithFormat:@"passenger/%@.jpg",mobileNumber];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    NSData *data = UIImageJPEGRepresentation(profileImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:BucketForProfile
                  mimeType:@"image/jpeg"
           completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
               if (!error) {
                   if(type == 1)
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (delegate && [delegate respondsToSelector:@selector(uploadFileDidUploadSuccessfullyWithUrl:)]) {
                               [delegate uploadFileDidUploadSuccessfullyWithUrl:[NSString stringWithFormat:@"%@/%@/%@", BaseURL, BucketForProfile, fullImageName]];
                           }
                            [[ProgressIndicator sharedInstance] hideProgressIndicator];
                       });
               } else {
                   if (delegate && [delegate respondsToSelector:@selector(uploadFileDidFailedWithError:)]) {
                       [delegate uploadFileDidFailedWithError:error];
                   }
               }
    }];
}





@end
