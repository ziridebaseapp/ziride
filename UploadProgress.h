//
//  UploadProgress.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
enum ShowViewFrom {
	Top,
    Bottom
};
@interface UploadProgress : UIView
@property(nonatomic,strong)UILabel *lblMessage;
@property(nonatomic,strong)UIProgressView *progressBar;
@property(nonatomic,assign)int showViewFrom;
-(void)setMessage:(NSString*)message;
-(void)hide;
+ (id)sharedInstance;
-(void)updateProgress:(float)progress;

@end
