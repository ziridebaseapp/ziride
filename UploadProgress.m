//
//  UploadProgress.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "UploadProgress.h"

@implementation UploadProgress
@synthesize lblMessage;
@synthesize progressBar;
@synthesize showViewFrom;
#define _height  50
#define fromTop 0
#define fromBottom 1

static UploadProgress *pv;

+ (id)sharedInstance {
	if (!pv) {
		pv  = [[self alloc] init];
	}
	return pv;
}

-(id)init {
    self = [super init];
    if (self) {
        [self deafultInit];
    }
    return self;
}


-(void)deafultInit {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    progressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(5, 40, screenWidth-10, 10)];
    progressBar.progress = 0;
    [self addSubview:progressBar];
    lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(55, _height/2-25/2, 200, 25)];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.textColor = [UIColor whiteColor];
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = [UIFont boldSystemFontOfSize:14];
    lblMessage.text = NSLocalizedString(@"Uploading...", @"Uploading...");
    [self addSubview:lblMessage];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    self.backgroundColor = [UIColor blackColor];
    if (showViewFrom == fromBottom) {
        self.frame = CGRectMake(0,screenHeight, screenWidth, _height);
    }
    else {
        self.frame = CGRectMake(0,-_height, screenWidth, _height);
    }
    [window addSubview:self];
    CGRect frame = self.frame;
    if (showViewFrom == fromBottom) {
        frame.origin.y = screenHeight - _height;
    }
    else {
        frame.origin.y = 0;
    }
    [UIView animateWithDuration:.4f
                     animations:^{
                         
                         self.frame = frame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}
    
-(void)setMessage:(NSString*)message {
    lblMessage.text = message;
}

-(void)hide {
    CGSize size;
    CGRect frame = self.frame;
    if (showViewFrom == fromBottom) {
        size = [[UIScreen mainScreen] bounds].size;
        frame.origin.y  = size.height;
    } else {
        frame.origin.y = -50;
    }
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         pv = nil;
                     }];
}

-(void)updateProgress:(float)progress {
    [progressBar setProgress:progress animated:YES];
}

@end
