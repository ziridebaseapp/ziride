//
//  AboutViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "WebViewController.h"
#import "TermsnConditionViewController.h"

@interface AboutViewController ()<CustomNavigationBarDelegate>

@end

@implementation AboutViewController
@synthesize topView;
@synthesize topViewevery1Label;
@synthesize topviewroadyoLabel;
@synthesize likeButton;
@synthesize legalButton;
@synthesize rateButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
#pragma mark - UILife Cycle -
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    [Helper setToLabel:topViewevery1Label Text:NSLocalizedString(@"Drives you happy", @"Drives you happy") WithFont:fontNormal FSize:20 Color:UIColorFromRGB(0x222328)];
    [Helper setButton:topviewroadyoLabel Text:websiteLabel WithFont:fontNormal FSize:16 TitleColor:UIColorFromRGB(0x019F6E) ShadowColor:nil];
    
    [Helper setButton:rateButton Text:NSLocalizedString(@"Rate us on App Store", @"Rate us on App Store") WithFont:fontNormal FSize:13 TitleColor:[UIColor blackColor] ShadowColor:nil];
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:NSLocalizedString(@"Like us on facebook", @"Like us on facebook") WithFont:fontNormal FSize:13 TitleColor:[UIColor blackColor] ShadowColor:nil];
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:NSLocalizedString(@"Legal", @"Legal") WithFont:fontNormal FSize:13 TitleColor:[UIColor blackColor] ShadowColor:nil];
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    if ([Helper isCurrentLanguageRTL]) {
        rateButton.titleLabel.textAlignment = NSTextAlignmentRight;
        likeButton.titleLabel.textAlignment = NSTextAlignmentRight;
        legalButton.titleLabel.textAlignment = NSTextAlignmentRight;
        rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    } else {
        rateButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        likeButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        legalButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom Methods -

- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"About", @"About")];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation Left Button Action

 @param sender leftBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark - UIButton Action -

- (IBAction)rateButtonClicked:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
        return;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://%@",itunesURL]]];
}

- (IBAction)likeonFBButtonClicked:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
        return;
    }
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebView"];
    webView.title = NSLocalizedString(@"Like", @"Like");
    webView.weburl = facebookURL;
    webView.isComingFromVC = 1;
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:webView animated:NO];
}

- (IBAction)legalButtonClicked:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
        return;
    }
    TermsnConditionViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsNCondition"];
    VC.isCommingFromSignUp = NO;
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

- (IBAction) webButtonClicked:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
        return;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteURL]];
}

@end
