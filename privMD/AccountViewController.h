//
//  AccountViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RPButton.h"

@interface AccountViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate> {
    BOOL textFieldEditedFlag;
    BOOL profilePicChanged;
    BOOL isCancelled;
}

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIImageView *accProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *accProfileButton;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;

@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *genderTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirthTextField;

@property (strong, nonatomic) UIImage *pickedImage;
@property (strong,nonatomic)  UITextField *activeTextField;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *backgroundVerifocationView;
@property (weak, nonatomic) IBOutlet UIView *verificationView;
@property (weak, nonatomic) IBOutlet UILabel *enterPasswordLabel;
@property (weak, nonatomic) IBOutlet UIView *passwordTextfieldBackgroundView;
@property (weak, nonatomic) IBOutlet UITextField *passwordtextField;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *verticalLineView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet RPButton *logoutBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;

- (IBAction)profilePicButtonClicked:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;
- (IBAction)forgotPasswordBtnAction:(id)sender;

- (IBAction)logoutBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *languageView;
@property (strong, nonatomic) IBOutlet UILabel *chooseLanLabel;
@property (strong, nonatomic) IBOutlet UIButton *englsihBtn;
@property (strong, nonatomic) IBOutlet UIButton *arabicBtn;
- (IBAction)englishSelected:(id)sender;
- (IBAction)arabicSelected:(id)sender;

@end
