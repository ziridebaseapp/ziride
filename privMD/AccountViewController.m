//
//  AccountViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "Database.h"
#import "LanguageManager.h"
#import "Locale.h"
#import "User.h"
#import "EnterPhoneNumberViewController.h"
#import "RPCustomIOSAlertView.h"
#import "UILabel+DynamicHeight.h"
#import "GenericUtility.h"
#import "AmazonTransfer.h"
#import "UploadImagesToAmazonServer.h"


@interface AccountViewController () <CustomNavigationBarDelegate, UITextFieldDelegate, UserDelegate, UploadImageOnAWSDelegate> {
    CustomNavigationBar *customNavigationBarView;
    float initialOffset_Y;
    float keyboardHeight;
    BOOL isForgotPassClicked;
}
@property (nonatomic, retain) UIToolbar *keyboardToolbar;

@end

@implementation AccountViewController
@synthesize mainScrollView, topView, activeTextField;
@synthesize fullNameLabel, emailLabel, mobileNumberLabel, genderLabel, dateOfBirthLabel;
@synthesize fullNameTextField, emailTextField, mobileNumberTextField, genderTextField, dateOfBirthTextField, passwordtextField;
@synthesize accProfilePic,accProfileButton, pickedImage;
@synthesize activityIndicator, keyboardToolbar;
@synthesize backgroundVerifocationView, verificationView, verticalLineView, cancelBtn, submitBtn;
@synthesize chooseLanLabel, englsihBtn, arabicBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    isCancelled = NO;
    isForgotPassClicked = NO;
    self.submitBtn.userInteractionEnabled = NO;
    self.backgroundVerifocationView.hidden = YES;
    accProfileButton.userInteractionEnabled = NO;
    emailTextField.userInteractionEnabled = NO;
    mobileNumberTextField.userInteractionEnabled = NO;
    fullNameTextField.userInteractionEnabled = NO;
    genderTextField.userInteractionEnabled = NO;
    dateOfBirthTextField.userInteractionEnabled = NO;
    self.logoutBtn.hidden = YES;
    [self updateUI];
    [self updateVerifyPasswordView];
    [self sendServiceGetProfileData];
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear: -16];
    NSDate *maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    [comps setYear: -100];
    NSDate *minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinimumDate: minDate];
    [datePicker setMaximumDate: maxDate];
    [datePicker setDate:maxDate];
    [datePicker setDate:maxDate animated:YES];
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [dateOfBirthTextField setInputView:datePicker];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (isForgotPassClicked) {
        isForgotPassClicked = NO;
        [self cancelBtnAction:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    keyboardHeight = 220;
    initialOffset_Y = mainScrollView.contentOffset.y;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    topView.frame = CGRectMake(0, 0, screenWidth, 300);
    CGRect frame = self.languageView.frame;
    frame.origin.y = CGRectGetMaxY(topView.frame);
    frame.size.width = screenWidth;
    self.languageView.frame = frame;
    
    frame = englsihBtn.frame;
    frame.origin.x = 10;
    frame.size.width = (screenWidth - 30)/2;
    englsihBtn.frame = frame;
    
    frame = arabicBtn.frame;
    frame.origin.x = CGRectGetMaxX(englsihBtn.frame) + 10;
    frame.size.width = (screenWidth - 30)/2;
    arabicBtn.frame = frame;
    
    mainScrollView.frame = CGRectMake(0, 64, screenWidth, self.view.frame.size.height);
    [mainScrollView setContentSize:CGSizeMake(screenWidth, CGRectGetMaxY(self.topView.frame))];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITapGestureRecognizer Delegate -

/**
 Dismiss Keyboard from view
 */
-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [emailTextField resignFirstResponder];
    [genderTextField resignFirstResponder];
    [fullNameTextField resignFirstResponder];
    [dateOfBirthTextField resignFirstResponder];
    [mobileNumberTextField resignFirstResponder];
}

#pragma mark - UINavigation -

/**
 Menu Button Pressed Action
 */
- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

/**
 Navigation left Button action

 @param sender leftBarButton
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    if(customNavigationBarView.rightbarButton.isSelected == YES) {
        [self.view endEditing:YES];
        textFieldEditedFlag = 0;
        profilePicChanged = 0;
        isCancelled = YES;
        [self saveUserDetails:customNavigationBarView.rightbarButton];
        [self sendServiceGetProfileData];
    } else {
        if(backgroundVerifocationView.hidden == NO) {
            [self cancelBtnAction:nil];
        }
        [self menuButtonPressedAccount];
    }
}

/**
 Navigation Right Button

 @param sender rightBarButton
 */
-(void)rightBarButtonClicked:(UIButton *)sender {
    if (customNavigationBarView.rightbarButton.isSelected == YES) {
        if(!fullNameTextField.text.length) {
            [fullNameTextField becomeFirstResponder];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your name", @"Please enter your name")];
        } else if(!emailTextField.text.length) {
            [emailTextField becomeFirstResponder];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your e -mail ID", @"Please enter your e-mail ID")];
        } else {
            [self saveUserDetails:sender];
        }
    } else {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = self.view.frame.size.height-64;
        backgroundVerifocationView.hidden = NO;
        self.logoutBtn.hidden = YES;
        [customNavigationBarView hideRightBarButton:YES];
        backgroundVerifocationView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        verificationView.frame = CGRectMake(20, screenHeight/2 - 150 - 40, screenWidth - 40, 200);
        screenWidth = verificationView.frame.size.width;
        screenHeight = verificationView.frame.size.height;
        verticalLineView.frame = CGRectMake(screenWidth/2-0.5, screenHeight-40, 1, 40);
        cancelBtn.frame = CGRectMake(0, screenHeight-40, screenWidth/2, 40);
        submitBtn.frame = CGRectMake(screenWidth/2, screenHeight-40, screenWidth/2, 40);
        [mainScrollView bringSubviewToFront:backgroundVerifocationView];
        verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
        [UIView animateWithDuration:0.5
                              delay:0.2
                            options: UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         }
                         completion:^(BOOL finished){
                         }];
    }
}

#pragma mark Custom Methods -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle: NSLocalizedString(@"View Profile", @"View Profile")];
    [customNavigationBarView createRightBarButton];
    [customNavigationBarView setRightBarButtonTitle:NSLocalizedString(@"Edit", @"Edit")];
    [self.view addSubview:customNavigationBarView];
}


/**
 Update UI
 */
-(void)updateUI {
    [self addCustomNavigationBar];
    accProfilePic.layer.cornerRadius = accProfilePic.frame.size.width/2;
    accProfilePic.layer.masksToBounds = YES;
    
    CGFloat screenWidth = self.view.frame.size.width;
    CGRect frame = accProfilePic.frame;
    frame.origin.x = (screenWidth - frame.size.width)/2;
    accProfilePic.frame = frame;
    
    frame = accProfileButton.frame;
    frame.origin.x = (screenWidth - frame.size.width)/2;
    accProfileButton.frame = frame;
    
    [Helper setToLabel:self.chooseLanLabel Text:NSLocalizedString(@"Choose your language/اختر لغتك", @"Choose your language/اختر لغتك") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0x333333)];
}


/**
 Update verify password view
 */
-(void)updateVerifyPasswordView {
    self.verificationView.layer.borderColor = UIColorFromRGB(0xAAAAAA).CGColor;
    self.verificationView.layer.borderWidth = 1.0f;
    self.verificationView.layer.cornerRadius = 5.0f;
    self.verificationView.layer.masksToBounds = YES;
    self.passwordTextfieldBackgroundView.layer.borderColor = UIColorFromRGB(0x019F6E).CGColor;
    self.passwordTextfieldBackgroundView.layer.borderWidth = 1.0f;
}

/**
 Save user details

 @param sender rightBarButton
 */
- (void)saveUserDetails:(id)sender {
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES; {
        if(mBut.isSelected) {
            mBut.selected =NO;
            [mBut setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
            [customNavigationBarView hideLeftMenuButton:NO];
            [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"", @"")];
            emailTextField.userInteractionEnabled = NO;
            genderTextField.userInteractionEnabled = NO;
            fullNameTextField.userInteractionEnabled = NO;
            dateOfBirthTextField.userInteractionEnabled = NO;
            mobileNumberTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = NO;
            if(textFieldEditedFlag == 1 && isCancelled == NO) {
                if(!fullNameTextField.text.length) {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your name", @"Please enter your name")];
                } else if(!emailTextField.text.length) {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your e-mail ID", @"Please enter your e-mail ID")];
                } else {
                    if (profilePicChanged) {
                        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Uploading Profile Pic...", @"Uploading Profile Pic...")];
                        [UploadImagesToAmazonServer sharedInstance].delegate = self;
                        [[UploadImagesToAmazonServer sharedInstance] uploadProfileImageToServer:pickedImage andMobile:flStrForStr(mobileNumberTextField.text) andType:1];
                    }
                }
            } else if(profilePicChanged) {
                [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Uploading Profile Pic...", @"Uploading Profile Pic...")];
                [UploadImagesToAmazonServer sharedInstance].delegate = self;
                [[UploadImagesToAmazonServer sharedInstance] uploadProfileImageToServer:pickedImage andMobile:flStrForStr(mobileNumberTextField.text) andType:1];
            } else {
                [self sendServiceForUpdateProfile];
            }
        } else {
            mBut.selected = YES;
            [mBut setTitle:NSLocalizedString(@"Save", @"Save") forState:UIControlStateSelected];
            [customNavigationBarView hideLeftMenuButton:YES];
            [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")];
            emailTextField.userInteractionEnabled = YES;
            genderTextField.userInteractionEnabled = NO;
            fullNameTextField.userInteractionEnabled = YES;
            dateOfBirthTextField.userInteractionEnabled = YES;
            accProfileButton.userInteractionEnabled = YES;
            mobileNumberTextField.userInteractionEnabled = NO;
        }
    }
}

/**
 Update textfield for dateOfBirth Textfield

 @param sender date picker
 */
-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*) dateOfBirthTextField.inputView;
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:picker.date]];
    dateOfBirthTextField.text = [NSString stringWithFormat:@"%@",str];
}



#pragma mark - UIbutton Actions -

/**
 Profile pic Button Action

 @param sender profilePicButton
 */
- (IBAction)profilePicButtonClicked:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Picture", @"Edit Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"), NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

/**
 Password Button Action

 @param sender passwordBtn
 */
- (IBAction)passwordButtonClicked:(id)sender {
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please visit our website www.ziride.com to change your password.", @"Please visit our website www.ziride.com to change your password.")];
}

/**
 Log out Button Action

 @param sender logoutBtn
 */
- (IBAction)logoutBtnAction:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        return;
    }
    UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Confirm", @"Confirm") Message:NSLocalizedString(@"Are you sure you want to logout?", @"Are you sure you want to logout?")];
    RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
    [alertView setContainerView:containerView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Yes", @"Yes"), NSLocalizedString(@"No", @"No"), nil]];
    [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
        if (buttonIndex == 0) {
            [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Logging Out...", @"Logging Out...")];
            User *user = [[User alloc] init];
            user.delegate = self;
            [user logout];
        }
        [alertView close];
    }];
    [alertView setUseMotionEffects:true];
    [alertView show];
}

/**
 Cacnecl Button Action

 @param sender cancelBtn
 */
- (IBAction)cancelBtnAction:(id)sender {
    [self.view endEditing:YES];
    self.passwordtextField.text = @"";
    verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished){
                         self.submitBtn.userInteractionEnabled = NO;
                         self.logoutBtn.hidden = NO;
                         [customNavigationBarView hideRightBarButton:NO];
                         self.backgroundVerifocationView.hidden = YES;
                         [mainScrollView sendSubviewToBack:backgroundVerifocationView];
                         verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                     }];
}

/**
 Submit Button Action

 @param sender submitBtn
 */
- (IBAction)submitBtnAction:(id)sender {
    if (self.passwordtextField.text.length) {
        [self.view endEditing:YES];
        [self verifyPasssword];
        self.passwordtextField.text = @"";
        self.submitBtn.userInteractionEnabled = NO;
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your password and try again.", @"Please enter your password and try again.")];
    }
}

/**
 Forgot Password Button Action

 @param sender forgotPassBtn
 */
- (IBAction)forgotPasswordBtnAction:(id)sender {
    [self.view endEditing:YES];
    isForgotPassClicked = YES;
    EnterPhoneNumberViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"EnterPhoneNumber"];
    [Helper pushLikePresentVC:self.navigationController.view];
    [self.navigationController pushViewController:viewcontroller animated:YES];
}

/**
 English Button Action

 @param sender englishBtn
 */
- (IBAction)englishSelected:(id)sender {
    [Utility setSelectedLangaugeID:0];
    [self updateSelectedLanguage:@"0"];
}

/**
 Arabic Button Action
 
 @param sender arabicBtn
 */
- (IBAction)arabicSelected:(id)sender {
    [Utility setSelectedLangaugeID:1];
    [self updateSelectedLanguage:@"1"];
}



#pragma mark - UIActionSheet Methods -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1) {
        switch (buttonIndex) {
            case 0: {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1: {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}

-(void)cameraButtonClicked:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:imagePicker animated:YES completion:nil];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Camera is not available", @"Camera is not available")];
    }
}

-(void)libraryButtonClicked:(id)sender {
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    } else {
        [self presentViewController:picker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePickerController - 

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    accProfilePic.image = pickedImage;
    pickedImage = [Helper imageWithImage:pickedImage scaledToSize:CGSizeMake(70, 70)];
    profilePicChanged = 1;
}



#pragma mark - UploadFileDelegate -

/**
 Upload File successfully with URL

 @param imageURL Image URL
 */
-(void)uploadFileDidUploadSuccessfullyWithUrl:(NSString*) imageURL{
    if (imageURL) {
        [[SDImageCache sharedImageCache] removeImageForKey:imageURL fromDisk:YES];
        [Utility setUserProfileImage:flStrForStr(imageURL)];
        if (profilePicChanged && !textFieldEditedFlag) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your profile image upload successfully.", @"Your profile image upload successfully.")];
        }
        [self sendServiceForUpdateProfile];
    }
}

/**
 Upload File Did Failed

 @param error error
 */
-(void)uploadFileDidFailedWithError:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [Helper showAlertWithTitle:NSLocalizedString(@"Oops!", @"Oops!") Message:NSLocalizedString(@"Your profile photo has not been updated try again.", @"Your profile photo has not been updated try again.")];
    NSString *strImageUrl  = [Utility userProfileImage];
    if (strImageUrl.length) {
        strImageUrl = [strImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
        [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                         placeholderImage:[UIImage imageNamed:@"driverImage"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                    [self.activityIndicator stopAnimating];
                                    if (!error && image) {
                                        accProfilePic.image = image;
                                    } else {
                                        accProfilePic.image = [UIImage imageNamed:@"driverImage"];
                                    }
                                }];
    } else {
        [self.activityIndicator stopAnimating];
        accProfilePic.image = [UIImage imageNamed:@"driverImage"];
    }
    [self sendServiceForUpdateProfile];
}

#pragma mark - UserDelegate -

/**
 User Logout Successfully

 @param sucess success
 */
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}

/**
 User Logout failed

 @param error error
 */
-(void)userDidFailedToLogout:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}


#pragma mark - UITextFieldDelegates -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    if(keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)] ;
        [keyboardToolbar setBarStyle:UIBarStyleDefault];
        [keyboardToolbar sizeToFit];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        UIBarButtonItem *doneButton1 =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(textFieldShouldReturn:)];
        doneButton1.tintColor = UIColorFromRGB(0x222328);
        NSArray *itemsArray = [NSArray arrayWithObjects:flexButton,doneButton1, nil];
        [keyboardToolbar setItems:itemsArray];
        [UIView commitAnimations];
    }
    if([textField isEqual:dateOfBirthTextField]) {
        [dateOfBirthTextField setInputAccessoryView:keyboardToolbar];
    }
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField {
    textFieldEditedFlag = 1;
    if(!fullNameTextField.text.length) {
        [fullNameTextField becomeFirstResponder];
    } else if(!emailTextField.text.length) {
        [emailTextField becomeFirstResponder];
    } else if([textField isEqual:passwordtextField]) {
        textFieldEditedFlag = 0;
    }else if(!dateOfBirthTextField.text.length) {
        [dateOfBirthTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    if (newString.length) {
        self.submitBtn.userInteractionEnabled = YES;
        [self.submitBtn setTitleColor:UIColorFromRGB(0x019F6E) forState:UIControlStateNormal];
    } else {
        self.submitBtn.userInteractionEnabled = NO;
        [self.submitBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:fullNameTextField]) {
        if(!fullNameTextField.text.length) {
            [fullNameTextField becomeFirstResponder];
        } else {
            [fullNameTextField resignFirstResponder];
        }
    } else if([textField isEqual:emailTextField]) {
        if(!emailTextField.text.length) {
            [emailTextField becomeFirstResponder];
        } else {
            [emailTextField resignFirstResponder];
        }
    } else if([textField isEqual:passwordtextField]) {
        [self submitBtnAction:nil];
    }
    else {
        [dateOfBirthTextField resignFirstResponder];
        [self updateTextField:nil];
    }
    return YES;
}

#pragma mark - WebService call -

/**
 Verify Password API call
 */
-(void)verifyPasssword {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Verifying...", @"Verifying...")];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_password":flStrForStr(passwordtextField.text),
                               @"ent_sid": flStrForStr([Utility userID]),
                               @"ent_date_time":[Helper getCurrentNetworkDateTime]
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"checkPassword"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self verifyPassswordResponse:response];
                             }
                         }];
}

/**
 Password Verify service response

 @param response response data
 */
-(void)verifyPassswordResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([response[@"errFlag"] intValue] == 0) {
            verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
            [UIView animateWithDuration:0.5
                                  delay:0.2
                                options: UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                             }
                             completion:^(BOOL finished){
                                 self.submitBtn.userInteractionEnabled = NO;
                                 self.backgroundVerifocationView.hidden = YES;
                                 self.logoutBtn.hidden = NO;
                                 [customNavigationBarView hideRightBarButton:NO];
                                 [self saveUserDetails:customNavigationBarView.rightbarButton];
                                 verificationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                             }];

        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}


/**
 Send service to update profile data
 */
-(void)sendServiceForUpdateProfile {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Saving...", @"Saving...")];
    NSString *birthDate = flStrForStr(dateOfBirthTextField.text);
    if (birthDate.length) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        NSDate *birthDt = [dateFormat dateFromString:birthDate];
        dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"yyyy/MM/dd"];
        birthDate = [dateFormat  stringFromDate:birthDt];
    }
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_first_name":flStrForStr(fullNameTextField.text),
                               @"ent_last_name":@"",
                               @"ent_email":flStrForStr(emailTextField.text),
                               @"ent_phone":flStrForStr(mobileNumberTextField.text),
                               @"ent_dob":flStrForStr(birthDate),
                               @"ent_propic":[Utility userProfileImage],
                               @"ent_date_time":[Helper getCurrentNetworkDateTime]
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"updateProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self updateProfileResponse:response];
                             }
                         }];
    
}

/**
 Update profile service response

 @param response response data
 */
-(void)updateProfileResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:fullNameTextField.text forKey:kNSUUserName];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (textFieldEditedFlag) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Profile updated successfully.", @"Profile updated successfully.")];
        }
        textFieldEditedFlag = 0;
        profilePicChanged = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];

    }
}

/**
 Send service for get profile data
 */
-(void)sendServiceGetProfileData {
    CGRect fra = accProfilePic.frame;
    fra.origin.x = accProfilePic.frame.size.width/2-10;
    fra.origin.y = accProfilePic.frame.size.height/2-10;
    fra.size.width = 20;
    fra.size.height = 20;
    activityIndicator = [[UIActivityIndicatorView alloc]init];
    activityIndicator.frame = fra;
    [self.accProfilePic addSubview:activityIndicator];
    activityIndicator.backgroundColor=[UIColor clearColor];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];
    
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getProfile"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getProfileResponse:response];
                             }
                         }];
}

/**
 Get profile service response
 @param response response data
 */
-(void)getProfileResponse:(NSDictionary *)response {
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            NSString *strImageUrl  = [Utility userProfileImage];
            if (strImageUrl.length) {
                strImageUrl = [strImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
                [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                 placeholderImage:[UIImage imageNamed:@"driverImage"]
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                            [self.activityIndicator stopAnimating];
                                            if (!error && image) {
                                            } else {
                                                accProfilePic.image = [UIImage imageNamed:@"driverImage"];
                                            }
                                        }];
            } else {
                [self.activityIndicator stopAnimating];
                accProfilePic.image = [UIImage imageNamed:@"driverImage"];
            }
            if ([Helper isCurrentLanguageRTL]) {
                chooseLanLabel.textAlignment = NSTextAlignmentRight;
                emailTextField.textAlignment = NSTextAlignmentRight;
                genderTextField.textAlignment = NSTextAlignmentRight;
                fullNameTextField.textAlignment = NSTextAlignmentRight;
                dateOfBirthTextField.textAlignment = NSTextAlignmentRight;
                mobileNumberTextField.textAlignment = NSTextAlignmentRight;
            } else {
                chooseLanLabel.textAlignment = NSTextAlignmentLeft;
                emailTextField.textAlignment = NSTextAlignmentLeft;
                genderTextField.textAlignment = NSTextAlignmentLeft;
                fullNameTextField.textAlignment = NSTextAlignmentLeft;
                dateOfBirthTextField.textAlignment = NSTextAlignmentLeft;
                mobileNumberTextField.textAlignment = NSTextAlignmentLeft;
            }
            emailTextField.text = dictResponse[@"email"];
            fullNameTextField.text = [NSString stringWithFormat:@"%@ %@",flStrForStr(dictResponse[@"fName"]), flStrForStr(dictResponse[@"lName"])];
            mobileNumberTextField.text = dictResponse[@"phone"];
            if([dictResponse[@"gender"] integerValue] == 1) {
                genderTextField.text = @"Male";
            } else if([dictResponse[@"gender"] integerValue] == 2) {
                genderTextField.text = @"Female";
            }
            NSString *birthDate = flStrForStr(dictResponse[@"dob"]);
            if (birthDate.length) {
                NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
                dateFormat.dateStyle=NSDateFormatterMediumStyle;
                [dateFormat setDateFormat:@"yyyy/MM/dd"];
                NSDate *birthDt = [dateFormat dateFromString:birthDate];
                dateFormat=[[NSDateFormatter alloc]init];
                dateFormat.dateStyle=NSDateFormatterMediumStyle;\
                [dateFormat setDateFormat:@"dd/MM/yyyy"];
                birthDate = [dateFormat  stringFromDate:birthDt];
            }
            dateOfBirthTextField.text = birthDate;
            self.logoutBtn.hidden = NO;
            [Utility setUserName:flStrForStr(fullNameTextField.text)];
            [Utility setUserProfileImage:flStrForStr(response[@"pPic"])];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
        }
    }
}

/**
 Update langauage service

 @param selectedLanguage selectedLanguage
 */
- (void) updateSelectedLanguage:(NSString *)selectedLanguage {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Updating Language...", @"Updating Laguage...")];    
    NSDictionary *params = @{
                             @"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             KDASignUpLanguage:selectedLanguage,
                             @"ent_user_type":@"2"
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateLanguage"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self updateSelectedLanguageResponse:response];
                                   }
                               }];
}

/**
 Update selected language response

 @param response response data
 */
-(void)updateSelectedLanguageResponse :(NSDictionary *)response {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (!response) {
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"] isEqualToString:@"1"]) {
                [englsihBtn setSelected:YES];
                [arabicBtn setSelected:NO];
                LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
                Locale *localeObj = languageManager.availableLocales[0];
                [languageManager setLanguageWithLocale:localeObj];
            } else  if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"] isEqualToString:@"2"]) {
                [englsihBtn setSelected:NO];
                [arabicBtn setSelected:YES];
                LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
                Locale *localeObj = languageManager.availableLocales[1];
                [languageManager setLanguageWithLocale:localeObj];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHomeVC" object:nil userInfo:nil];
            [self updateUI];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

#pragma mark - Keyboard Notifications -

/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [keyboardToolbar removeFromSuperview];
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 20;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(topView.frame) + height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    CGFloat screenWidth = self.view.frame.size.width;
    mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(topView.frame));
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}

@end
