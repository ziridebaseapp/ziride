//
//  AddMoneyOrCardTableViewCell.h
//  ZiRide
//
//  Created by 3Embed on 01/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMoneyOrCardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (weak, nonatomic) IBOutlet UILabel *addLabel;

@end
