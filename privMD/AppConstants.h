//
//  AppConstants.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


#pragma mark - enums

typedef enum {
    
    kNotificationTypeBookingAccept = 2,
    kNotificationTypeBookingOnMyWay = 6,
    kNotificationTypeBookingReachedLocation = 7,
    kNotificationTypeBookingStarted = 8,
    kNotificationTypeBookingComplete = 9,
    kNotificationTypeBookingReject = 10,
    kNotificationTypePassengerRejected = 12,
    kNotificationTypeBookingCompletedButInvoiceNotRaised = 100,
    kNotificationTypeUserLoggedOut = 111,
    kNotificationTypePassengerRecieveMessage = 420,
    kNotificationTypeAllDriversBusy = 421,
    kNotificationTypePassengerWalletUpdated = 425
}BookingNotificationType;

typedef enum CancelBookingReasons :NSUInteger
{
    kCAPassengerDoNotShow = 4,
    kCAWrongAddressShown = 5,
    kCAPassengerRequestedCancel = 6,
    kCADoNotChargeClient = 7,
    kCAOhterReasons = 8,
    
}CancelBookingReasons;

typedef enum
{
    kPubNubStartStreamAction = 1,
    kPubNubStopStreamAction = 2,
    kPubNubGetAppointmentDetails = 3,
}PubNubStreamAction;


typedef enum
{
    kAppointmentTypeNow = 3,
    kAppointmentTypeLater = 4
}AppointmentType;

typedef enum {
    kSourceAddress = 1,
    kDestinationAddress = 2,
    kUnknownAddress = 3
}AddressTypeForPickUpLocation;

typedef enum
{
    kVerifyViewController = 1,
    kPaymentViewController = 2,
    kMapViewController = 3
} isComingFromViewController;


#pragma mark - Constants

extern NSString *const kPubNubPublisherKey;
extern NSString *const kPubNubSubcriptionKey;
extern NSString *const kGoogleMapAPIKey;
extern NSString *const kPlaceAPIKey;
extern NSString *const kAllMasterETADistanceMatrixAPIKey;
extern NSString *const kNearestMasterETADistanceMatrixAPIKey;

extern NSString *const kNSUTestDeviceidKey;
extern NSString *const kDeviceIdKey;
extern NSString *const kNSUCarArrayKey;
extern NSString *const kStripeTestKey;
extern NSString *const kStripeLiveKey;


#pragma mark - mark URLs

extern NSString *BASE_URL;
extern NSString *BASE_NODE_URL;
extern NSString *BASE_URL_SUPPORT;
extern NSString *BASE_URL_UPLOADIMAGE;
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;

// Whatsapp URLs
extern NSString *const whatsAppUrl;
extern NSString *const whatsAppSendTextUrl;

extern NSString *const termsAndCondition;
extern NSString *const privacyPolicy;
extern NSString *const itunesURL;
extern NSString *const facebookURL;
extern NSString *const websiteLabel;
extern NSString *const websiteURL;
extern NSString *const imgLinkForSharing;
extern NSString *const TOKEN_URL;

//Methods
extern NSString *MethodPatientSignUp;
extern NSString *MethodPatientLogin;
extern NSString *MethodDoctorUploadImage;
extern NSString *MethodPassengerLogout;
extern NSString *MethodFareCalculator;


#pragma mark - ServiceMethods
// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMCancelAppointment;
extern NSString *const kSMCancelOngoingAppointment;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;


#pragma marks - Distance Km or Miles
extern double   const kDistanceMetric;
extern NSString *const kDistanceParameter;
extern NSString *const kMPHorKMPH;

#pragma mark - Wallet Amount Update Button Titles -
extern NSString *const kWalletButton1;
extern NSString *const kWalletButton2;
extern NSString *const kWalletButton3;


#pragma marks - Langauge Support 
extern BOOL const kFCMPushSupport;
extern NSString *const DEFAULTS_KEY_LANGUAGE_CODE;

#pragma mark - Wallet Check
extern double   const  kWalletMiniumCheck;

extern BOOL const kPaymentType;
extern BOOL const kCardOrCash;
extern BOOL const kBookLater;
extern BOOL const kWithDispatch;


//Request Params For SignUp
extern NSString *KDASignUpFirstName;
extern NSString *KDASignUpLastName;
extern NSString *KDASignUpMobile;
extern NSString *KDASignUpEmail;
extern NSString *KDASignUpPassword;
extern NSString *KDASignUpCountry;
extern NSString *KDASignUpCity;
extern NSString *KDASignUpLanguage;
extern NSString *KDASignUpDeviceType;
extern NSString *KDASignUpDeviceId;
extern NSString *KDASignUpAddLine1;
extern NSString *KDASignUpAddLine2;
extern NSString *KDASignUpPushToken;
extern NSString *KDASignUpZipCode;
extern NSString *KDASignUpAccessToken;
extern NSString *KDASignUpDateTime;
extern NSString *KDASignUpCreditCardNo;
extern NSString *KDASignUpCreditCardCVV;
extern NSString *KDASignUpCreditCardExpiry;
extern NSString *KDASignUpTandC;
extern NSString *KDASignUpPricing;
extern NSString *KDASignUpReferralCode;
extern NSString *KDASignUpLatitude;
extern NSString *KDASignUpLongitude;


//Request Params For Login

extern NSString *KDALoginEmail;
extern NSString *KDALoginPassword;
extern NSString *KDALoginDeviceType;
extern NSString *KDALoginDevideId;
extern NSString *KDALoginPushToken;
extern NSString *KDALoginUpDateTime;

//Request for Upload Image
extern NSString *KDAUploadDeviceId;
extern NSString *KDAUploadSessionToken;
extern NSString *KDAUploadImageName;
extern NSString *KDAUploadImageChunck;
extern NSString *KDAUploadfrom;
extern NSString *KDAUploadtype;
extern NSString *KDAUploadDateTime;
extern NSString *KDAUploadOffset;

//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;


//#pragma mark - NSUserDeafults Keys
////eg : give prefix kNSU
//extern NSString *const kNSUIsNewSignUp;
//extern NSString *const kNSUMongoDataBaseAPIKey;
//extern NSString *const kNSUIsPassengerBookedKey;
//extern NSString *const kNSUPassengerBookingStatusKey;
//extern NSString *const kNSUPatientPaypalkey;
//
//
//extern NSString *const KNSUPubnubPublishKey;
//extern NSString *const kNSUPubnubSubscribeKey;
//
//extern NSString *const kNSUUserPubnubChannel;
//extern NSString *const kNSUServerPubnubChannel;
//extern NSString *const kNSUPresensePubnubChannel;
//extern NSString *const kNSUDriverPubnubChannel;
//
//extern NSString *const kNSUUserName;
//extern NSString *const kNSUSID;
//extern NSString *const kNSUUserEmailID;
//extern NSString *const kNSUUserReferralCode;
//extern NSString *const kNSUUserShareMessage;
//extern NSString *const kNSUPaymentGatewayKey;
//extern NSString *const kNSUProfileImage;
//extern NSString *const kNSUPromoCode;
//extern NSString *const kNSUPlaceAPIKey;
//extern NSString *const kNSUStripeKey;
//extern NSString *const kNSUFCMPushToken;
//extern NSString *const kNSUWalletAmount;
//

#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU
extern NSString *const kNSUUserSessionToken;
extern NSString *const kNSUDeviceID;
extern NSString *const kNSUPushToken;

extern NSString *const kNSUCurrentLat;
extern NSString *const kNSUCurrentLong;
extern NSString *const kNSUserCurrentCity;
extern NSString *const kNSUserCurrentCountry;

extern NSString *const kNSUDistancematrixAPIKey;

extern NSString *const kNSUIsNewSignUp;

extern NSString *const KNSUPubnubPublishKey;
extern NSString *const kNSUPubnubSubscribeKey;

extern NSString *const kNSUUserPubnubChannel;
extern NSString *const kNSUServerPubnubChannel;
extern NSString *const kNSUPresensePubnubChannel;
extern NSString *const kNSUDriverPubnubChannel;

extern NSString *const kNSUUserName;
extern NSString *const kNSUUserID;
extern NSString *const kNSUUserEmailID;

extern NSString *const kNSUUserReferralCode;
extern NSString *const kNSUUserShareMessage;

extern NSString *const kNSUPaymentGatewayKey;
extern NSString *const kNSUUserProfileImage;

extern NSString *const kNSUPlaceAPIKey;
extern NSString *const kNSUStripeKey;

extern NSString *const kNSUWalletBalance;
extern NSString *const kNSUSelectedLangaugeID;

extern NSString *const kNSUIsNeedToPublishMessageOnPubnub;
extern NSString *const kNSUSplashData;

extern NSString *const kNSUDriverID;
extern NSString *const kNSUDriverEmailID;
extern NSString *const kNSUDriverName;
extern NSString *const kNSUDriverEmail;
extern NSString *const kNSUDriverMobileNumber;
extern NSString *const kNSUDriverRating;
extern NSString *const kNSUDriverProfileImageURL;
extern NSString *const kNSUDriverLatitude;
extern NSString *const kNSUDriverLongitude;

extern NSString *const kNSUBookingID;
extern NSString *const kNSUBookingData;
extern NSString *const kNSUBookingDate;
extern NSString *const kNSUBookingStatus;
extern NSString *const kNSUIsUserInBooking;
extern NSString *const kNSUPromoCode;
extern NSString *const kNSUBookingApproximateFare;

extern NSString *const kNSUVehicleImageURL;
extern NSString *const kNSUVehicleMapImageURL;
extern NSString *const kNSUVehicleMakeName;
extern NSString *const kNSUVehicleModelName;
extern NSString *const kNSUVehicleNumberPlate;
extern NSString *const kNSUVehicleColor;

extern NSString *const kNSUFavouritePickUpLocation;
extern NSString *const kNSUPickUpLocationAddressLine1;
extern NSString *const kNSUPickUpLocationAddressLine2;
extern NSString *const kNSUPickUpLatitude;
extern NSString *const kNSUPickUpLongitude;

extern NSString *const kNSUFavouriteDropOffLocation;
extern NSString *const kNSUDropOffLocationAddressLine1;
extern NSString *const kNSUDropOffLocationAddressLine2;
extern NSString *const kNSUDropOffLatitude;
extern NSString *const kNSUDropOffLongitude;

extern NSString *const kNSUPubnubPublishTimeInterval;
extern NSString *const kNSUPubnubPublishOnBookingTimeInterval;
extern NSString *const kNSUGoogleMatrixTimeInterval;
extern NSString *const kNSURadiusForNotOperatingArea;
extern NSString *const kNSURadiusForOperatingArea;
extern NSString *const kNSUDistanceThresholdForFareEstimate;
extern NSString *const kNSUOperatingAreaType;
extern NSString *const kNSUGoogleKeysArray;
extern NSString *const kNSUMaximumETA;
extern NSString *const kNSUBookingAmountAllowUpto;


#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadDoctorNameKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadDoctorEmailKey;
extern NSString *const kPNPayloadDoctorContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadStatusIDKey;
extern NSString *const kPNPayloadAppointmentIDKey;
extern NSString *const kPNPayloadStatusIDKey;



#pragma mark - Controller Keys

#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;
extern NSString *const kNotificationLocationServicesChangedNameKey;
extern NSString *const kNotificationBookingConfirmationNameKey;
#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;

extern NSString *const KNUCurrentLat;
extern NSString *const KNUCurrentLong;
extern NSString *const KNUserCurrentCity;
extern NSString *const KNUserCurrentState;
extern NSString *const KNUserCurrentCountry;

extern NSString *const KUDriverEmail;
extern NSString *const KUDriverID;
extern NSString *const KUBookingDate;
extern NSString *const KUBookingID;


