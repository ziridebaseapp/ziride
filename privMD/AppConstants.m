//
//  AppConstants.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AppConstants.h"

NSString *const kPubNubPublisherKey                      = @"pub-c-4389ccac-4753-4c46-87ab-73cc46955534";
NSString *const kPubNubSubcriptionKey                    = @"sub-c-57893620-dc9a-11e6-9c30-0619f8945a4f";
NSString *const kGoogleMapAPIKey                         = @"AIzaSyCA3Wi4-y5uCx2RE5peeL91BdCOxqaAiLQ";
NSString *const kPlaceAPIKey                             = @"AIzaSyBpQIUsC4ErBJiOc4DXpVs1HrZkOvvof9Q";
NSString *const kAllMasterETADistanceMatrixAPIKey        = @"AIzaSyCQKBRZqkiBvp8jTWHjx-9Lb5K4EolzgcE";
NSString *const kNearestMasterETADistanceMatrixAPIKey    = @"AIzaSyAMYdZ2gGxh9KvZftAErMAZOC5aicOSyl0";

NSString *const kNSUTestDeviceidKey                         = @"C2A33350-D9CF-4A7E-8751-A36016838381";
NSString *const kDeviceIdKey                             = @"deviceid";
NSString *const kStripeLiveKey                           = @"pk_test_cC3kx8H5s2mKTMkFcEmPBbOZ";

#pragma mark - mark URLsn

//Dev
#define BASE_IP  @"http://138.197.94.9/ZiRide/"

NSString *BASE_URL                            = BASE_IP @"services.php/";
NSString *BASE_NODE_URL                       = @"http://138.197.94.9:2040/";
NSString *BASE_URL_SUPPORT                    = BASE_IP;
NSString *BASE_URL_UPLOADIMAGE                = BASE_IP @"services.php/uploadImage";
NSString *const   baseUrlForXXHDPIImage       = BASE_IP @"pics/xxhdpi/";
NSString *const   baseUrlForOriginalImage     = BASE_IP @"pics/";

NSString *const termsAndCondition             = BASE_IP @"terms_of_use.php";
NSString *const privacyPolicy                 = BASE_IP @"privacy_policy.php";
NSString *const itunesURL                     = @"itunes.apple.com/us/";
NSString *const facebookURL                   = @"https://www.facebook.com/Ziride-672321359607822";
NSString *const websiteLabel                  = @"www.ziride.com";
NSString *const websiteURL                    = @"http://ziride.com";
NSString *const imgLinkForSharing             = @"http://www.roadyo.in/roadyo1.0/appimages/iosPassenger.png";

#pragma mark - Distance Km Or Miles
double   const  kDistanceMetric            = 1000;
NSString *const kDistanceParameter         = @"KM";
NSString *const kMPHorKMPH                 = @"KMPH";

#pragma mark - Wallet Amount Update Button Titles -
NSString *const kWalletButton1             = @"100";
NSString *const kWalletButton2             = @"200";
NSString *const kWalletButton3             = @"300";


#pragma  mark - Langauge Support 
NSString *const DEFAULTS_KEY_LANGUAGE_CODE    = @"selected_langauge_key";
BOOL const kFCMPushSupport                    = true;

// Payment Type = true then Card and cash both
// Payment Type = false then Card or cash
BOOL const kPaymentType                    = true;


// CardOrCash = true then Card
// CardOrCash = false then Cash
BOOL const kCardOrCash                     = true;

// BookLater = true then module present
// BookLater = false then module not present
BOOL const kBookLater                      = true;

// WithDispatch = true then Later booking with dispatch
// WithDispatch = false then Later booking without dispatch
BOOL const kWithDispatch                   = true;

#pragma mark - Wallet Check 
double   const  kWalletMiniumCheck         = 100.00;


// Whatsapp URLs
NSString *const whatsAppUrl = @"whatsapp://app";
NSString *const whatsAppSendTextUrl = @"whatsapp://send?text=";


#pragma mark - ServiceMethods

NSString *const kSMLiveBooking                = @"liveBooking";
NSString *const kSMGetAppointmentDetial       = @"getAppointmentDetails";
NSString *const kSMUpdateSlaveReview          = @"updateSlaveReview";
NSString *const kSMGetMasters                 = @"getMasters";
NSString *const kSMCancelAppointment          = @"cancelAppointment";
NSString *const kSMCancelOngoingAppointment   = @"cancelAppointmentRequest";

//Methods

NSString *MethodPatientSignUp                = @"slaveSignup";
NSString *MethodPatientLogin                 = @"slaveLogin";
NSString *MethodDoctorUploadImage            = @"uploadImage";
NSString *MethodPassengerLogout              = @"logout";
NSString *MethodFareCalculator               = @"fareCalculator";

//SignUp

NSString *KDASignUpFirstName                  = @"ent_first_name";
NSString *KDASignUpLastName                   = @"ent_last_name";
NSString *KDASignUpMobile                     = @"ent_mobile";
NSString *KDASignUpEmail                      = @"ent_email";
NSString *KDASignUpPassword                   = @"ent_password";
NSString *KDASignUpAddLine1                   = @"ent_address_line1";
NSString *KDASignUpAddLine2                   = @"ent_address_line2";
NSString *KDASignUpAccessToken                = @"ent_token";
NSString *KDASignUpDateTime                   = @"ent_date_time";
NSString *KDASignUpCountry                    = @"ent_country";
NSString *KDASignUpCity                       = @"ent_city";
NSString *KDASignUpLanguage                   = @"ent_language";
NSString *KDASignUpDeviceType                 = @"ent_device_type";
NSString *KDASignUpDeviceId                   = @"ent_dev_id";
NSString *KDASignUpPushToken                  = @"ent_push_token";
NSString *KDASignUpZipCode                    = @"ent_zipcode";
NSString *KDASignUpCreditCardNo               = @"ent_cc_num";
NSString *KDASignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *KDASignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *KDASignUpTandC                      = @"ent_terms_cond";
NSString *KDASignUpPricing                    = @"ent_pricing_cond";
NSString *KDASignUpReferralCode               = @"ent_referral_code";
NSString *KDASignUpLatitude                   = @"ent_latitude";
NSString *KDASignUpLongitude                  = @"ent_longitude";

// Login

NSString *KDALoginEmail                       = @"ent_email";
NSString *KDALoginPassword                    = @"ent_password";
NSString *KDALoginDeviceType                  = @"ent_device_type";
NSString *KDALoginDevideId                    = @"ent_dev_id";
NSString *KDALoginPushToken                   = @"ent_push_token";
NSString *KDALoginUpDateTime                  = @"ent_date_time";


//Upload
NSString *KDAUploadDeviceId                    = @"ent_dev_id";
NSString *KDAUploadSessionToken                = @"ent_sess_token";
NSString *KDAUploadImageName                   = @"ent_snap_name";
NSString *KDAUploadImageChunck                 = @"ent_snap_chunk";
NSString *KDAUploadfrom                        = @"ent_upld_from";
NSString *KDAUploadtype                        = @"ent_snap_type";
NSString *KDAUploadDateTime                    = @"ent_date_time";
NSString *KDAUploadOffset                      = @"ent_offset";

// Logout the user

NSString *KDALogoutSessionToken                = @"user_session_token";
NSString *KDALogoutUserId                      = @"logout_user_id";


//Parsms for checking user loged out or not

NSString *KDAcheckUserId                        = @"user_id";
NSString *KDAcheckUserSessionToken              = @"ent_sess_token";
NSString *KDAgetPushToken                       = @"ent_push_token";

//Params to store the Country & City.

NSString *KDACountry                            = @"country";
NSString *KDACity                               = @"city";
NSString *KDALatitude                           = @"latitudeQR";
NSString *KDALongitude                          = @"longitudeQR";

//params for Firstname
NSString *KDAFirstName                          = @"ent_first_name";
NSString *KDALastName                           = @"ent_last_name";
NSString *KDAEmail                              = @"ent_email";
NSString *KDAPhoneNo                            = @"ent_mobile";
NSString *KDAPassword                           = @"ent_password";


#pragma mark - NSUserDeafults Keys

//NSString *const kNSUIsNewSignUp                       = @"isNewSignUp";
//NSString *const KNSUPubnubPublishKey                  = @"pubnubPublish";
//NSString *const kNSUPubnubSubscribeKey                = @"pubnubSubscribe";
//
//NSString *const kNSUUserPubnubChannel                 = @"userChannel";
//NSString *const kNSUServerPubnubChannel               = @"serverChhannel";
//NSString *const kNSUPresensePubnubChannel             = @"presenseChannel";
//NSString *const kNSUDriverPubnubChannel               = @"driverChannel";
//
//NSString *const kNSUUserName                          = @"userName";
//NSString *const kNSUSID                               = @"SId";
//NSString *const kNSUUserEmailID                       = @"userEmailId";
//NSString *const kNSUUserReferralCode                  = @"referralCode";
//NSString *const kNSUUserShareMessage                  = @"shareMessage";
//NSString *const kNSUPaymentGatewayKey                    = @"paymentURL";
//NSString *const kNSUProfileImage                      = @"profileImage";
//NSString *const kNSUPromoCode                         = @"promoCode";
//NSString *const kNSUFCMPushToken                      = @"FCMPushToekn";
//
//NSString *const kNSUPlaceAPIKey                       = @"placeAPIKey";
//NSString *const kNSUStripeKey                         = @"stripeKey";
//
//NSString *const kNSUMongoDataBaseAPIKey               = @"mongoDBapi";
//NSString *const kNSUIsPassengerBookedKey              = @"passengerBooked";
//NSString *const kNSUPassengerBookingStatusKey         = @"STATUSKEY";
//NSString *const kNSUCarArrayKey                      = @"carTypeArray";
//NSString *const kNSUPatientCouponkey                  = @"coupon";
//NSString *const kNSUPatientPaypalkey                  = @"paypal";
//
//NSString *const kNSUWalletAmount                      = @"WalletAmount";

#pragma mark - NSUserDeafults Keys

NSString *const kNSUUserSessionToken                  = @"sessionToken";
NSString *const kNSUDeviceID                          = @"deviceID";
NSString *const kNSUPushToken                         = @"pushToken";

NSString *const kNSUCurrentLat                        = @"latitude";
NSString *const kNSUCurrentLong                       = @"longitude";
NSString *const kNSUserCurrentCity                    = @"usercity";
NSString *const kNSUserCurrentCountry                 = @"userCountry";


NSString *const kNSUIsNewSignUp                       = @"isNewSignUp";

NSString *const kNSUDistancematrixAPIKey              = @"distancematrixAPIKey";

NSString *const KNSUPubnubPublishKey                  = @"pubnubPublish";
NSString *const kNSUPubnubSubscribeKey                = @"pubnubSubscribe";

NSString *const kNSUUserPubnubChannel                 = @"userChannel";
NSString *const kNSUServerPubnubChannel               = @"serverChhannel";
NSString *const kNSUPresensePubnubChannel             = @"presenseChannel";
NSString *const kNSUDriverPubnubChannel               = @"driverChannel";

NSString *const kNSUUserName                          = @"userName";
NSString *const kNSUUserID                               = @"SId";
NSString *const kNSUUserEmailID                       = @"userEmailId";

NSString *const kNSUUserReferralCode                  = @"referralCode";
NSString *const kNSUUserShareMessage                  = @"shareMessage";

NSString *const kNSUPaymentGatewayKey                 = @"paymentGatewayKey";
NSString *const kNSUUserProfileImage                  = @"profileImage";


NSString *const kNSUPlaceAPIKey                       = @"placeAPIKey";

NSString *const kNSUCarArrayKey                       = @"carTypeArray";
NSString *const kNSUWalletBalance                     = @"WalletAmount";
NSString *const kNSUSelectedLangaugeID                = @"selectedLangaugeID";

NSString *const kNSUIsNeedToPublishMessageOnPubnub    = @"isNeedToPublishMessageOnPubnub";

NSString *const kNSUSplashData                       = @"splashData";

NSString *const kNSUDriverID                          = @"driverID";
NSString *const kNSUDriverEmailID                     = @"driverEmailID";
NSString *const kNSUDriverName                        = @"driverName";
NSString *const kNSUDriverEmail                       = @"driverEmail";
NSString *const kNSUDriverMobileNumber                = @"driverMobileNumber";
NSString *const kNSUDriverRating                      = @"driverRating";
NSString *const kNSUDriverProfileImageURL             = @"driverProfileImageURL";
NSString *const kNSUDriverLatitude                    = @"driverLatitude";
NSString *const kNSUDriverLongitude                   = @"driverLongitude";

NSString *const kNSUBookingID                         = @"bookingID";
NSString *const kNSUBookingData                       = @"bookingData";
NSString *const kNSUBookingDate                       = @"bookingDate";
NSString *const kNSUBookingStatus                     = @"bookingStatus";
NSString *const kNSUIsUserInBooking                   = @"userInBooking";
NSString *const kNSUPromoCode                         = @"promoCode";
NSString *const kNSUBookingApproximateFare            = @"bookingApproximateFare";

NSString *const kNSUVehicleImageURL                   = @"vehicleImageURL";
NSString *const kNSUVehicleMapImageURL                = @"vehicleMapImageURL";
NSString *const kNSUVehicleMakeName                   = @"vehicleMakeName";
NSString *const kNSUVehicleModelName                  = @"vehicleModelName";
NSString *const kNSUVehicleNumberPlate                = @"vehicleNumberPlate";
NSString *const kNSUVehicleColor                      = @"vehicleColor";

NSString *const kNSUFavouritePickUpLocation           = @"favouritePickUpLocation";
NSString *const kNSUPickUpLocationAddressLine1        = @"pickUpLocationAddressLine1";
NSString *const kNSUPickUpLocationAddressLine2        = @"pickUpLocationAddressLine2";
NSString *const kNSUPickUpLatitude                    = @"pickUpLatitude";
NSString *const kNSUPickUpLongitude                   = @"pickUpLongitude";

NSString *const kNSUFavouriteDropOffLocation          = @"favouriteDropOffLocation";
NSString *const kNSUDropOffLocationAddressLine1       = @"dropOffLocationAddressLine1";
NSString *const kNSUDropOffLocationAddressLine2       = @"dropOffLocationAddressLine2";
NSString *const kNSUDropOffLatitude                   = @"dropOffLatitude";
NSString *const kNSUDropOffLongitude                  = @"dropOffLongitude";

NSString *const kNSUPubnubPublishTimeInterval         = @"pubnubPublishTimeInterval";
NSString *const kNSUPubnubPublishOnBookingTimeInterval= @"pubnubPublishOnBookingTimeInterval";
NSString *const kNSUGoogleMatrixTimeInterval          = @"googleMatrixTimeInterval";
NSString *const kNSURadiusForNotOperatingArea         = @"radiusForNotOperatingArea";
NSString *const kNSURadiusForOperatingArea            = @"radiusForOperatingArea";
NSString *const kNSUDistanceThresholdForFareEstimate  = @"distanceThresholdForFareEstimate";
NSString *const kNSUOperatingAreaType                 = @"operatingAreaType";
NSString *const kNSUGoogleKeysArray                   = @"googleKeysArray";
NSString *const kNSUMaximumETA                        = @"maximumETA";
NSString *const kNSUBookingAmountAllowUpto            = @"bookingAmountAllowUpto";


#pragma mark - PushNotification Payload Keys

 NSString *const kPNPayloadDoctorNameKey            = @"n";
 NSString *const kPNPayloadAppoinmentTimeKey        = @"dt";
 NSString *const kPNPayloadDistanceKey              = @"dis";
 NSString *const kPNPayloadEstimatedTimeKey         = @"eta";
 NSString *const kPNPayloadDoctorEmailKey           = @"e";
 NSString *const kPNPayloadDoctorContactNumberKey   = @"ph";
 NSString *const kPNPayloadProfilePictureUrlKey     = @"pic";
NSString *const  kPNPayloadStatusIDKey              = @"nt";
NSString *const  kPNPayloadAppointmentIDKey         = @"id";

#pragma mark - Car DescriptionKeys
NSString *const KUBERCarTypeID               = @"type_id";
NSString *const KUBERCarTypeName             = @"type_name";
NSString *const KUBERCarMaxSize              = @"max_size";
NSString *const KUBERCarBaseFare             = @"basefare";
NSString *const KUBERCarMinFare              = @"min_fare";
NSString *const KUBERCarPricePerMin          = @"price_per_min";
NSString *const KUBERCarPricePerKM           = @"price_per_km";
NSString *const KUBERCarTypeDescription      = @"type_desc";
NSString *const KUBERDriverStatus            = @"status";


#pragma mark - Notification Name keys
NSString *const kNotificationNewCardAddedNameKey   = @"cardAdded";
NSString *const kNotificationCardDeletedNameKey   = @"cardDeleted";
NSString *const kNotificationLocationServicesChangedNameKey = @"CLChanged";
NSString *const kNotificationBookingConfirmationNameKey = @"bookingConfirmed";

#pragma mark - Network Error
NSString *const kNetworkErrormessage          = @"No network connection";

NSString *const KNUCurrentLat          = @"latitude";
NSString *const KNUCurrentLong         = @"longitude";
NSString *const KNUserCurrentCity      = @"usercity";
NSString *const KNUserCurrentState     = @"userstate";
NSString *const KNUserCurrentCountry   = @"userCountry";

NSString *const KUDriverEmail           = @"DriverEmail";
NSString *const KUDriverID              = @"mid";
NSString *const KUBookingDate           = @"BookingDate";
NSString *const KUBookingID             = @"BookingID";
