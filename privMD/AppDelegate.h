//
//  AppDelegate.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <GoogleSignIn/GoogleSignIn.h>

@import Firebase;
@import UserNotifications;
@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate, GIDSignInUIDelegate> {
@private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)noPushForceChangingController:(NSDictionary *)userInfo :(int)type;

@end
