//
//  AppDelegate.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AppDelegate.h"
#import "HelpViewController.h"
#import "SplashViewController.h"
#import "MapViewController.h"
#import "PaymentViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "PMDReachabilityWrapper.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AudioToolbox/AudioToolbox.h>
#import "XDKAirMenuController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "NetworkStatusShowingView.h"
#import "Locale.h"
#import "LanguageManager.h"
#import "MenuViewController.h"
#import "NHNetworkTime.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "CheckAppVersion.h"
#import "InvoiceView.h"
#import <AWSCore/AWSCore.h>
#import "AmazonTransfer.h"
#import <Google/Analytics.h>
//#import "ACTReporter.h"

@interface AppDelegate() {
    UIBackgroundTaskIdentifier bgTask;
}

@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) SplashViewController *splashViewController;
@property (nonatomic,strong) MapViewController *mapViewContoller;

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static UIView *mySharedView;

+ (void) initialize {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    mySharedView = [[UIView alloc]initWithFrame:CGRectMake(0, 430, screenWidth, 50)];
    mySharedView.backgroundColor = [[UIColor redColor]colorWithAlphaComponent:0.5];
}

void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [GMSServices provideAPIKey:kGoogleMapAPIKey];
    [self setupAppearance];
    [AmazonTransfer setConfigurationWithRegion:AWSRegionUSWest1 accessKey:AmazonAccessKey secretKey:AmazonSecretKey];

    //save device id
    NSString *uuidSting =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [Utility setDeviceID:uuidSting];
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    [GIDSignIn sharedInstance].delegate = self;
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    // Enable automated usage reporting.
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"86109 7334"];
    [ACTConversionReporter reportWithConversionID:@"86109 7334" label:@"9NKCCJOrqW4Q9prNmgM" value:@"0.00" isRepeatable:NO];

    [[NHNetworkClock sharedNetworkClock] synchronize];
    [self handlePushNotificationWithLaunchOption:launchOptions];
    
    [Fabric with:@[[Crashlytics class]]];
    
    //Network Check
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.reachabilityManager startMonitoring];
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi: {
                [NetworkStatusShowingView removeViewShowingNetworkStatus];
            }
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default: {
                [NetworkStatusShowingView sharedInstance];
            }
                break;
        }
    }];
//    [[CheckAppVersion sharedInstance] checkAppHasUpdatedVersion];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    return YES;
}
    
    
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
   
    NSLog(@"FIR device token :%@", deviceToken);
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        [Utility setPushToken:@""];
    } else {
        [Utility setPushToken:dt];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation]) {
        return YES;
    }else if([[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]){
        return YES;
    }
    return NO;
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (userInfo) {
        [self handleNotificationForUserInfo:userInfo];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self handleNotificationForUserInfo:userInfo];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        [Utility setPushToken:@""];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if([[UIDevice currentDevice].systemVersion floatValue] < 9.0) {
        UIView *containerView;
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        if ([[[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode] isEqualToString: @"ar"]) {
            containerView = [Helper createViewWithTitle:@"رسالة" Message:@"برجاء تحديث نظام iOS للتمتع بالخدمة"];
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"موافق", nil]];
        } else {
            containerView = [Helper createViewWithTitle:@"Message" Message:@"You will not be able to use the app because you are using an old iOS version. Please update it so that you can enjoy an ultimate experience with PQ."];
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Ok", nil]];
        }
        [alertView setContainerView:containerView];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root=General&path=SOFTWARE_UPDATE_LINK"]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=General&path=SOFTWARE_UPDATE_LINK"]];
                }
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
        return;
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLocationServicesChangedNameKey object:nil userInfo:nil];
    // Use Reachability to monitor connectivity
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        if ([menu.currentViewController isKindOfClass:[MapViewController class]]) {
            PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
            [pubnub subscribeToUserChannel];
            if ([[Utility driverPubnubChannel] length]) {
                [pubnub subscribeToChannel:[Utility driverPubnubChannel]];
            }
            [self performSelector:@selector(publishAfterSomeDelay) withObject:nil afterDelay:1.0];
        }
        if ([[Utility userID] length] && [[PMDReachabilityWrapper sharedInstance] isNetworkAvailable]) {
            [self getWalletCurrentMoney];
        }
    }
}


-(void)publishAfterSomeDelay {
    [[MapViewController getSharedInstance] publishPubNubStream];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if ([menu.currentViewController isKindOfClass:[MapViewController class]]) {
        //end Background Task
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
        MapViewController *mapVC = [MapViewController getSharedInstance];
        if(mapVC.requestingView) {
            [mapVC.requestingView setPulsingAnimation];
        }
    }
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
        [pubnub unsubscribeFromMyChannel];
        if ([[Utility driverPubnubChannel] length]) {
            [pubnub unsubscribeFromChannel:[Utility driverPubnubChannel]];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
        [pubnub unsubscribeFromMyChannel];
        if ([[Utility driverPubnubChannel] length]) {
            [pubnub unsubscribeFromChannel:[Utility driverPubnubChannel]];
        }
    }
   // [ACTAutomatedUsageTracker disableAutomatedUsageReportingWithConversionID:@"86109 7334"];
    [self saveContext];
    [self stopBackgroundTask];
}

/**
 Stop Back ground task
 */
-(void)stopBackgroundTask {
    bgTask = 0;
    UIApplication *app = [UIApplication sharedApplication];
    [app endBackgroundTask:bgTask];
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    [Utility setPushToken:refreshedToken];
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
         if (error != nil) {
             NSLog(@"Unable to connect to FCM. %@", error);
         } else {
             NSLog(@"Connected to FCM.");
         }
     }];
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray *_Nullable))restorationHandler {
    if (userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
        [ACTConversionReporter reportUniversalLinkWithUserActivity:userActivity];
        return YES;
    }
    return NO;
}


#if defined(__IPHONE_10_0) && IPHONE_OS_VERSION_MAX_ALLOWED >= IPHONE_10_0
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    // Print full message.
    NSLog(@"%@", userInfo);
    [self handleNotificationForUserInfo:userInfo];
}
#endif

#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions {
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        //check if user is logged in
        if([Utility sessionToken]){
            [self handleNotificationForUserInfo:remoteNotificationPayload];
        }
    }
}


/**
 This is to handle Push Notification data when in booking

 @param userInfo Booking Data
 @param type     Push notification id
 */
-(void)noPushForceChangingController:(NSDictionary *)userInfo :(int)type {
    NSDictionary *dictionary = [userInfo mutableCopy];
    if(type == kNotificationTypeBookingOnMyWay) {
        [Utility setDriverEmailID:flStrForStr(dictionary[@"email"])];
        [Utility setBookingDate:flStrForStr(dictionary[@"apptDt"])];
        [Utility setBookingID:flStrForStr(dictionary[@"bid"])];
        [Utility setDriverName:flStrForStr(dictionary[@"fName"])];
        [Utility setDriverProfileImageURL:flStrForStr(dictionary[@"pPic"])];
        [Utility setDriverMobileNumber:flStrForStr(dictionary[@"mobile"])];
        [Utility setDriverPubnubChannel:flStrForStr(dictionary[@"chn"])];
        [Utility setBookingStatus:kNotificationTypeBookingOnMyWay];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERONTHEWAY" object:nil userInfo:nil];
    } else if(type == kNotificationTypeBookingReachedLocation) {
        [Utility setBookingStatus:kNotificationTypeBookingReachedLocation];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERREACHED" object:nil userInfo:nil];
    } else if(type == kNotificationTypeBookingStarted) {
        [Utility setBookingStatus:kNotificationTypeBookingStarted];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBSTARTED" object:nil userInfo:nil];
    } else if (type == kNotificationTypeBookingComplete) {
        if ([userInfo allKeys].count == 0) {
            return;
        } else {
            for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
                if([view isKindOfClass:[InvoiceView class]]) {
                    return;
                }
            }
            [Utility setBookingStatus:kNotificationTypeBookingComplete];
            InvoiceView* invoiceView = [[InvoiceView alloc]init];
            invoiceView.isComingFromBookingHistory = NO;
            [invoiceView showPopUpWithDetailedDict:userInfo];
        }
    } else if (type == kNotificationTypeBookingReject) {
//        NSString *cancelStatus = flStrForStr(dictionary[@"r"]);
        [Helper showAlertWithTitle:NSLocalizedString(@"Cancel Report", @"Cancel Report") Message:NSLocalizedString(@"Driver cancelled the booking.", @"Driver cancelled the booking.")];//[NSString stringWithFormat:@"%@",cancelStatus]];
        [Utility setBookingStatus:0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    }
}


/**
 Play notification sound

 @param type This is integer to identify different notication
 */
-(void)playNotificationSound:(NSInteger )type {
    SystemSoundID	pewPewSound;
    NSString *pewPewPath;
    if (type == 1) {
        pewPewPath = [[NSBundle mainBundle] pathForResource:@"sms-received" ofType:@"wav"];
    } else {
        pewPewPath = [[NSBundle mainBundle] pathForResource:@"pq" ofType:@"wav"];
    }
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}

- (NSString*)convertEntities:(NSString*)string {
    NSString *returnStr = nil;
    if( string ) {
        returnStr = [string stringByReplacingOccurrencesOfString:@"&amp;" withString: @"&"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&#027;" withString:@"'"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&#092;" withString:@"'"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&#096;" withString:@"'"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        returnStr = [[NSString alloc] initWithString:returnStr];
    }
    return returnStr;
}

/**
 Handle push notification

 @param userInfo push data
 */
-(void)handleNotificationForUserInfo:(NSDictionary*)userInfo {
    NSLog(@"Push Notification Data = %@", userInfo);
    if (kFCMPushSupport) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjects:@[[self convertEntities:userInfo[@"alert"]], userInfo[@"nt"], userInfo[@"sound"]] forKeys: @[@"alert", @"nt", @"sound"]];
        if([[userInfo allKeys] containsObject:@"badge"])
            [dict setObject:userInfo[@"badge"] forKey:@"badge"];
        if ([userInfo[@"nt"] integerValue] == kNotificationTypeBookingReject) {
            [dict setObject:userInfo[@"r"] forKey:@"r"];
        }
        NSDictionary *aps = @{ @"aps" : dict};
        userInfo = [aps mutableCopy];
    }
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
    int type = [userInfo[@"aps"][@"nt"] intValue];
    [[MapViewController getSharedInstance] publishA3ToGetBookingData];
    if (type == 420 || type == 425) {
        [self playNotificationSound:2];
    } else {
        [self playNotificationSound:1];
    }
    if (type == kNotificationTypeBookingAccept) {

    } else if(type == kNotificationTypeBookingOnMyWay) {
        BOOL isCameFrom = [Utility isUserInBooking];
        if (!isCameFrom) {
            [Utility setIsUserInBooking:YES];
        }
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        } else if (!isAlreadyBooked) {
            //already completed
        } else {
            NSDictionary *dictionary = userInfo[@"aps"];
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate) {
                //this will call when App was killed
            } else if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
                });
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
            }
        }
    } else if(type == kNotificationTypeBookingReachedLocation) {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingReachedLocation];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        } else if (!isAlreadyBooked) {
            //already completed
        } else {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate) {
            } else if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    [[MapViewController getSharedInstance] publishA3ToGetBookingData];
                    [self noPushForceChangingController:nil :kNotificationTypeBookingReachedLocation];
                });
            } else {
                [self noPushForceChangingController:nil :kNotificationTypeBookingReachedLocation];
            }
        }
    } else if (type == kNotificationTypeBookingStarted) {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingStarted];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        } else if (!isAlreadyBooked){
            //already completed
        } else {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate) {
                //this will call when App was killed
            } else if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
                });
            } else {
                [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
            }
        }
    } else if (type == kNotificationTypeBookingComplete) {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingComplete];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        } else if (!isAlreadyBooked) {
            //already completed
        } else {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate) {
            } else if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    [self noPushForceChangingController:nil :kNotificationTypeBookingComplete];
                });
            } else {
                [self noPushForceChangingController:nil :kNotificationTypeBookingComplete];
            }
        }
    } else if(type == kNotificationTypeBookingReject) {
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        BOOL isDriverCancel = [[NSUserDefaults standardUserDefaults] boolForKey:@"isDriverCanceledBookingOnce"];
        if (!isDriverCancel) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDriverCanceledBookingOnce"];
        }
        if (isDriverCancel && isAlreadyBooked) {
            NSDictionary *dictionary = userInfo[@"aps"];
            [self noPushForceChangingController:dictionary :kNotificationTypeBookingReject];
        }
    } else if (type == kNotificationTypePassengerRejected) {
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@" :\n%@",userInfo[@"aps"][@"alert"]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if (type == 11) {
        [Utility setBookingStatus:0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    } else if (type == kNotificationTypeAllDriversBusy) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AllDriversBusy" object:nil userInfo:userInfo];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
    } else if (type == kNotificationTypePassengerRecieveMessage) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
    } else if (type == kNotificationTypeUserLoggedOut) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if (type == kNotificationTypePassengerWalletUpdated) {
        if ([[Utility userID] length] && [[PMDReachabilityWrapper sharedInstance] isNetworkAvailable]) {
            [self getWalletCurrentMoney];
        }
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        if (!menu.airDelegate)
        {
            //this will call when App was killed
        } else if (![menu.currentViewController isKindOfClass:[PaymentViewController class]]) {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:3 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        }
    }
}

/**
 Check current status when push comes with booking data

 @param Status Integer value which is coming from value
 @return Yes if is for current status else No
 */
-(BOOL)checkCurrentStatus:(NSInteger)status {
    if([Utility bookingStatus] != status && [Utility bookingStatus] < status) {
        return NO;
    } else {
        return YES;
    }
}

/**
 Check if it booked or not

 @return Yes if it is booked or else No
 */
-(BOOL)checkIfBokkedOrNot {
    if([Utility isUserInBooking]) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Web Services -

/**
This is to check current wallet amount of user
*/
-(void) getWalletCurrentMoney {
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr([Utility userID]),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
}


/**
 Check wallet amount response
 
 @param response return type void and takes no argument
 */
-(void) getWalletCurrentMoneyResponse:(NSDictionary *) response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil)
        return;
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([response [@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
    } else {
        if ([Helper checkForUpdateAppVersion:response[@"versions"][@"iosCust"]]) {
            return;
        }
        NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"amount"]) floatValue]];
        [Utility setWalletBalance:currentWalletAmount];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
    }
}


#pragma mark - Core Data stack -

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PQ" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PQ.sqlite"];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            abort();
        }
    }
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/**
 Set appearance to NAvigation Bar and UIAlertController
 */
- (void)setupAppearance {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:fontNormal size:16], NSFontAttributeName, UIColorFromRGB(0xFFFFFF), NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:UIColorFromRGB(0x019F6E)];
}

/**
 *  Handle Localization
 */
- (void)handleLocalization {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    /*
     * Check the user defaults to find whether a localisation has been set before.
     * If it hasn't been set, (i.e. first run of the app), select the locale based
     * on the device locale setting.
     */
    // Check whether the language code has already been set.
    if (![userDefaults stringForKey:DEFAULTS_KEY_LANGUAGE_CODE]) {
        NSLocale *currentLocale = [NSLocale currentLocale];
        // Iterate through available localisations to find the matching one for the device locale.
        for (Locale *localisation in languageManager.availableLocales) {
            if ([localisation.languageCode caseInsensitiveCompare:[currentLocale objectForKey:NSLocaleLanguageCode]] == NSOrderedSame) {
                [languageManager setLanguageWithLocale:localisation];
                break;
            }
        }
        // If the device locale doesn't match any of the available ones, just pick the first one.
        if (![userDefaults stringForKey:DEFAULTS_KEY_LANGUAGE_CODE]) {
            [languageManager setLanguageWithLocale:languageManager.availableLocales[0]];
        }
    }
}

#pragma mark - 3D touch delegates

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    if ([shortcutItem.type isEqualToString:@"com.pq.home"]) {
        if([Utility sessionToken]) {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
    if ([shortcutItem.type isEqualToString:@"com.pq.bookings"]) {
        if([Utility sessionToken]) {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:1 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
    if ([shortcutItem.type isEqualToString:@"com.pq.share"]) {
        if([Utility sessionToken]) {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:5 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
}

@end
