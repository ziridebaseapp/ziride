//
//  BookingDetailsViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"

@interface BookingDetailsViewController : UIViewController
@property(nonatomic, strong) NSDictionary *invoiceData;
@property(nonatomic, strong) NSString *bookingID;
@property(nonatomic, strong) NSString *appointmentDate;

@property (weak, nonatomic) IBOutlet UITableView *bookingdetailsTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet RPButton *needhelpBtn;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;

- (IBAction)needhelpBtnAction:(id)sender;

@end
