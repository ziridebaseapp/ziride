//
//  BookingDetailsViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "BookingDetailsViewController.h"
#import "BookingDetailsTableViewCell.h"
#import "NeedHelpViewController.h"
#import "ReceiptCell.h"

static BookingDetailsViewController *bookingDetailVC = nil;

@interface BookingDetailsViewController ()<CustomNavigationBarDelegate> {
    BOOL isSubTotalAvailable;
    NSInteger numberOfRows;
    NSMutableArray *titleArray;
    NSMutableArray *priceArray;
    NSMutableArray *lineArray;
    ReceiptCell *cell;
}
@end

@implementation BookingDetailsViewController
@synthesize invoiceData;
@synthesize bookingID;
@synthesize appointmentDate;
@synthesize driverImageView, driverNameLabel, statusLabel, bookingdetailsTableView, needhelpBtn, bookingIDLabel;


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *cellNib = [UINib nibWithNibName:@"ReceiptCell" bundle:nil];
    [bookingdetailsTableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell"];
    [self addCustomNavigationBar];

    NSString *driverImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (driverImageURL.length) {
        driverImageURL = [NSString stringWithFormat:@"%@",flStrForStr(invoiceData[@"pPic"])];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:driverImageURL]
                                placeholderImage:[UIImage imageNamed:@"driverImage"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                       }];
    }
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    
    driverNameLabel.text = flStrForObj(invoiceData[@"fname"]).uppercaseString;
    NSString *status = flStrForStr(invoiceData[@"status"]);
    NSString *at =  NSLocalizedString(@"at", @"at");
    NSString *bookingDate = [Helper getDateInStringForBookingDetails:flStrForStr(invoiceData[@"apntDt"])];
    statusLabel.text = [NSString stringWithFormat:@"%@ %@ %@", status, at, bookingDate];
    bookingIDLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Booking ID:", @"Booking ID:"), flStrForStr(invoiceData[@"bid"])];
    [self updateUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [self calculateNumberOfRows];
    [bookingdetailsTableView reloadData];
}

#pragma mark - Custom Methods -

/**
 Update UI
 */
-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width - 30;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.driverImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverImageView.frame = frame;
        
        frame = self.driverNameLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverNameLabel.frame = frame;
        
        frame = self.statusLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.statusLabel.frame = frame;
        
        frame = self.needhelpBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.needhelpBtn.frame = frame;
    }
}

/**
 This adds navigation bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    NSString *title = [Helper getDateInString:appointmentDate];
    [customNavigationBarView setTitle:flStrForStr(title)];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [self.view addSubview:customNavigationBarView];
}

/**
 Left navigation button

 @param sender back button
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - Custom Methods -
/**
 This calculates number of rows in table

 @return This returns interger value
 */
-(NSInteger) calculateNumberOfRows {
    numberOfRows = 0;
    if([invoiceData[@"stateCode"] integerValue] == 4) {
        
    } else {
        NSString *distance = NSLocalizedString(@"Distance Fare", @"Distance Fare");
        distance = [NSString stringWithFormat:@"%@ (%@ %@)", distance, flStrForStr(invoiceData[@"TripDistance"]), kDistanceParameter];
        NSString *waitingTime = NSLocalizedString(@"Waiting time", @"Waiting time");
        if ([flStrForStr(invoiceData[@"waitingTime"]) intValue] != 0){
            NSString *timeForDisplay = [Helper timeFormatted:[flStrForStr(invoiceData[@"waitingTime"])intValue]];
            waitingTime = [NSString stringWithFormat:@"%@ (%@)", waitingTime, timeForDisplay];
        }
        NSString *duration = NSLocalizedString(@"Duration", @"Duration");
        if ([flStrForStr(invoiceData[@"TripTime"]) intValue] != 0){
            NSString *timeForDisplay = [Helper timeFormatted:[flStrForStr(invoiceData[@"TripTime"])intValue]];
            duration = [NSString stringWithFormat:@"%@ (%@)", duration, timeForDisplay];
        }
        
        titleArray = [[NSMutableArray alloc] initWithObjects: NSLocalizedString(@"Base Fare", @"Base Fare"), distance, waitingTime, duration, nil];
        priceArray = [[NSMutableArray alloc] initWithObjects:invoiceData[@"baseFare"], invoiceData[@"TripDistanceFee"], invoiceData[@"waitingFee"], invoiceData[@"TripTimeFee"], nil];
        if([invoiceData[@"airportFee"] floatValue] > 0){
            if([invoiceData[@"extraChargeTitle"] length]) {
                [titleArray addObject:flStrForStr(invoiceData[@"extraChargeTitle"])];
            } else {
                [titleArray addObject:NSLocalizedString(@"Airport Fare", @"Airport Fare")];
            }
            [priceArray addObject:invoiceData[@"airportFee"]];
            lineArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"1", nil];
            numberOfRows++;
        } else {
            lineArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"1", nil];
        }
        
        [titleArray addObject:NSLocalizedString(@"Trip fare", @"Trip fare")];
        [priceArray addObject:invoiceData[@"subtotal"]];
        [lineArray addObject:@"1"];
        
        if([invoiceData[@"discountVal"] floatValue] > 0){
            NSString *discountLabel = NSLocalizedString(@"Promo Code", @"Promo Code");
            if([flStrForStr(invoiceData[@"code"]) length]) {
                if ([invoiceData[@"discountType"] integerValue] == 2) {
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ %% )",discountLabel, flStrForStr(invoiceData[@"code"]), flStrForStr(invoiceData[@"discountPercentage"])];
                } else if ([invoiceData[@"discountType"] integerValue] == 1) {
                    NSString *discountWithCurrency = [Helper getCurrencyLocal:[flStrForStr(invoiceData[@"discountVal"]) floatValue]];
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ )", discountLabel, flStrForStr(invoiceData[@"code"]), discountWithCurrency];
                }
            }
            [titleArray addObject:discountLabel];
            [priceArray addObject:invoiceData[@"discountVal"]];
            numberOfRows++;
        }
        
        if ([invoiceData[@"sumOfLastDue"] floatValue] > 0) {
            [titleArray addObject:NSLocalizedString(@"Dues from previous trips", @"Dues from previous trips")];
            [priceArray addObject:invoiceData[@"sumOfLastDue"]];
            [lineArray addObject:@"0"];
            [lineArray addObject:@"1"];
            numberOfRows++;
        } else if([invoiceData[@"discountVal"] integerValue] > 0) {
            [lineArray addObject:@"1"];
        }
        
        if ([invoiceData[@"duePayment"] floatValue] > 0) {
            [titleArray addObject:NSLocalizedString(@"Dues payment", @"Dues payment")];
            [priceArray addObject:invoiceData[@"duePayment"]];
            [lineArray addObject:@"1"];
            numberOfRows++;
        }
        
        [titleArray addObject:NSLocalizedString(@"Payment Method:", @"Payment Method:")];
        [priceArray addObject:@" "];
        [lineArray addObject:@"0"];
        
        if([flStrForStr(invoiceData[@"payType"]) integerValue] == 1 && [invoiceData[@"cardDeduct"] floatValue] > 0) {
            NSString *card = flStrForStr(invoiceData[@"cardlastDigit"]);
            if (card.length) {
                NSString *last4 = [card substringFromIndex: [card length] - 4];
                [titleArray addObject:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Paid by ****", @"Paid by ****"), last4]];
            } else {
                [titleArray addObject:NSLocalizedString(@"Paid by card", @"Paid by card")];
            }
            [priceArray addObject:flStrForStr(invoiceData[@"cardDeduct"])];
            numberOfRows++;
        } else if([flStrForStr(invoiceData[@"payType"]) integerValue] == 2 && [invoiceData[@"cashCollected"] floatValue] > 0) {
            [titleArray addObject:NSLocalizedString(@"Cash collected", @"Cash collected")];
            [priceArray addObject:flStrForStr(invoiceData[@"cashCollected"])];
            numberOfRows++;
        }

        if([invoiceData[@"walletDeducted"] floatValue] != 0){
            [titleArray addObject:NSLocalizedString(@"Paid from Wallet", @"Paid from Wallet")];
            [priceArray addObject:invoiceData[@"walletDeducted"]];
            if([invoiceData[@"cashCollected"] floatValue] > 0 || [invoiceData[@"cardDeduct"] floatValue] > 0){
                [lineArray addObject:@"0"];
            }
            [lineArray addObject:@"1"];
            numberOfRows++;
        } else {
            [lineArray addObject:@"1"];
        }
        
        if([invoiceData[@"walletDebitCredit"] integerValue] != 0){
            [titleArray addObject:NSLocalizedString(@"Wallet transaction", @"Wallet transaction")];
            [priceArray addObject:invoiceData[@"walletDebitCredit"]];
            [lineArray addObject:@"1"];
            numberOfRows++;
        }
        numberOfRows +=6;
    }
    return numberOfRows;
}


#pragma mark -UIButton Action -

/**
 Need Help Button Action

 @param sender Need Help Button 
 */
- (IBAction)needhelpBtnAction:(id)sender {
    NeedHelpViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NeedHelp"];
    VC.invoiceData = invoiceData;
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}


#pragma mark - UITableView Datasource and Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (invoiceData == nil) {
        numberOfRows = 0;
        titleArray = [[NSMutableArray alloc] init];
        priceArray = [[NSMutableArray alloc] init];
        return numberOfRows;
    } else {
        return numberOfRows;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
    cell = (ReceiptCell *)[bookingdetailsTableView dequeueReusableCellWithIdentifier:@"ReceiptCell"];
    if(!cell) {
        cell = [cellArray firstObject];
    }
    NSString *price = [Helper getCurrencyLocal:[flStrForStr(priceArray[indexPath.row]) floatValue]];
    [Helper setToLabel:cell.itemLabel Text:titleArray[indexPath.row] WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x000000)];
    if([titleArray[indexPath.row] isEqualToString:NSLocalizedString(@"Payment Method:", @"Payment Method:")]) {
        price = @" ";
        [Helper setToLabel:cell.itemLabel Text:titleArray[indexPath.row] WithFont:fontBold FSize:13 Color:UIColorFromRGB(0x000000)];
    }
    [Helper setToLabel:cell.priceLabel Text:price WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x000000)];
    if ([lineArray[indexPath.row] integerValue] == 1) {
        cell.lineLabel.hidden = NO;
    } else {
        cell.lineLabel.hidden = YES;
    }
    return cell;
}



@end
