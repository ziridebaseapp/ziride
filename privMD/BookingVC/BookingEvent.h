//
//  BookingEvent.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingEvent : NSObject

@property (nonatomic,strong) NSDate *date;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSString *pickAdd;
@property (nonatomic,strong) NSString *desAdd;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *amount;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSDictionary *info;
@property (nonatomic,strong) NSString *bid;
@property (nonatomic,strong) NSString *apptDate;

-(instancetype)initWithDictionary:(NSDictionary *)eventsDict;

@end
