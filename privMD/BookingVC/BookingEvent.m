//
//  BookingEvent.m
//  UBER
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "BookingEvent.h"

#define kPic                                @"pPic"
#define kEmail                              @"email"
#define kFirstName                          @"fname"
#define kAddressLine1                       @"addrLine1"
#define kDropAddressLine1                   @"dropLine1"
#define kAppointmentDateTime                @"apntDt"
#define kAmount                             @"amount"
#define kDistance                           @"distance"
#define kStatus                             @"status"
#define kBid                                @"bid"
#define kapntDt                             @"apntDt"


@implementation BookingEvent

-(instancetype)initWithDictionary:(NSDictionary *)eventsDict {
    self = [super init];
    if (self) {
        self.title = flStrForStr([eventsDict  objectForKey:kEmail]);
        self.image = flStrForObj([eventsDict objectForKey:kPic]);
        self.name = flStrForStr([eventsDict objectForKey:kFirstName]);
        self.pickAdd = flStrForStr([eventsDict  objectForKey:kAddressLine1]);
        self.desAdd = flStrForStr([eventsDict  objectForKey:kDropAddressLine1]);
        self.time = flStrForStr([eventsDict objectForKey:kAppointmentDateTime]);
        self.distance = flStrForStr([eventsDict objectForKey:kDistance]);
        self.amount = flStrForStr([eventsDict  objectForKey:kAmount]);
        self.status = flStrForStr([eventsDict objectForKey:kStatus]);
        self.bid = flStrForStr([eventsDict objectForKey:kBid]);
        self.apptDate = flStrForStr([eventsDict objectForKey:kapntDt]);
        self.info =  eventsDict;
    }
    return self;
}

@end
