//
//  BookingsViewController.h
//  PQ
//
//  Created by Rahul patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property(nonatomic) NSInteger pageIndex;
@end
