//
//  BookingsViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "BookingsViewController.h"
#import "BookingEvent.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "EmptyCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "SplashViewController.h"
#import "InvoiceView.h"
#import "CancelledPopUpView.h"
#import "BookingDetailsViewController.h"
#import "NeedHelpViewController.h"
#import "HistoryTableViewCell.h"
#import "User.h"

@interface BookingsViewController ()<CustomNavigationBarDelegate> {
    NSInteger indicatorCell;
    BOOL reachedLastCell;
    NSIndexPath *selectedCellIndex;
}
@property (nonatomic,strong) NSMutableArray *events;
@end

@implementation BookingsViewController
@synthesize pageIndex;
@synthesize footerView;


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    _events = [NSMutableArray array];
    pageIndex = 0;
    indicatorCell = 0;
    reachedLastCell = NO;
    [self sendServiceGetAppointment:pageIndex];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"IssueRaised"] == 1) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IssueRaised"];
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark- Custom Navigation Methods

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"History", @"History")];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation left Button

 @param sender leftBarButton
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [self menuButtonclicked];
}

- (void)menuButtonclicked {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


#pragma mark - WebServiceCall -

/**
 Send serivce to get all appoinment
 
 @param index index Number
 */
-(void)sendServiceGetAppointment:(NSInteger)index; {
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NSString *indexValue = [NSString stringWithFormat:@"%ld", (long)index];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_date_time":[Helper getCurrentNetworkDateTime],
                               @"ent_appnt_dt":@"",
                               @"ent_page_index":flStrForStr(indexValue)
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getSlaveAppointments"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getAppointmentResponse:response];
                             } else {
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 indicatorCell = 0;
                                 [[self table] reloadData];
                                 --pageIndex;
                             }
                         }];
}

/**
 Get appoinmentservice response
 
 @param response response data
 */
-(void)getAppointmentResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 99 || [response[@"errNum"] intValue] == 101 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            [self.events addObjectsFromArray:dictResponse[@"appointments"]];
            reachedLastCell = [dictResponse[@"lastcount"] boolValue];
            if (!reachedLastCell)
                indicatorCell = 1;
            else
                indicatorCell = 0;
            [[self table] reloadData];
            ++pageIndex;
        } else if([[dictResponse objectForKey:@"errFlag"] intValue] == 1 && [[dictResponse objectForKey:@"errNum"] intValue] == 65) {
            indicatorCell = 0;
            reachedLastCell = YES;
            [[self table] reloadData];
        }
    }
}


#pragma mark- Custom Methods

/**
 Add events from array to custom class
 
 @param eventsArray events array
 */
-(void)addEvents:(NSArray *)eventsArray {
    NSMutableDictionary *eventsDict = nil;
    for (int i =0; i< eventsArray.count ;i++) {
        eventsDict = eventsArray[i];
        BookingEvent *event = [[BookingEvent alloc] initWithDictionary:eventsDict];
        [_events addObject: event];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [self events].count;
    if (count == 0) {
        count = 1;
    } else {
        count = count + indicatorCell;
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger count = [self.events count];
    if (count == 0) {
        static NSString *CellIdentifier = @"emptyCell";
        EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.emptyStatuslabel.frame = CGRectMake(0, (cell.frame.size.height - 50)/2, cell.frame.size.width, 50);
        return cell;
    } else if (indexPath.section == count) {
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        cell.backgroundColor = UIColorFromRGB(0x019F6E);
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
        activityIndicator.frame = CGRectMake((self.table.frame.size.width-25)/2, (50-25)/2, 25, 25);
        [cell addSubview:activityIndicator];
        activityIndicator.color = UIColorFromRGB(0xFFFFFF);
        activityIndicator.backgroundColor = [UIColor clearColor];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        [activityIndicator startAnimating];
        return cell;
    } else {
        static NSString *historyCellIdentifier = @"HistoryCell";
        HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:historyCellIdentifier];
        if (cell == nil) {
            cell = (HistoryTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:historyCellIdentifier];
        }
        NSDictionary *event = self.events[indexPath.section];
        cell.pickUpDotView.layer.cornerRadius = cell.pickUpDotView.frame.size.width/2;
        cell.pickUpDotView.layer.masksToBounds = YES;
        cell.destinationDotView.layer.cornerRadius = cell.destinationDotView.frame.size.width/2;
        cell.destinationDotView.layer.masksToBounds =  YES;
        
        cell.pickUpLabel.text = flStrForStr(event[@"addrLine1"]);
        if ([flStrForStr(event[@"dropLine1"]) length] == 0) {
            cell.destinationLabel.text = @"Dropoff was not added";
        } else {
            cell.destinationLabel.text = flStrForStr(event[@"dropLine1"]);
        }
        NSString *driverImageURL = flStrForStr(event[@"pPic"]);
        if (driverImageURL.length) {
            driverImageURL = [NSString stringWithFormat:@"%@",flStrForStr(event[@"pPic"])];
            [cell.driverProfileImageView sd_setImageWithURL:[NSURL URLWithString:driverImageURL]
                                                  placeholderImage:[UIImage imageNamed:@"driverImage"]
                                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                         }];
        }
        cell.driverProfileImageView.layer.cornerRadius = cell.driverProfileImageView.frame.size.width/2;
        cell.driverProfileImageView.layer.masksToBounds = YES;
        NSString *bookingID = [NSString stringWithFormat:@"%@ %@ | %@", NSLocalizedString(@"Booking ID:", @"Booking ID:"), flStrForStr(event[@"bid"]), [Helper getDateInString:flStrForStr(event[@"apntDt"])]];
        [cell.dateAndTimeLabel setText:bookingID];
        [cell.driverNameLabel setText:flStrForStr(event[@"fname"]).uppercaseString];
        [cell.statusLabel setText:flStrForStr(event[@"status"])];
        NSString *amount =  [Helper getCurrencyLocal:[flStrForStr(event[@"invoice"][@"amount"]) floatValue]];
        cell.amountLabel.text = [NSString stringWithFormat:@"%@", amount];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.events.count == 0)
        return;
    NSDictionary *dictionary = self.events[indexPath.section];
    if([[dictionary allKeys] containsObject:@"invoice"]){
        NSDictionary *dict1 = dictionary[@"invoice"];
        NSMutableDictionary *dict2 = [dictionary mutableCopy];
        [dict2 removeObjectForKey:@"invoice"];
        [dict2 addEntriesFromDictionary:dict1];
        dictionary = [dict2 mutableCopy];
    }
    if ([dictionary[@"stateCode"] integerValue] == 4 || [dictionary[@"stateCode"] integerValue] == 5){
        selectedCellIndex = indexPath;
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
            if([view isKindOfClass:[CancelledPopUpView class]]) {
                return;
            }
        }
        CancelledPopUpView* view = [[CancelledPopUpView alloc]init];
        [view showPopUpWithDetailedDict:dictionary Onwindow:[UIApplication sharedApplication].keyWindow];
        view.onCompletion = ^(NSInteger needBtnClicked) {
            if (needBtnClicked == 1) {
                NeedHelpViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NeedHelp"];
                VC.invoiceData = [dictionary mutableCopy];
                [Helper checkForPush:self.navigationController.view];
                [self.navigationController pushViewController:VC animated:NO];
            }
        };
    } else {
        BookingDetailsViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"BookDetails"];
        VC.invoiceData = [dictionary mutableCopy];
        VC.bookingID = flStrForStr(dictionary[@"bid"]);
        VC.appointmentDate = flStrForStr(dictionary[@"apntDt"]);
        [Helper checkForPush:self.navigationController.view];
        [self.navigationController pushViewController:VC animated:NO];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath; {
    NSInteger count = [self events].count;
    if (count == 0) {
        return self.table.frame.size.height;
    } else if(indexPath.section == count) {
        return 50;
    } else {
        return 150;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSInteger count = [self events].count;
    if (count == 0) {
        return 0;
    }
    return 10;
}

- (void)scrollViewDidScroll: (UIScrollView *)scroll {
    CGFloat currentOffset = scroll.contentOffset.y;
    CGFloat contentHeight = scroll.contentSize.height;
    CGFloat scrollHeight  = scroll.frame.size.height;
    if (currentOffset+scrollHeight == contentHeight && !reachedLastCell) {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable]) {
            pageIndex++;
            indicatorCell = 1;
            [self sendServiceGetAppointment:pageIndex];
        } else {
            indicatorCell = 0;
            [[self table] reloadData];
        }
    }
}



@end
