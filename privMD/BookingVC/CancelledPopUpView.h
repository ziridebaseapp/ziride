//
//  CancelledPopUpView.h
//  PQ
//
//  Created by 3Embed on 10/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"

@interface CancelledPopUpView : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *cancelledTimeLabel;
@property (weak, nonatomic) IBOutlet RPButton *needHelpBtn;
@property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *cancelStatusLabel;


- (IBAction)closeBtnAction:(id)sender;
- (IBAction)needHelpBtnAction:(id)sender;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger needHelpBtnClick);
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;

@end
