//
//  CancelledPopUpView.m
//  PQ
//
//  Created by 3Embed on 10/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CancelledPopUpView.h"

@implementation CancelledPopUpView
@synthesize dateTitleLabel, cancelledTimeLabel, cancelStatusLabel, bookingIDLabel;
@synthesize driverImageView, driverNameLabel, needHelpBtn, closeBtn;
@synthesize onCompletion;


-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"CancelledPopUpView" owner:self options:nil] firstObject];
    return self;
}


#pragma mark - Custom Methods -

/**
 Show pop up with datails

 @param dict   invoice data
 @param window root Window
 */
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    [dateTitleLabel setText:[Helper getDateInString:flStrForStr(dict[@"apntDt"])]];
    [driverNameLabel setText:flStrForStr(dict[@"fname"])];
    [bookingIDLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Booking ID:", @"Booking ID:"), flStrForStr(dict[@"bid"])]];
    [cancelledTimeLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Cancelled at", @"Cancelled at"), [Helper getDateInStringForCancelledPopUp:flStrForStr(dict[@"cancel_dt"])]]];
    if ([dict[@"cancelTimeApplied"] integerValue] != 0) {
        NSString *amount = [Helper getCurrencyLocal:[flStrForStr(dict[@"amount"]) floatValue]];
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@ %@", amount, NSLocalizedString(@"Minimum fare was applied for cancelling after", @"Minimum fare was applied for cancelling after"), flStrForStr(dict[@"cancelTime"]), NSLocalizedString(@"Minutes", @"Minutes")];
        [cancelStatusLabel setText:str];
    } else {
        [cancelStatusLabel setText:NSLocalizedString(@"No payment due", @"No payment due")];
    }
    if ([Helper isCurrentLanguageRTL]) {
        self.driverNameLabel.textAlignment = NSTextAlignmentRight;
        self.cancelledTimeLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.driverNameLabel.textAlignment = NSTextAlignmentLeft;
        self.cancelledTimeLabel.textAlignment = NSTextAlignmentRight;
    }
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    [self layoutIfNeeded];
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         self.driverImageView.layer.cornerRadius = self.driverImageView.frame.size.width/2;
                         self.driverImageView.layer.masksToBounds = YES;
                         NSString *driverImageURL = flStrForStr(dict[@"pPic"]);
                         if (driverImageURL.length) {
                             [driverImageView sd_setImageWithURL:[NSURL URLWithString:driverImageURL]
                                                placeholderImage:[UIImage imageNamed:@"driverImage"]
                                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                           if (!error && image) {
                                                               driverImageView.image = [Helper imageWithImage:image scaledToSize:CGSizeMake(55, 55)];
                                                           }
                                                       }];
                         }
                     }
                     completion:^(BOOL finished){
                     }];
}

/**
 Hide Pop up and remove from Window
 */
-(void)hidePOPup {
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

#pragma mark -UIButton Action -

- (IBAction)closeBtnAction:(id)sender {
    onCompletion(0);
    [self hidePOPup];
}

- (IBAction)needHelpBtnAction:(id)sender {
    onCompletion(1);
    [self hidePOPup];
}

@end
