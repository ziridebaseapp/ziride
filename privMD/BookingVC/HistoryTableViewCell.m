//
//  HistoryTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell
@synthesize pickUpDotView, destinationDotView;
@synthesize pickUpLabel, destinationLabel;
@synthesize driverProfileImageView, driverNameLabel;
@synthesize dateAndTimeLabel, statusLabel, amountLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame = self.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    [self subviews][0].frame = frame;

    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGRect frame = self.pickUpDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.pickUpDotView.frame = frame;
        
        frame = self.destinationDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.destinationDotView.frame = frame;
        
        frame = self.pickUpLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.pickUpLabel.frame = frame;
        
        frame = self.destinationLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.destinationLabel.frame = frame;
        
        frame = self.driverProfileImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverProfileImageView.frame = frame;
        
        frame = self.driverNameLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverNameLabel.frame = frame;
        
        frame = self.dateAndTimeLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dateAndTimeLabel.frame = frame;
        
        frame = self.statusLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.statusLabel.frame = frame;
        
        frame = self.amountLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.amountLabel.frame = frame;
        self.amountLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.amountLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
