//
//  CancelledReasonView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelledReasonView : UIView<UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *reasonData;
    int previousSelcetedCell;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headinglabel1;
@property (weak, nonatomic) IBOutlet UILabel *headinglabel2;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;


@property (nonatomic, copy)   void (^onCompletion)(NSInteger acceptOrRejects);
-(void)showPopUpViewOnwindow:(UIWindow *)window;
-(void)hidePOPup;

@end
