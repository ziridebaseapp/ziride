//
//  CancelledReasonView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CancelledReasonView.h"
#import "CancelledReasonCell.h"
#import "SplashViewController.h"
@implementation CancelledReasonView
@synthesize contentView, headerView, headinglabel1, headinglabel2, tableView, submitBtn,cancelBtn;
@synthesize onCompletion;


-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"CancelledReason" owner:self options:nil] firstObject];
    UINib *cellNib = [UINib nibWithNibName:@"CancelledReasonCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"CancelledReasonCell"];
    return self;
}

/**
 Get Cancel Reason from server
 */
-(void) getCancelledReasons {
    NSDictionary *params = @{@"ent_lan":@"0",
                             @"ent_user_type":@"2",
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCancellationReson"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseCancelledReasonResponse:response];
                                   else
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:[UIApplication sharedApplication].keyWindow withMessage:@"Loading..."];
}

/**
 Parser cancel reason service response

 @param responseDict response
 */
-(void) parseCancelledReasonResponse:(NSDictionary *)responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 0) {
            reasonData = [[NSMutableArray alloc] initWithArray:[responseDict[@"getdata"] mutableCopy]];
            [self.tableView reloadData];
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            if (self.tableView.contentSize.height > screenHeight - 115) {
                self.tableViewHeight.constant = screenHeight - 115;
            } else {
                self.tableViewHeight.constant = self.tableView.contentSize.height;
            }
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        }
    }
}


#pragma mark- Service Call

/**
 Send request for cancel appointmnent
 */
-(void)sendRequestForCancelAppointment {
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    NSString *currentDate = [Helper getCurrentDateTime];
    NSString *rideCancelReason = flStrForStr(reasonData[previousSelcetedCell]);
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mid":[Utility driverID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":currentDate,
                             @"ride_cancelled":rideCancelReason,
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseCancelAppointmentResponse:response];
                               }];
}

/**
 Parse cancel appoinment service response

 @param response response
 */
-(void)parseCancelAppointmentResponse:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 42) {
        [self hidePOPup];
        NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"walletAmt"]) floatValue]];
        [Utility setWalletBalance:currentWalletAmount];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    } else if([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 43) {
        [self hidePOPup];
        onCompletion(1);
        NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"walletAmt"]) floatValue]];
        [Utility setWalletBalance:currentWalletAmount];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}


/**
 Show Cancel Reason Pop Up View

 @param window Application Window
 */
-(void)showPopUpViewOnwindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    previousSelcetedCell = -1;
    [self.tableView setEstimatedRowHeight:40];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self getCancelledReasons];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}


/**
 Cancel Button Action

 @param sender cancel Button
 */
-(IBAction)cancelBtnAction:(id)sender {
    onCompletion(0);
    [self hidePOPup];
}


/**
 Hide Pop Up view
 */
-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}


/**
 Submit Button Action

 @param sender Submit Button
 */
- (IBAction)submitBtnAction:(id)sender {
    if (previousSelcetedCell == -1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Messge", @"Messge") Message: NSLocalizedString(@"Please select one of the reason.", @"Please select one of the reason.")];
    } else {
        [self sendRequestForCancelAppointment];
    }
}

/**
 UITapGesture Recognizer Action Method

 @param sender tapGestureRecognizer
 */
- (IBAction)tapGestureRecognizerAction:(id)sender {
    CGPoint location = [sender locationInView:self];
    id tappedView = [self hitTest:location withEvent:nil];
    if ([tappedView isEqual:self]) {
        onCompletion(2);
        [self hidePOPup];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return reasonData.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CancelledReasonCell *cell = (CancelledReasonCell *)[tableView1 dequeueReusableCellWithIdentifier:@"CancelledReasonCell"];
    cell.itemLabel.text = flStrForStr(reasonData[indexPath.row]);
    cell.selectedImageView.image = [UIImage new];
    return cell;
}

-(void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView1 deselectRowAtIndexPath:indexPath animated:YES];
    CancelledReasonCell *cell;
    if (previousSelcetedCell != -1) {
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:previousSelcetedCell inSection:0];
        cell = (CancelledReasonCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
        cell.backgroundColor = [UIColor clearColor];
        cell.itemLabel.textColor = UIColorFromRGB(0x000000);
    }
    previousSelcetedCell = (int)indexPath.row;
    cell = (CancelledReasonCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = UIColorFromRGB(0x019F6E);
    cell.itemLabel.textColor = [UIColor whiteColor];
}



@end
