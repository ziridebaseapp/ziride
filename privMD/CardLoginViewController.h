//
//  CardLoginViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"


typedef void (^CardAddedOrNot)(BOOL isCardAdded);

@interface CardLoginViewController : UIViewController <UITextFieldDelegate>
{
        
}
@property (weak, nonatomic) IBOutlet UIScrollView *cardScrollView;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;


@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *monthTextField;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UIButton *scanCardButton;

@property (weak, nonatomic) IBOutlet UITextField *cvcTextField;


@property (weak, nonatomic) IBOutlet UIView *supportFrameView;
@property (strong, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIView *paymentTextFieldBackgroundView;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (strong, nonatomic)  UIButton *navNextButton;
@property (strong, nonatomic)  NSArray *getSignupDetails;
@property (strong,nonatomic)   NSArray *getInfoDetails;
@property (strong, nonatomic) UIImage *pickedImage;

@property (assign,nonatomic) isComingFromViewController isComingFromVC;
@property (nonatomic,copy) CardAddedOrNot callback;

@property (assign,nonatomic)  NSMutableArray *arrayContainingCardInfo;

- (IBAction)doneButtonClicked:(id)sender;
- (IBAction)tabgestureButtonClick:(id)sender;
- (IBAction)cardScanButtonAction:(id)sender;


@end
