//
//  CardLoginViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CardLoginViewController.h"
#import "MapViewController.h"
#import "UploadFiles.h"
#import "MenuViewController.h"
#import "Database.h"
#import "CardDetails.h"
#import "SplashViewController.h"
#import "User.h"
#import "CardIO.h"
#import <Stripe/Stripe.h>
#import "CardIOUtilities.h"

@interface CardLoginViewController ()<STPPaymentCardTextFieldDelegate, CardIOPaymentViewControllerDelegate,CardIOViewDelegate>
{
    BOOL isSuccessed;
    STPPaymentCardTextField *paymentTextField;
}
@end

@implementation CardLoginViewController
@synthesize callback;
@synthesize isComingFromVC;
@synthesize supportFrameView, paymentTextFieldBackgroundView, doneButton, scanButton;
@synthesize navNextButton;
@synthesize getSignupDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    [self setPaymentTextFieldProperties];
    [[self.scanCardButton layer] setBorderWidth:0.5f];
    [[self.scanCardButton layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [self.cardNumberTextField setValue:BLACK_COLOR
                     forKeyPath:@"_placeholderLabel.textColor"];
    
    
    [self.cvcTextField setValue:BLACK_COLOR
                          forKeyPath:@"_placeholderLabel.textColor"];
    
    
    [self.fullNameTextField setValue:BLACK_COLOR
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.monthTextField setValue:BLACK_COLOR
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.yearTextField setValue:BLACK_COLOR
                    forKeyPath:@"_placeholderLabel.textColor"];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
   // [self.cardScrollView ];
    [self.cardScrollView setContentSize:CGSizeMake(screenWidth,screenHeight)];
    self.cardScrollView.scrollEnabled=YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [CardIOUtilities preload];
    [self createNavView];
    if (isComingFromVC == kVerifyViewController)
    {
        self.navigationItem.hidesBackButton = YES;
        [self createNavRightButton];
    }
    else
    {
        self.navigationItem.hidesBackButton = NO;
        [self createNavLeftButton];
    }
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UINavigation Methods -

/**
 Creates Navigation view
 */

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80, 10, 160, 44)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 160, 44)];
    navTitle.text = NSLocalizedString(@"Add Card", @"Add Card");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:fontNormal size:15];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}

/**
 Creates Navigation right button
 */
-(void)createNavRightButton
{
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"map_menu"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];
    [navNextButton addTarget:self action:@selector(skipButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:NSLocalizedString(@"Skip", @"Skip") WithFont:fontNormal FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"Skip", @"Skip") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"Skip", @"Skip") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Creates Navigation left button
 */

-(void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    UIImage *buttonImage;
    if(isComingFromVC == kMapViewController) {
        buttonImage = [UIImage imageNamed:@"payment_cancel_btn_off"];
        [navCancelButton setImage:[UIImage imageNamed:@"payment_cancel_btn_off"] forState:UIControlStateNormal];
        [navCancelButton setImage:[UIImage imageNamed:@"payment_cancel_btn_on"] forState:UIControlStateSelected];
        [navCancelButton setImage:[UIImage imageNamed:@"payment_cancel_btn_on"] forState:UIControlStateHighlighted];
    } else {
        buttonImage = [UIImage imageNamed:@"back_icon_off"];
        [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
        [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
        [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation right button action
 */

-(void)skipButtonClicked
{
    [self.view endEditing:YES];
    UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"You can edit the Card settings anytime from 'MY WALLET' in the Menu", @"You can edit the Card settings anytime from 'MY WALLET' in the Menu")];
    RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
    [alertView setContainerView:containerView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Ok",@"Ok"), nil]];
    [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
        if (buttonIndex == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        }
        [alertView close];
    }];
    [alertView setUseMotionEffects:true];
    [alertView show];
}

/**
 Navigation left button action
 */
-(void)cancelButtonClicked
{
    if(isComingFromVC == kMapViewController)
    {
        callback(NO);
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [Helper checkForPop:self.navigationController.view];
        [self.navigationController popViewControllerAnimated:NO];
    }
}


#pragma mark - Custom Methods -


/**
 Update UI
 */

-(void)updateUI
{
    if ([Helper isCurrentLanguageRTL])
    {
//        CGFloat screenWidth = self.view.frame.size.width;
//        CGRect frame = self.label1.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.label1.frame = frame;
//        
//        frame = self.label3.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.label3.frame = frame;
    }
}

-(void)setPaymentTextFieldProperties {
    paymentTextField = [[STPPaymentCardTextField alloc] init];
    paymentTextField.delegate = self;
    paymentTextField.cursorColor = [UIColor blueColor];
    paymentTextField.borderColor = [UIColor clearColor];
    paymentTextField.borderWidth = 0;
    paymentTextField.font = [UIFont fontWithName:fontNormal size:12];
    paymentTextField.textColor = [UIColor blackColor];
    paymentTextField.cornerRadius = 2.0;
    paymentTextField.frame = CGRectMake(0, 0,paymentTextFieldBackgroundView.frame.size.width, paymentTextFieldBackgroundView.frame.size.height);
}

/**
 Get card details when you add new card to store in Local DB
 */

-(void) getCardDetailsFromService {
    if ([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable]) {
        [self sendServiceToGetCardDetail];
    } else {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   if (textField == self.fullNameTextField) {
       [self.monthTextField becomeFirstResponder];
   } else if (textField == self.monthTextField) {
       [self.yearTextField becomeFirstResponder];
   } else if (textField == self.yearTextField) {
       [self.cvcTextField becomeFirstResponder];
   } else if (textField == self.cvcTextField) {
       [self.cvcTextField resignFirstResponder];
   }
   return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField!=self.cardNumberTextField)
    {
        [self keyboardWillShow];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField!=self.cardNumberTextField)
    {
         [self keyboardWillHide];
    }
}

#pragma mark - keyboard movements

- (void)keyboardWillShow
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.cardScrollView.frame;
        f.origin.y = -50;
        self.cardScrollView.frame = f;
    }];
}

-(void)keyboardWillHide
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.cardScrollView.frame;
        f.origin.y = 0.0f;
        self.cardScrollView.frame = f;
    }];
}

#pragma mark - UIButton Action -

/**
 Done Button Clicked Action

 @param sender Done Button
 */

- (IBAction)doneButtonClicked:(id)sender {
    [self.view endEditing:YES];
    if(!([self.cardNumberTextField.text length] > 0)) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter your card number"];
        return;
    } else if(!([self.fullNameTextField.text length] > 0)) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter your full name"];
        return;
    } else if (!([self.monthTextField.text length] > 0)) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter the month"];
        return;
    } else if (!([self.cvcTextField.text length] > 0)) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter your cvc number"];
        return;
    } else if (!([self.yearTextField.text length] > 0)) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter the year"];
        return;
    }
    STPCardParams *param = [[STPCardParams alloc]init];
    NSString *cardNumer = self.cardNumberTextField.text;
    param.number = [cardNumer stringByReplacingOccurrencesOfString:@" " withString:@""];
    param.expMonth = [self.monthTextField.text integerValue];
    param.expYear = [self.yearTextField.text integerValue];
    param.cvc = self.cvcTextField.text;
    param.name = self.fullNameTextField.text;
    paymentTextField.cardParams = param;
    
    if (![paymentTextField isValid]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Enter Valid Card Details", @"Please Enter Valid Card Details")];
        return;
    } else if (![Stripe defaultPublishableKey]) {
        if([Utility paymentGatewayKey]) {
            [Stripe setDefaultPublishableKey:[Utility paymentGatewayKey]];
        } else {
            NSError *error = [NSError errorWithDomain:StripeDomain
                                                 code:STPInvalidRequestError
                                             userInfo:@{
                                                        NSLocalizedDescriptionKey:NSLocalizedString(@"Please specify a Stripe Publishable Key", @"Please specify a Stripe Publishable Key")
                                                        }];
            if (error) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[error localizedDescription]];
            }
            return;
        }
    }
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Adding Card...", @"Adding Card...")];
    [[STPAPIClient sharedClient] createTokenWithCard:paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              if (error) {
                                                  [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                                  [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[error localizedDescription]];
                                              } else {
                                                  NSLog(@"%@",token.tokenId);
                                                  [self sendServiceAddCardsDetails:token.tokenId];
                                              }
                                          }];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.cardNumberTextField) {
        __block NSString *text = [textField text];
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        NSString *newString = @"";
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        if (newString.length >= 19) {
            [textField setText:newString];
            [self.fullNameTextField becomeFirstResponder];
            return NO;
        }
        [textField setText:newString];
        return NO;
    } else if(textField == self.monthTextField) {
        __block NSString *text = [textField text];
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound || ((text.length == 0 && [string intValue] >= 2) || (text.length == 1 && [text integerValue] == 1 && [string intValue] > 3)) || (text.length == 2 && ([text integerValue] > 12 || [text integerValue] == 0))) {
            return NO;
        }
        if (text.length == 1 && [text integerValue] > 1) {
            NSString *str = [NSString stringWithFormat:@"0%@", text];
            [textField setText:str];
            [self.yearTextField becomeFirstResponder];
            return NO;
        }
        if (text.length == 2) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
            if (self.yearTextField.text.length && ([self.yearTextField.text integerValue] ==  [components year]) && ([text integerValue] < [components month])) {
                self.monthTextField.text = @"";
                [self.view endEditing:YES];
                [Helper showAlertWithTitle:@"Message" Message:@"Invalid expiry month"];
                return  NO;
            }
            [textField setText:text];
            [self.yearTextField becomeFirstResponder];
            return  NO;
        } else if(text.length > 2) {
            [self.yearTextField becomeFirstResponder];
            return NO;
        }
        return YES;
    }  else if (textField==self.yearTextField) {
        __block NSString *text = [textField text];
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound || (text.length == 1 && [text integerValue] != 2) || (text.length == 2 && [text integerValue] != 20)) {
            return NO;
        }
        if (text.length >= 4) {
            if(text.length > 4) {
                [self.cvcTextField becomeFirstResponder];
                return NO;
            }
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
            if ([text integerValue] < [components year]) {
                self.yearTextField.text = @"";
                [self.view endEditing:YES];
                [Helper showAlertWithTitle:@"Message" Message:@"Invalid expiry year"];
                return  NO;
            } else if ([text integerValue] == [components year]){
                if (self.monthTextField.text.length && ([self.monthTextField.text integerValue] < [components month])) {
                    [textField setText:text];
                    self.monthTextField.text = @"";
                    [self.view endEditing:YES];
                    [Helper showAlertWithTitle:@"Message" Message:@"Invalid expiry month"];
                    return  NO;
                } else {
                    [textField setText:text];
                    [self.cvcTextField becomeFirstResponder];
                    return  NO;
                }
            } else {
                [textField setText:text];
                [self.cvcTextField becomeFirstResponder];
                return  NO;
            }
        }
        return YES;
    } else if (textField == self.cvcTextField) {
        __block NSString *text = [textField text];
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        if (text.length == 3) {
            [textField setText:text];
            [textField resignFirstResponder];
            return  NO;
        } else if(text.length > 3) {
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    } else {
        return YES;
    }
}

- (IBAction)tabgestureButtonClick:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)cardScanButtonAction:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.hideCardIOLogo=YES;
    scanViewController.disableManualEntryButtons = YES;
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.title = NSLocalizedString(@"Scan Card", @"Scan Card");
    scanViewController.navigationController.navigationBarHidden = NO;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    NSLog(@"User canceled payment info");
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    // The full card number is available as info.cardNumber, but don't log that!
    NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
    // Use the card info..
    self.cardNumberTextField.text=info.redactedCardNumber;
    self.monthTextField.text =[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth];
    self.yearTextField.text =[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear];
    self.cvcTextField.text =[NSString stringWithFormat:@"%@",info.cvv];
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - WebService call -

-(void)sendServiceAddCardsDetails:(NSString *)token {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable]) {
        NSDictionary *params = @{
                                 @"ent_sess_token":[Utility sessionToken],
                                 @"ent_dev_id":[Utility deviceID],
                                 @"ent_token":token,
                                 @"ent_date_time":[Helper getCurrentDateTime],
                                 };
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"addCard"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       if (success) {
                                           [self addCardDetails:response];
                                       }
                                   }];
    } else {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No Network", @"No Network")];
    }
}

-(void)addCardDetails:(NSDictionary *)response
{
    if (!response) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    } else if ([response objectForKey:@"Error"]) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    } else {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        NSDictionary *dictResponse= [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            _arrayContainingCardInfo = dictResponse[@"cards"];
            if (!_arrayContainingCardInfo || !_arrayContainingCardInfo.count){
            } else {
                [self addCardsInDataBase:_arrayContainingCardInfo];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
            }
            if(isComingFromVC == kMapViewController) {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                callback(YES);
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            } else if(isComingFromVC == kVerifyViewController) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            if(isComingFromVC == kMapViewController) {
                callback(NO);
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self dismissViewControllerAnimated:YES completion:nil];
            } else if(isComingFromVC == kVerifyViewController) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            } else {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

/**
 API call to get all added cards
 */
-(void)sendServiceToGetCardDetail {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getCardDetails:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
}

/**
 Get Cards API response

 @param response response data
 */

-(void)getCardDetails:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7)) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
            User *user = [[User alloc] init];
            [user deleteUserSavedData];
        }
        else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            [self addCardsInDataBase:dictResponse[@"cards"]];
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"amount"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            isSuccessed = NO;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        } else if([response[@"errFlag"] integerValue] == 1 && [response[@"errNum"] integerValue] == 51) {
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"amount"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            isSuccessed = NO;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        }
    }
}

#pragma mark - Database Methods -

/**
 Add cards in local DB

 @param newCards cards array from Get Cards API response
 */
-(void)addCardsInDataBase:(NSArray *)newCards
{
    NSString *defaultCardID;
    NSArray *arrDBResult = [[Database getCardDetails] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filterdArray.count)
    {
        CardDetails *card = filterdArray[0];
        defaultCardID = card.idCard;
    }
    [Database DeleteAllCard];
    [newCards enumerateObjectsUsingBlock:^(id dict, NSUInteger index, BOOL *stop) {
        NSMutableDictionary *cardDict = [dict mutableCopy];
        if ((defaultCardID.length && [cardDict[@"id"] isEqualToString:defaultCardID]) || (!defaultCardID.length && index == 0)) {
            [cardDict setObject:@"1" forKey:@"isDefault"];
        } else {
            [cardDict setObject:@"0" forKey:@"isDefault"];
        }
        [Database makeDataBaseEntry:cardDict];
        if (index >= newCards.count) {
            *stop = YES;
        }
    }];
}

+(BOOL)canReadCardWithCamera
{
    return YES;
}

@end
