//
//  CenterPickUpView.h
//  ZiRide
//
//  Created by 3Embed on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterPickUpView : UIView
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *setPickUpLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *setPickUpLocationBtn;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthOfBackgroundView;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger acceptOrRejects);
-(void)showPopUpOnView:(UIView *) backgroundView;

- (void)expand;
- (void)collapse;


@end
