//
//  CenterPickUpView.m
//  ZiRide
//
//  Created by 3Embed on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CenterPickUpView.h"

@implementation CenterPickUpView
@synthesize arrowImageView, setPickUpLocationLabel, timeLabel, activityIndicator;

-(void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        arrowImageView.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        arrowImageView.transform = CGAffineTransformMakeScale(1, 1);
    }
}

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"CenterPickUpView" owner:self options:nil] firstObject];
    return self;
}

-(void)showPopUpOnView:(UIView *)backgroundView {
    self.frame = CGRectMake(0, 0, backgroundView.frame.size.width, backgroundView.frame.size.height);
    [backgroundView addSubview:self];
    self.timeLabel.backgroundColor = UIColorFromRGB(0x019F6E);
    self.timeLabel.text = NSLocalizedString(@"No Drivers", @"No Drivers");
    [self layoutIfNeeded];
    [self addShadowTo:self.backgroundView];
}

-(void)hidePOPup {
    [self removeFromSuperview];
}

- (void) addShadowTo:(UIView *)view {
    [view.layer setBorderColor:[UIColor whiteColor].CGColor];
    [view.layer setBorderWidth:0.5f];
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.7];
    [view.layer setShadowRadius:5.0];
    [view.layer setShadowOffset:CGSizeMake(3.0, 2.0)];
}

- (void)expand {
    CGRect frame = self.frame;
    frame.origin.y = 0;
    frame.size.height = 75;
    [UIView animateWithDuration:0.3 animations:^{
        self.widthOfBackgroundView.constant = 270;
        self.setPickUpLocationLabel.alpha = 1;
        self.arrowImageView.alpha = 1;
        self.frame = frame;
        [self layoutIfNeeded];
    }];
}

- (void)collapse {
    CGRect frame = self.frame;
    frame.origin.y = -25;
    frame.size.height = 95;
    [UIView animateWithDuration:0.3 animations:^{
        self.widthOfBackgroundView.constant = 0;
        self.setPickUpLocationLabel.alpha = 0;
        self.arrowImageView.alpha = 0;
        self.frame = frame;
        [self layoutIfNeeded];
    }];
}


@end
