//
//  ChoosePaymentTypeTableViewCell.h
//  ZiRide
//
//  Created by 3Embed on 01/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePaymentTypeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *paymentTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioBtnImageView;
@property (weak, nonatomic) IBOutlet UIImageView *paymentIconImageView;
@end
