//
//  ChoosePaymentView.h
//  ZiRide
//
//  Created by 3Embed on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePaymentView : UIView <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *paymentTypeTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;

@property (nonatomic) NSInteger previousPaymentType;
@property (strong, nonatomic) NSIndexPath *selectedIndex;
@property (strong, nonatomic) NSMutableArray *paymentTypesArray;
@property (strong, nonatomic) NSMutableArray *paymentTypeCheckArray;

- (IBAction)gestureRecognizerAction:(id)sender;

-(void)showPopUpOnView:(UIView *)backgroundView;
-(void)hidePOPup;

@property (nonatomic, copy) void (^onCompletion)(NSInteger cashOrCard);

@end
