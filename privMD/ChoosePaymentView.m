//
//  ChoosePaymentView.m
//  ZiRide
//
//  Created by 3Embed on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ChoosePaymentView.h"
#import "ChoosePaymentTypeTableViewCell.h"
#import "AddMoneyOrCardTableViewCell.h"
#import "Database.h"
#import "CardDetails.h"
@implementation ChoosePaymentView
@synthesize paymentTypeTableView;
@synthesize contentViewBottomConstant, tableHeight;
@synthesize previousPaymentType, selectedIndex;
@synthesize paymentTypesArray, paymentTypeCheckArray;
@synthesize onCompletion;

-(void) awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
    } else {
    }
}

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ChoosePaymentView" owner:self options:nil] firstObject];
    UINib *choosePaymentCellNib = [UINib nibWithNibName:@"ChoosePaymentTypeTableViewCell" bundle:nil];
    [paymentTypeTableView registerNib:choosePaymentCellNib forCellReuseIdentifier:@"ChoosePaymentTypeTableViewCell"];
    UINib *addCellNib = [UINib nibWithNibName:@"AddMoneyOrCardTableViewCell" bundle:nil];
    [paymentTypeTableView registerNib:addCellNib forCellReuseIdentifier:@"AddMoneyOrCardTableViewCell"];
    return self;
}


/**
 Show Choose Payment Type Pop Up View

 @param window Application Window
 */
-(void)showPopUpOnView:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    contentViewBottomConstant.constant = - 150;
    NSString *cardBooking;
    if ([[Database getCardDetails] count]) {
        NSMutableArray *cards = [[Database getCardDetails] mutableCopy];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
        CardDetails *card;
        NSArray *filterdArray = [[cards filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            card = filterdArray[0];
        } else {
            card = cards[0];
        }
        NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
        cardBooking = [NSString stringWithFormat:@"%@ %@", @"XXXX", last4];

        NSString *wallet = [NSString stringWithFormat:@"%@          %@", @"ZiRide Money", [Utility walletBalance]];
        paymentTypesArray = [[NSMutableArray alloc] initWithObjects: wallet, @"CASH", cardBooking, @"", @"Add Debit/Credit Card", @"Add ZiRide Money to Wallet", nil];
        paymentTypeCheckArray = [[NSMutableArray alloc] initWithObjects: @"1", @"1", @"1", @"0", @"2", @"2", nil];
        switch (previousPaymentType) {
            case 0:
                selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
                break;
            case 1:
                selectedIndex = [NSIndexPath indexPathForRow:1 inSection:0];
                break;
            default:
                selectedIndex = [NSIndexPath indexPathForRow:2 inSection:0];
                break;
        }
        tableHeight.constant = 202;
    } else {
        NSString *wallet = [NSString stringWithFormat:@"%@          %@", @"ZiRide Money", [Utility walletBalance]];
        paymentTypesArray = [[NSMutableArray alloc] initWithObjects: wallet, @"CASH", @"", @"Add Debit/Credit Card", @"Add ZiRide Money to Wallet", nil];
        paymentTypeCheckArray = [[NSMutableArray alloc] initWithObjects: @"1", @"1", @"0", @"2", @"2", nil];
        switch (previousPaymentType) {
            case 0:
                selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
                break;
            case 1:
                selectedIndex = [NSIndexPath indexPathForRow:1 inSection:0];
                break;
            default:
                selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
                break;
        }
        tableHeight.constant = 162;
    }
    
    self.alpha = 0;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.4 animations:^{
        contentViewBottomConstant.constant = 0;
        self.alpha = 1;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

/**
 Hide Choose Payment Pop Up view
 */
-(void)hidePOPup {
    [UIView animateWithDuration:0.3 animations:^{
        contentViewBottomConstant.constant = - 150;
        self.alpha = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return paymentTypeCheckArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([paymentTypeCheckArray[indexPath.row] integerValue] == 0) {
        return 1;
    } else {
        return 40;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([paymentTypeCheckArray[indexPath.row] integerValue]) {
        case 0: {
            ChoosePaymentTypeTableViewCell *cell = (ChoosePaymentTypeTableViewCell *)[self.paymentTypeTableView dequeueReusableCellWithIdentifier:@"ChoosePaymentTypeTableViewCell"];
            [Helper setToLabel:cell.paymentTypeLabel Text:@"" WithFont:fontMedium FSize:13 Color:UIColorFromRGB(0x484848)];
            cell.paymentIconImageView.image = [UIImage new];
            cell.radioBtnImageView.image = [UIImage new];
            cell.backgroundColor = UIColorFromRGB(0xE4E7F0);
            [Helper addTopShadow:cell.backgroundView];
            return  cell;
        }
            break;
            
        case 1: {
            ChoosePaymentTypeTableViewCell *cell = (ChoosePaymentTypeTableViewCell *)[self.paymentTypeTableView dequeueReusableCellWithIdentifier:@"ChoosePaymentTypeTableViewCell"];
            [Helper setToLabel:cell.paymentTypeLabel Text:flStrForStr(paymentTypesArray[indexPath.row]) WithFont:fontMedium FSize:13 Color:UIColorFromRGB(0x484848)];
            switch (indexPath.row) {
                case 0:
                    cell.paymentIconImageView.image = [UIImage imageNamed:@"wallet_icon"];
                    break;
                case 1:
                    cell.paymentIconImageView.image = [UIImage imageNamed:@"cash_icon"];
                    break;
                case 2:
                    cell.paymentIconImageView.image = [UIImage imageNamed:@"card_icon"];
                    break;
                    
                default:
                    break;
            }
            if (indexPath.row == selectedIndex.row)
                cell.radioBtnImageView.image = [UIImage imageNamed:@"radioBtn_on"];
            else
                cell.radioBtnImageView.image = [UIImage imageNamed:@"radioBtn_off"];
            return  cell;
        }
            break;
            
        case 2: {
            AddMoneyOrCardTableViewCell *cell = (AddMoneyOrCardTableViewCell *)[self.paymentTypeTableView dequeueReusableCellWithIdentifier:@"AddMoneyOrCardTableViewCell"];
            [Helper setToLabel:cell.addLabel Text:flStrForStr(paymentTypesArray[indexPath.row]) WithFont:fontMedium FSize:13 Color:UIColorFromRGB(0x1E96EA)];
            if (indexPath.row == paymentTypeCheckArray.count - 2) {
                cell.addImageView.image = [UIImage imageNamed:@"addziridemoney"];
            } else {
                cell.addImageView.image = [UIImage imageNamed:@"addcard"];
            }
            return  cell;
        }
            break;
        default:
            break;
    }
    return  nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([paymentTypeCheckArray[indexPath.row] integerValue]) {
        case 0:
            break;
        case 1:
            selectedIndex = indexPath;
            [paymentTypeTableView reloadData];
            onCompletion(indexPath.row);
            [self performSelector:@selector(hidePOPup) withObject:nil afterDelay:0.1];
            break;
        case 2:
            onCompletion(3);
            [self performSelector:@selector(hidePOPup) withObject:nil afterDelay:0.1];
            break;
        default:
            break;
    }
}


#pragma mark UIGestureRecognizerDelegate methods

- (IBAction)gestureRecognizerAction:(id)sender {
    onCompletion(previousPaymentType);
    [self hidePOPup];
}



@end
