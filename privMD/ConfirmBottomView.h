//
//  ConfirmBottomView.h
//  ZiRide
//
//  Created by 3Embed on 18/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"

@interface ConfirmBottomView : UIView
@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UIImageView *paymentTypeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UIButton *choosePaymentTypeBtn;


@property (weak, nonatomic) IBOutlet UIView *bookingDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *rideEstimateBtn;
@property (weak, nonatomic) IBOutlet UIButton *promoCodeBtn;

@property (weak, nonatomic) IBOutlet RPButton *requestBookingBtn;


-(void)showPopUpOnView:(UIView *)backgroundView;
-(void)hidePOPup;


@end
