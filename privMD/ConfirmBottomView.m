//
//  ConfirmBottomView.m
//  ZiRide
//
//  Created by 3Embed on 18/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ConfirmBottomView.h"
#import "Database.h"
#import "CardDetails.h"

@implementation ConfirmBottomView
@synthesize paymentView, paymentTypeImageView, selectedImageView, choosePaymentTypeBtn;
@synthesize bookingDetailsView, rideEstimateBtn, promoCodeBtn;
@synthesize requestBookingBtn;

-(void) awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        promoCodeBtn.transform = CGAffineTransformMakeScale(-1, 1);
        paymentTypeImageView.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        promoCodeBtn.transform = CGAffineTransformMakeScale(1, 1);
        paymentTypeImageView.transform = CGAffineTransformMakeScale(1, 1);
    }
}

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ConfirmBottomView" owner:self options:nil] firstObject];
    return self;
}

-(void)showPopUpOnView:(UIView *)backgroundView {
    self.frame = CGRectMake(0, 0, backgroundView.frame.size.width, backgroundView.frame.size.height);
    [backgroundView addSubview:self];
    selectedImageView.image = [Helper imageWithImage:[UIImage imageNamed:@"radioBtn_on"] scaledToSize:CGSizeMake(25, 25)];
    [self layoutIfNeeded];
    [requestBookingBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x019F6E)] forState:UIControlStateNormal];
    [requestBookingBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0xC7C7C7)] forState:UIControlStateHighlighted];
    [requestBookingBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0xC7C7C7)] forState:UIControlStateSelected];
    requestBookingBtn.layer.cornerRadius = 3.0f;
    requestBookingBtn.layer.masksToBounds = YES;
    [Helper addBottomShadowOnButtonForBottomView: requestBookingBtn];
    NSArray *cards = [Database getCardDetails];
    if (cards.count) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
        CardDetails *card;
        NSArray *filterdArray = [[cards filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            card = filterdArray[0];
        } else {
            card = cards[0];
        }
        NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
        [Helper setButton:choosePaymentTypeBtn Text:[NSString stringWithFormat:@"%@ %@", @"XXXX", last4] WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
        UIImage *cardImage = [Helper setPlaceholderToCardType:card.cardtype];
        paymentTypeImageView.image = cardImage;
    } else {
        [Helper setButton:choosePaymentTypeBtn Text:NSLocalizedString(@"ZiRide Money", @"ZiRide Money") WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
        paymentTypeImageView.image = [UIImage imageNamed:@"wallet_icon"];
    }
    self.rideEstimateBtn.titleLabel.numberOfLines = 0;
    self.rideEstimateBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    NSString *fareEstimateStr1 = NSLocalizedString(@"Get Total Fare", @"Get Total Fare");
    NSString *fareEstimateStr2 = NSLocalizedString(@"Enter drop locatio", @"Enter drop locatio");
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *font1 = [UIFont fontWithName:fontMedium size:16];
    UIFont *font2 = [UIFont fontWithName:fontMedium  size:13];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSForegroundColorAttributeName:UIColorFromRGB(0x1E96EA),
                            NSParagraphStyleAttributeName:style}; // Added line
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font2,
                            NSForegroundColorAttributeName:UIColorFromRGB(0x484848),
                            NSParagraphStyleAttributeName:style}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",fareEstimateStr1] attributes:dict1]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:fareEstimateStr2 attributes:dict2]];
    [rideEstimateBtn setAttributedTitle:attString forState:UIControlStateNormal];
    [[rideEstimateBtn titleLabel] setNumberOfLines:0];
    [[rideEstimateBtn titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
}

-(void)hidePOPup {
    [self removeFromSuperview];
}

@end
