//
//  ConfirmOTP.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmOTP : UIViewController
@property NSString *phoneNumberEntered;
- (IBAction)clearOTPBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *textFleildsView;

@end
