
//
//  ConfirmOTP.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ConfirmOTP.h"
#import "SetNewPassword.h"

@interface ConfirmOTP ()<UITextFieldDelegate, CustomNavigationBarDelegate>
@property (strong, nonatomic) IBOutlet UILabel *enterOTPText;
@property (strong, nonatomic) IBOutlet UITextField *firstOtpField;
@property (strong, nonatomic) IBOutlet UITextField *secondOtpField;
@property (strong, nonatomic) IBOutlet UITextField *thirdOtpField;
@property (strong, nonatomic) IBOutlet UITextField *fourthOtpField;
@property (weak, nonatomic) IBOutlet UITextField *fifthOtpField;

@end

@implementation ConfirmOTP
@synthesize phoneNumberEntered;

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGRect frame = self.textFleildsView.frame;
    frame.size.width = 320;
    frame.origin.x = (screenWidth - 320)/2;
    self.textFleildsView.frame = frame;
    [_firstOtpField addTarget:self action:@selector(firstOtpAction:) forControlEvents:UIControlEventEditingChanged];
    [_secondOtpField addTarget:self action:@selector(secondOtpAction:) forControlEvents:UIControlEventEditingChanged];
    [_thirdOtpField addTarget:self action:@selector(thirdOtpAction:) forControlEvents:UIControlEventEditingChanged];
    [_fourthOtpField addTarget:self action:@selector(fourthOtpAction:) forControlEvents:UIControlEventEditingChanged];
    [_fifthOtpField addTarget:self action:@selector(fifthOtpAction:) forControlEvents:UIControlEventEditingChanged];
//    [self updateUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    if ([Helper isCurrentLanguageRTL]) {
        phoneNumberEntered = [NSString stringWithFormat:@"%@+", [phoneNumberEntered substringFromIndex:1]];
    }
    NSString *str = NSLocalizedString(@"Please enter the OTP Digits that we sent via Text Message to", @"Please enter the OTP Digits that we sent via Text Message to");
    _enterOTPText.text = [NSString stringWithFormat:@"%@ %@", str, phoneNumberEntered];
    [_firstOtpField becomeFirstResponder];
}

#pragma mark - Custom Methods -
//-(void) updateUI {
//    if ([Helper isCurrentLanguageRTL]) {
//        CGFloat screenWidth = 320;
//        CGRect frame = self.firstOtpField.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.firstOtpField.frame = frame;
//        
//        frame = self.secondOtpField.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.secondOtpField.frame = frame;
//        
//        frame = self.fourthOtpField.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.fourthOtpField.frame = frame;
//        
//        frame = self.fifthOtpField.frame;
//        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
//        self.fifthOtpField.frame = frame;
//    }
//}

#pragma mark TextFieldDelegate

- (IBAction)firstOtpAction:(id)sender {
    if([_firstOtpField.text length] >= 1) {
        [_secondOtpField becomeFirstResponder];
    }
}
- (IBAction)secondOtpAction:(id)sender {
    if([_secondOtpField.text length] >= 1) {
        [_thirdOtpField becomeFirstResponder];
    }
}

- (IBAction)thirdOtpAction:(id)sender {
    if([_thirdOtpField.text length] >= 1) {
        [_fourthOtpField becomeFirstResponder];
    }
}

- (IBAction)fourthOtpAction:(id)sender {
    if([_fourthOtpField.text length] >= 1) {
        [_fifthOtpField becomeFirstResponder];
    }
}

- (IBAction)fifthOtpAction:(id)sender {
    if([_fifthOtpField.text length] >= 1) {
        [_fifthOtpField resignFirstResponder];
    }
}

- (IBAction)resendOTPBtnAction:(id)sender {
    [self sendRequestForOTP:phoneNumberEntered];
}

- (IBAction)submitButtonClicked:(id)sender {
    if([_firstOtpField.text length] && [_secondOtpField.text length] && [_thirdOtpField.text length] && [_fourthOtpField.text length] && [_fifthOtpField.text length]) {
        [self verifyOTP];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Fill the OTP!", @"Please Fill the OTP!")];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if(textField.tag == 5) {
        [self submitButtonClicked:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.length >= 1 && range.length == 0) {
        return NO; 
    } else {
        return YES;
    }
}

#pragma mark Custom Methods

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Confirm OTP", @"Confirm OTP")];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation Left Button

 @param sender navigation left Button
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - GestureRecognizer Delegate -

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - UIButton Action - 

- (IBAction)clearOTPBtnAction:(id)sender {
    _firstOtpField.text = @"";
    _secondOtpField.text = @"";
    _thirdOtpField.text = @"";
    _fourthOtpField.text = @"";
    _fifthOtpField.text = @"";
    [_firstOtpField becomeFirstResponder];
}

#pragma mark - WebService -

/**
 Verify OTP with Phone Number
 */
-(void)verifyOTP {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Verifying OTP...", @"Verifying OTP...")];
    NSString *codeEntered = @"";
    if([_firstOtpField.text length] && [_secondOtpField.text length] && [_thirdOtpField.text length] && [_fourthOtpField.text length] && [_fifthOtpField.text length]) {
    codeEntered = [NSString stringWithFormat:@"%@%@%@%@%@",_firstOtpField.text,_secondOtpField.text,_thirdOtpField.text,_fourthOtpField.text,_fifthOtpField.text];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Fill the OTP!",@"Please Fill the OTP!")];
        return;
    }
    NSDictionary *params = @{
                            @"ent_phone":flStrForStr(self.phoneNumberEntered),
                            @"ent_code": flStrForStr(codeEntered),
                            @"ent_user_type": @"2",
                            KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]]
                            };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyPhone"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getResponseForVerifyOTP:response];
                               }];
}

/**
 Response for verify OTP

 @param response response Data
 */
-(void)getResponseForVerifyOTP:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else {
        if ([response[@"errFlag"] integerValue] == 0) {
            SetNewPassword *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SetNewPassword"];
            [self.view endEditing:YES];
            VC.phoneNumber = phoneNumberEntered;
            [Helper checkForPush:self.navigationController.view];
            [self.navigationController pushViewController:VC animated:NO];
        } else {
            self.firstOtpField.text = @"";
            self.secondOtpField.text = @"";
            self.thirdOtpField.text = @"";
            self.fourthOtpField.text = @"";
            self.fifthOtpField.text = @"";
            [self.firstOtpField becomeFirstResponder];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}


/**
 Send request for OTP

 @param phoneNumber etered Phone Number
 */
-(void)sendRequestForOTP:(NSString *)phoneNumber {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Sending OTP...", @"Sending OTP...")];
    NSDictionary *params = @{
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_user_type":@"2",
                             @"ent_mobile":flStrForStr(phoneNumber),
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ForgotPasswordWithOtp"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getResponseForOTP:response];
                               }];
}

/**
 Response from OTP

 @param response response Data
 */
-(void)getResponseForOTP:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else {
        if ([response[@"errFlag"] integerValue] == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}


@end
