//
//  TableViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryNameTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,UISearchDisplayDelegate>

@property(strong , nonatomic) NSArray *details;
@property (nonatomic,copy) void (^oncomplete)(NSString * code,UIImage *flagimg,NSString * countryCode);
- (IBAction)backButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *countrytableView;


@end
