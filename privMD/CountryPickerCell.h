//
//  CountryPickerCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageFlag;
@property (weak, nonatomic) IBOutlet UILabel *labelCountryName;
@property (weak, nonatomic) IBOutlet UILabel *labelCountryCode;

@end
