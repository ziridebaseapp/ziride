//
//  CountryPickerCell.m
//  Sup
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CountryPickerCell.h"

@implementation CountryPickerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame = self.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    [self subviews][0].frame = frame;
    
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width - 10;
        CGRect frame = self.imageFlag.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.imageFlag.frame = frame;
        
        frame = self.labelCountryName.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.labelCountryName.frame = frame;
        self.labelCountryName.textAlignment = NSTextAlignmentRight;
        
        frame = self.labelCountryCode.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width + 10;
        self.labelCountryCode.frame = frame;
        self.labelCountryCode.textAlignment = NSTextAlignmentRight;
    } else {
        self.labelCountryName.textAlignment = NSTextAlignmentLeft;
        self.labelCountryCode.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
