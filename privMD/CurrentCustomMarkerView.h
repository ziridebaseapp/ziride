//
//  CurrentCustomMarkerView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentCustomMarkerView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cashBookingBtn;
@property (weak, nonatomic) IBOutlet UIButton *cardBookingBtn;
@property (weak, nonatomic) IBOutlet UIImageView *currentCustomMarker;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *driverStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *crossImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashBookingBtnWidthConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardLeading;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger acceptOrRejects);
-(void)showPopUpOnView:(UIView *) backgroundView;

- (void)expand;
- (void)collapse;

@end
