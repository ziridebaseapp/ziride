//
//  CurrentCustomMarkerView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CurrentCustomMarkerView.h"
#import "Database.h"
#import "CardDetails.h"

@implementation CurrentCustomMarkerView

-(void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        _cardBookingBtn.transform = CGAffineTransformMakeScale(-1, 1);
        _cashBookingBtn.transform = CGAffineTransformMakeScale(-1, 1);
        _cardBookingBtn.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
        _cashBookingBtn.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        _cardBookingBtn.transform = CGAffineTransformMakeScale(1, 1);
        _cashBookingBtn.transform = CGAffineTransformMakeScale(1, 1);
        _cardBookingBtn.titleLabel.transform = CGAffineTransformMakeScale(1, 1);
        _cashBookingBtn.titleLabel.transform = CGAffineTransformMakeScale(1, 1);
    }
}
    
-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"CurrentCustomMarkerView" owner:self options:nil] firstObject];
    return self;
}


/**
 Show Current Custom marker Pop Up view

 @param backgroundView background View
 */
-(void)showPopUpOnView:(UIView *)backgroundView {
    [backgroundView addSubview:self];
    [self layoutIfNeeded];
    [Helper setButton:self.cashBookingBtn Text:NSLocalizedString(@"CASH BOOKING", @"CASH BOOKING") WithFont:fontNormal FSize:10 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    NSArray *cards = [Database getCardDetails];
    if (cards.count) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
        NSArray *filterdArray = [[cards filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            CardDetails *card = filterdArray[0];
            NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
            NSString *cardBooking = [NSString stringWithFormat:@"%@ **%@", NSLocalizedString(@"CARD", @"CARD"), last4];
            [Helper setButton:self.cardBookingBtn Text:cardBooking WithFont:fontNormal FSize:10 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        } else {
            CardDetails *card = cards[0];
            NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
            NSString *cardBooking = [NSString stringWithFormat:@"%@ **%@", NSLocalizedString(@"CARD", @"CARD"), last4];
            [Helper setButton:self.cardBookingBtn Text:cardBooking WithFont:fontNormal FSize:10 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        }
    } else {
        [Helper setButton:self.cardBookingBtn Text:NSLocalizedString(@"ADD CARD", @"ADD CARD") WithFont:fontNormal FSize:10 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    }
}


/**
 Hide pop up view
 */
-(void)hidePOPup {
    [self removeFromSuperview];
}


/**
 Expand View Animation
 */
- (void)expand {
    CGRect frame = self.frame;
    frame.origin.y = 0;
    frame.size.height = 70;
    [UIView animateWithDuration:0.3 animations:^{
        self.cashBookingBtnWidthConstant.constant = 110;
        self.cardLeading.constant = 0;
        self.cashTrailing.constant = 0;
        self.frame = frame;
        self.crossImage.hidden = YES;
        [self layoutIfNeeded];
    }];
}

/**
 Collapse View Animation
 */
- (void)collapse {
    CGRect frame = self.frame;
    frame.origin.y = -25;
    frame.size.height = 95;
    [UIView animateWithDuration:0.3 animations:^{
        self.cashBookingBtnWidthConstant.constant = 0;
        self.cardLeading.constant = -5;
        self.cashTrailing.constant = -5;
        self.frame = frame;
        self.crossImage.hidden = NO;
        [self layoutIfNeeded];
    }];
}

@end
