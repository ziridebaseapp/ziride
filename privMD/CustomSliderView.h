//
//  CustomSliderView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomSliderViewDelegate <NSObject>

-(void)sliderAction;

@end

@interface CustomSliderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *sliderImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderImageXConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *growingView;

@property(weak, nonatomic) id <CustomSliderViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *sliderTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingOfImage;
- (IBAction)panGestureAction:(UIPanGestureRecognizer *)sender;

@end
