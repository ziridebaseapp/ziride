//
//  CustomSliderView.m
//  StackOverFlow
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CustomSliderView.h"

@implementation CustomSliderView

/**
 Pan Gesture Action method

 @param sender pan Gesture Recognizer
 */
- (IBAction)panGestureAction:(UIPanGestureRecognizer *)sender {
    CGPoint location = [sender locationInView:[_sliderImage superview]];
    float locationX = location.x;
    float widthOfView = CGRectGetWidth([_sliderImage superview].frame);
    float maxLeading = widthOfView - 48;
    if ([Helper isCurrentLanguageRTL]) {
        if (locationX >= maxLeading) {
            self.leadingOfImage.constant = 0;
        } else if (locationX <= 0) {
            self.leadingOfImage.constant = maxLeading;
        } else {
            self.leadingOfImage.constant = widthOfView - locationX;
        }
    } else {
        if (locationX >= maxLeading) {
            self.leadingOfImage.constant = maxLeading;
        } else if (locationX <= 0) {
            self.leadingOfImage.constant = 0;
        } else {
            self.leadingOfImage.constant = locationX;
        }
    }
    if (sender.state == UIGestureRecognizerStateBegan) {
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        if (self.leadingOfImage.constant < widthOfView * 0.7) {
            self.leadingOfImage.constant = 0;
        } else {
            self.leadingOfImage.constant = maxLeading;
            if (_delegate && [_delegate respondsToSelector:@selector(sliderAction)]) {
                [_delegate sliderAction];
            }
        }
    } else if (sender.state == UIGestureRecognizerStateChanged) {
    } else {
    }
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self layoutIfNeeded];
                     }];
}
@end
