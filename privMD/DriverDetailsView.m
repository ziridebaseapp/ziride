//
//  DriverDetailsView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "DriverDetailsView.h"

@implementation DriverDetailsView
@synthesize driverDetailsBackgroundView;
@synthesize driverImageView, driverNameLabel;
@synthesize carTypeLabel, carColorView, carDetailsLabel;
@synthesize ratingView, ratingValueLabel;
@synthesize contactBtn, cancelBtn, trackBtn;
@synthesize driverStatusLabel;

-(void) awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        self.contactBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
        self.cancelBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
        self.trackBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
    } else {
        self.contactBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
        self.cancelBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
        self.trackBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
    }
}

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"DriverDetailsView" owner:self options:nil] firstObject];
    self.frame  = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 145);
    return self;
}


/**
 Show Driver Details Pop Up View

 @param backgroundView Background View where user want to Show
 */
-(void)showPopUpOnView:(UIView *)backgroundView {
    [backgroundView addSubview:self];
    [self addShadowToDriverDetailsView:driverDetailsBackgroundView];
    [self layoutIfNeeded];
    NSString *driverImageURL = [Utility driverProfileImageURL];
    if (!driverImageURL.length) {
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    } else {
        driverImageURL = [NSString stringWithFormat:@"%@", driverImageURL];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:driverImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      if (error || image == nil) {
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      }
                                  }];
    }
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    driverNameLabel.text = flStrForStr([Utility driverName]).uppercaseString;
    carTypeLabel.text = [NSString stringWithFormat:@"%@ | %@", flStrForStr([Utility vehicleMakelName]).uppercaseString, flStrForStr([Utility vehicleModelName]).uppercaseString];
    carColorView.layer.cornerRadius = carColorView.frame.size.width/2;
    carColorView.layer.masksToBounds = YES;
    carColorView.backgroundColor = UIColorFromRGB(0x019F6E);
    carDetailsLabel.text = [NSString stringWithFormat:@"%@ | %@", flStrForStr([Utility vehicleColor]).uppercaseString, flStrForStr([Utility vehicleNumberPlate]).uppercaseString];
    ratingValueLabel.text = [NSString stringWithFormat:@"%0.1f",[flStrForStr([Utility driverRating]) floatValue]];
    NSInteger bookingStatus = [Utility bookingStatus];
    if (bookingStatus == 6 || bookingStatus == 7) {
        cancelBtn.hidden = NO;
    } else {
        cancelBtn.hidden = YES;
    }
}


/**
 Hide Driver Details Pop Up
 */
-(void)hidePOPup {
    [self removeFromSuperview];
}


/**
 Add shadow to driver details view

 @param view any view
 */
- (void) addShadowToDriverDetailsView:(UIView *)view {
    [view.layer setCornerRadius:2.0f];
    [view.layer setBorderColor:UIColorFromRGB(0xE4E7F0).CGColor];
    [view.layer setBorderWidth:2.0f];
    [view.layer setShadowColor:UIColorFromRGB(0xE4E7F0).CGColor];
    [view.layer setShadowOpacity:1.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 3.0)];
}

@end
