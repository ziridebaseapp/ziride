//
//  EnterPhoneNumberViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryNameTableViewController.h"
#import "CountryPicker.h"
#import "ConfirmOTP.h"

@interface EnterPhoneNumberViewController : UIViewController
    @property (weak, nonatomic) IBOutlet UILabel *enterPhoneMessageLabel;
@property (strong, nonatomic) IBOutlet UIImageView *countryPickerImageView;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (weak, nonatomic) IBOutlet UILabel *phoneNoCountryCode;
- (IBAction)countryCode:(id)sender;

@end
