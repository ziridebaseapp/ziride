//
//  EnterPhoneNumberViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "EnterPhoneNumberViewController.h"

@interface EnterPhoneNumberViewController ()<UIGestureRecognizerDelegate, CustomNavigationBarDelegate> {
    NSString *countryCode;
}

@end

@implementation EnterPhoneNumberViewController
@synthesize phoneNoCountryCode;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(customTouchesBegan:)];
    [self.view addGestureRecognizer:tapGesture];
    [self giveCurrentCountryCode];
    [self updateUI];
    if ([Helper isCurrentLanguageRTL]) {
        self.enterPhoneMessageLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.enterPhoneMessageLabel.textAlignment = NSTextAlignmentLeft;
    }
}

-(void)viewDidAppear:(BOOL)animated {
    _phoneNumberTextField.text = @"";
    [_phoneNumberTextField becomeFirstResponder];
}

#pragma mark - Navigation Bar -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Forgot Password", @"Forgot Password")];
    [customNavigationBarView setLeftBarButtonTitle:@""];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setleftBarButtonImage:[UIImage imageNamed:@"payment_cancel_btn_on"] :[UIImage imageNamed:@"payment_cancel_btn_off"]];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation Left Button Action
 
 @param sender leftBarButton
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [Helper popLikeDismissVC:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - GestureRecognizer Delegate -

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:_countryPickerImageView]) {
        return NO;
    }
    return YES;
}

-(void)customTouchesBegan:(UITapGestureRecognizer*)sender {
    [self.view endEditing:YES];
}

-(void) updateUI {
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = self.view.frame.size.width;
        CGRect frame = self.countryPickerImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryPickerImageView.frame = frame;
        
        frame = self.phoneNoCountryCode.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.phoneNoCountryCode.frame = frame;
        
        frame = self.phoneNumberTextField.frame;
        frame.size.width = screenWidth - 45 - phoneNoCountryCode.frame.size.width - self.countryPickerImageView.frame.size.width;
        frame.origin.x = 18;
        self.phoneNumberTextField.frame = frame;
        
        frame = self.countryBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryBtn.frame = frame;
    }
}


#pragma mark - CountryPickerMethod and Default Locale -
/**
 Set a country flag and country code when view appears
 */
-(void)giveCurrentCountryCode {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCodee = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    countryCode = [dictionaryWithCode objectForKey:countryCodee];
    countryCode = [NSString stringWithFormat:@"+%@",countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCodee];
    self.countryPickerImageView.image = [UIImage imageNamed:imagePath];
    [self.countryPickerImageView setContentMode:UIViewContentModeScaleAspectFit];
    [Helper setToLabel:phoneNoCountryCode Text:countryCode WithFont:fontNormal FSize:15 Color:UIColorFromRGB(0x1D1D26)];
}


#pragma mark - UIButton Action - 

/**
 Country code or Country Flag Button

 @param sender countryCodeBtn
 */
- (IBAction)countryCode:(id)sender {
    [_phoneNumberTextField resignFirstResponder];
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString  *code, UIImage *flagimg, NSString *countryName) {
        self.countryPickerImageView.image = flagimg;
        [self.countryPickerImageView setContentMode:UIViewContentModeScaleAspectFit];
        countryCode = [NSString stringWithFormat:@"+%@", code];
        [Helper setToLabel:phoneNoCountryCode Text:countryCode WithFont:fontNormal FSize:15 Color:UIColorFromRGB(0x1D1D26)];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 Get OTP Button

 @param sender getOTPBtn
 */
- (IBAction)getOTPButton:(id)sender {
    [self customTouchesBegan:nil];
    if(![_phoneNumberTextField.text length]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Phone Number!", @"Enter Phone Number!")];
        [_phoneNumberTextField becomeFirstResponder];
        return;
    }
    NSString *phoneNumber = [NSString stringWithFormat:@"%@%@",countryCode, self.phoneNumberTextField.text];
    [self sendRequestForOTP:phoneNumber];
}

#pragma mark - WebService -

/**
 Sends request to get OTP for entered Number

 @param phoneNumber enteredNumber
 */
-(void)sendRequestForOTP:(NSString *)phoneNumber {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Sending OTP...", @"Sending OTP...")];
    NSDictionary *params = @{
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_user_type":@"2",
                             @"ent_mobile":flStrForStr(phoneNumber),
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ForgotPasswordWithOtp"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getResponseForOTP:response];
                               }];
}

/**
 Get OTP response

 @param response response data
 */
-(void)getResponseForOTP:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else {
        if ([response[@"errFlag"] integerValue] == 0) {
//            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            ConfirmOTP *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmOTP"];
            VC.phoneNumberEntered = [NSString stringWithFormat:@"%@%@",countryCode,_phoneNumberTextField.text];
            [Helper checkForPush:self.navigationController.view];
            [self.navigationController pushViewController:VC animated:NO];

        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}

@end
