//
//  Fonts.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Constant <NSObject>

#define fontBold           @"Hind-Bold"
#define fontNormal         @"Hind"
#define fontMedium         @"Hind-Medium"
#define fontSemiBold       @"Hind-SemiBold"



#define sizeSmallFont1                 10
#define sizeSmallFont2                 12
#define sizeMediumFont1                13
#define sizeMediumFont2                15
#define sizeLargeFont1                 18
#define sizeLargeFont2                 20

//For Labels
#define color1                      UIColorFromRGB(0x333333)
#define color2                      UIColorFromRGB(0x666666)

// For Button Title
#define color3                      UIColorFromRGB(0xFFFFFF)
#define color4                      UIColorFromRGB(0xE1E1E1)

//For Background
#define color5                      UIColorFromRGB(0x019F6E)
#define color6                      UIColorFromRGB(0x122D4A)

//For View, TableView, ScrollView Background
#define color7                      UIColorFromRGB(0xE5E5E5)


#define CLEAR_COLOR     [UIColor clearColor]
#define WHITE_COLOR     [UIColor whiteColor]
#define BLACK_COLOR     [UIColor blackColor]
#define GREEN_COLOR     [UIColor greenColor]

@end
