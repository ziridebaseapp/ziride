//
//  HelpViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"
@interface HelpViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UIButton *langaugeChangeBtn;
@property (weak, nonatomic) IBOutlet UIView *languageView;



- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
- (IBAction)langugaeChange:(id)sender;

@end
