//
//  HelpViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "UILabel+DynamicHeight.h"
#import <AVFoundation/AVFoundation.h>
#import "LocationServicesMessageVC.h"
#import "LanguageManager.h"
#import "Locale.h"
#import "UIImage+GIF.h"
#import "SplashViewController.h"
#import "LangaugeViewController.h"

static NSBundle *bundle = nil;
@interface HelpViewController () {
    NSMutableDictionary *userData;
    NSMutableArray *languageArray;
    BOOL isGoingToChangelang;
}

@end

@implementation HelpViewController
@synthesize registerButton, signInButton;

@synthesize languageView;
@synthesize langaugeChangeBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
    int screenHeight = self.view.frame.size.height;
    NSString *imageName = [NSString stringWithFormat:@"Default-%d", screenHeight];
    imageview.image = [UIImage imageNamed:imageName];
    [self.view addSubview:imageview];
    [self getLanguageCodesFromServer];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    if (userData) {
        userData = nil;
    }
    [self setLangaugeFromServiceResponse];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UIButton ACtion -

/**
 Sign Button Action Method

 @param sender signInBtn
 */
- (IBAction)signInButtonClicked:(id)sender {
    SignInViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignIn"];
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

/**
 Register Button Action Method
 
 @param sender registerBtn
 */
- (IBAction)registerButtonClicked:(id)sender {
    SignUpViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUp"];
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

/**
 Langauge change Button Action Method

 @param sender langaugeChangeBtn
 */

- (IBAction)langugaeChange:(id)sender {
    LangaugeViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LangaugeVC"];
    VC.languageArray = [languageArray mutableCopy];
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

#pragma  mark - Custom Method -

/**
 This makes spring animation for Sign In and Register Button
 */

- (void)animateIn {
    [self.view bringSubviewToFront:registerButton];
    [self.view bringSubviewToFront:signInButton];
    return;
//    CGRect frame1 = signInButton.frame;
//    CGRect frame2 = registerButton.frame;
//    frame1.origin.y += 50;
//    frame2.origin.y += 50;
//    
//    [UIView animateWithDuration:2 delay:0.1 usingSpringWithDamping:0.3 initialSpringVelocity:5 options:UIViewAnimationOptionLayoutSubviews animations:^{
//        signInButton.frame = frame1;
//        registerButton.frame = frame2;
//        [self.view bringSubviewToFront:self.languageView];
//    } completion:^(BOOL finished) {
//        
//    }];
}

/**
 Update UI to reload after langauge change
 */
-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    signInButton.frame = CGRectMake(10, screenHeight - 70, (screenWidth - 20)/2 - 10, 50);
    registerButton.frame = CGRectMake((screenWidth - 20)/2 + 20, screenHeight - 70, (screenWidth - 20)/2 - 10, 50);
    [Helper setButton:signInButton Text:NSLocalizedString(@"Log In", @"Log In") WithFont:fontMedium FSize:18 TitleColor:UIColorFromRGB(0x019F6E) ShadowColor:nil];
    [Helper setButton:registerButton Text:NSLocalizedString(@"Register", @"Register") WithFont:fontMedium FSize:18 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
    if ([Helper isCurrentLanguageRTL]) {
        [UIApplication sharedApplication].keyWindow.semanticContentAttribute = UIUserInterfaceLayoutDirectionRightToLeft;
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    } else {
        [UIApplication sharedApplication].keyWindow.semanticContentAttribute = UIUserInterfaceLayoutDirectionLeftToRight;
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
}

#pragma mark - Webservices -

-(void) getLanguageCodesFromServer {
    NSDictionary *params = @{
                             KDALoginDeviceType:@"1",
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getLangueages"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getLanguageCodesFromServerResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..." , @"Loading...")];
}

-(void)getLanguageCodesFromServerResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
        [self setDefaultLangaugeAsEnglsih];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
        } else {
            if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
            } else {
                languageArray = [response[@"languages"] mutableCopy];
                if ([Helper checkForUpdateAppVersion:response[@"versions"][@"iosCust"]]) {
                    return;
                }
                [self setLangaugeFromServiceResponse];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
            }
        }
    }
}

-(void)setDefaultLangaugeAsEnglsih {
    [self.langaugeChangeBtn setHidden:NO];
    [self.view bringSubviewToFront:languageView];
    [self.view bringSubviewToFront:langaugeChangeBtn];
    [Utility setSelectedLangaugeID:0];
    Locale *localeObj = [[Locale alloc] initWithLanguageCode:@"en" countryCode:@"" name:@""];
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    [languageManager setLanguageWithLocale:localeObj];
    [self updateUI];
    [self animateIn];
}


-(void)setLangaugeFromServiceResponse {
    if(languageArray.count) {
        if(languageArray.count == 1) {
            [self.langaugeChangeBtn setHidden:YES];
            [self.view sendSubviewToBack:languageView];
            [self.view sendSubviewToBack:langaugeChangeBtn];
        } else {
            [self.langaugeChangeBtn setHidden:NO];
            [self.view bringSubviewToFront:languageView];
            [self.view bringSubviewToFront:langaugeChangeBtn];
        }
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", [Utility selectedLangaugeID]];
        NSArray *selectedLangaugeArray = [languageArray filteredArrayUsingPredicate:predicate];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        NSString *langaugeCode;
        NSString *langBtnTitle;
        if (selectedLangaugeArray.count) {
            langBtnTitle = selectedLangaugeArray[0][@"name"];
            langaugeCode = selectedLangaugeArray[0][@"code"];
            [Utility setSelectedLangaugeID:[selectedLangaugeArray[0][@"id"] integerValue]];
        } else {
            [Utility setSelectedLangaugeID:[languageArray[0][@"id"] integerValue]];
            langBtnTitle = languageArray[0][@"name"];
            langaugeCode = languageArray[0][@"code"];
        }
        [langaugeChangeBtn setTitle:langBtnTitle forState:UIControlStateNormal];
        [langaugeChangeBtn setTitle:langBtnTitle forState:UIControlStateHighlighted];
        [langaugeChangeBtn setTitle:langBtnTitle forState:UIControlStateSelected];
        Locale *localeObj = [[Locale alloc] initWithLanguageCode:langaugeCode countryCode:@"" name:@""];
        [languageManager setLanguageWithLocale:localeObj];
        [self updateUI];
        [self animateIn];
    } else {
        [self.langaugeChangeBtn setHidden:YES];
        [self.view sendSubviewToBack:languageView];
        [self.view sendSubviewToBack:langaugeChangeBtn];
    }
}

#pragma mark - Location Service Custom Methods -

-(void)locationServicesChanged:(NSNotification*)notification {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [self gotoLocationServicesMessageViewController];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController {
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

    

@end
