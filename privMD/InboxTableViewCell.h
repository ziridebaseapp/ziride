//
//  InboxTableViewCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayOrTimeDifferenceLabel;

-(void) updateUIForCell:(NSDictionary *)dict;

@end
