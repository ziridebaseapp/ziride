//
//  InboxTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InboxTableViewCell.h"

@implementation InboxTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;

    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = screenWidth;
    [self subviews][0].frame = frame;
    
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.messageLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.messageLabel.frame = frame;
        
        frame = self.bookingIDLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.bookingIDLabel.frame = frame;

        frame = self.messageStatusLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.messageStatusLabel.frame = frame;
        
        frame = self.dayOrTimeDifferenceLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width + 10;
        self.dayOrTimeDifferenceLabel.frame = frame;
        self.dayOrTimeDifferenceLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.dayOrTimeDifferenceLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) updateUIForCell:(NSDictionary *)dict {
    [Helper setToLabel:self.messageLabel Text:[flStrForStr(dict[@"subject"]) capitalizedString] WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0x222328)];
    NSString *bid = [Helper extractNumberFromText:flStrForStr(dict[@"subject"])];
    NSString* bookingID = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Booking ID:", "Booking ID:"), bid];
    [Helper setToLabel:self.bookingIDLabel Text:bookingID WithFont:fontNormal FSize:10 Color:UIColorFromRGB(0xA1A6BB)];
    [Helper setToLabel:self.messageStatusLabel Text:[flStrForStr(dict[@"status"]) capitalizedString] WithFont:fontNormal FSize:10 Color:[UIColor whiteColor]];
    NSDate *date = [Helper getDateAndTimeFromString:flStrForStr(dict[@"created_at"])];
    self.dayOrTimeDifferenceLabel.text = [Helper relativeDateStringForDate:date];
    [Helper setToLabel:self.dayOrTimeDifferenceLabel Text:[Helper relativeDateStringForDate:date] WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0xA1A6BB)];
}
@end
