//
//  InboxViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InboxViewController.h"
#import "InboxTableViewCell.h"
#import "MessageDetailsViewController.h"
#import "SplashViewController.h"
#import "PubNubWrapper.h"
#import "EmptyCell.h"
#import "User.h"

@interface InboxViewController ()<CustomNavigationBarDelegate, CAAnimationDelegate> {
    NSMutableArray *messages;
}

@end

@implementation InboxViewController
@synthesize inboxTableView;

#pragma mark - UILife Cycle -  

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCustomerMessages];
    [self addCustomNavigationBar];
    messages = [[NSMutableArray alloc] init];
    [Helper setToLabel:self.headerLabel Text:NSLocalizedString(@"SUPPORT MESSAGE", @"SUPPORT MESSAGE") WithFont:fontBold FSize:10 Color:UIColorFromRGB(0x222328)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark- Custom Methods

/**
 Add Custom Navigation Bar
 */
- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Inbox", @"Inbox")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    [self menuButtonclicked];
}

- (void)menuButtonclicked {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


#pragma mark -
#pragma mark UITableView DataSource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (messages.count == 0) {
        return 1;
    }
    return messages.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (messages.count == 0) {
        self.headerLabel.hidden = YES;
        static NSString *CellIdentifier = @"emptyCell";
        EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.emptyStatuslabel.frame = CGRectMake(0, (cell.frame.size.height - 50)/2, cell.frame.size.width, 50);
        return cell;
    } else {
        self.headerLabel.hidden = NO;
        static NSString *cellIdentifier = @"InboxCell";
        InboxTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell==nil) {
            cell = (InboxTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.backgroundColor = [UIColor clearColor];
        }
        [cell updateUIForCell:messages[indexPath.section]];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)tableViewCell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (messages.count != 0) {
        InboxTableViewCell *cell = (InboxTableViewCell*)tableViewCell;
        if([[messages[indexPath.section][@"status"] capitalizedString] isEqualToString:@"New"]) {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0xFFD112);
        } else if([[messages[indexPath.section][@"status"] capitalizedString] isEqualToString:@"Open"]) {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0xEE4F4F);
        } else if([[messages[indexPath.section][@"status"] capitalizedString] isEqualToString:@"Pending"]) {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0x0DB5E8);
        } else if([[messages[indexPath.section][@"status"] capitalizedString] isEqualToString:@"Solved"]) {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0x3CB878);
        } else if([[messages[indexPath.section][@"status"] capitalizedString] isEqualToString:@"Closed"]) {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0x808080);
        } else {
            cell.messageStatusLabel.backgroundColor = UIColorFromRGB(0x3CB878);
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (messages.count == 0) {
        return self.view.frame.size.height-64;
    }
    return 70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 5;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (messages.count == 0) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessageDetailsViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageDetails"];
    NSDictionary *dict = [messages[indexPath.section] mutableCopy];
    VC.navigationTitle = [flStrForStr(dict[@"subject"]) capitalizedString];
    VC.ticketID = flStrForStr(dict[@"id"]);
    VC.status = [flStrForStr(dict[@"status"]) capitalizedString];
    VC.submitterID = flStrForStr(dict[@"submitter_id"]);
    VC.createdDateAndTime = [Helper getDateInStringForInvoice:flStrForStr(dict[@"created_at"])];
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}


#pragma mark - Webservices -

-(void) getCustomerMessages {
    NSDictionary *params = @{
                             @"ent_action":@"GetTickets",
                             @"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ZendeskOperation"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getCustomerMessagesResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..." , @"Loading...")];
}

-(void)getCustomerMessagesResponse:(NSDictionary *)responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:responseDict[@"errMsg"]];
        } else {
            messages = [responseDict[@"response"][@"results"] mutableCopy];
            [inboxTableView reloadData];
        }
    }
}

@end
