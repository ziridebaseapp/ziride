//
//  InviteViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *promoCodeMsgShowingLabel;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeLablel;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UIButton *smsButton;
@property (strong, nonatomic) IBOutlet UIButton *whatsappButton;
@property (strong, nonatomic) IBOutlet UILabel *shareyourcodeLabel;

- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)whatsappBtnClicked:(id)sender;
- (IBAction)smsButtonClicked:(id)sender;

@end
