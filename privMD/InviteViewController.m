//
//  InviteViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InviteViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import "SplashViewController.h"
#import "User.h"


@interface InviteViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate> {
    MFMailComposeViewController *mailer;
    NSString *shareText;
}

@end

@implementation InviteViewController

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    [self getRefferalDetails];
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Custom Methods -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Refer a Friend", @"Refer a Friend")];
    [self.view addSubview:customNavigationBarView];
}

/**
 NAvigation Left Button Action

 @param sender leftBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonclicked];
}

- (void)menuButtonclicked {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


#pragma mark - UIButton Action -

/**
 Facebook Button Action

 @param sender facebookBtn
 */
- (IBAction)facebookButtonClicked:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
        return;
    }
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent  alloc]init];
    [content setContentTitle: NSLocalizedString(@"PQ Referral", @"PQ Referral")];
    [content setContentURL:[NSURL URLWithString:websiteURL]];
    [content setContentDescription:shareText];
    [content setImageURL:[NSURL URLWithString:imgLinkForSharing]];
    FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeFeedWeb;
    dialog.fromViewController = self;
    [dialog setShareContent:content];
    [dialog show];
}

/**
 Twitter Button Action

 @param sender twitterBtn
 */
- (IBAction)twitterButtonClicked:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            [self showPostedSuccessMessage:result];
        }];
        [controller setInitialText:shareText];
        [controller addImage:[UIImage imageNamed:@"appIcon"]];
        [controller addURL:[NSURL URLWithString:websiteURL]];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else{
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"No Twitter Account!", @"No Twitter Account!") Message:NSLocalizedString(@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings",@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings")];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Settings",@"Settings"), NSLocalizedString(@"Cancel", @"Cancel"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
    }
}

/**
 SMS Button Action

 @param sender smsBtn
 */
- (IBAction)smsButtonClicked:(id)sender {
    if([MFMessageComposeViewController canSendText]){
        MFMessageComposeViewController *viewController = [[MFMessageComposeViewController alloc] init];
        viewController.messageComposeDelegate = self;
        viewController.body = shareText;
        [[viewController navigationBar] setTintColor: [UIColor blackColor]];
        viewController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        [self presentViewController:viewController animated:YES completion:NULL];
    }
    else{
        [Helper showAlertWithTitle:NSLocalizedString(@"Failure",@"Failure") Message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!")];
    }
}

/**
 Email Button Action

 @param sender emailBtn
 */
- (IBAction)whatsappBtnClicked:(id)sender {
    NSString *message = shareText;
    message = [message stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    message = [message stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    message = [message stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    message = [message stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    message = [message stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    message = [message stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    message = [message stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    NSString *urlWhats = [NSString stringWithFormat:@"%@%@",whatsAppSendTextUrl,message];
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:whatsAppUrl]]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
    } else {
        [Helper showAlertWithTitle:@"Message" Message:@"Your device has no WhatsApp installed."];
    }
}

#pragma mark - Helper methods

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed: {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error")  Message:NSLocalizedString(@"Failed to send !", @"Failed to send !")];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [[self.navigationController navigationBar] setTintColor: [UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
}

-(void)showPostedSuccessMessage:(NSInteger )result {
    switch (result) {
        case SLComposeViewControllerResultCancelled:
            break;
        case SLComposeViewControllerResultDone:
            [Helper showAlertWithTitle:NSLocalizedString(@"Success!",@"Success!") Message:NSLocalizedString(@"Posted successfully.",@"Posted successfully.")];
            break;
        default:
            break;
    }
}

#pragma mark - MFMailComposeViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
    [[self.navigationController navigationBar] setTintColor: [UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
}


#pragma mark - WebService call -

/**
 Get refferal details
 */
-(void)getRefferalDetails {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Verifying...", @"Verifying...")];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_dev_type": @"1",
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getReferralCode"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getRefferalDetailsResponse:response];
                             }
                         }];
}

/**
 Get referral details response

 @param response response data
 */
-(void)getRefferalDetailsResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([response[@"errFlag"] intValue] == 0) {
            [Helper addDotedBorder:_promoCodeLablel];
            if (![response[@"referralCode"] length]) {
                _promoCodeLablel.text = @"Not added for your city";
            } else {
                _promoCodeLablel.text = flStrForStr(response[@"referralCode"]);
            }
            _promoCodeMsgShowingLabel.text = flStrForStr(response[@"referralTitle"]);
            shareText = flStrForStr(response[@"referralBody"]);
            _shareyourcodeLabel.text = NSLocalizedString(@"SHARE YOUR CODE", @"SHARE YOUR CODE");
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}

@end
