//
//  InvoiceView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXRatingView.h"
//#import <AXRatingView/AXRatingView.h>

@class AXRatingView;

@interface InvoiceView : UIView <RatingChangedDelegate> {
    UIWindow *window;
    NSDictionary *invoiceData;
    NSInteger ratingValue;
    NSString *textWritenByUser;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *fristView;
@property (weak, nonatomic) IBOutlet UILabel *yourLastRideLabe;
@property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpBtn;
@property (weak, nonatomic) IBOutlet UILabel *line1Label;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *redDotLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *verticalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *horizontalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenDotLabel;

@property (weak, nonatomic) IBOutlet UIView *thirdView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;

@property (weak, nonatomic) IBOutlet UIButton *receiptBtn;
@property (weak, nonatomic) IBOutlet UIView *forthView;
@property (weak, nonatomic) IBOutlet UILabel *rateYourDriverLabel;
@property (weak, nonatomic) IBOutlet UILabel *unpleasantLabel;
@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *delightfulLabel;

- (IBAction)needHelpBtnAction:(id)sender;
- (IBAction)receiptBtnAction:(id)sender;


@property (strong, nonatomic) NSString *bookingID;
@property (nonatomic) BOOL isComingFromBookingHistory;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict;

@end
