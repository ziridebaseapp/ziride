//
//  InvoiceView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InvoiceView.h"
#import "ReceiptView.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "AXRatingView.h"
#import "NegativeRatingView.h"
#import "PostiveRatingView.h"

@implementation InvoiceView 
@synthesize bookingID;

@synthesize yourLastRideLabe, bookingIDLabel, needHelpBtn;
@synthesize pickUpWithDateLabel, pickUpAddressLabel, dropOffWithDateLabel, dropOffAddressLabel;
@synthesize greenDotLabel, redDotLabel, verticalLineLabel, horizontalLineLabel;
@synthesize driverImageView, driverNameLabel, totalAmountLabel, receiptBtn;
@synthesize rateYourDriverLabel, ratingView;
@synthesize isComingFromBookingHistory;


- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] firstObject];
    return self;
}

#pragma mark - Show And Hide Pop Up Methods - 

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict {
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    greenDotLabel.layer.cornerRadius = greenDotLabel.frame.size.width/2;
    greenDotLabel.layer.masksToBounds = YES;
    redDotLabel.layer.cornerRadius = redDotLabel.frame.size.width/2;
    redDotLabel.layer.masksToBounds = YES;
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    needHelpBtn.layer.cornerRadius = 4.0f;
    needHelpBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    needHelpBtn.layer.borderWidth = 1.0f;
    receiptBtn.layer.cornerRadius = 4.0f;
    receiptBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    receiptBtn.layer.borderWidth = 1.0f;
    [self setRatingViewInInvoice];
    if (isComingFromBookingHistory) {
        [self layoutIfNeeded];
    } else {
        [self layoutIfNeeded];
    }
    if (dict == nil) {
        [self sendRequestForAppointmentInvoice];
    } else if ([[dict allKeys] containsObject:@"Invoice"]) {
        NSDictionary *dict1 = dict[@"Invoice"];
        NSMutableDictionary *dict2 = [dict mutableCopy];
        [dict2 removeObjectForKey:@"Invoice"];
        [dict2 addEntriesFromDictionary:dict1];
        invoiceData = [dict2 mutableCopy];
        [self updateUI];
    } else {
        invoiceData = [dict mutableCopy];
        [self updateUI];
    }
    NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(invoiceData[@"walletbal"]) floatValue]];
    [Utility setWalletBalance:currentWalletAmount];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];

    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}


#pragma mark - UIButton Action - 

- (IBAction)receiptBtnAction:(id)sender {
    ReceiptView* receiptView = [[ReceiptView alloc]init];
    [receiptView showPopUpWithDetailedDict:invoiceData Onwindow:window];
    receiptView.onCompletion = ^(NSInteger closeClicked) {
        [self setHidden:NO];
    };
    [self setHidden:YES];
}


- (IBAction)needHelpBtnAction:(id)sender {
    NegativeRatingView* negativeRatingView = [[NegativeRatingView alloc] init];
    negativeRatingView.ratingValue = ratingValue;
    negativeRatingView.isComingFromNeedHelp = 1;
    [negativeRatingView showPopUpWithDetailedDict:invoiceData Onwindow:window];
    negativeRatingView.onCompletion = ^(NSInteger success) {
        if(success == 2)
            [self removeFromSuperview];
        else
            [self setHidden:NO];
    };
    [self setHidden:YES];
    
}

#pragma mark - Custom Methods -

-(void)updateUI {
    yourLastRideLabe.text = NSLocalizedString(@"Your Details", @"Your Details");
    bookingIDLabel.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"BOOKING ID : ", @"BOOKING ID : "), flStrForStr(invoiceData[@"bid"])];
    
    NSString *pickUp = NSLocalizedString(@"PICK UP", @"PICK UP");
    NSString *pTime = [self getTime:flStrForObj(invoiceData[@"pickupDt"])];
    pickUpWithDateLabel.text = [NSString stringWithFormat:@"%@ %@", pickUp,pTime];
    pickUpAddressLabel.text = flStrForObj(invoiceData[@"addr1"]);
    
    NSString *dropOff = NSLocalizedString(@"DROP OFF", @"DROP OFF");
    NSString *dTime = [self getTime:flStrForObj(invoiceData[@"dropDt"])];
    dropOffWithDateLabel.text = [NSString stringWithFormat:@"%@ %@", dropOff,dTime];
    dropOffAddressLabel.text = flStrForObj(invoiceData[@"dropAddr1"]);
    
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    NSString *strImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (strImageURL.length) {
        strImageURL = [NSString stringWithFormat:@"%@",invoiceData[@"pPic"]];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      if (error || !image)
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      else
                                          driverImageView.image = image;
                                  }];
    } else
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    driverNameLabel.text = [[NSString stringWithFormat:@"%@ %@",flStrForObj(invoiceData[@"fName"]),flStrForObj(invoiceData[@"lName"])] capitalizedString];
    NSString *totalAmount = @"";
    if ([invoiceData[@"payType"] integerValue] == 1) {
        totalAmount =  [Helper getCurrencyLocal:[flStrForObj(invoiceData[@"cardDeduct"]) floatValue]];
    } else if ([invoiceData[@"payType"] integerValue] == 2) {
        totalAmount =  [Helper getCurrencyLocal:[flStrForObj(invoiceData[@"CashCollected"]) floatValue]];
    }
    totalAmountLabel.text = totalAmount;
    
    if (isComingFromBookingHistory) {
        //        self.needHelpBtn.hidden = YES;
        [Helper setToLabel:rateYourDriverLabel Text:NSLocalizedString(@"DRIVER RATING", @"DRIVER RATING") WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x333333)];
        ratingView.value = [invoiceData[@"r"] floatValue];
        ratingView.userInteractionEnabled = NO;
        [self layoutIfNeeded];
    } else {
        ratingView.value = 0.0;
        ratingView.stepInterval = 1.0;
        ratingView.userInteractionEnabled = YES;
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isComingFromNegativeRating"]) {
        ratingValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"ratingValue"];
        if (!ratingValue) {
        }else if(ratingValue < 3) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isComingFromNegativeRating"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ratingValue"];
            NegativeRatingView* negativeRatingView = [[NegativeRatingView alloc] init];
            negativeRatingView.ratingValue = ratingValue;
            [negativeRatingView showPopUpWithDetailedDict:invoiceData Onwindow:window];
            negativeRatingView.onCompletion = ^(NSInteger success) {
                if(success == 2)
                    [self removeFromSuperview];
                else
                    [self setHidden:NO];
            };
            [self setHidden:YES];
        }
    }
}

-(void) setRatingViewInInvoice {
    ratingView.delegate = self;
    ratingView.stepInterval = 1.0;
    ratingView.markFont = [UIFont systemFontOfSize:28];
    ratingView.baseColor = UIColorFromRGB(0xDEDEDE);
    ratingView.highlightColor = UIColorFromRGB(0x019F6E);
}


-(void)ratingChanged {
    ratingValue = ratingView.value;
    if (!ratingValue) {
    } else {
        PostiveRatingView* positiveRatingView = [[PostiveRatingView alloc] init];
        positiveRatingView.ratingValue = ratingValue;
        [positiveRatingView showPopUpWithDetailedDict:invoiceData];
        positiveRatingView.onCompletion = ^(NSInteger success) {
            if (success == 1) {
                [self removeFromSuperview];
            } else {
                [self setHidden:NO];
            }
            ratingView.value = 0;
            ratingValue = 0;
        };
        [self setHidden:YES];
    }
//    else if(ratingValue < 3) {
//        NegativeRatingView* negativeRatingView = [[NegativeRatingView alloc] init];
//        negativeRatingView.ratingValue = ratingValue;
//        negativeRatingView.isComingFromNeedHelp = 0;
//        [negativeRatingView showPopUpWithDetailedDict:invoiceData Onwindow:window];
//        negativeRatingView.onCompletion = ^(NSInteger success) {
//            if(success == 2)
//                [self removeFromSuperview];
//            else
//                [self setHidden:NO];
//            ratingView.value = 0;
//            ratingValue = 0;
//        };
//        [self setHidden:YES];
//    }
}

-(NSString*)getTime:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

-(NSString*)getDateAndTime:(NSString *)aDatefromServer{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    //    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
    //    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}


#pragma mark - WebServiceCall -

-(void)sendRequestForAppointmentInvoice {
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
    @try
    {
        NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                                 @"ent_dev_id":[Utility deviceID],
                                 @"ent_booking_id":[Utility bookingID],
                                 @"ent_user_type":@"2",
                                 @"ent_date_time":[Helper getCurrentNetworkDateTime],
                                 };
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       if (success)
                                           [self parseAppointmentDetailResponse:response];
                                       else
                                           [self  hidePOPup];
                                   }];
    }
    @catch (NSException *exception)
    {
    }
}

-(void)parseAppointmentDetailResponse:(NSDictionary*)response {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        [self  hidePOPup];
        return;
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
         [self  hidePOPup];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
            invoiceData = [response[@"Invoice"] mutableCopy];
            [self updateUI];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"errMsg", @"errMsg")];
            [self hidePOPup];
        }
    }
}

-(void)sendRequestForReviewSubmit{
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    NSString *review;
    if (textWritenByUser)
        review = flStrForStr(textWritenByUser);
    else
        review = @"";
    
    NSString *rateValue = [NSString stringWithFormat:@"%ld",(long)ratingValue];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mid": [Utility driverID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             @"ent_rating_num":flStrForStr(rateValue),
                             @"ent_review_msg":review,
                             @"ent_promo":flStrForStr([Utility promoCode])
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseSubmitReviewResponse:response];
                                   else
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil){
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self hidePOPup];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
            [Utility setPromoCode:flStrForStr(response[@"isPromoValid"])];
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"walletAmt"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            [self hidePOPup];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error")  Message:[response objectForKey:@"errMsg"]];
        }
    }
}



@end
