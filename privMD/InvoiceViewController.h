//
//  InvoiceViewController.h
//  ZiRide
//
//  Created by 3Embed on 13/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"

@interface InvoiceViewController : UIViewController <CustomNavigationBarDelegate, RatingChangedDelegate> {
    NSDictionary *invoiceData;
    NSInteger ratingValue;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *driverDetailsView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverCarLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceAndTimeLabel;

@property (weak, nonatomic) IBOutlet UIView *zigzagView;
@property (weak, nonatomic) IBOutlet RPButton *receiptBtn;


@property (weak, nonatomic) IBOutlet UIView *ratingBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *rateYourDriverLabel;
@property (weak, nonatomic) IBOutlet UIView *rateView;

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

@property (weak, nonatomic) IBOutlet UITableView *helpTableView;

@property (weak, nonatomic) IBOutlet UIView *positiveRatingView;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn1;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn2;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn3;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn4;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn5;
@property (weak, nonatomic) IBOutlet RPButton *reasonBtn6;

@property (weak, nonatomic) IBOutlet UITextView *reasonTextView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet RPButton *submitBtn;

- (IBAction)receiptBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;

@end
