//
//  InvoiceViewController.m
//  ZiRide
//
//  Created by 3Embed on 13/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InvoiceViewController.h"
#import "AXRatingView.h"
#import "PostiveRatingView.h"

@class AXRatingView;

@interface InvoiceViewController ()
@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;

@end

@implementation InvoiceViewController
@synthesize ratingView;
@synthesize rateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    [self setRatingViewInInvoice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Custom Methods -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    NSString *str = NSLocalizedString(@"Your last trip on", @"Your last trip on");
    NSString *date = [Helper getDateInString:[Helper getCurrentDateTime]];
    [customNavigationBarView setTitle:[NSString stringWithFormat:@"%@\n%@",str, date] withColor:UIColorFromRGB(0xFFFFFF)];
    [self.view addSubview:customNavigationBarView];
}

/**
 NAvigation Left Button Action
 
 @param sender leftBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonclicked];
}

- (void)menuButtonclicked {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


-(void) setRatingViewInInvoice {
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    ratingView = [[AXRatingView alloc] initWithFrame:CGRectMake(0,0, 50*5, 50)];
    ratingView.delegate = self;
    ratingView.stepInterval = 1.0;
    ratingView.value = 5.0f;
    ratingView.markFont = [UIFont systemFontOfSize:50];
    ratingView.baseColor = UIColorFromRGB(0xDEDEDE);
    ratingView.highlightColor = UIColorFromRGB(0xFFC400);
    rateView.frame = CGRectMake((screenWidth - (50*5))/2, 30 + (80 - 50)/2, 50*5, 50);
    [rateView addSubview:ratingView];
    [rateView bringSubviewToFront:ratingView];
}


-(void)ratingChanged {
    ratingValue = ratingView.value;
}
//    if (!ratingValue) {
//    } else {
//        PostiveRatingView* positiveRatingView = [[PostiveRatingView alloc] init];
//        positiveRatingView.ratingValue = ratingValue;
//        [positiveRatingView showPopUpWithDetailedDict:invoiceData];
//        positiveRatingView.onCompletion = ^(NSInteger success) {
//            if (success == 1) {
//                [self removeFromSuperview];
//            } else {
//                [self setHidden:NO];
//            }
//            ratingView.value = 0;
//            ratingValue = 0;
//        };
//        [self setHidden:YES];
//    }
//        else if(ratingValue < 3) {
//            NegativeRatingView* negativeRatingView = [[NegativeRatingView alloc] init];
//            negativeRatingView.ratingValue = ratingValue;
//            negativeRatingView.isComingFromNeedHelp = 0;
//            [negativeRatingView showPopUpWithDetailedDict:invoiceData Onwindow:window];
//            negativeRatingView.onCompletion = ^(NSInteger success) {
//                if(success == 2)
//                    [self removeFromSuperview];
//                else
//                    [self setHidden:NO];
//                ratingView.value = 0;
//                ratingValue = 0;
//            };
//            [self setHidden:YES];
//        }
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)receiptBtnAction:(id)sender {
}

- (IBAction)submitBtnAction:(id)sender {
}
@end
