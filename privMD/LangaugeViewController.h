//
//  LangaugeViewController.h
//  PQDev
//
//  Created by 3Embed on 13/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomNavigationBar.h"

@interface LangaugeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CustomNavigationBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *languageTableView;

@property (strong, nonatomic) NSArray *languageArray;

@end
