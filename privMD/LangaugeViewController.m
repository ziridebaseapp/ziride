//
//  LangaugeViewController.m
//  PQDev
//
//  Created by 3Embed on 13/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LangaugeViewController.h"
#import "LanguageManager.h"
#import "Locale.h"


@interface LangaugeViewController () {
    NSIndexPath *selectedIndexPath;
}
@end

@implementation LangaugeViewController
@synthesize languageArray;
@synthesize languageTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    NSInteger selectedLangID = [Utility selectedLangaugeID];
    [self.languageArray enumerateObjectsUsingBlock:^(id dict, NSUInteger index, BOOL *stop) {
        if (selectedLangID == [dict[@"id"] integerValue]) {
            selectedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.languageTableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            [self tableView:self.languageTableView didSelectRowAtIndexPath:selectedIndexPath];
            *stop = YES;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Select Langauge", @"Select Langauge")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [customNavigationBarView addRightButton:@""];
    [customNavigationBarView setRightBarButtonTitle:NSLocalizedString(@"Done", @"Done")];
    [self.view addSubview:customNavigationBarView];
}

/**
 Left navigation button action method
 */
-(void)leftBarButtonClicked:(UIButton *)sender{
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

/**
 Right navigation button action method
 */
-(void)rightBarButtonClicked:(UIButton *)sender {
    [Utility setSelectedLangaugeID:[languageArray[selectedIndexPath.row][@"id"] integerValue]];
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    NSString *langaugeCode = languageArray[selectedIndexPath.row][@"code"];
    Locale *localeObj = [[Locale alloc] initWithLanguageCode:langaugeCode countryCode:@"" name:@""];
    [languageManager setLanguageWithLocale:localeObj];
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return languageArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath; {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = languageArray[indexPath.row][@"name"];
    if ([Helper isCurrentLanguageRTL]) {
        if ([languageArray[indexPath.row][@"dir"] isEqualToString:@"rtl"]) {
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        } else if (indexPath.row == 1) {
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        }
    } else {
        if ([languageArray[indexPath.row][@"dir"] isEqualToString:@"rtl"]) {
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        } else if (indexPath.row == 1) {
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
    }

    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"radioBtn_off"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"radioBtn_on"]];
    selectedIndexPath = indexPath;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"radioBtn_off"]];
}

@end
