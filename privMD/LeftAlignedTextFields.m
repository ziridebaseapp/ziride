//
//  LeftAlignedTextFields.m
//  PQTest
//
//  Created by Rahul Patil on 20/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LeftAlignedTextFields.h"

@implementation LeftAlignedTextFields

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        self.textAlignment = NSTextAlignmentRight;
    }else {
        self.textAlignment = NSTextAlignmentLeft;
    }
}
@end
