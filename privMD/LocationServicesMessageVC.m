//
//  LocationServicesMessageVC.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LocationServicesMessageVC.h"

@interface LocationServicesMessageVC ()
@end

@implementation LocationServicesMessageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Location", @"Location");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
