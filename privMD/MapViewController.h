//
//  MapViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "WildcardGestureRecognizer.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "MyAppTimerClass.h"
#import "ProviderRequestingView.h"
#import "User.h"
#import "PubNubWrapper.h"
#import "AppDelegate.h"

@interface MapViewController : UIViewController<GMSMapViewDelegate,  UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIScrollViewDelegate, PubNubWrapperDelegate, CLLocationManagerDelegate, CustomNavigationBarDelegate, UserDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate> {
    BOOL isLaterSelected;
    BOOL isNowSelected;
    BOOL isCustomMarkerSelected;
    BOOL isFareButtonClicked;
    BOOL isRequestingButtonClicked;
    
    AppDelegate* appDelegate;
    NSMutableArray* arrDBResult;
	NSManagedObjectContext* context;    
    MFMailComposeViewController* mailer;
    
    UIDatePicker* datePicker;
    NSInteger selectedRowForTip;
}

@property(nonatomic,assign) BOOL isSelectinLocation;
@property(nonatomic,strong) UIPickerView *pkrView;
@property(nonatomic,strong) UIPickerView *pkrViewForAddTip;
@property(strong, nonatomic) NSMutableArray *drivers;
@property(strong, nonatomic) UITextField *searchAddressTextField;
@property(strong, nonatomic) UITextField *pickUpAddressTextField;
@property(strong, nonatomic) UITextField *dropOffAddressTextField;
@property(strong, nonatomic) ProviderRequestingView *requestingView;
@property(nonatomic)NSInteger carTypesForLiveBooking;
@property(nonatomic)NSInteger carTypesForLiveBookingServer;
@property(nonatomic) BOOL isComingFromSignUp;


+(instancetype) getSharedInstance;

-(void)publishPubNubStream;
-(void)hideAcitvityIndicator;
-(void)sendRequestgetETAnDistance;
-(void)publishA3ToGetBookingData;

@property(nonatomic,strong) UIView* addProgressBarView;

-(IBAction)locationButtonClicked:(UIButton *)sender;
-(void)sendAServiceForFareCalculator;
-(void)sendAppointmentRequestForLiveBookingCancellation;
-(void) handleMessageA2ComesHaere:(NSDictionary *)messageDict;


@end
