//  MapViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "MapViewController.h"
#import "PickUpViewController.h"
#import "XDKAirMenuController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import "PaymentViewController.h"
#import "CustomNavigationBar.h"
#import "NetworkHandler.h"
#import "UploadProgress.h"
#import "LocationServicesMessageVC.h"
#import "CardDetails.h"
#import "Database.h"
#import "AXRatingView.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "MyAppTimerClass.h"
#import "SplashViewController.h"
#import "surgePricePopUp.h"
#import "SaveLocationViewController.h"
#import "CancelledReasonView.h"
#import "DirectionService.h"
#import "PromoCodeView.h"
#import "DriverDetailsView.h"
#import "WebViewController.h"
#import "CustomSliderView.h"
#import "SourceAddress.h"
#import <Google/Analytics.h>
#import "CenterPickUpView.h"
#import "ConfirmBottomView.h"
#import "ChoosePaymentView.h"
#import "RateCard.h"
#import "CardLoginViewController.h"
#import "Utility.h"

//Define Constants tag for my view
//#define myProgressTag  5001

#define mapZoomLevel 16
#define pickupAddressTag 2500
#define addProgressViewTag 6000
#define carType1TAG 7000
#define carType2TAG 7001
#define carType3TAG 7002
#define carType4TAG 7003
#define msgLabelTag 7004

#define nowBookingBtnTag 2050
#define laterBookingBtnTag 2051
#define bookingOptionViewTag 2052   // Now and Later button
#define confirmationBottomViewTag 499

#define currentPickUpMarker 5001

#define driverArrivedViewTag 3000
#define driverMessageViewTag 3001
#define _height  110
#define _heightCenter  160
#define _width   274
#define _widthLabel   216

#define bottomViewWithCarTag 106
#define customLocationViewTag 90

#define searchAddressViewTag 10690

#define pickerViewTag 9001 

#define loctionButtonsTag 190

static MapViewController *sharedInstance = nil;

typedef enum {
    isChanging = 0,
    isFixed = 1
} locationchange;

@interface CoordsList : NSObject
@property(nonatomic, readonly, copy) GMSPath *path;
@property(nonatomic, readonly) NSUInteger target;

- (id)initWithPath:(GMSPath *)path;
- (CLLocationCoordinate2D)next;

@end

@implementation CoordsList

- (id)initWithPath:(GMSPath *)path {
    if ((self = [super init])) {
        _path = [path copy];
        _target = 0;
    }
    return self;
}

- (CLLocationCoordinate2D)next {
    ++_target;
    if (_target == [_path count]) {
        _target = 0;
    }
    return [_path coordinateAtIndex:_target];
}
@end


@interface MapViewController () {
    
    GMSMapView* mapView_;
    GMSGeocoder* geocoder_;
    PubNubWrapper* pubNub;
    
    float srcLat;
    float srcLong;
    NSString* srcAddr;
    NSString* srcAddrline2;
    
    float desLat;
    float desLong;
    NSString* desAddr;
    NSString* desAddrline2;

    NSInteger paymentTypesForLiveBooking;
    NSInteger maximumETA;
    
    
    BOOL isVehicleBtnClickedForRateCard;
    
    NSString* distanceOfClosetCar;
    
    NSString* shareUrlViaSMS;
    
    NSString* driverNotes;
    NSString* promocode;
    
    NSMutableArray* arrayOfImages;
    NSMutableArray* arrayOfButtons;
    NSMutableArray* arrayOfMapIcons;
    NSMutableArray* arrayOfMapImages;
    NSMutableArray* allHorizontalData;
    NSMutableArray* arrayOfVehicleIcons;


    UIScrollView* scroller;
    UIView *shadowViewForScroller;
    
    double surgePrice;
    NSString* surgePriceTime;
    
    UIBackgroundTaskIdentifier bgTask;
    
    BOOL isComingTocheckCarTypes;
    
    CGPoint oldOffset;
    
    BOOL isUpdatingDropOff;
    
    NSInteger selectedPaymentType;
    NSString *currencyStr;
    double walletAmount;
    
    BOOL isBookingSend;
    BOOL isBookingButtonClicked;
    NSInteger isInBooking;
    NSInteger expiryPerDriver;
    NSInteger expiryForBooking;
    NSInteger timeElapsedForBooking;
    NSInteger remainingTimeForBooking;
    NSString* bookingSentDriverEmail;

    NSString* zipCodeOnMap;
    NSInteger locationTypeForFavourite;
    BOOL isChangeLocationChangingInBooking;

    
    //Path Plotting Variables
    NSMutableArray* waypoints_;
    NSMutableArray* waypointStrings_;
    BOOL isUpdatedDropOffAddress;
    
    CenterPickUpView *centerPickUpObj;
    ConfirmBottomView *confirmBottomViewObj;
    DriverDetailsView *driverDetailsObj;
    
    //Booking Flow
   // GMSMarker *pickUpMarkerWithTime;
    NSMutableArray *pathArray;
    GMSPolyline *polyline1;
    GMSPolyline *polyline2;
}

@property(nonatomic,strong) NSString* laterSelectedDate;
@property(nonatomic,strong) NSString* laterSelectedDateServer;

@property(nonatomic,strong) CustomNavigationBar* customNavigationBarView;

@property(nonatomic,strong) NSMutableDictionary* allMarkers;
@property(nonatomic,strong) WildcardGestureRecognizer* tapInterceptor;

@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;

@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,strong) CLLocationManager* locationManager;

@property(nonatomic,strong) UIView* PikUpViewForConfirmScreen;
@property(nonatomic,strong) UIView* DropOffViewForConfirmScreen;

@property(nonatomic,strong) NSString* cardId;
@property(nonatomic,strong) NSString* subscribedChannel;

@property(nonatomic,assign) BOOL isDriverArrived;
@property(nonatomic,assign) BOOL isDriverOnTheWay;
@property(nonatomic,assign) BOOL isPathPlotted;

@property(nonatomic,assign) CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong) GMSMarker* sourceMarker;
@property(nonatomic,strong) GMSMarker* destinationMarker;
@property(nonatomic,strong) GMSMarker* bookedCarMarker;

@property(nonatomic,strong) NSString* startLocation;

@property(nonatomic,assign) double driverCurLat;
@property(nonatomic,assign) double driverCurLong;

@property(nonatomic,strong) NSMutableArray* arrayOfCarTypes;

@property(nonatomic,strong) NSMutableArray* arrayOfpickerData;

@property(nonatomic,strong) NSMutableArray* arrayOfMastersAround;
@property(nonatomic,strong) NSMutableArray* arrayOfDriverID;
@property(nonatomic,strong) NSMutableArray* arrayOfFirstOnlineDrivers;
@property(nonatomic,strong) NSMutableArray* arrayOfFirstOnlineDriversButton;
@property(nonatomic,strong) NSMutableArray* arrayOfFirstOfflineDriversButton;

@property(nonatomic,strong) UIView* spinnerView;
@property(nonatomic,assign) BOOL isAddressManuallyPicked;

@property(nonatomic,assign) BOOL isMapMoving;
@property(nonatomic,strong) CLLocation *previousLocation;

@property(nonatomic,weak) NSTimer* pollingTimer;
@property(nonatomic,weak) NSTimer* bookingTimer;
@property(nonatomic,weak) NSTimer* pubnubStreamTimer;

@end

static int isLocationChanged;

@implementation MapViewController
@synthesize isComingFromSignUp;
@synthesize drivers;
@synthesize arrayOfMastersAround;
@synthesize PikUpViewForConfirmScreen;
@synthesize DropOffViewForConfirmScreen;
@synthesize laterSelectedDate, laterSelectedDateServer;
@synthesize addProgressBarView;
@synthesize customNavigationBarView;
@synthesize cardId;
@synthesize pollingTimer,pubnubStreamTimer,subscribedChannel;
@synthesize bookingTimer;
@synthesize searchAddressTextField, pickUpAddressTextField, dropOffAddressTextField;
@synthesize requestingView;
@synthesize carTypesForLiveBooking, carTypesForLiveBookingServer;
@synthesize arrayOfCarTypes;
@synthesize arrayOfDriverID, arrayOfFirstOnlineDrivers, arrayOfFirstOnlineDriversButton, arrayOfFirstOfflineDriversButton;
@synthesize arrayOfpickerData, allMarkers;
@synthesize pkrViewForAddTip;
@synthesize isMapMoving, previousLocation;
@synthesize sourceMarker, destinationMarker, bookedCarMarker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

+ (instancetype) getSharedInstance {
    if (!sharedInstance) {
        sharedInstance  = [[self alloc] init];
    }
    return sharedInstance;
}



#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    isInBooking = 0;
    isNowSelected = NO;
    isLaterSelected = NO;
    sharedInstance = self;
    maximumETA = 0;
    carTypesForLiveBooking = 1;
    carTypesForLiveBookingServer = 0;
    isVehicleBtnClickedForRateCard = NO;
    _isPathPlotted = NO;
    currencyStr =  [Helper getCurrencyLocal:0.00];
    NSRange removeRange = [currencyStr rangeOfString:@"0.00"];
    currencyStr = [currencyStr stringByReplacingCharactersInRange:removeRange withString:@""];
    pubNub = [PubNubWrapper sharedInstance];
    self.navigationController.navigationBarHidden = YES;
    CGRect rect = self.view.frame;
    rect.origin.y = -44;
    [self.view setFrame:rect];
    double curLat = [[Utility currentLatitude] doubleValue];
    double curLong = [[Utility currentLongitude] doubleValue];
    srcLat = curLat;
    srcLong = curLong;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:curLat longitude:curLong zoom:mapZoomLevel];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    UIView *customMapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    mapView_ = [GMSMapView mapWithFrame:customMapView.frame camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
    [mapView_ clear];
    customMapView = mapView_;
    [self.view addSubview:customMapView];
    geocoder_ = [[GMSGeocoder alloc] init];
    [self publishPubNubStream];
    [self subscribeToPassengerChannel];
    [self unSubscribeOnlyBookedDriver];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
    UIView *topContainerView =[self.view viewWithTag:customLocationViewTag];
    if (topContainerView != nil) {
        topContainerView.hidden = NO;
        UIView *currentLocationView = [self.view viewWithTag:loctionButtonsTag];
        currentLocationView.hidden = NO;
    }
    [self addPickUpLocationView];
    [self createCenterView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"DRIVERONTHEWAY" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"DRIVERREACHED"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"JOBSTARTED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"JOBCOMPLETED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookingConfirmed:) name:kNotificationBookingConfirmationNameKey object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(allDriversBusy:) name:@"AllDriversBusy" object:nil];
    if([Utility bookingData] != nil) {
        if([Utility bookingStatus] != 9) {
            [self subscribeOnlyBookedDriver];
            _isPathPlotted = NO;
        }
        [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[Utility bookingData] :(int)[Utility bookingStatus]];
    } else if([Utility splashData] != nil && [Utility bookingData] == nil) {
        [self clearUserDefaultsAfterBookingCompletion];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(srcLat, srcLong);
        [self getAddress:position];
    } else {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(srcLat, srcLong);
        [self getAddress:position];
    }

}

- (void) handleTapOnHelp:(UITapGestureRecognizer *)recognize {
    UIImageView *imageView = [[UIApplication sharedApplication].keyWindow viewWithTag:121];
    [imageView removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated {
    pubNub = [PubNubWrapper sharedInstance];
    if(pubNub.delegate == nil) {
        [pubNub setDelegate: self];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [[MyAppTimerClass sharedInstance] startPublishTimer];
    }
    if(isCustomMarkerSelected) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
        if (![Utility isUserInBooking] && !_isSelectinLocation) {
            [[MyAppTimerClass sharedInstance] stopPublishTimer];
            [[MyAppTimerClass sharedInstance] startPublishTimer];
            _isSelectinLocation = YES;
        }
        if(arrayOfMastersAround.count > 0) {
            if (arrayOfMastersAround.count < carTypesForLiveBooking) {
                carTypesForLiveBooking = 1;
            }
            carTypesForLiveBookingServer = [arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
            if (isUpdatedDropOffAddress) {
                isUpdatedDropOffAddress = NO;
            }
            [self changeMapMarker:carTypesForLiveBooking-1];
        }
        if([Utility splashData] != nil) {
            NSDictionary *messageDict = [Utility splashData];
            if ([messageDict[@"bookingStatus"] integerValue] == 2 && [messageDict[@"ExpiredTime"] integerValue] > 0) {
                [Utility setBookingID:flStrForStr(messageDict[@"bid"])];
                remainingTimeForBooking = [messageDict[@"ExpiredTime"] integerValue]+5;
                [self makeMyProgressBarMoving];
                [self makeMyProgressBarMoving:remainingTimeForBooking];
                [self startTimer];
            } else {
                [self handleMessageA2ComesHaere:messageDict];
            }
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUSplashData];
        }
    });
    if(centerPickUpObj) {
        if(!isUpdatedDropOffAddress) {
            [centerPickUpObj.activityIndicator startAnimating];
            [[MyAppTimerClass sharedInstance] startSpinTimer];
            [[MyAppTimerClass sharedInstance] startPublishTimer];
            arrayOfDriverID = nil;
            arrayOfFirstOnlineDrivers = nil;
            arrayOfMastersAround = nil;
            [Helper setToLabel:centerPickUpObj.timeLabel Text:NSLocalizedString(@"No Drivers", @"No Drivers") WithFont:fontNormal FSize:7 Color:[UIColor whiteColor]];
            centerPickUpObj.timeLabel.hidden = YES;
            centerPickUpObj.activityIndicator.hidden = NO;
        }
        isUpdatedDropOffAddress = NO;
    }
    self.navigationController.navigationBarHidden = YES;
    if ([[Utility driverPubnubChannel] length]) {
        [self subsCribeToPubNubChannel:[Utility driverPubnubChannel]];
        [self publishA3ToGetBookingData];
    }

    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"Home"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    if (isChangeLocationChangingInBooking) {
        isChangeLocationChangingInBooking = NO;
        return;
    }
    if([Utility bookingData] == nil) {
        [self initiateSetViewController];
    } else {
        [self addCustomNavigationBar];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isComingFromNegativeRating"]) {
            [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[Utility bookingData] :(int)[Utility bookingStatus]];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [self addgestureToMap];
    if (IS_SIMULATOR) {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:14];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if (!_isSelectinLocation) {
        _isSelectinLocation = NO;
        [allMarkers removeAllObjects];
        if (![Utility isUserInBooking]) {
            [mapView_ clear];
        }
        [self removeAddProgressView];
        [self stopPubNubStream];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - XDKAirmenu Delegate -

-(void) leftBarButtonClicked:(UIButton *)sender {
    if(isCustomMarkerSelected == YES) {
        [self navCancelButtonClciked];
    } else {
        [self menuButtonPressed:sender];
    }
}

-(void) rightBarButtonClicked:(UIButton *)sender {

}

- (IBAction)menuButtonPressed:(id)sender {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    menu.isMenuOnRight = NO;
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)navCancelButtonClciked {
    [customNavigationBarView addTitleButton];
    [customNavigationBarView hideLeftMenuButton:NO];
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    [self pubnubStreamTimer];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
    isLocationChanged = isChanging;
    [self getCurrentLocation:nil];
    UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
    [customLocationView removeFromSuperview];
    
    [confirmBottomViewObj hidePOPup];
    UIView *confirmBottomView = [self.view viewWithTag:confirmationBottomViewTag];
    [confirmBottomView removeFromSuperview];
    
    UIView *addLocationButtonsView = [self.view viewWithTag:loctionButtonsTag];
    [addLocationButtonsView removeFromSuperview];
    [self addLocationButtonsOnMap];
    
    isCustomMarkerSelected = NO;
    dropOffAddressTextField.text = @"";
    
    confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = YES;
    confirmBottomViewObj.requestBookingBtn.selected = NO;
    
    [self addCustomNavigationBar];
    [self createCenterView];
    [self getCurrentLocation:nil];
    [self showBottomViews];
    [self addPickUpLocationView];
    UIButton *nowBtn =  (UIButton *)[[self.view viewWithTag:bookingOptionViewTag]viewWithTag:nowBookingBtnTag];
    UIButton *laterBtn =  (UIButton *)[[self.view viewWithTag:bookingOptionViewTag] viewWithTag:laterBookingBtnTag];
    [nowBtn setSelected:NO];
    [laterBtn setSelected:NO];
    isNowSelected = NO;
    isLaterSelected = NO;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    [self getAddress:position];
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
    [mapView_ animateWithCameraUpdate:zoomCamera];
    [self stopBackgroundTask];
}


-(void)hideBottomViews {
    UIView *nowLaterView = [self.view viewWithTag:bookingOptionViewTag];
    [nowLaterView setHidden:YES];
    [scroller setHidden:YES];
    [shadowViewForScroller setHidden:YES];
}

-(void)showBottomViews {
    UIView *nowLaterView = [self.view viewWithTag:bookingOptionViewTag];
    [nowLaterView setHidden:NO];
    [scroller setHidden:NO];
    [shadowViewForScroller setHidden:NO];
}



#pragma mark - Custom NavigationBar -

/**
 This adds custom navigation bar
 */
- (void) addCustomNavigationBar {
    NSInteger bookingStatus = [Utility bookingStatus];
    if(!customNavigationBarView) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
        customNavigationBarView.tag = 78;
        customNavigationBarView.delegate = self;
        [customNavigationBarView addTitleButton];
        [customNavigationBarView removeRightBarButton];
        [self.view addSubview:customNavigationBarView];
    }
    if(isCustomMarkerSelected == YES) {
//        [customNavigationBarView setBackgroundColor:UIColorFromRGB(0xFFFFFF)];
        NSString *carTypeName = flStrForStr(arrayOfCarTypes[carTypesForLiveBooking - 1][@"type_name"]);
        UIButton *selectedButton = arrayOfButtons[carTypesForLiveBooking - 1];
        NSString *selectedTypeETA = selectedButton.titleLabel.text;
        NSRange range = [selectedTypeETA rangeOfString:[NSString stringWithFormat:@"\n\n\n\n%@", carTypeName]];
        if (range.location != NSNotFound) {
            selectedTypeETA = [selectedTypeETA stringByReplacingCharactersInRange:range withString:@""];
        }
        [customNavigationBarView setTitle:[NSString stringWithFormat:@"%@, %@", carTypeName, [selectedTypeETA capitalizedString]] withColor:UIColorFromRGB(0xFFFFFF)];
        UIImage *onImage = [UIImage imageNamed:@"close_icon"];
        [customNavigationBarView setleftBarButtonImage:onImage :onImage];
        [customNavigationBarView removeTitleButton];
        [customNavigationBarView hideRightBarButton:YES];
        [customNavigationBarView removeRightBarButton];
        if (arrayOfDriverID.count) {
            confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = YES;
            confirmBottomViewObj.requestBookingBtn.selected = NO;
        } else {
            confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = NO;
            confirmBottomViewObj.requestBookingBtn.selected = YES;
        }
    } else if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        [customNavigationBarView setTitle:NSLocalizedString(@"On my way to ZiRide", @"On my way to ZiRide") withColor:UIColorFromRGB(0xFFFFFF)];
        [customNavigationBarView hideRightBarButton:YES];
        [customNavigationBarView removeRightBarButton];
        [customNavigationBarView removeTitleButton];
    } else if (bookingStatus == kNotificationTypeBookingReachedLocation) {
        [customNavigationBarView setTitle:NSLocalizedString(@"I have arrived, You ready?", @"I have arrived, You ready?") withColor:UIColorFromRGB(0xFFFFFF)];
        [customNavigationBarView hideRightBarButton:YES];
        [customNavigationBarView removeRightBarButton];
        [customNavigationBarView removeTitleButton];
    } else if (bookingStatus == kNotificationTypeBookingStarted) {
        [customNavigationBarView setTitle:NSLocalizedString(@"Trip started, Relax and Enjoy the ride", @"Trip started, Relax and Enjoy the ride") withColor:UIColorFromRGB(0xFFFFFF)];
        [customNavigationBarView hideRightBarButton:YES];
        [customNavigationBarView removeRightBarButton];
        [customNavigationBarView removeTitleButton];
    } else {
        [customNavigationBarView setTitle:@""];
        [customNavigationBarView setRightBarButtonTitle:@""];
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideTitleButton:NO];
        [customNavigationBarView hideLeftMenuButton:NO];
        [customNavigationBarView hideRightBarButton:NO];
        [customNavigationBarView removeRightBarButton];
    }
}

#pragma mark - UIGestureRecognizer -

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    XDKAirMenuController *controller = [XDKAirMenuController sharedMenu];
    if (controller.isMenuOpened) {
        [controller closeMenuAnimated];
    }
    return YES;
}

#pragma mark - WebServices -

/**
 This updates drop off address on server DB
 */
-(void)sendServiceToUpdateDropAddress {
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_booking_id":flStrForObj([Utility bookingID]),
                             @"ent_mid":flStrForObj([Utility userID]),
                             @"ent_drop_addr1":flStrForStr(desAddr),
                             @"ent_drop_addr2":flStrForStr(desAddrline2),
                             @"ent_lat":[NSNumber numberWithFloat:desLat],
                             @"ent_long":[NSNumber numberWithFloat:desLong],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    isUpdatingDropOff = YES;
    NetworkHandler* networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateDropOff"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getupdateAddressResponse:response];
                               }];
}


/**
 This tell us update drop off has successed or not

 @param response response from service response
 */
-(void)getupdateAddressResponse:(NSDictionary *)response {
    NSString *titleMsg = NSLocalizedString(@"Message", @"Message");
    NSString *errMsg = @"";
    if (!response) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUFavouriteDropOffLocation];
        dropOffAddressTextField.text = NSLocalizedString(@"Add Drop Location", @"Add Drop Location");
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"Message"]];
    } else {
        if ([response [@"errFlag"] intValue] == 0) {
            errMsg =response [@"errMsg"];
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            NSString *destinationAddress = flStrForStr(desAddr);
            NSString *destinationAddress1 = flStrForStr(desAddrline2);
            if ([destinationAddress1 rangeOfString:destinationAddress].location == NSNotFound) {
                if(destinationAddress.length)
                    [arr addObject:destinationAddress];
                if (destinationAddress1.length)
                    [arr addObject:destinationAddress1];
            } else {
                if (destinationAddress1.length)
                    [arr addObject:destinationAddress1];
            }
            NSString *addressText = [arr componentsJoinedByString:@", "];
            dropOffAddressTextField.text = flStrForStr(addressText);
            [Utility setDropOffLocationAddressLine1:flStrForStr(desAddr)];
            [Utility setDropOffLocationAddressLine2:flStrForStr(desAddrline2)];
            [Utility setDropOffLatitude:desLat];
            [Utility setDropOffLongitude:desLong];
        } else if ([response[@"errFlag"] intValue] == 1) {
            errMsg = response[@"errMsg"];
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            NSString *destinationAddress = flStrForStr(desAddr);
            NSString *destinationAddress1 = flStrForStr(desAddrline2);
            if ([destinationAddress1 rangeOfString:destinationAddress].location == NSNotFound) {
                if(destinationAddress.length)
                    [arr addObject:destinationAddress];
                if (destinationAddress1.length)
                    [arr addObject:destinationAddress1];
            } else {
                if (destinationAddress1.length)
                    [arr addObject:destinationAddress1];
            }
            NSString *addressText = [arr componentsJoinedByString:@", "];
            dropOffAddressTextField.text = flStrForStr(addressText);
        } else {
            errMsg = response[@"errMsg"];
        }
    }
    [self publishA3ToGetBookingData];
    [Helper showAlertWithTitle:titleMsg Message:errMsg];
}

/**
 This is to check cancellation time. If it is more than required then cancellation charge will apply else not.
 */
-(void)checkCancellationTimeBeforeCancel {
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];

    NetworkHandler* networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"cancelAppointmentCheck"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success){
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       if (!response) {
                                           return;
                                       } else if([response[@"errFlag"] integerValue] == 0) {
                                           UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                           RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
                                           [alertView setContainerView:containerView];
                                           [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Yes", @"Yes"), NSLocalizedString(@"No", @"No"), nil]];
                                           [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
                                               if (buttonIndex == 0 && [response[@"flag"] integerValue] == 1) {
                                                   CancelledReasonView* cancelledView = [[CancelledReasonView alloc] init];
                                                   UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
                                                   [cancelledView showPopUpViewOnwindow:window];
                                                   cancelledView.onCompletion = ^(NSInteger success) {
                                                       if (success) {
                                                           isInBooking = 0;
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
                                                           [[MapViewController getSharedInstance] getCurrentLocation:nil];
                                                           CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
                                                           [self getAddress:position];
                                                           [self bookingNotAccetedByAnyDriverScreenLayout];
                                                           [self publishPubNubStream];
                                                       }
                                                   };
                                               } else if (buttonIndex == 0 && [response[@"flag"] integerValue] == 0){
                                                   [self sendRequestForCancelAppointment];
                                               }
                                               [alertView close];
                                           }];
                                           [alertView setUseMotionEffects:true];
                                           [alertView show];
                                       }
                                   }else {
                                       [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                   }
                               }];
}




/**
 This cancels live booking service
 */
-(void)sendAppointmentRequestForLiveBookingCancellation{
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_booking_id":[Utility bookingID]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelOngoingAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getLiveCancelResponse:response];
                                   }
                               }];
}

/**
 *  Cancel booking response
 *
 *  @param response return type void and takes no argument
 */
-(void)getLiveCancelResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    if (!requestingView) {
        requestingView = [ProviderRequestingView sharedInstance];
    }
    [requestingView stopTimer];
    requestingView = nil;
    if (!response) {
        return;
    } else {
        if ([response[@"errFlag"] intValue] == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
    [self stopTimer];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
    [self removeAddProgressView];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    arrayOfMastersAround = [NSMutableArray new];
    [self clearTheMapBeforeChagingTheCarTypes];
    [self stopBackgroundTask];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    [self publishPubNubStream];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
}


/**
 This is to check current wallet amount of user
 */
-(void) getWalletCurrentMoney {
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr([Utility userID]),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"", @"")];
}


/**
 Check wallet amount response

 @param response return type void and takes no argument
 */
-(void) getWalletCurrentMoneyResponse:(NSDictionary *) response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil)
        return;
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([response [@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
    } else {
        if ([Helper checkForUpdateAppVersion:response[@"versions"][@"iosCust"]]) {
            return;
        }
        walletAmount = [flStrForObj(response[@"amount"]) floatValue];
        NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"amount"]) floatValue]];
        [Utility setWalletBalance:currentWalletAmount];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self sendAppointmentRequestForLiveBooking];
    }
}



/**
 This is to cancel on going booking
 */
-(void)sendRequestForCancelAppointment {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Cancelling...", @"Cancelling...")];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mid":[Utility driverID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             @"ride_cancelled":@"",
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseCancelAppointmentResponse:response];
                               }];
}

/**
 Cancel on going booking response

 @param response return type void and takes no argument
 */
-(void)parseCancelAppointmentResponse:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response != nil) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        if([response[@"errFlag"] integerValue] == 0 && ([response[@"errNum"] integerValue] == 42 ||[response[@"errNum"] integerValue] == 43)) {
            isInBooking = 0;
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"walletAmt"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            [self getCurrentLocation:nil];
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
            [self getAddress:position];
            [self stopTimer];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingID];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
            [self removeAddProgressView];
            [self bookingNotAccetedByAnyDriverScreenLayout];
            arrayOfMastersAround = [NSMutableArray new];
            [self clearTheMapBeforeChagingTheCarTypes];
            [[MyAppTimerClass sharedInstance] stopPublishTimer];
            [self publishPubNubStream];
            [[MyAppTimerClass sharedInstance] startPublishTimer];
            [self stopBackgroundTask];
            [self bookingNotAccetedByAnyDriverScreenLayout];
        }
    }
}



/**
 This is to calculate fare estimate between two location
 */
-(void)sendAServiceForFareCalculator {
    if (srcLat == 0 || srcLong == 0 || _currentLatitude == 0 || _currentLongitude == 0 || desLat == 0 || desLong == 0 || [Utility isUserInBooking]) {
        return;
    }
    if (carTypesForLiveBookingServer == 0 && arrayOfCarTypes.count == 0) {
        return;
    }
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",_currentLatitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",_currentLongitude];
    NSString *pickupLatitude = [NSString stringWithFormat:@"%f",srcLat];
    NSString *pickupLongitude = [NSString stringWithFormat:@"%f",srcLong];
    NSString *dropLatitude = [NSString stringWithFormat:@"%f",desLat];
    NSString *dropLongitude = [NSString stringWithFormat:@"%f",desLong];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_type_id":[NSNumber numberWithInteger:carTypesForLiveBookingServer],
                             @"ent_curr_lat":currentLatitude,
                             @"ent_curr_long":currentLongitude,
                             @"ent_from_lat":pickupLatitude,
                             @"ent_from_long":pickupLongitude,
                             @"ent_to_lat":dropLatitude,
                             @"ent_to_long":dropLongitude,
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             @"ent_coupon":[Utility promoCode],
                             @"ent_bid":[Utility bookingID]
                             };
    NSLog(@"Fare Estimate Params = %@",params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodFareCalculator
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   NSLog(@"Fare Estimate Response = %@", response);
                                   if (success)
                                       [self parsegetBookingAppointment:response];
                                   else
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                               }];
}

/**
 Fare estimate response

 @param responseDictionary return type void and takes no argument
 */
-(void)parsegetBookingAppointment:(NSDictionary *)responseDictionary {
    if (!responseDictionary) {
    } else {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0) {
            NSString *fareEstimateValue =  [Helper getCurrencyLocal:[responseDictionary[@"fare"] floatValue]];
            confirmBottomViewObj.rideEstimateBtn.titleLabel.numberOfLines = 0;
            confirmBottomViewObj.rideEstimateBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            NSString *fareEstimateStr = NSLocalizedString(@"Ride Estimate", @"Ride Estimate");
            
            NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            [style setAlignment:NSTextAlignmentCenter];
            [style setLineBreakMode:NSLineBreakByWordWrapping];
            
            UIFont *font1 = [UIFont fontWithName:fontSemiBold size:16];
            UIFont *font2 = [UIFont fontWithName:fontSemiBold  size:13];
            NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                                    NSFontAttributeName:font1,
                                    NSForegroundColorAttributeName:UIColorFromRGB(0x000000),
                                    NSParagraphStyleAttributeName:style}; // Added line
            NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                                    NSFontAttributeName:font2,
                                    NSForegroundColorAttributeName:UIColorFromRGB(0x484848),
                                    NSParagraphStyleAttributeName:style}; // Added line
            
            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
            [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",fareEstimateValue] attributes:dict1]];
            [attString appendAttributedString:[[NSAttributedString alloc] initWithString:fareEstimateStr attributes:dict2]];
            [confirmBottomViewObj.rideEstimateBtn setAttributedTitle:attString forState:UIControlStateNormal];
            [[confirmBottomViewObj.rideEstimateBtn titleLabel] setNumberOfLines:0];
            [[confirmBottomViewObj.rideEstimateBtn titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        }
    }
}


/**
 This is valid all input parametrs like address and there lat longs, promo code, driver notes, wallet used and
 genrate booking Id.
 */
-(void)callServiceToBookDriver {
    isLocationChanged = isChanging;
    if (!srcAddrline2 || srcAddrline2 == (id)[NSNull null] || srcAddrline2.length == 0 )
        srcAddrline2 = @"";
    if (isNowSelected == YES)
        laterSelectedDateServer = @"";
    
    NSString *favPickUpLocationType = NSLocalizedString(@"Pickup Location", @"Pickup Location");
    NSString *favDropOffLocationType = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
    if (srcAddr.length && srcAddrline2.length) {
        BOOL isFavPickUp = [Database checkAddressIsFavouriteInDatabase:srcAddr and:srcAddrline2];
        if (isFavPickUp) {
            NSArray *arr = [Database getFavouriteAddressFromDataBase:srcAddr and:srcAddrline2];
            SourceAddress *add = arr[0];
            favPickUpLocationType = [flStrForStr(add.locationType) capitalizedString];
        }
    }
    if (desAddr.length && desAddrline2.length) {
        BOOL isFavDropOff = [Database checkAddressIsFavouriteInDatabase:desAddr and:desAddrline2];
        if (isFavDropOff) {
            NSArray *arr = [Database getFavouriteAddressFromDataBase:desAddr and:desAddrline2];
            SourceAddress *add = arr[0];
            favDropOffLocationType = [flStrForStr(add.locationType) capitalizedString];
        }
    }
    NSString *pickupLatitude =  [NSString stringWithFormat:@"%f",srcLat];
    NSString *pickupLongitude = [NSString stringWithFormat:@"%f",srcLong];
    if (srcAddr == (id)[NSNull null] || srcAddr.length == 0 ) {
        UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
        srcAddr = textAddress.text;
        textAddress.textColor = UIColorFromRGB(0x333333);
    }
    NSString *pickAddress = srcAddr;
    
    NSString *dropLatitude;
    NSString *dropLongitude;
    NSString *dropAddress;
    NSString *dropAddress2;
    if (!desAddrline2 || desAddrline2 == (id)[NSNull null] || desAddrline2.length == 0 ) {
        desAddrline2 = @"";
    }
    if(desLat == 0 && desLong == 0) {
        dropLatitude = @"";
        dropLongitude =@"";
        dropAddress =  @"";
        dropAddress2 = @"";
    } else {
        dropLatitude = [NSString stringWithFormat:@"%f",desLat];
        dropLongitude = [NSString stringWithFormat:@"%f",desLong];
        dropAddress = desAddr;
        dropAddress2 = desAddrline2;
    }
    if(!cardId || [cardId isKindOfClass:[NSNull class]]) {
        cardId = @"";
    }
    NSInteger useWallet = 0;
    if (selectedPaymentType == 0)
        useWallet = 1;
   
    @try {
        NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                                 @"ent_dev_id":[Utility deviceID],
                                 @"ent_wrk_type":[NSString stringWithFormat:@"%ld",(long)carTypesForLiveBookingServer],
                                 @"ent_lat":pickupLatitude,
                                 @"ent_long":pickupLongitude,
                                 @"ent_addr_line1":pickAddress,
                                 @"ent_drop_lat":dropLatitude,
                                 @"ent_drop_long":dropLongitude,
                                 @"ent_drop_addr_line1":dropAddress,
                                 @"ent_extra_notes":@"",
                                 @"ent_date_time":[Helper getCurrentNetworkDateTime],
                                 @"ent_later_dt":laterSelectedDateServer,
                                 @"ent_payment_type":[NSNumber numberWithInteger:paymentTypesForLiveBooking],
                                 @"ent_token":cardId,
                                 @"ent_coupon":[Utility promoCode],
                                 @"ent_surge":[NSNumber numberWithDouble:surgePrice],
                                 @"ent_wallet":[NSNumber numberWithInteger:useWallet],
                                 @"ent_wallet_bal":[Utility walletBalance],
                                 @"ent_additional_info":flStrForStr(driverNotes),
                                 @"ent_fav_pickup":flStrForStr(favPickUpLocationType),
                                 @"ent_fav_drop":flStrForStr(favDropOffLocationType)
                                 };
        NSLog(@"Live Booking Parmas = %@", params);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMLiveBooking
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       NSLog(@"Live Booking Response = %@", response);
                                       if (success) {
                                           [self getLiveResponse:response];
                                       } else {
                                           [self bookingNotAccetedByAnyDriverScreenLayout];
                                           [self stopBackgroundTask];
                                       }
                                   }];
    }
    @catch (NSException *exception){
    }
}

/**
 This is live booking response.

 @param responseDictionary Live booking response dictionary.
 */
-(void)getLiveResponse:(NSDictionary *)responseDictionary {
    if (!responseDictionary) {
        [self stopBackgroundTask];
        return;
    } else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 6 || [responseDictionary[@"errNum"] intValue] == 7 || [responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
        [self stopBackgroundTask];
    } else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0 &&[[responseDictionary objectForKey:@"errNum"] intValue] == 78) {
        laterSelectedDateServer = @"";
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        [self stopBackgroundTask];
    } else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1 &&[[responseDictionary objectForKey:@"errNum"] intValue] == 122) {
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        [self stopBackgroundTask];
    }  else {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1) {
            if ([[responseDictionary objectForKey:@"errNum"] intValue] == 1) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDictionary[@"errMsg"]];
                [self stopBackgroundTask];
            } else if ([responseDictionary[@"errNum"] intValue] == 100) {
                [self bookingNotAccetedByAnyDriverScreenLayout];
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDictionary[@"errMsg"]];
            }
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
            [self stopBackgroundTask];
        } else if ([responseDictionary[@"errFlag"] intValue] == 0 && [responseDictionary[@"errNum"] intValue] == 21) {
            [Utility setBookingID:flStrForStr(responseDictionary[@"BookingId"])];
            expiryPerDriver = [responseDictionary[@"ExpiryPerDriver"] integerValue];
            expiryForBooking = [responseDictionary[@"ExpiryForBooking"] integerValue]+5;//Rahul sir Suggestion
            remainingTimeForBooking = expiryForBooking;
            [addProgressBarView setHidden:NO];
            [self makeMyProgressBarMoving:expiryForBooking];
            [self startTimer];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkLostWhileBooking:) name:@"NetworkLostWhileBooking" object:nil];
        }
    }
}


/**
 This removes address from favourite
 
 @param addressID Address ID is unique value given by server to store in server DB.
 */
-(void)removeFromFavouriteAddress:(NSString *) addressID {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_sid":[Utility userID],
                             @"ent_action":@"2",
                             @"ent_aid":flStrForStr(addressID),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"favourateAdresss"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                       }  else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
                                       } else if ([[response objectForKey:@"errFlag"] intValue] == 1) {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
                                       } else {
                                           BOOL isFavouriteRemoved = [Database updateAddressWithAddressID:addressID asFavouriteTag:0];
                                           if(isFavouriteRemoved) {
                                               UIButton *btn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:97];
                                               btn.selected = NO;
                                               UILabel *pickupLabel = (UILabel *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:1010];
                                               pickupLabel.text = NSLocalizedString(@"Pickup Location", @"Pickup Location");
                                           }
                                       }
                                   }
                               }];
}

/**
 Send serivce for not operating area
 
 @param index index Number
 */
-(void)serviceToNotifyUserIsComingFromNotOperatingArea {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_lat":[NSNumber numberWithFloat:srcLat],
                               @"ent_long":[NSNumber numberWithFloat:srcLong],
                               @"ent_user_type":@"2"
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getWorkplaces"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self notifyOperatingAreaResponse:response];
                             }
                         }];
}

/**
 Get Notify for Not Operating Area response
 
 @param response response data
 */
-(void)notifyOperatingAreaResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 99 || [response[@"errNum"] intValue] == 101 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
        } else {
            if ([dictResponse[@"types"] count]) {
                [Utility setOpratingTypeArea:kOperatingArea];
                arrayOfMastersAround = [NSMutableArray new];
                [self clearTheMapBeforeChagingTheCarTypes];
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
                [self publishPubNubStream];
                [[MyAppTimerClass sharedInstance] startPublishTimer];
            } else {
                [Utility setOpratingTypeArea:kNotOperatingArea];
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
            }
        }
    }
}

-(void)updateGoogleDistanceMatrixAPICountOnServer:(NSString *)url andDriverID:(NSString *)driverID {
    NSDictionary *params =  @{ @"url":url,
                               @"dev_type": @"1",
                               @"sid":[Utility userID],
                               @"mid": flStrForStr(driverID)
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"updateGooogleCalls"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             NSLog(@"Updated Google Service");
                         }];
}


/**
 Send serivce to get Google Keys
 
 @param index index Number
 */
-(void)getKeysService {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"action":@"GoogleKey",
                               @"ent_user_type":@"2"
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getKeys"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getKeysServiceResponse:response];
                             } else {
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             }
                         }];
}

/**
 Get Google Keys response
 
 @param response response data
 */
-(void)getKeysServiceResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 99 || [response[@"errNum"] intValue] == 101 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            
        } else {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            NSMutableArray *googleKeysArray = [response[@"GoogleKeysArray"] mutableCopy];
            NSString *distanceMatrixKey = [googleKeysArray firstObject];
            [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
            [googleKeysArray removeObjectAtIndex:0];
            [Utility setGoogleKeysArray:googleKeysArray];
        }
    }
}



#pragma mark - Background Task -

-(void)startBackgroundTask {
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

-(void)stopBackgroundTask {
    UIApplication *app = [UIApplication sharedApplication];
    [app endBackgroundTask:bgTask];
}


-(void)removeAddProgressView {
    if(addProgressBarView != nil) {
        [addProgressBarView removeFromSuperview];
    }
}


-(void)clearUserDefaultsAfterBookingCompletion {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:kNSUBookingData];
    [ud removeObjectForKey:kNSUBookingDate];
    [ud removeObjectForKey:kNSUBookingID];
    [ud removeObjectForKey:kNSUBookingStatus];
    [ud removeObjectForKey:kNSUBookingApproximateFare];
    
    [ud setBool:NO forKey:kNSUIsUserInBooking];
    [ud setBool:NO forKey:@"isServiceCalledOnce"];
    
    [ud removeObjectForKey:kNSUVehicleColor];
    [ud removeObjectForKey:kNSUVehicleMakeName];
    [ud removeObjectForKey:kNSUVehicleModelName];
    [ud removeObjectForKey:kNSUVehicleNumberPlate];
    
    [ud removeObjectForKey:kNSUDriverID];
    [ud removeObjectForKey:kNSUDriverName];
    [ud removeObjectForKey:kNSUDriverEmail];
    [ud removeObjectForKey:kNSUDriverRating];
    [ud removeObjectForKey:kNSUDriverMobileNumber];
    [ud removeObjectForKey:kNSUDriverProfileImageURL];
    [ud removeObjectForKey:kNSUDriverPubnubChannel];
    [ud removeObjectForKey:kNSUDriverLatitude];
    [ud removeObjectForKey:kNSUDriverLongitude];
    
    
    [ud removeObjectForKey:kNSUPickUpLatitude];
    [ud removeObjectForKey:kNSUPickUpLongitude];
    [ud removeObjectForKey:kNSUPickUpLocationAddressLine1];
    [ud removeObjectForKey:kNSUPickUpLocationAddressLine2];
    
    [ud removeObjectForKey:kNSUDropOffLatitude];
    [ud removeObjectForKey:kNSUDropOffLongitude];
    [ud removeObjectForKey:kNSUDropOffLocationAddressLine1];
    [ud removeObjectForKey:kNSUDropOffLocationAddressLine2];
    
    [ud synchronize];
}


#pragma mark - UserDelegate

-(void)userDidLogoutSucessfully:(BOOL)sucess {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}

-(void)userDidFailedToLogout:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}




#pragma Mark - ChangeController

-(void)initiateSetViewController {
    _isDriverOnTheWay = _isDriverArrived = isRequestingButtonClicked = isLaterSelected = NO;
    [mapView_ animateToViewingAngle:0];
    UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
    [driverView removeFromSuperview];
    UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
    [driverMsgLabel removeFromSuperview];
    UIView *topContainerView =[self.view viewWithTag:customLocationViewTag];
    if (topContainerView != nil) {
        topContainerView.hidden = NO;
        UIView *currentLocationView = [self.view viewWithTag:loctionButtonsTag];
        currentLocationView.hidden = NO;
    }
    [self addCustomNavigationBar];
    [self addPickUpLocationView];
    [self createCenterView];
    [self getCurrentLocation:nil];
    if(_currentLatitude == 0 || _currentLongitude == 0) {
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
    }
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    [self getAddress:position];
    [self createCenterView];
    [self addSearchAddressView];
    NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey: kNSUCarArrayKey]];
    arrayOfCarTypes = [carTypes mutableCopy];
    if (arrayOfCarTypes.count > 0 ) {
        [self clearTheMapBeforeChagingTheCarTypes];
        [self addBottomView];
        if (carTypesForLiveBookingServer == 0) {
            carTypesForLiveBookingServer = [arrayOfCarTypes[0][@"type_id"] integerValue];
        }
        [self changeMapMarker:carTypesForLiveBooking-1];
    }
    [self subscribeToPassengerChannel];
    [self publishPubNubStream];
}


-(BOOL)checkCurrentStatus:(NSInteger)status {
    if([Utility bookingStatus] != status && [Utility bookingStatus] < status)
        return NO;
    else
        return YES;
}

-(BOOL)checkBookingID:(NSInteger)status {
    if([[Utility bookingID] integerValue] != status && [[Utility bookingID] integerValue] < status)
        return YES;
    else
        return NO;
}

-(BOOL)checkIfBokkedOrNot {
    if([Utility isUserInBooking])
        return YES;
    else
        return NO;
}

-(BOOL)checkServiceCalledAtleastOnce {
    BOOL isCalledOnce = [[NSUserDefaults standardUserDefaults] boolForKey:@"isServiceCalledOnce"];
    if(isCalledOnce)
        return YES;
    else
        return NO;
}

-(BOOL)checkBookingIDForCancel:(NSInteger)status {
    if([[Utility bookingID] integerValue] == status)
        return YES;
    else
        return NO;
}


#pragma mark - PubNub Methods -

-(void)subsCribeToPubNubChannel:(NSString*)channel {
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub subscribeToChannel:channel];
    [pubNub setDelegate:self];
}

-(void)unSubsCribeToPubNubChannel:(NSString *)channel {
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub unsubscribeFromChannel:channel];
    [pubNub setDelegate:self];
}

-(void)subscribeToPassengerChannel {
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub subscribeToUserChannel];
    [pubNub setDelegate:self];
}

-(void) unSubscribeToPassengerChannel {
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub unsubscribeFromMyChannel];
    [pubNub setDelegate:self];
}

-(void)subscribeOnlyBookedDriver {
    if ([[Utility driverPubnubChannel] length])
        [self subsCribeToPubNubChannel:[Utility driverPubnubChannel]];
}

-(void) unSubscribeOnlyBookedDriver {
    if ([[Utility driverPubnubChannel] length])
        [self unSubsCribeToPubNubChannel:[Utility driverPubnubChannel]];
}

/**
 *  publishPubNubStream to receiving masters near around user current location
 */
-(void)publishPubNubStream {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if (isMapMoving) {
        return;
    }
    int  status;
    if (isNowSelected)
        status  =  kAppointmentTypeNow;
    else
        status =  kAppointmentTypeLater;
    NSString *dt;
    if(status == kAppointmentTypeLater){
        dt = laterSelectedDateServer;
    }
    else {
        dt = @"";
    }
    if (dt.length == 0) {
        dt = @"";
    }
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if ([srcAddrline2 rangeOfString:srcAddr].location == NSNotFound) {
        if(srcAddr.length)
            [arr addObject:srcAddr];
        if (srcAddrline2.length)
            [arr addObject:srcAddrline2];
    } else {
        if (srcAddrline2.length)
            [arr addObject:srcAddrline2];
    }
    NSString *pickUpAddress = [arr componentsJoinedByString:@", "];
    if ([reachability isNetworkAvailable]) {
        NSDictionary *message = @{@"a":[NSNumber numberWithDouble:kPubNubStartStreamAction],
                                  @"pid": flStrForStr([Utility userEmailID]),
                                  @"lt": [NSNumber numberWithDouble:srcLat],
                                  @"lg": [NSNumber numberWithDouble:srcLong],
                                  @"addr":flStrForStr(pickUpAddress),
                                  @"chn": flStrForObj([Utility userPubnubChannel]),
                                  @"st": [NSNumber numberWithInt:kAppointmentTypeNow],
                                  @"tp":[NSNumber numberWithInteger:carTypesForLiveBookingServer],
                                  @"dt":dt,
                                  @"sid":flStrForStr([Utility userID])
                                  };
        if (!(srcLat == 0 || srcLong == 0)) {
            if ([[Utility serverPubnubChannel] length] == 0) {
                return;
            }
            NSLog(@"Pubnub Publish Message =%@", message);
            [pubNub publishWithParameter:message toChannel:[Utility serverPubnubChannel]];
        }
    }
}

-(void)publishA3ToGetBookingData {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        NSDictionary *message = @{@"a":[NSNumber numberWithDouble:kPubNubGetAppointmentDetails],
                                  @"sid": flStrForStr([Utility userID]),
                                  @"chn": flStrForObj([Utility userPubnubChannel]),
                                  };
        if (!(srcLat == 0 || srcLong == 0)) {
            if ([[Utility serverPubnubChannel] length] == 0) {
                return;
            }
            NSLog(@"Pubnub Publish Message =%@", message);
            [pubNub publishWithParameter:message toChannel:[Utility serverPubnubChannel]];
        }
    }
}

/**
 *  Stop the receiving data from pubnub server
 */
-(void)stopPubNubStream {
    pubNub = [PubNubWrapper sharedInstance];
    pubNub.delegate = nil;
}


#pragma mark - Pubnub Delegate Method -

- (void)receivedMessage:(NSDictionary *) messageDict andChannel:(NSString *)channelName {
    NSLog(@"Pubnub Receive Data =%@ on Channel =%@", messageDict, channelName);
    if (![messageDict isKindOfClass:[NSDictionary class]] || messageDict == nil) {
        return;
    } else {
        NSInteger status = [flStrForStr(messageDict[@"a"]) integerValue];
        switch (status) {
            case 2:
                if ([channelName isEqualToString:[Utility userPubnubChannel]]) {
                    [self handleMessageA2ComesHaere:messageDict];
                }
                break;
            case 4:
                [self messageA4ComesHere:messageDict];
                break;
            case 5:
                [self messageA5ComesHere:messageDict];
                break;
            case 6:
                [self messageA6ComesHere:messageDict];
                break;
            case 7:
                [self messageA7ComesHere:messageDict];
                break;
            case 8:
                [self messageA8ComesHere:messageDict];
                break;
            case 9:
                [self messageA9Comeshere:messageDict];
                break;
            case 10:
                [self messageA10Comeshere:messageDict];
                break;
            default:
                break;
        }
        if (![channelName isEqualToString:[Utility userPubnubChannel]] && [Utility isUserInBooking]) {
            [self updatedDrivermarkerInLiveBooking:messageDict];
        }
    }
}


#pragma mark - Pubnub Custom Methods -
//-(void) handleMessageA2ComesHaere:(NSDictionary *)messageDict {
//    if ([messageDict[@"bookingStatus"] integerValue] == 1) {
//        isInBooking = 1;
//        [[MyAppTimerClass sharedInstance] startPublishTimer];
//        if (addProgressBarView) {
//            [self removeAddProgressView];
//        }
//        [self publishPubNubStream];
//        [[MyAppTimerClass sharedInstance] startPublishTimer];
//        [[MyAppTimerClass sharedInstance] stopPublishTimer];
//        [allMarkers removeAllObjects];
//    } else if ([[messageDict objectForKey:@"flag"] intValue] == 1) {
//        isInBooking = 0;
//        NSDictionary *bookingData = [[NSUserDefaults standardUserDefaults] objectForKey:@"BookingData"];
//        if(bookingData != nil) {
//            [self initiateSetViewController];
//            [self clearUserDefaultsAfterBookingCompletion];
//        }
//        [self messageA2ComeshereWithFlag1:messageDict];
//        [self messageA2ComeshereWithCarTypes:messageDict];
//    } else if ([[messageDict objectForKey:@"flag"] intValue] == 0) {
//        isInBooking = 0;
//        NSDictionary *bookingData = [[NSUserDefaults standardUserDefaults] objectForKey:@"BookingData"];
//        if(bookingData != nil) {
//            [self initiateSetViewController];
//            [self clearUserDefaultsAfterBookingCompletion];
//        }
//        maximumETA = [messageDict [@"eta"] integerValue]*60;
//        bookingAmountAllowUpto = -[messageDict [@"bookingAmountAllowUpto"] doubleValue];
//        if ([Helper checkForUpdateAppVersion:messageDict[@"versions"][@"iosCust"]]) {
//            [[MyAppTimerClass sharedInstance] stopPublishTimer];
//            return;
//        }
//        BOOL pubnubDataChanged = [self messageA2ForTestingAnyChanges:messageDict];
//        if (pubnubDataChanged) {
//            [arrayOfDriverID removeAllObjects];
//            [arrayOfFirstOnlineDrivers removeAllObjects];
//            arrayOfDriverID = nil;
//            arrayOfFirstOnlineDrivers = nil;
//            [Helper setToLabel:centerPickUpObj.timeLabel Text:NSLocalizedString(@"No Drivers", @"No Drivers") WithFont:fontNormal FSize:7 Color:[UIColor whiteColor]];
//            [self messageA2ComeshereWithCarTypes:messageDict];
//            [self messageA2SortingDriversWithShortestDuration:messageDict];
//            [self changeMapMarker:carTypesForLiveBooking-1];
//        }
//    }
//}

-(void) handleMessageA2ComesHaere:(NSDictionary *)messageDict {
    if ([messageDict[@"bookingStatus"] integerValue] == 1) {
        if (addProgressBarView) {
            [self removeAddProgressView];
        }
        [self publishA3ToGetBookingData];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [allMarkers removeAllObjects];
    } else if ([[messageDict objectForKey:@"flag"] intValue] == 1) {
        [Helper showToastWithMessage:NSLocalizedString(@"Sorry we don't operate in your area yet, please contact www.ziride.com to request our service in your area", @"Sorry we don't operate in your area yet, please contact www.ziride.com to request our service in your area") onView:self.view];
        [Utility setOpratingTypeArea:kNotOperatingArea];
        if([Utility bookingData] != nil) {
            [self initiateSetViewController];
            [self clearUserDefaultsAfterBookingCompletion];
        }
        [Utility setOpratingTypeArea:kNotOperatingArea];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [self messageA2ComeshereWithFlag1:messageDict];
        [self messageA2ComeshereWithCarTypes:messageDict];
    } else if ([[messageDict objectForKey:@"flag"] intValue] == 0) {
        [Helper removeToastMessageOnView:self.view];
        [Utility setOpratingTypeArea:kOperatingArea];
        if([Utility bookingData] != nil) {
            [self initiateSetViewController];
            [self clearUserDefaultsAfterBookingCompletion];
        }
        [self messageA2ForTestingAnyChanges:messageDict];
        [self messageA2ComeshereWithCarTypes:messageDict];
        
        if (arrayOfButtons.count && arrayOfMastersAround.count) {
            arrayOfFirstOnlineDrivers = [NSMutableArray new];
            arrayOfFirstOnlineDriversButton = [NSMutableArray new];
            arrayOfFirstOfflineDriversButton = [NSMutableArray new];
            for (int i = 0; i < arrayOfButtons.count; i++) {
                if ([arrayOfMastersAround[i][@"mas"] count]) {
                    [arrayOfFirstOnlineDrivers addObject:[arrayOfMastersAround[i][@"mas"] firstObject]];
                    [arrayOfFirstOnlineDriversButton addObject:arrayOfButtons[i]];
                } else {
                    [arrayOfFirstOfflineDriversButton addObject:arrayOfButtons[i]];
                }
            }
        }
        if (arrayOfFirstOnlineDrivers.count) {
            [self startSpinitAgain];
            [self calculateTimeAndDistanceToArrangeDriversArray:[arrayOfFirstOnlineDrivers valueForKey:@"loc"] andDriverIDsArray:arrayOfFirstOnlineDrivers];
        }
        arrayOfDriverID = [[NSMutableArray alloc] initWithArray:[messageDict[@"DriverList"] componentsSeparatedByString:@","]];
    } else {
        [self clearTheMapBeforeChagingTheCarTypes];
    }
}

-(void) messageA2ForTestingAnyChanges:(NSDictionary *)messageDict {
    NSMutableArray *newArrayOfDrivers = [messageDict[@"masArr"] mutableCopy];
    if (!arrayOfMastersAround) {
        arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        [self changeMapMarker:carTypesForLiveBooking-1];
    } else if (newArrayOfDrivers.count != arrayOfMastersAround.count) {
        if(arrayOfMastersAround) {
            [arrayOfMastersAround removeAllObjects];
            arrayOfMastersAround = nil;
            arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        } else {
            arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        }
        NSMutableArray *arrOfDriverChannel = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in arrayOfMastersAround) {
            if ([dict[@"mas"] count]) {
                arrOfDriverChannel = [dict[@"mas"] valueForKeyPath:@"chn"];
            }
        }
        NSMutableArray *newDriverChannel = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in newArrayOfDrivers) {
            if ([dict[@"mas"] count]) {
                newDriverChannel = [dict[@"mas"] valueForKeyPath:@"chn"];
            }
        }
        for(NSString *channel in arrOfDriverChannel) {
            if (![newDriverChannel containsObject:channel]) {
                GMSMarker *marker =  allMarkers[channel];
                [self removeMarker:marker];
                if(allMarkers.count)
                    [allMarkers removeObjectForKey:channel];
            }
        }
        [self changeMapMarker:carTypesForLiveBooking-1];
    } else if (newArrayOfDrivers.count == arrayOfMastersAround.count && ![newArrayOfDrivers isEqualToArray: arrayOfMastersAround]) {
        NSMutableArray *arrOfDriverChannel = [[NSMutableArray alloc] init];
        NSMutableArray *newDriverChannel = [[NSMutableArray alloc] init];
        NSArray *oldArrayData = [[NSArray alloc] initWithArray:arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
        NSArray *newArrayData = [[NSArray alloc] initWithArray:newArrayOfDrivers[carTypesForLiveBooking-1][@"mas"]];
        if ([oldArrayData count]) {
            arrOfDriverChannel = [oldArrayData valueForKeyPath:@"chn"];
        }
        if ([newArrayData count]) {
            newDriverChannel = [newArrayData valueForKeyPath:@"chn"];
        }
        for(NSString *channel in arrOfDriverChannel) {
            if (![newDriverChannel containsObject:channel]) {
                GMSMarker *marker =  allMarkers[channel];
                [self removeMarker:marker];
                if(allMarkers.count)
                    [allMarkers removeObjectForKey:channel];
            }
        }
        [arrayOfMastersAround removeAllObjects];
        arrayOfMastersAround = nil;
        arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        [self changeMapMarker:carTypesForLiveBooking-1];
    }
}

-(void) messageA2ComeshereWithCarTypes:(NSDictionary *)messageDict {
    NSArray *carTypesArray = messageDict[@"types"];
    if (carTypesArray.count > 0) {
        if(!arrayOfCarTypes) {
            arrayOfCarTypes = [carTypesArray mutableCopy];
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [[NSUserDefaults standardUserDefaults] setObject:arrayOfCarTypes forKey:kNSUCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [scroller removeFromSuperview];
            scroller = nil;
            [shadowViewForScroller removeFromSuperview];
            shadowViewForScroller = nil;
            [self addBottomView];
        }
        if (carTypesArray.count != arrayOfCarTypes.count) {
            [arrayOfCarTypes removeAllObjects];
            arrayOfCarTypes = nil;
            arrayOfCarTypes = [carTypesArray mutableCopy];
            if ([messageDict[@"types"] count] < carTypesForLiveBooking) {
                carTypesForLiveBooking = 1;
            }
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [[NSUserDefaults standardUserDefaults] setObject:arrayOfCarTypes forKey:kNSUCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [scroller removeFromSuperview];
            scroller = nil;
            [shadowViewForScroller removeFromSuperview];
            shadowViewForScroller = nil;
            [self addBottomView];
        } else if (carTypesArray.count == arrayOfCarTypes.count && ![carTypesArray isEqualToArray:arrayOfCarTypes]) {
            [arrayOfCarTypes removeAllObjects];
            arrayOfCarTypes = nil;
            arrayOfCarTypes = [carTypesArray mutableCopy];
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [[NSUserDefaults standardUserDefaults] setObject:arrayOfCarTypes forKey:kNSUCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [scroller removeFromSuperview];
            scroller = nil;
            [shadowViewForScroller removeFromSuperview];
            shadowViewForScroller = nil;
            [self addBottomView];
        }
    } else {
        if (!isComingTocheckCarTypes) {
            isComingTocheckCarTypes = YES;
            [arrayOfCarTypes removeAllObjects];
            arrayOfCarTypes = nil;
            for (UIView *va in [self.view subviews]) {
                if (va.tag == bottomViewWithCarTag) {
                    [va removeFromSuperview];
                }
            }
        }
    }
}

-(void)messageA2ComeshereWithFlag1:(NSDictionary *)messageDict {
    arrayOfMastersAround = [NSMutableArray new];
    arrayOfCarTypes = [NSMutableArray new];
    [self clearTheMapBeforeChagingTheCarTypes];
    [self hideAcitvityIndicator];
    [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"Sorry, we are not available in your area.", @"Sorry, we are not available in your area.") On:self.view];
    [[MyAppTimerClass sharedInstance] stopSpinTimer];
}

-(void)messageA4ComesHere:(NSDictionary *)responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if ([responseDict[@"status"] intValue] == 3) { // Rejected
    } else {
        [self stopTimer];
        if (!requestingView) {
            requestingView = [ProviderRequestingView sharedInstance];
        }
        [requestingView stopTimer];
        requestingView = nil;
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        pathArray = [[NSMutableArray alloc] init];
        centerPickUpObj.hidden = YES;
        UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
        [currentPickUpMarkerView removeFromSuperview];
        UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
        [bottomContainerView removeFromSuperview];
        [shadowViewForScroller removeFromSuperview];
        UIView *bookingOptionView =[self.view viewWithTag:bookingOptionViewTag];
        [bookingOptionView removeFromSuperview];
        UIView *currentLocationView = [self.view viewWithTag:loctionButtonsTag];
        [currentLocationView removeFromSuperview];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        UIView *confirmBottomView = [self.view viewWithTag:confirmationBottomViewTag];
        [confirmBottomView removeFromSuperview];
        [allMarkers removeAllObjects];
        UIView *locationView = [self.view viewWithTag:customLocationViewTag];
        [locationView removeFromSuperview];
        [self addPickUpAndDropOffLocationViewInBookingDetails];
        [self addCustomNavigationBar];
        [self removeAddProgressView];
        [self stopBackgroundTask];
        [self addCustomNavigationBar];
        NSDictionary *dictionary =  responseDict[@"data"];
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked){
        } else {
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [self bookingAccetedBySomeDriverScreenLayout];
        }
        [Utility setBookingData:dictionary];
        shareUrlViaSMS = dictionary[@"share"];
        NSInteger newBookingStatus = [flStrForStr(dictionary[@"status"]) integerValue];
        
        _driverCurLat = [dictionary[@"driverLat"] doubleValue];
        _driverCurLong = [dictionary[@"driverLong"] doubleValue];
        
        _currentLatitude = [dictionary[@"pickLat"] doubleValue];
        _currentLongitude = [dictionary[@"pickLong"] doubleValue];
        srcLat = _currentLatitude;
        srcLong = _currentLongitude;
        
        desAddr = dictionary[@"dropAddr1"];
        desAddrline2 = dictionary[@"dropAddr2"];
        desLat = [dictionary[@"desLat"] doubleValue];
        desLong = [dictionary[@"desLong"] doubleValue];
        
        [Utility setPickUpLatitude:_currentLatitude];
        [Utility setPickUpLongitude:_currentLongitude];
        [Utility setDropOffLatitude:desLat];
        [Utility setDropOffLongitude:desLong];
        [Utility setDriverLatitude:_driverCurLat];
        [Utility setDriverLongitude:_driverCurLong];
        
        [Utility setPickUpLocationAddressLine1:flStrForStr(dictionary[@"addr1"])];
        [Utility setPickUpLocationAddressLine2:flStrForStr(dictionary[@"addr2"])];
        [Utility setFavouritePickUpLocation:flStrForStr(dictionary[@"favPickUpLocationType"])];
        [Utility setDropOffLocationAddressLine1:flStrForStr(dictionary[@"dropAddr1"])];
        [Utility setPickUpLocationAddressLine2:flStrForStr(dictionary[@"dropAddr2"])];
        [Utility setFavouriteDropOffLocation:flStrForStr(dictionary[@"favDropOffLocationType"])];
        
        if (newBookingStatus == 5) {
            [self clearUserDefaultsAfterBookingCompletion];
            newBookingStatus = 10;
        } else if (newBookingStatus == 4) {
            [self clearUserDefaultsAfterBookingCompletion];
            newBookingStatus = 11;
        }
        if (newBookingStatus == 6 || newBookingStatus == 7 || newBookingStatus == 8 || (newBookingStatus == 9 )) {
            [[MyAppTimerClass sharedInstance] stopPublishTimer];
            [Utility setBookingStatus:newBookingStatus];
            [Utility setBookingID:flStrForStr(dictionary[@"bid"])];
            [Utility setBookingDate:flStrForStr(dictionary[@"apptDt"])];
            
            [Utility setDriverID:flStrForStr(dictionary[@"mid"])];
            [Utility setDriverEmailID:flStrForStr(dictionary[@"email"])];
            [Utility setDriverPubnubChannel:flStrForStr(dictionary[@"chn"])];
            [Utility setDriverMobileNumber:flStrForStr(dictionary[@"mobile"])];
            [Utility setDriverProfileImageURL:flStrForStr(dictionary[@"pPic"])];
            [Utility setDriverRating:flStrForStr(dictionary[@"r"])];
            [Utility setDriverName:[NSString stringWithFormat:@"%@ %@",flStrForStr(dictionary[@"fName"]), flStrForStr(dictionary[@"lName"])]];
            
            [Utility setVehicleColor:flStrForStr(dictionary[@"color"])];
            [Utility setVehicleMakeName:flStrForStr(dictionary[@"make"])];
            [Utility setVehicleModelName:flStrForStr(dictionary[@"model"])];
            [Utility setVehicleImageURL:flStrForStr(dictionary[@"carImage"])];
            [Utility setVehicleNumberPlate:flStrForStr(dictionary[@"plateNo"])];
            [Utility setVehicleMapImageURL:flStrForStr(dictionary[@"carMapImage"])];
            [Utility setIsUserInBooking:YES];
            
            if (_isPathPlotted == NO) {
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_driverCurLat,_driverCurLong);
                GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:position];
                [mapView_ animateWithCameraUpdate:locationUpdate];
            }
            
            if(newBookingStatus == 9) {
                [self unSubscribeOnlyBookedDriver];
                [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
            } else if(newBookingStatus == 6 || newBookingStatus == 7 || newBookingStatus == 8) {
                [self subscribeOnlyBookedDriver];
                if([[dictionary allKeys] containsObject:@"typeId"])
                    carTypesForLiveBookingServer = [flStrForStr(dictionary[@"typeId"]) integerValue];
                [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
            }
            [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:dictionary :(int)newBookingStatus];
        } else {
            [self changeContentOfPresentController:nil];
        }
    }
}

-(void)messageA5ComesHere:(NSDictionary *)messageDict {
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    BOOL isDriverCancel = [[NSUserDefaults standardUserDefaults]boolForKey:@"isDriverCanceledBookingOnce"];
    if (!isDriverCancel) {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDriverCanceledBookingOnce"];
    }
    if (isDriverCancel && isAlreadyBooked) {
        [self unSubscribeOnlyBookedDriver];
        [self clearUserDefaultsAfterBookingCompletion];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
        [self bookingNotAccetedByAnyDriverScreenLayout];
        
        [self createCenterView];
        int newBookingStatus =  10;
        NSDictionary *dictReason = @{@"r": flStrForStr(messageDict[@"r"])};
        [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:dictReason :newBookingStatus];
    }
}

-(void)messageA6ComesHere:(NSDictionary *)messageDict {
    float latitude = [messageDict[@"lt"] floatValue];
    float longitude = [messageDict[@"lg"] floatValue];
    BOOL isNewBooking = [self checkBookingID:[messageDict[@"bid"] integerValue]];
    if (isNewBooking) {
        [allMarkers removeAllObjects];
        isRequestingButtonClicked = NO;
        [Utility setBookingID:flStrForStr(messageDict[@"bid"])];
        [Utility setIsUserInBooking:YES];
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked){
        } else if(!isAlreadyBooked){
        } else {
            [Utility setDriverEmailID:flStrForStr(messageDict[@"e_id"])];
            BOOL isAlreadyCalled = [self checkServiceCalledAtleastOnce];
            if (!isAlreadyCalled)
                [self publishA3ToGetBookingData];
            UIView *locationView = [self.view viewWithTag:customLocationViewTag];
            [locationView removeFromSuperview];
            [self addPickUpAndDropOffLocationViewInBookingDetails];
        }
    } else if([messageDict[@"bid"] integerValue] != [[Utility bookingID] integerValue]) {
        return;
    }
    _driverCurLat = [messageDict[@"lt"] doubleValue];
    _driverCurLong = [messageDict[@"lg"] doubleValue];
    [Utility setDriverLatitude:_driverCurLat];
    [Utility setDriverLongitude:_driverCurLong];
    [self fitBoundsWithAllMarkers];
//    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_driverCurLat,_driverCurLong);
//    GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:position];
//    [mapView_ animateWithCameraUpdate:locationUpdate];
    [self updateDestinationLocationWithLatitude:latitude Longitude:longitude];
}

-(void)messageA7ComesHere:(NSDictionary *)messageDict {
    if([messageDict[@"bid"] integerValue] != [[Utility bookingID] integerValue]) {
        return;
    }
    [allMarkers removeAllObjects];
    BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingReachedLocation];
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    if (isAlreadyCame && isAlreadyBooked) {
    } else if(!isAlreadyBooked) {
    } else {
        [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingReachedLocation];
    }
    _driverCurLat = [messageDict[@"lt"] doubleValue];
    _driverCurLong = [messageDict[@"lg"] doubleValue];
    [Utility setDriverLatitude:_driverCurLat];
    [Utility setDriverLongitude:_driverCurLong];
    [self fitBoundsWithAllMarkers];
//    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_driverCurLat,_driverCurLong);
//    GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:position];
//    [mapView_ animateWithCameraUpdate:locationUpdate];
    [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
}

-(void)messageA8ComesHere:(NSDictionary *)messageDict {
    if([messageDict[@"bid"] integerValue] != [[Utility bookingID] integerValue]) {
        return;
    }
    [allMarkers removeAllObjects];
    BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingStarted];
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    if (isAlreadyCame && isAlreadyBooked) {
    } else if(!isAlreadyBooked) {
    } else {
        [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
    }
    _driverCurLat = [messageDict[@"lt"] doubleValue];
    _driverCurLong = [messageDict[@"lg"] doubleValue];
    [Utility setDriverLatitude:_driverCurLat];
    [Utility setDriverLongitude:_driverCurLong];
    [self fitBoundsWithAllMarkers];
//    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_driverCurLat,_driverCurLong);
//    GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:position];
//    [mapView_ animateWithCameraUpdate:locationUpdate];
    [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
}

-(void)messageA9Comeshere:(NSDictionary *)messageDict {
    if([messageDict[@"bid"] integerValue] != [[Utility bookingID] integerValue]) {
        return;
    }
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    BOOL isNewBooking = [self checkBookingID:[messageDict[@"bid"] integerValue]];
    if (isNewBooking || isAlreadyBooked) {
        [allMarkers removeAllObjects];
        _driverCurLat = [messageDict[@"lt"] doubleValue];
        _driverCurLong = [messageDict[@"lg"] doubleValue];
        [Utility setDriverLatitude:_driverCurLat];
        [Utility setDriverLongitude:_driverCurLong];
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingComplete];
        if (isAlreadyCame && isAlreadyBooked) {
        } else if(!isAlreadyBooked) {
        } else {
            [self publishA3ToGetBookingData];
            [self pubnubStreamTimer];
            isInBooking = 1;
            [[MyAppTimerClass sharedInstance] startPublishTimer];
        }
    }
}

-(void)messageA10Comeshere:(NSDictionary *)messageDict {
    if ([[Utility bookingID] integerValue] == [messageDict[@"bid"] integerValue]) {
        [self stopTimer];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
        [self removeAddProgressView];
        [self bookingNotAccetedByAnyDriverScreenLayout];
        arrayOfMastersAround = [NSMutableArray new];
        [self clearTheMapBeforeChagingTheCarTypes];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [self publishPubNubStream];
        [[MyAppTimerClass sharedInstance] startPublishTimer];
        [self stopBackgroundTask];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:messageDict[@"msg"]];
    }
}

/**
 *  Update driver car marker while live tracking.
 *
 *  @param messageDict pubnub response from driver
 */
-(void) updatedDrivermarkerInLiveBooking:(NSDictionary *)messageDict {
    if ([messageDict[@"a"] integerValue] == 6  || [messageDict[@"a"] integerValue] == 7 || [messageDict[@"a"] integerValue] == 8  || [messageDict[@"a"] integerValue] == 9) {
        if ([messageDict[@"bid"] integerValue] == [[Utility bookingID] integerValue]) {
            _driverCurLat = [messageDict[@"lt"] doubleValue];
            _driverCurLong = [messageDict[@"lg"] doubleValue];
            [Utility setDriverLatitude:_driverCurLat];
            [Utility setDriverLongitude:_driverCurLong];
            [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
        }
    }
}



#pragma Mark- Path Plotting

-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude :(int)type {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:mapZoomLevel];
    [mapView_ setCamera:camera];
    if (type == 2) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
        sourceMarker = [GMSMarker markerWithPosition:position];
        sourceMarker.icon = [UIImage imageNamed:@"default_marker_p"];
        sourceMarker.map = mapView_;
        [waypoints_ addObject:sourceMarker];
    } else if (type == 3) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
        destinationMarker = [GMSMarker markerWithPosition:position];
        destinationMarker.icon = [UIImage imageNamed:@"default_marker_d"];
        destinationMarker.map = mapView_;
        [waypoints_ addObject:destinationMarker];
    }
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
}

/**
 *  Update Destination of an incoming Doctor
 *  Returns void and accept two arguments
 *  @param latitude  Doctor Latitude
 *  @param longitude incoming Doctor longitude
 */

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude {
    if (!_isPathPlotted) {
        _isPathPlotted = YES;
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        _previouCoord = position;
        bookedCarMarker = [GMSMarker markerWithPosition:position];
        bookedCarMarker.flat = YES;
        bookedCarMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        NSString *markerIconUrl = [Utility vehicleMapImageURL];
        if(markerIconUrl.length && arrayOfCarTypes.count) {
            markerIconUrl = [NSString stringWithFormat:@"%@", arrayOfCarTypes[carTypesForLiveBooking-1][@"MapIcon"]];
        }
        if ([PMDReachabilityWrapper sharedInstance].isNetworkAvailable) {
            [self downloadDriverCarImage:markerIconUrl markerName:bookedCarMarker];
        } else {
            _isPathPlotted = NO;
        }
    } else {
        if (bookedCarMarker == nil) {
            _isPathPlotted = NO;
            return;
        }
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        if (position.latitude == _previouCoord.latitude && position.longitude == _previouCoord.longitude)
            return;
        [CATransaction begin];
        [CATransaction setAnimationDuration:5.0];
        bookedCarMarker.position = position;
        [CATransaction commit];
        if (heading > 0)
            bookedCarMarker.rotation = heading;
        if (waypoints_.count >= 2) {
            [waypoints_ removeObjectAtIndex:0];
            [waypointStrings_ removeObjectAtIndex:0];
        }
        [CATransaction begin];
        [CATransaction setAnimationDuration:7.0];
        [waypoints_ addObject:bookedCarMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude];
        [waypointStrings_ addObject:positionString];
        if([waypoints_ count] > 1) {
            waypoints_ = [[[waypoints_ reverseObjectEnumerator] allObjects] mutableCopy];
            waypointStrings_ = [[[waypointStrings_ reverseObjectEnumerator] allObjects] mutableCopy];
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            [mds setDirectionsQuery:query
                       withSelector:@selector(addDirections:)
                       withDelegate:self];
        }
        [CATransaction commit];
        _previouCoord = position;
    }
}


-(void) downloadDriverCarImage:(NSString *) markerURL markerName:(GMSMarker *)marker {
    UIImageView *markerImageView;
    if (!markerImageView){
        markerImageView = [[UIImageView alloc] init];
    }
    if (markerURL.length){
        [markerImageView sd_setImageWithURL:[NSURL URLWithString:markerURL]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                      if(image) {
                                          image = [Helper imageWithImage:image scaledToSize:[Helper makeSize:image.size fitInSize:CGSizeMake(35, 40)]];
                                          marker.icon = image;
                                          marker.map = mapView_;
                                          [waypoints_ addObject:marker];
                                          NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",marker.position.latitude,marker.position.longitude];
                                          [waypointStrings_ addObject:positionString];
                                          if([waypoints_ count] > 1) {
                                              waypoints_ = [[[waypoints_ reverseObjectEnumerator] allObjects] mutableCopy];
                                              waypointStrings_ = [[[waypointStrings_ reverseObjectEnumerator] allObjects] mutableCopy];
                                              
                                              NSString *sensor = @"false";
                                              NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_, nil];
                                              NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
                                              NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters forKeys:keys];
                                              DirectionService *mds=[[DirectionService alloc] init];
                                              SEL selector = @selector(addDirections:);
                                              [mds setDirectionsQuery:query
                                                         withSelector:selector
                                                         withDelegate:self];
                                          }
                                          return;
                                      }
                                  }];
    }
    if (marker.icon == nil){
        _isPathPlotted = NO;
    }
}

- (void)addDirections:(NSDictionary *)json{
    if ([json[@"routes"] count] > 0){
        if(pathArray.count == 0) {
            polyline1.map = nil;
            polyline2.map = nil;
        }
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        if (polyline1.map == nil) {
            polyline1 = [GMSPolyline polylineWithPath:path];
            [polyline1 setStrokeWidth:3.0];
            [polyline1 setStrokeColor:UIColorFromRGB(0x484848)];
            polyline1.map = mapView_;
            [pathArray addObject:polyline1];
        } else {
            polyline2 = [GMSPolyline polylineWithPath:path];
            [polyline2 setStrokeWidth:3.0];
            [polyline2 setStrokeColor:UIColorFromRGB(0x484848)];
            polyline2.map = mapView_;
            [pathArray addObject:polyline2];
        }
        if (pathArray.count == 2) {
            GMSPolyline *removablePolyline = pathArray[0];
            [CATransaction begin];
            [CATransaction setAnimationDuration:10.0];
            removablePolyline.map = nil;
            [CATransaction commit];
            [pathArray removeObjectAtIndex:0];
        }
        [self fitBoundsWithAllMarkers];
    }
}


-(void)fitBoundsWithAllMarkers {
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    if (sourceMarker) {
        NSLog(@"Source Marker Position = %f %f", sourceMarker.position.latitude, sourceMarker.position.longitude);
        sourceMarker.icon = [UIImage imageNamed:@"default_marker_p"];
        bounds = [bounds includingCoordinate:sourceMarker.position];
    }
    if (destinationMarker) {
        destinationMarker.icon = [UIImage imageNamed:@"default_marker_d"];
        bounds = [bounds includingCoordinate:destinationMarker.position];
    }
    if (bookedCarMarker) {
        bounds = [bounds includingCoordinate:bookedCarMarker.position];
    }
    UIView *locationView = [self.view viewWithTag:customLocationViewTag];
    CGFloat topMargin = CGRectGetMaxY(locationView.frame);
    UIView *driverArrivedView = [self.view viewWithTag:driverArrivedViewTag];
    CGFloat bottomMargin = driverArrivedView.frame.size.height;
    UIEdgeInsets mapInsets = UIEdgeInsetsMake(topMargin+5, 10.0, bottomMargin-10, 10.0);
    mapView_.padding = mapInsets;
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

-(void)addgestureToMap {
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype) {
        if (touchtype == 2) {
            [weakSelf startAnimation];
        } else if (touchtype == 3){
            [weakSelf performSelector:@selector(endAnimation) withObject:nil afterDelay:0.5];
        } else {
            [weakSelf endAnimation];
        }
    };
    [mapView_  addGestureRecognizer:_tapInterceptor];
}

#pragma mark GMSMapviewDelegate -


- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture
{
    if (gesture) {
        [centerPickUpObj collapse];
        isMapMoving = YES;
        previousLocation = [[CLLocation alloc] initWithLatitude:_currentLatitude longitude:_currentLongitude];
    }
    if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen) {
        isLocationChanged = isFixed;
    } else {
        if(isFareButtonClicked == NO) {
            isLocationChanged = isChanging;
        } else {
            isFareButtonClicked = NO;
        }
    }
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position
{
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
    isLocationChanged = isChanging;
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    NSInteger bookingStatus = [Utility bookingStatus];
    if ((bookingStatus == kNotificationTypeBookingOnMyWay) || (bookingStatus == kNotificationTypeBookingReachedLocation) || (bookingStatus == kNotificationTypeBookingStarted)) {
        return;
    } else {
        isMapMoving = NO;
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
        if (_isAddressManuallyPicked) {
            _isAddressManuallyPicked = NO;
            return;
        } else {
            [self getAddress:coor];
            [self startSpinitAgain];
        }
    }
    [centerPickUpObj expand];
}

#pragma mark Custom Methods -

/**
 Add GPS Button, Satellite View Button and Traffic mode Button
 */
-(void)addLocationButtonsOnMap {
    UIView *vb = [self.view viewWithTag:loctionButtonsTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = self.view.frame.size.height;
        UIView *customView;
        if (isCustomMarkerSelected) {
            customView = [[UIView alloc]initWithFrame:CGRectMake(screenWidth - 45, screenHeight - 140 - 150, 40, 140)];
        } else if (kBookLater) {
            customView = [[UIView alloc]initWithFrame:CGRectMake(screenWidth - 45, screenHeight - 140 - 135, 40, 140)];
        } else {
            customView = [[UIView alloc]initWithFrame:CGRectMake(screenWidth - 45, screenHeight - 140 - 85, 40, 140)];
        }
        customView.tag = loctionButtonsTag;
        [customView setHidden:NO];
        customView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:customView];

        UIButton *trafficBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        trafficBtn.frame = CGRectMake(0, 5, 40, 40);
        trafficBtn.tag = 32;
        [Helper setButton:trafficBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [trafficBtn setImage:[UIImage imageNamed:@"home_traffic_icon_off"] forState:UIControlStateNormal];
        [trafficBtn setImage:[UIImage imageNamed:@"home_traffic_icon_on"] forState:UIControlStateSelected];
        [trafficBtn addTarget:self action:@selector(changeMapTrafficMode:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:trafficBtn];

        UIButton *sateliteViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sateliteViewBtn.frame = CGRectMake(0, 50, 40, 40);
        sateliteViewBtn.tag = 32;
        [Helper setButton:sateliteViewBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [sateliteViewBtn setImage:[UIImage imageNamed:@"home_satelight_icon_off"] forState:UIControlStateNormal];
        [sateliteViewBtn setImage:[UIImage imageNamed:@"home_satelight_icon_on"] forState:UIControlStateSelected];
        [sateliteViewBtn addTarget:self action:@selector(changeMapType:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:sateliteViewBtn];

        UIButton *currentLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        currentLoc.frame = CGRectMake(0, 95, 40, 40);
        currentLoc.tag = 31;
        [Helper setButton:currentLoc Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [currentLoc setImage:[UIImage imageNamed:@"home_gps_icon_on"] forState:UIControlStateNormal];
        [currentLoc setImage:[UIImage imageNamed:@"home_gps_icon_off"] forState:UIControlStateSelected];
        [currentLoc addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:currentLoc];
    }
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        UIView *locationButtonView = [self.view viewWithTag:loctionButtonsTag];
        CGRect frame = locationButtonView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        locationButtonView.frame = frame;
    }
}


/**
 Add Pick location view on Home Screen
 */
-(void) addSearchAddressView {
    UIImage *backgroundImage;
    if ([[UIScreen mainScreen] bounds].size.width == 375) {
        backgroundImage = [UIImage imageNamed:@"pickUpLocation_Background_375"];
    } else {
        backgroundImage = [UIImage imageNamed:@"pickUpLocation_Background"];
    }
    CGFloat screenWidth = backgroundImage.size.width;
    UIImage *searchImage = [UIImage imageNamed:@"home_search"];
    UIView *searchAddressView = [self.view viewWithTag:searchAddressViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:searchAddressView]) {
        UIView *addressView = [[UIView alloc] init];
        addressView.tag = searchAddressViewTag;
        addressView.backgroundColor = [UIColor colorWithPatternImage: backgroundImage];

        //Pick Up Location
        UIImageView *pickUpDotView = [[UIImageView alloc] initWithImage:searchImage];
        pickUpDotView.tag = 9101;
        [addressView addSubview:pickUpDotView];
        
        if(searchAddressTextField == nil) {
            searchAddressTextField = [[UITextField alloc] init];
        }
        searchAddressTextField.placeholder = NSLocalizedString(@"Fetching Location...", @"Fetching Location...");
        [searchAddressTextField setValue:UIColorFromRGB(0x666666) forKeyPath:@"_placeholderLabel.textColor"];
        searchAddressTextField.tag = 104;
        searchAddressTextField.enabled = NO;
        searchAddressTextField.delegate = self;
        searchAddressTextField.textAlignment = NSTextAlignmentLeft;
        searchAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        searchAddressTextField.textColor = UIColorFromRGB(0x666666);
        [addressView addSubview:searchAddressTextField];
        searchAddressView = addressView;
        [self.view addSubview:addressView];
    }
    
    UIImageView *pickUpDotView = (UIImageView *)[searchAddressView viewWithTag:9101];
    searchAddressView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - screenWidth)/2, 74, screenWidth, backgroundImage.size.height);
    CGFloat customViewWidth = searchAddressView.frame.size.width;
    pickUpDotView.frame = CGRectMake(10, (backgroundImage.size.height - searchImage.size.height)/2 - 5, searchImage.size.width, searchImage.size.height);
    searchAddressTextField.frame = CGRectMake( 50, (backgroundImage.size.height - 25)/2, customViewWidth - 50, 25);
    searchAddressTextField.textAlignment = NSTextAlignmentLeft;
    searchAddressView.alpha = 0;
    [self.view bringSubviewToFront:searchAddressView];
    
    if ([Helper isCurrentLanguageRTL]) {
        screenWidth = searchAddressView.frame.size.width;
        CGRect frame = pickUpDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpDotView.frame = frame;
        
        frame = searchAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        searchAddressTextField.frame = frame;
        searchAddressTextField.textAlignment = NSTextAlignmentRight;
    }
}

/**
 Add Pick location view on Home Screen
 */
-(void) addPickUpLocationView {
    UIImage *backgroundImage;
    if ([[UIScreen mainScreen] bounds].size.width == 375) {
        backgroundImage = [UIImage imageNamed:@"pickUpLocation_Background_375"];
    } else {
        backgroundImage = [UIImage imageNamed:@"pickUpLocation_Background"];
    }
    CGFloat screenWidth = backgroundImage.size.width;
    CGFloat screenHeight = mapView_.frame.size.height;
    UIImage *starImage = [UIImage imageNamed:@"home_star_icon_off"];
    UIImage *searchImage = [UIImage imageNamed:@"home_search"];
    UIImage *pickerImage = [UIImage imageNamed:@"map_pin"];
    
    UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:customLocationView]) {
        UIView *customView = [[UIView alloc] init];
        customView.tag = customLocationViewTag;
        customView.backgroundColor = [UIColor colorWithPatternImage: backgroundImage];
        //Pick Up Location
        UIButton *favAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        favAddressBtn.tag = 97;
        [Helper setButton:favAddressBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [favAddressBtn setImage:[UIImage imageNamed:@"home_star_icon_off"] forState:UIControlStateNormal];
        [favAddressBtn setImage:[UIImage imageNamed:@"home_star_icon_on"] forState:UIControlStateSelected];
        [favAddressBtn addTarget:self action:@selector(addLocationAsFavourite:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:favAddressBtn];
        
        UIButton *buttonSearchLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonSearchLoc.tag = 91;
        [Helper setButton:buttonSearchLoc Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [buttonSearchLoc addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:buttonSearchLoc];
        
        UIImageView *pickUpDotView = [[UIImageView alloc] initWithImage:searchImage];
        pickUpDotView.tag = 910;
        [customView addSubview:pickUpDotView];
        
        if(pickUpAddressTextField == nil) {
            pickUpAddressTextField = [[UITextField alloc] init];
        }
        pickUpAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        [pickUpAddressTextField setValue:UIColorFromRGB(0x666666) forKeyPath:@"_placeholderLabel.textColor"];
        pickUpAddressTextField.tag = 101;
        pickUpAddressTextField.enabled = NO;
        pickUpAddressTextField.delegate = self;
        pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;
        pickUpAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        pickUpAddressTextField.textColor = UIColorFromRGB(0x666666);
        [customView addSubview:pickUpAddressTextField];
        
        UILabel *labelPickUp = [[UILabel alloc] init];
        [Helper setToLabel:labelPickUp Text:NSLocalizedString(@"Your Location", @"Your Location") WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0x2598ED)];
        labelPickUp.textAlignment = NSTextAlignmentLeft;
        labelPickUp.tag = 1010;
        [customView addSubview:labelPickUp];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.tag = 101010;
        lineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"doted_line"]];
        [customView addSubview:lineView];
        
        customLocationView = customView;
        [self.view addSubview:customView];
    }
    [self addLocationButtonsOnMap];
    UIButton *favAddressBtn = (UIButton *)[customLocationView viewWithTag:97];
    UIButton *buttonSearchLoc = (UIButton *)[customLocationView viewWithTag:91];
    UIImageView *pickUpDotView = (UIImageView *)[customLocationView viewWithTag:910];
    UILabel *labelPickUp = (UILabel *)[customLocationView viewWithTag:1010];
    UIView *lineView = [customLocationView viewWithTag:101010];
    
    customLocationView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - backgroundImage.size.width)/2, 74, backgroundImage.size.width, backgroundImage.size.height);
    CGFloat customViewWidth = customLocationView.frame.size.width;
    favAddressBtn.frame = CGRectMake(customViewWidth - (starImage.size.width + 10), 0, starImage.size.width + 10, backgroundImage.size.height);
    buttonSearchLoc.frame = CGRectMake(30, 0, favAddressBtn.frame.origin.x - 40, backgroundImage.size.height);
    pickUpDotView.frame = CGRectMake(10, (backgroundImage.size.height - searchImage.size.height)/2 - 5, searchImage.size.width, searchImage.size.height);
    
    labelPickUp.frame = CGRectMake(30, 5, favAddressBtn.frame.origin.x - 40, 15);
    labelPickUp.textAlignment = NSTextAlignmentCenter;
    pickUpAddressTextField.frame = CGRectMake(50, 20, favAddressBtn.frame.origin.x - 50, backgroundImage.size.height -20-10);
    pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;
    lineView.frame = CGRectMake(customViewWidth/2, backgroundImage.size.height, 1, screenHeight/2 - CGRectGetMaxY(customLocationView.frame) - pickerImage.size.height + 5);
    [self.view bringSubviewToFront:customLocationView];
    
    if ([Helper isCurrentLanguageRTL]) {
        screenWidth = customLocationView.frame.size.width;
        CGRect frame = favAddressBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        favAddressBtn.frame = frame;
        
        frame = buttonSearchLoc.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        buttonSearchLoc.frame = frame;
        
        frame = pickUpDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpDotView.frame = frame;
        
        frame = pickUpAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpAddressTextField.frame = frame;
        pickUpAddressTextField.textAlignment = NSTextAlignmentRight;
        
        frame = labelPickUp.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        labelPickUp.frame = frame;
        labelPickUp.textAlignment = NSTextAlignmentCenter;
    }
}

/**
 Add pick up and drop off address on confirm screen.
 */
-(void)addPickUpAndDropOffLocationViewForConfirmationScreen {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat pickUpImageSize = 12.5;
    CGFloat dropOffImageSize = 12.5;
    UIImage *searchImage = [UIImage imageNamed:@"home_search"];
    UIImage *dotLineImage = [UIImage imageNamed:@"doted_line_Pickup_drop"];

    UIView *locationView = [self.view viewWithTag:customLocationViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:locationView]) {
        UIView *customLocationView = [[UIView alloc] init];
        customLocationView.tag = customLocationViewTag;
        [customLocationView setHidden:NO];
        customLocationView.backgroundColor = [UIColor whiteColor];
        [Helper addShadowToView:customLocationView withShadowOpacity:0.1];

        UIImageView *pickUpDotView = [[UIImageView alloc] init];
        pickUpDotView.tag = 910;
        pickUpDotView.backgroundColor = UIColorFromRGB(0x00A651);
        pickUpDotView.layer.cornerRadius = pickUpImageSize/2;
        pickUpDotView.layer.masksToBounds = YES;
        [customLocationView addSubview:pickUpDotView];
        
        UIImageView *dropOffDotView = [[UIImageView alloc] init];
        dropOffDotView.tag = 5050;
        dropOffDotView.backgroundColor = UIColorFromRGB(0xED1C24);
        dropOffDotView.layer.cornerRadius = dropOffImageSize/2;
        dropOffDotView.layer.masksToBounds = YES;
        [customLocationView addSubview:dropOffDotView];
        
        UIImageView *dotLineImageView = [[UIImageView alloc] init];
        dotLineImageView.tag = 50510;
        dotLineImageView.image = dotLineImage;
        [customLocationView addSubview:dotLineImageView];

        UIButton *pickUpSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pickUpSearchBtn.tag = 91;
        [Helper setButton:pickUpSearchBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
//        [pickUpSearchBtn addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customLocationView addSubview:pickUpSearchBtn];
 
        if(pickUpAddressTextField == nil) {
            pickUpAddressTextField = [[UITextField alloc] init];
        }
        pickUpAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        [pickUpAddressTextField setValue:UIColorFromRGB(0x484848) forKeyPath:@"_placeholderLabel.textColor"];
        pickUpAddressTextField.tag = 101;
        pickUpAddressTextField.enabled = NO;
        pickUpAddressTextField.delegate = self;
        pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;
        pickUpAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        pickUpAddressTextField.textColor = UIColorFromRGB(0x484848);
        [customLocationView addSubview:pickUpAddressTextField];
        
        //Drop Off View
        UIView *dropOffAddressView = [[UIView alloc] init];
        dropOffAddressView.tag = 1232145;
        dropOffAddressView.backgroundColor = UIColorFromRGB(0xE4E7F0);
        [customLocationView addSubview:dropOffAddressView];
        
        UIButton *searchDropOffLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        searchDropOffLoc.tag = 165;
        [Helper setButton:searchDropOffLoc Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [searchDropOffLoc addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [dropOffAddressView addSubview:searchDropOffLoc];

        if(dropOffAddressTextField == nil) {
            dropOffAddressTextField = [[UITextField alloc] init];
        }
        dropOffAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        [dropOffAddressTextField setValue:UIColorFromRGB(0x484848) forKeyPath:@"_placeholderLabel.textColor"];
        dropOffAddressTextField.tag = 161;
        dropOffAddressTextField.enabled = NO;
        dropOffAddressTextField.delegate = self;
        dropOffAddressTextField.textAlignment = NSTextAlignmentLeft;
        dropOffAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        dropOffAddressTextField.textColor = UIColorFromRGB(0x484848);
        [dropOffAddressView addSubview:dropOffAddressTextField];
        
        UIImageView *searchImageView = [[UIImageView alloc] initWithImage:searchImage];
        searchImageView.tag = 5051;
        [dropOffAddressView addSubview:searchImageView];
        
        locationView = customLocationView;
        [self.view addSubview:customLocationView];
    }
    
    UIButton *pickUpSearchBtn = (UIButton *)[locationView viewWithTag:91];
    UIImageView *pickUpDotView = (UIImageView *)[locationView viewWithTag:910];
    UIImageView *dropOffDotView = (UIImageView *)[locationView viewWithTag:5050];
    UIView *dropOffAddressView = (UIView *)[locationView viewWithTag:1232145];
    UIButton *dropOffSearchBtn = (UIButton *)[dropOffAddressView viewWithTag:165];
    UIImageView *searchImageView = (UIImageView *)[dropOffAddressView viewWithTag:5051];
    UIImageView *dotLineImageView = (UIImageView *)[locationView viewWithTag:50510];

    locationView.frame = CGRectMake(0, 64, screenWidth, 80);
    pickUpDotView.frame = CGRectMake(7.5, 12, pickUpImageSize, pickUpImageSize);
    dropOffDotView.frame = CGRectMake(7.5, 42 + (dropOffImageSize/2), dropOffImageSize, dropOffImageSize);
    dotLineImageView.frame = CGRectMake(7.5 +(pickUpImageSize/2) - 2, 12 + pickUpImageSize + 5, dotLineImage.size.width, dotLineImage.size.height - 4);

    pickUpSearchBtn.frame = CGRectMake(pickUpImageSize + 15 + 5, 5, screenWidth - (pickUpImageSize + 15 + 15), 30);
    pickUpAddressTextField.frame = CGRectMake(pickUpImageSize + 15 + 5, 5, screenWidth - (pickUpImageSize + 15 + 15), 30);
    pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;

    dropOffAddressView.frame = CGRectMake(dropOffImageSize + 15, 40, screenWidth - (dropOffImageSize + 15 + 15), 30);
    dropOffSearchBtn.frame = CGRectMake(0, 0, screenWidth - 40, 30);
    searchImageView.frame = CGRectMake(screenWidth - (dropOffImageSize + 15 + 15 + searchImage.size.width + 5), (30 - searchImage.size.height)/2, searchImage.size.width, searchImage.size.height);

    dropOffAddressTextField.frame = CGRectMake(5, (dropOffAddressView.frame.size.height - 30)/2, screenWidth - (dropOffImageSize + 15 + 15 + searchImage.size.width + 10), 30);
    dropOffAddressTextField.textAlignment = NSTextAlignmentLeft;
    
    [Helper addShadowToView:dropOffAddressView withShadowOpacity:1.0];

    if ([Helper isCurrentLanguageRTL]) {
        screenWidth = locationView.frame.size.width;
        CGRect frame = pickUpSearchBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpSearchBtn.frame = frame;
        
        frame = pickUpDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpDotView.frame = frame;
        
        frame = pickUpAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpAddressTextField.frame = frame;
        pickUpAddressTextField.textAlignment = NSTextAlignmentRight;
        
        frame = dropOffSearchBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffSearchBtn.frame = frame;
        
        frame = dropOffDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffDotView.frame = frame;
        
        frame = dropOffAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffAddressTextField.frame = frame;
        dropOffAddressTextField.textAlignment = NSTextAlignmentRight;

        frame = searchImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        searchImageView.frame = frame;

        frame = dotLineImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dotLineImageView.frame = frame;
    }
}

/**
 Add pick up and drop off address in booking flow screens.
 */
-(void)addPickUpAndDropOffLocationViewInBookingDetails {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIImage *pickUpImage = [UIImage imageNamed:@"pickUpLocation"];
    UIImage *dropOffImage = [UIImage imageNamed:@"dropOffLocation"];
    UIImage *dotLineImage = [UIImage imageNamed:@"doted_line_Pickup_drop"];

    UIView *locationView = [self.view viewWithTag:customLocationViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:locationView]) {
        UIView *customLocationView = [[UIView alloc] init];
        customLocationView.tag = customLocationViewTag;
        [customLocationView setHidden:NO];
        customLocationView.backgroundColor = [UIColor whiteColor];
        [Helper addBottomShadow:customLocationView withOpacity:0.5];
        
        UIButton *pickUpSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pickUpSearchBtn.tag = 91;
        [Helper setButton:pickUpSearchBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [pickUpSearchBtn addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customLocationView addSubview:pickUpSearchBtn];
        
        UIImageView *pickUpDotView = [[UIImageView alloc] initWithImage:pickUpImage];
        pickUpDotView.tag = 910;
        pickUpDotView.image = pickUpImage;
        [customLocationView addSubview:pickUpDotView];
        
        if(pickUpAddressTextField == nil) {
            pickUpAddressTextField = [[UITextField alloc] init];
        }
        pickUpAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        [pickUpAddressTextField setValue:UIColorFromRGB(0x666666) forKeyPath:@"_placeholderLabel.textColor"];
        pickUpAddressTextField.tag = 101;
        pickUpAddressTextField.enabled = NO;
        pickUpAddressTextField.delegate = self;
        pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;
        pickUpAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        pickUpAddressTextField.textColor = UIColorFromRGB(0x666666);
        [customLocationView addSubview:pickUpAddressTextField];
        
        //Drop Off View
        UIView *dropOffAddressView = [[UIView alloc] init];
        dropOffAddressView.tag = 1232145;
        dropOffAddressView.backgroundColor = [UIColor whiteColor];
        [customLocationView addSubview:dropOffAddressView];
        
        UIButton *searchDropOffLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        searchDropOffLoc.tag = 165;
        [Helper setButton:searchDropOffLoc Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [searchDropOffLoc addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [dropOffAddressView addSubview:searchDropOffLoc];
        
        UIImageView *dropOffDotView = [[UIImageView alloc] initWithImage:dropOffImage];
        dropOffDotView.tag = 5050;
        dropOffDotView.image = dropOffImage;
        [dropOffAddressView addSubview:dropOffDotView];
        
        if(dropOffAddressTextField == nil) {
            dropOffAddressTextField = [[UITextField alloc] init];
        }
        dropOffAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        [dropOffAddressTextField setValue:UIColorFromRGB(0x666666) forKeyPath:@"_placeholderLabel.textColor"];
        dropOffAddressTextField.tag = 161;
        dropOffAddressTextField.enabled = NO;
        dropOffAddressTextField.delegate = self;
        dropOffAddressTextField.textAlignment = NSTextAlignmentLeft;
        dropOffAddressTextField.font = [UIFont fontWithName:fontNormal size:14];
        dropOffAddressTextField.textColor = UIColorFromRGB(0x666666);
        [dropOffAddressView addSubview:dropOffAddressTextField];
        
        UIImageView *dotLineImageView = [[UIImageView alloc] init];
        dotLineImageView.tag = 50510;
        dotLineImageView.image = dotLineImage;
        [customLocationView addSubview:dotLineImageView];

        locationView = customLocationView;
        [self.view addSubview:customLocationView];
    }
    
    UIButton *pickUpSearchBtn = (UIButton *)[locationView viewWithTag:91];
    UIImageView *pickUpDotView = (UIImageView *)[locationView viewWithTag:910];
    UIView *dropOffAddressView = (UIView *)[locationView viewWithTag:1232145];
    UIButton *dropOffSearchBtn = (UIButton *)[dropOffAddressView viewWithTag:165];
    UIImageView *dropOffDotView = (UIImageView *)[dropOffAddressView viewWithTag:5050];
    UIImageView *dotLineImageView = (UIImageView *)[locationView viewWithTag:50510];

    locationView.frame = CGRectMake(0, 64, screenWidth, 2*(pickUpImage.size.height + 30));
    pickUpSearchBtn.frame = CGRectMake(25, 0, screenWidth - 50, pickUpImage.size.height + 30);
    pickUpDotView.frame = CGRectMake(25, 20, pickUpImage.size.width, pickUpImage.size.height);
    pickUpAddressTextField.frame = CGRectMake(25 + pickUpImage.size.width + 10, 20, screenWidth - 50 -(25 + pickUpImage.size.width), pickUpImage.size.height);
    pickUpAddressTextField.textAlignment = NSTextAlignmentLeft;
    
    dropOffAddressView.frame = CGRectMake(12.5, pickUpImage.size.height + 30, screenWidth - 25, pickUpImage.size.height + 30);
    dropOffSearchBtn.frame = CGRectMake(0, 0, screenWidth - 25, pickUpImage.size.height + 30);
    dropOffDotView.frame = CGRectMake(12.5, 10, dropOffImage.size.width, dropOffImage.size.height);
    
    dropOffAddressTextField.frame = CGRectMake(12.5 + dropOffImage.size.width + 10, 10, screenWidth - 25 -5 - (12.5 + dropOffImage.size.width + 10), dropOffImage.size.height);
    dropOffAddressTextField.textAlignment = NSTextAlignmentLeft;
    dotLineImageView.frame = CGRectMake(25+(pickUpImage.size.width/2) - 2, 20 + pickUpImage.size.height, dotLineImage.size.width, dotLineImage.size.height);
    
    if ([Helper isCurrentLanguageRTL]) {
        screenWidth = locationView.frame.size.width;
        CGRect frame = pickUpSearchBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpSearchBtn.frame = frame;
        
        frame = pickUpDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpDotView.frame = frame;
        
        frame = pickUpAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        pickUpAddressTextField.frame = frame;
        pickUpAddressTextField.textAlignment = NSTextAlignmentRight;
        
        frame = dropOffSearchBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffSearchBtn.frame = frame;
        
        frame = dropOffDotView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffDotView.frame = frame;
        
        frame = dropOffAddressTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dropOffAddressTextField.frame = frame;
        dropOffAddressTextField.textAlignment = NSTextAlignmentRight;
        
        frame = dotLineImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        dotLineImageView.frame = frame;
    }
}

/**
 *  Add view for showing current position and a Button to choose pcikup location
 *
 */
-(void)createCenterView {
    UIView *alreadyContains = [self.view viewWithTag:currentPickUpMarker];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:alreadyContains]) {
        float screenWidth = [UIScreen mainScreen].bounds.size.width;
        float screenHeight = mapView_.frame.size.height;

        UIImage *pickerImage = [UIImage imageNamed:@"map_pin"];
        UIView *centerPickUpView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth - pickerImage.size.width)/2, screenHeight/2 - pickerImage.size.height, pickerImage.size.width, pickerImage.size.height)];
        centerPickUpView.tag = currentPickUpMarker;
        centerPickUpView.backgroundColor = [UIColor clearColor];
        UIImageView *centerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pickerImage.size.width, pickerImage.size.height)];
        centerImageView.image = pickerImage;
        [centerPickUpView addSubview:centerImageView];
        [centerPickUpView bringSubviewToFront:centerImageView];
        [self.view addSubview:centerPickUpView];
    }
}

/**
 *  add bottom view on map for the selection of Medical specialist
 */
- (void) addBottomView {
    if (arrayOfCarTypes.count == 0) {
//        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Sorry we are not available in your area, please contact http://www.ziride.com to request our service in your area.", @"Sorry we are not available in your area, please contact http://www.ziride.com to request our service in your area.")];
//        UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:currentPickUpMarker] viewWithTag:msgLabelTag] viewWithTag:100];
//        [Helper setToLabel:msgLabel Text:NSLocalizedString(@"No Drivers", @"No Drivers") WithFont:fontNormal FSize:7 Color:[UIColor blackColor]];
        [Helper showToastWithMessage:NSLocalizedString(@"Sorry we don't operate in your area yet, please contact www.ziride.com to request our service in your area", @"Sorry we don't operate in your area yet, please contact www.ziride.com to request our service in your area") onView:self.view];
    } else {
        NSArray *arr = self.view.subviews;
        if (![arr containsObject:scroller]) {
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            scroller.delegate = self;
            allHorizontalData = [[NSMutableArray alloc]init];
            arrayOfImages = [[NSMutableArray alloc]init];
            arrayOfVehicleIcons = [[NSMutableArray alloc]init];
            arrayOfMapIcons = [[NSMutableArray alloc] init];
            for(NSDictionary *carType in arrayOfCarTypes) {
                [allHorizontalData addObject:flStrForObj(carType[@"type_name"])];
                [arrayOfMapIcons addObject:flStrForObj(carType[@"MapIcon"])];
                UIImageView *images = [[UIImageView alloc] init];
                [arrayOfImages addObject:images];
                [arrayOfVehicleIcons addObject:images];
            }
            if(scroller) {
                scroller = nil;
                shadowViewForScroller = nil;
            }
            if (kBookLater)
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenHeight - 135, screenWidth, 85)];
            else
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenHeight - 85, screenWidth, 85)];
            scroller.backgroundColor = UIColorFromRGB(0xFFFFFF);
            scroller.tag = bottomViewWithCarTag;
            scroller.delegate = self;
            [scroller setScrollEnabled:YES];
            scroller.hidden = NO;
            scroller.showsHorizontalScrollIndicator = NO;
            
            float numberToDivide = 1;
            CGFloat btnWidth = 55;
            switch (arrayOfCarTypes.count) {
                case 1:
                    numberToDivide = 1;
                    btnWidth = 80;
                    break;
                case 2:
                    numberToDivide = 2;
                    btnWidth = 80;
                    break;
                case 3:
                    numberToDivide = 3;
                    btnWidth = 80;
                    break;
                case 4:
                    numberToDivide = 4;
                    btnWidth = 70;
                    break;
                case 5:
                    numberToDivide = 5;
                    btnWidth = 60;
                    break;
                default:
                    numberToDivide = 5.5;
                    btnWidth = 55;
                    break;
            }
            if (btnWidth < screenWidth/numberToDivide) {
                btnWidth = 55;
            }
            
            [scroller setContentSize:CGSizeMake(allHorizontalData.count*screenWidth/numberToDivide, 80)];
            [self.view addSubview:scroller];
            arrayOfButtons = [[NSMutableArray alloc] init];
            arrayOfMapImages = [[NSMutableArray alloc] init];
            for (int i=0; i < arrayOfCarTypes.count; i++) {
                UIView *btnBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(screenWidth/numberToDivide*i, 0, screenWidth/numberToDivide, 85)];
                btnBackgroundView.backgroundColor = [UIColor clearColor];
                btnBackgroundView.tag = 201+i+1;
                UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                buttonCategory.frame = CGRectMake((btnBackgroundView.frame.size.width - btnWidth)/2, 0, btnWidth, 85);
                buttonCategory.tag = 201+i+1;
                buttonCategory.titleLabel.numberOfLines = 7;
                buttonCategory.titleLabel.minimumScaleFactor = 0.5;
                buttonCategory.titleLabel.textAlignment = NSTextAlignmentCenter;
                buttonCategory.imageView.layer.cornerRadius = 35/2;
                [arrayOfButtons addObject:buttonCategory];
                if (carTypesForLiveBooking <= arrayOfCarTypes.count) {
                    if(i == carTypesForLiveBooking-1) {
                        [buttonCategory setSelected:YES];
                        carTypesForLiveBookingServer = [arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
                        scroller.contentOffset = oldOffset;
                    }
                } else if (i == 0) {
                    [buttonCategory setSelected:YES];
                    carTypesForLiveBooking = 1;
                    carTypesForLiveBookingServer = [arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
                } else {
                    return;
                }

                [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"NO CABS\n\n\n\n%@",flStrForStr(allHorizontalData[i])] WithFont:fontSemiBold FSize:10 TitleColor:UIColorFromRGB(0x9E9E9E) ShadowColor:nil];
                [buttonCategory setTitleColor:UIColorFromRGB(0x9E9E9E) forState:UIControlStateNormal];
                [buttonCategory setTitleColor:UIColorFromRGB(0x484848) forState:UIControlStateSelected];
                [buttonCategory addTarget:self action:@selector(vehicleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [self setCarTypeButtonContentInsets:buttonCategory];
                
                [btnBackgroundView addSubview:buttonCategory];
                [scroller addSubview:btnBackgroundView];
                [scroller bringSubviewToFront:buttonCategory];
            }
            [self downloadUnSelectedVehicleTypeImages];
            [self downloadSelectedVehicleTypeImages];
            [self downloadMapImages];
            [self changeMapMarker:carTypesForLiveBooking-1];
        }
    }
    if(!shadowViewForScroller) {
        shadowViewForScroller = [[UIView alloc] initWithFrame:CGRectMake(0, scroller.frame.origin.y, scroller.frame.size.width, 5)];
        shadowViewForScroller.backgroundColor = [UIColor clearColor];
        [self.view addSubview:shadowViewForScroller];
        [Helper addTopShadow:shadowViewForScroller];
    }
    if (kBookLater) {
        [self addBookLaterView];
    }
    [self addLocationButtonsOnMap];
}

/**
 Add Book Now and Book Later Button in this View
 */
-(void)addBookLaterView {
    UIView *vb = [self.view viewWithTag:bookingOptionViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 50, screenWidth, 50)];
        backgroundView.backgroundColor =  [UIColor whiteColor];
        backgroundView.tag = bookingOptionViewTag;
        
        UIButton *nowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nowBtn.frame = CGRectMake(0, 0, screenWidth/2, 50);
        [Helper setButton:nowBtn Text:NSLocalizedString(@"BOOK NOW", @"BOOK NOW") WithFont:fontSemiBold FSize:16 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        nowBtn.tag = nowBookingBtnTag;
//        nowBtn.layer.cornerRadius = 5.0f;
//        nowBtn.layer.masksToBounds = YES;
//        [nowBtn setSelected:YES];
        [nowBtn addTarget:self action:@selector(bookingTypeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [nowBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateSelected];
        [nowBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x019F6E)] forState:UIControlStateNormal];
        [nowBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x555555)] forState:UIControlStateHighlighted];
        [nowBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x555555)] forState:UIControlStateSelected];
        [nowBtn setBackgroundColor:UIColorFromRGB(0x019F6E)];
//        [Helper addBottomShadowOnButtonForBottomView:nowBtn];
        [backgroundView addSubview:nowBtn];
        

        UIButton *laterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        laterBtn.frame = CGRectMake(screenWidth/2, 0, screenWidth/2, 50);
        [Helper setButton:laterBtn Text:NSLocalizedString(@"BOOK LATER", @"BOOK LATER") WithFont:fontSemiBold FSize:16 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        laterBtn.tag = laterBookingBtnTag;
//        laterBtn.layer.cornerRadius = 5.0f;
//        laterBtn.layer.masksToBounds = YES;
//        [laterBtn setSelected:NO];
        [laterBtn addTarget:self action:@selector(bookingTypeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [laterBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateSelected];
        [laterBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x555555)] forState:UIControlStateNormal];
        [laterBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x019F6E)] forState:UIControlStateHighlighted];
        [laterBtn setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x019F6E)] forState:UIControlStateSelected];
        [laterBtn setBackgroundColor:UIColorFromRGB(0x555555)];
//        [Helper addBottomShadowOnButtonForBottomView:laterBtn];
        [backgroundView addSubview:laterBtn];
        [self.view addSubview:backgroundView];
    }
}

-(void)bookingTypeBtnClicked:(UIButton *)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        return;
    }
    UIButton *nowBtn = (UIButton*)[(UIView*)[sender superview] viewWithTag:nowBookingBtnTag];
    UIButton *laterBtn = (UIButton*)[(UIView*)[sender superview] viewWithTag:laterBookingBtnTag];
    UIButton *mBtn = (UIButton *)sender;
    if (sender.tag == nowBookingBtnTag) {
        if(mBtn.selected == NO) {
            [nowBtn setSelected:NO];
            [laterBtn setSelected:NO];
            isNowSelected = YES;
            isLaterSelected = NO;
            [self openConfirmationScreen];
        }
    } else if(sender.tag == laterBookingBtnTag) {
        mBtn.selected = NO;
        if(mBtn.selected == NO) {
            [nowBtn setSelected:NO];
            [laterBtn setSelected:YES];
            isNowSelected = NO;
            isLaterSelected = YES;
            [self showDatePickerWithTitle:NSLocalizedString(@"SCHEDULE", @"SCHEDULE")];
        }
    }
}


/**
 This method will call when user clicks on Book Now or he may choose later date and time for ride.
 */
-(void)openConfirmationScreen {
    if (arrayOfCarTypes.count > 0) {
        surgePrice = [arrayOfCarTypes[carTypesForLiveBooking - 1][@"surg_price"] doubleValue];
        surgePriceTime = arrayOfCarTypes[carTypesForLiveBooking - 1][@"surgePriceTime"];
        if (pickUpAddressTextField.text.length == 0) {

        } else if (surgePrice > 1.0) {
            UIImageView *imageView = arrayOfVehicleIcons[carTypesForLiveBooking-1];
            NSDictionary *dataDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%0.1fx",surgePrice],@"surgePrice",
                                      [NSString stringWithFormat:@"%@",allHorizontalData [carTypesForLiveBooking-1]], @"vehicleName",
                                      imageView.image, @"vehicleImage",
                                      nil];
            surgePricePopUp* surgePopView = [[surgePricePopUp alloc]init];
            UIWindow *window2 = [[UIApplication sharedApplication] keyWindow];
            [surgePopView showPopUpWithDetailedDict:dataDict Onwindow:window2];
            surgePopView.onCompletion = ^(NSInteger acceptOrReject) {
                if (acceptOrReject == 1) {
                    isCustomMarkerSelected = YES;
                    UIView *customPickUpView = (UIView *)[self.view viewWithTag:customLocationViewTag];
                    [customPickUpView removeFromSuperview];
                    [self addCustomNavigationBar];
                    UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
                    [customLocationView removeFromSuperview];
                    [self addPickUpAndDropOffLocationViewForConfirmationScreen];
                    [self hideBottomViews];
                    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
                    [mapView_ animateWithCameraUpdate:zoomCamera];
                    _isAddressManuallyPicked = YES;
                    UIView *addLocationButtonsView = [self.view viewWithTag:loctionButtonsTag];
                    [addLocationButtonsView removeFromSuperview];
                    [self addLocationButtonsOnMap];
                    [self createConfirmationBottomView];
                }
            };
        } else {
            isCustomMarkerSelected = YES;
            [self addCustomNavigationBar];
            UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
            [customLocationView removeFromSuperview];
            [self addPickUpAndDropOffLocationViewForConfirmationScreen];
            
            UIView *locationButtonsView = [self.view viewWithTag:loctionButtonsTag];
            [locationButtonsView removeFromSuperview];
            [self addLocationButtonsOnMap];

            [self hideBottomViews];
            
            GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
            [mapView_ animateWithCameraUpdate:zoomCamera];
            _isAddressManuallyPicked = YES;
            
            UIView *confirmationBottomView = [self.view viewWithTag:confirmationBottomViewTag];
            [confirmationBottomView removeFromSuperview];
            [self createConfirmationBottomView];
        }
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No car types are availabe in your area.", @"No car are types availabe in your area.")];
    }
}

/**
 *  Add view for showing bottomView of Confirmation Screen
 *
 */
-(void)createConfirmationBottomView {
    UIView *alreadyContains = [self.view viewWithTag:confirmationBottomViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:alreadyContains]) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        UIView *confirmBottomView = [[UIView alloc]initWithFrame:CGRectMake(0, screenSize.height - 142, screenSize.width, 142)];
        confirmBottomView.tag = confirmationBottomViewTag;
        confirmBottomView.backgroundColor = [UIColor clearColor];
        confirmBottomViewObj = [[ConfirmBottomView alloc] init];
        [confirmBottomViewObj.choosePaymentTypeBtn addTarget:self action:@selector(choosePaymentTypeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//        [confirmBottomViewObj.rateCardBtn addTarget:self action:@selector(rateCardButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [confirmBottomViewObj.rideEstimateBtn addTarget:self action:@selector(fareButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [confirmBottomViewObj.promoCodeBtn addTarget:self action:@selector(promoCodeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [confirmBottomViewObj.requestBookingBtn addTarget:self action:@selector(sendBooking:) forControlEvents:UIControlEventTouchUpInside];

        if([[Database getCardDetails] count]) {
            paymentTypesForLiveBooking = 1;
            selectedPaymentType = 2;
        } else {
            paymentTypesForLiveBooking = 2;
            selectedPaymentType = 0;
        }
        [confirmBottomViewObj showPopUpOnView:confirmBottomView];
        
        //Request bottom
        NSString *msgText = NSLocalizedString(@"REQUEST ", @"REQUEST ");
        if (isNowSelected == YES) {
            NSString *str = flStrForStr(arrayOfCarTypes[carTypesForLiveBooking-1][@"type_name"]);
            msgText = [msgText stringByAppendingString:str];
            msgText = [msgText uppercaseString];
        } else if (isLaterSelected == YES) {
            msgText = laterSelectedDate;
            msgText = [msgText stringByAppendingString:@"  "];
            msgText = [msgText stringByAppendingString:arrayOfCarTypes[carTypesForLiveBooking - 1][@"type_name"]];
            msgText = [msgText uppercaseString];
        }
        [Helper setButton:confirmBottomViewObj.requestBookingBtn Text:msgText WithFont:fontSemiBold FSize:18 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        NSArray *cards = [Database getCardDetails];
        if (cards.count) {
            paymentTypesForLiveBooking = 1;
        } else {
            paymentTypesForLiveBooking = 2;
        }
        [self.view addSubview:confirmBottomView];
    }
}


/**
 This method will call when user clicks on Rate card button on Confirmation Screen or If user clicks on Car icons on Home screen.
 */
-(void) rateCardButtonClicked {
    isVehicleBtnClickedForRateCard = NO;
    for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
        if([view isKindOfClass:[RateCard class]]) {
            return;
        }
    }
    RateCard* view = [[RateCard alloc] init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSDictionary *selectedCarType =[arrayOfCarTypes[carTypesForLiveBooking-1] mutableCopy];
    [view showPopUpWithDictionary:selectedCarType Onwindow: window];
}


/**
 *  date picker
 *
 *  @param  Select date for the later booking
 *
 *  @return return void and accept title string to show title on its view
 */
#pragma UIPicker Delegate

- (void)addCustomActionSheet {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *customActnSheet = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight, screenWidth, screenHeight)];
    customActnSheet.backgroundColor = [UIColor clearColor];
    customActnSheet.tag = pickerViewTag;
    [self.view addSubview:customActnSheet];
}

/**
 Add Date Picker to choose Later Booking Date and Time

 @param title 
        This is string to Show as Title for View
 */
- (void)showDatePickerWithTitle:(NSString*)title {
    [self addCustomActionSheet];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    UIView *customActnSheet = [self.view viewWithTag:pickerViewTag];
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 50, screenWidth, 200)];
    [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:1];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-30];
    NSDate *endDate = [NSDate date];
    NSTimeInterval oneHours = 1 * 60 * 60;
    NSDate *dateOneHourAhead = [endDate dateByAddingTimeInterval:oneHours];
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:dateOneHourAhead];
    datePicker.minuteInterval = 15;
    [datePicker setBackgroundColor:UIColorFromRGB(0xFFFFFF)];
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-250, screenWidth, 250)];
    topView.tag = 9002;
    topView.backgroundColor = UIColorFromRGB(0x019f6e);
    UIButton *doneButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:NSLocalizedString(@"Done",@"Done") forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    doneButton.frame = CGRectMake(screenWidth - 60, 10, 60, 30);
    [doneButton addTarget:self action:@selector(saveActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:doneButton];
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:NSLocalizedString(@"Cancel",@"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 10, 60, 30);
    [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(cancelActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4, 10, screenWidth/2, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =title;
    titleLabel.textColor =  UIColorFromRGB(0xffffff);
    [topView addSubview:titleLabel];
    [topView addSubview:datePicker];
    [customActnSheet addSubview:topView];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame = customActnSheet.frame;
                         frame.origin.y = 0;
                         customActnSheet.frame = frame;
                     } completion:^(BOOL finished) {
                     }];
}

- (void)saveActionSheet:(id)type {
    UIView *customActnSheet = [self.view viewWithTag:pickerViewTag];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:pickerViewTag];
                         [customActionSht removeFromSuperview];
                     } completion:^(BOOL finished) {
                     }];
    NSDate *dateSelected = [datePicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    laterSelectedDateServer = [dateFormatter stringFromDate:dateSelected];
    [dateFormatter setDateFormat:@"dd MMM yyyy hh:mm a"];
    laterSelectedDate = [dateFormatter stringFromDate:dateSelected];
    [self openConfirmationScreen];
}

- (void)cancelActionSheet:(id)type {
    UIView *customActnSheet = [self.view viewWithTag:pickerViewTag];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:pickerViewTag];
                         [customActionSht removeFromSuperview];
                     } completion:^(BOOL finished) {
                     }];
    UIButton *nowButton =  (UIButton*)[[self.view viewWithTag:bookingOptionViewTag] viewWithTag:nowBookingBtnTag];
    UIButton *laterButton =  (UIButton*)[[self.view viewWithTag:bookingOptionViewTag] viewWithTag:laterBookingBtnTag];
    [nowButton setSelected:NO];
    [laterButton setSelected:NO];
    isNowSelected = NO;
    isLaterSelected = NO;
}

/**
 Make Progress Bar View
 
 */
- (void)makeMyProgressBarMoving {
    if(addProgressBarView) {
        if (!requestingView) {
            requestingView = [ProviderRequestingView sharedInstance];
        }
        [requestingView stopTimer];
        requestingView = nil;
    }
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    addProgressBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [addProgressBarView setUserInteractionEnabled:YES];
    [self.view addSubview:addProgressBarView];
    [self.view bringSubviewToFront:addProgressBarView];
    [Utility setPickUpLatitude:srcLat];
    [Utility setPickUpLongitude:srcLong];
    requestingView = [ProviderRequestingView sharedInstance];
    requestingView.totalDuration = 0;
    [addProgressBarView addSubview:requestingView];
    [requestingView updateUI];
}

- (void)makeMyProgressBarMoving:(CGFloat)bookingExpireTime {
    requestingView.totalDuration = bookingExpireTime;
    [Utility setPickUpLatitude:srcLat];
    [Utility setPickUpLongitude:srcLong];
    [requestingView updateUI];
    requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [addProgressBarView setHidden:NO];
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                     }
                     completion:^(BOOL finished) {
                         [requestingView updateUI];
                     }];
}

/*
 *
 *  CREATING A VIEW THAT WILL APPEAR AFTER GETTING THE FIRST PUSH
 */

-(void)createDriverArrivedView:(NSInteger )bookingStatus {
    if (!requestingView) {
        requestingView = [ProviderRequestingView sharedInstance];
    }
    [requestingView stopTimer];
    requestingView = nil;
    UIView *vb = [self.view viewWithTag:driverArrivedViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = self.view.frame.size.height;
        UIView *driverDetailsView = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight - 145, screenWidth, 145)];
        driverDetailsView.backgroundColor = [UIColor clearColor];
        driverDetailsView.tag = driverArrivedViewTag;
        [self.view addSubview:driverDetailsView];
        
        driverDetailsObj = [[DriverDetailsView alloc] init];
        [driverDetailsObj.contactBtn addTarget:self action:@selector(contactBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [driverDetailsObj.cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [driverDetailsObj.trackBtn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [driverDetailsObj showPopUpOnView:driverDetailsView];
        
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        srcAddr = [Utility pickUpLocationAddressLine1];
        srcAddrline2 = [Utility pickUpLocationAddressLine2];
        if ([srcAddrline2 rangeOfString:srcAddr].location == NSNotFound) {
            if(srcAddr.length)
                [arr addObject:srcAddr];
            if (srcAddrline2.length)
                [arr addObject:srcAddrline2];
        } else {
            if (srcAddrline2.length)
                [arr addObject:srcAddrline2];
        }
        NSString *addressText = [arr componentsJoinedByString:@", "];
        pickUpAddressTextField.text = flStrForStr(addressText);
        UIButton *pickUpSearchBtn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:91];
        pickUpSearchBtn.userInteractionEnabled = NO;
        UIButton *favPickupBtn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:97];
        favPickupBtn.userInteractionEnabled = NO;
        UIButton *fareEstimateBtn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:505];
        fareEstimateBtn.userInteractionEnabled = NO;
        UIButton *driverNotesBtn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:911];
        driverNotesBtn.hidden = YES;
        
        UIButton *favBtn = (UIButton *) [[self.view viewWithTag:customLocationViewTag] viewWithTag:97];
        favBtn.selected = NO;
        UILabel *pickUpLabel = (UILabel *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:1010];
        pickUpLabel.text = NSLocalizedString(@"Pickup Location", @"Pickup Location");
        if (srcAddr.length) {
            BOOL isPickUpFav = [Database checkAddressIsFavouriteInDatabase:srcAddr and:srcAddrline2];
            if (isPickUpFav) {
                NSArray *arr = [Database getFavouriteAddressFromDataBase:srcAddr and:srcAddrline2];
                SourceAddress *add = arr[0];
                pickUpLabel.text = [flStrForStr(add.locationType) capitalizedString];
                favBtn.selected = YES;
            }
        }
        
        desAddr = [Utility dropOffLocationAddressLine1];
        desAddrline2 = [Utility dropOffLocationAddressLine2];
        UITextField *dropOffTextfiled = (UITextField *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:161];
        if(!desAddr.length) {
            dropOffTextfiled.text = NSLocalizedString(@"Add Drop Location", @"Add Drop Location");
        } else {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            if ([desAddrline2 rangeOfString:desAddr].location == NSNotFound) {
                if(desAddr.length)
                    [arr addObject:desAddr];
                if (desAddrline2.length)
                    [arr addObject:desAddrline2];
            } else {
                if (desAddrline2.length)
                    [arr addObject:desAddrline2];
            }
            NSString *addressText = [arr componentsJoinedByString:@", "];
            dropOffTextfiled.text = flStrForStr(addressText);
        }
        UILabel *dropOffLabel = (UILabel *) [[self.view viewWithTag:customLocationViewTag] viewWithTag:1650];
        dropOffLabel.text = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
        if (desAddr.length) {
            BOOL isDropOffFav = [Database checkAddressIsFavouriteInDatabase:desAddr and:desAddrline2];
            if (isDropOffFav) {
                NSArray *arr = [Database getFavouriteAddressFromDataBase:desAddr and:desAddrline2 ];
                SourceAddress *add = arr[0];
                dropOffLabel.text = [flStrForStr(add.locationType) capitalizedString];
            }
        }
        [self updateDriverStatus];
    } else {
        [self updateDriverStatus];
    }
}


#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrayOfpickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *returnStr = [NSString stringWithFormat:@"%@",arrayOfpickerData[row]];
    return returnStr;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedRowForTip =row;
}

/**
 Show picker View for adding tip to Driver.
 */
-(void)showPickerViewForAddingTipForDriver {
    [self addCustomActionSheet];
    selectedRowForTip = 0;
    UIView *customActnSheet = [self.view viewWithTag:9001];
    pkrViewForAddTip = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    CGRect newFrame = pkrViewForAddTip.frame;
    newFrame.size.width =[UIScreen mainScreen].bounds.size.width;
    [pkrViewForAddTip setFrame:newFrame];
    [pkrViewForAddTip setBackgroundColor:[UIColor whiteColor]];
    [pkrViewForAddTip setDelegate:self];
    [pkrViewForAddTip setShowsSelectionIndicator:YES];
    pkrViewForAddTip.tag = 5600;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0.0, screenHeight-250, screenWidth, 250)];
    topView.backgroundColor = UIColorFromRGB(0x019f6E);
    topView.tag = 9002;
    UIButton *doneButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [Helper setButton:doneButton Text:NSLocalizedString(@"DONE", @"DONE") WithFont:fontNormal FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [doneButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    doneButton.frame = CGRectMake(screenWidth-60, 7, 60, 30);
    doneButton.tag = 5600;
    [doneButton addTarget:self action:@selector(btnDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:doneButton];
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7, 60, 30);
    cancelButton.tag = 5600;
    [Helper setButton:cancelButton Text:NSLocalizedString(@"CANCEL", @"CANCEL") WithFont:fontNormal FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [cancelButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(btnCloseTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4, 7, screenWidth/2, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"DRIVER TIP", @"DRIVER TIP");
    titleLabel.textColor =  UIColorFromRGB(0xffffff);
    [topView addSubview:titleLabel];
    [topView addSubview:pkrViewForAddTip];
    [customActnSheet addSubview:topView];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame = customActnSheet.frame;
                         frame.origin.y = 0;
                         customActnSheet.frame = frame;
                     } completion:^(BOOL finished) {
                     }];
}

/**
 Picker View Done Button Action

 @param sender picker side button
 */
- (void)btnDoneTapped:(UIButton *)sender {
    if (sender.tag == 5600)
    {
        [self sendRequestToAddTip:selectedRowForTip];
    }
    else
    {
        if ([_pkrView selectedRowInComponent:0]==0)
        {
            PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
            vc.callback = ^(NSString *cardIde , NSString *type, NSString *last4)
            {
                UIView *v = [self.view viewWithTag:501];
                UIButton *btn = (UIButton *)[v viewWithTag:502];
                [btn setImage:[Helper setPlaceholderToCardType:type] forState:UIControlStateNormal];
                [btn setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
                NSString *str1 = NSLocalizedString(@"PERSONAL ****", @"PERSONAL ****");
                [btn setTitle:[NSString stringWithFormat:@"%@%@",str1,cardIde] forState:UIControlStateNormal];
                cardId = cardIde;
                paymentTypesForLiveBooking = 1;
            };
            
            vc.isComingFromMapVC = YES;
            _isSelectinLocation = YES;
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
            [self presentViewController:navBar animated:YES completion:nil];
        }
        else
        {
            UIView *v = [self.view viewWithTag:501];
            UIButton *btn = (UIButton *)[v viewWithTag:502];
            btn.selected = NO;
            [btn setTitle:[NSString stringWithFormat:NSLocalizedString(@"  CASH", @"  CASH")] forState:UIControlStateNormal];
            [btn setImage:nil forState:UIControlStateNormal];
            paymentTypesForLiveBooking = 2;
        }
    }
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                         
                         
                     } completion:^(BOOL finished) {
                         
                     }];
}

-(void)btnCloseTapped:(UIButton *)sender{
    if (sender.tag == 5600) {
    }
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                     } completion:^(BOOL finished) {
                         
                     }];
}

-(void)sendRequestToAddTip:(NSInteger)tipForDriver {
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    if(tipForDriver) {
        tipForDriver = tipForDriver*5+5;
    } else {
        tipForDriver = 5;
    }
    
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_tip":[NSNumber numberWithInteger:tipForDriver],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateTip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       if (response == nil) {
                                           return;
                                       } else if ([response objectForKey:@"Error"]) {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
                                       } else {
                                           if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey: @"errMsg"]];
                                               NSString *tip;
                                               if (tipForDriver == 0){
                                                   tip = [NSString stringWithFormat:NSLocalizedString(@"TIP", @"TIP")];
                                               } else {                                                   tip = [NSString stringWithFormat:@"%ld%%", (long)tipForDriver];
                                               }
                                               [[NSUserDefaults standardUserDefaults] setObject:tip forKey:@"drivertip"];
                                               [[NSUserDefaults standardUserDefaults]synchronize];
                                               NSString *ti = [[NSUserDefaults standardUserDefaults] objectForKey:@"drivertip"];
                                               NSString *tipString = NSLocalizedString(@"TIP", @"TIP");
                                               if (ti.length != 0 && ![ti isEqualToString:tipString]){
                                                   [customNavigationBarView setRightBarButtonTitle:[NSString stringWithFormat:@"%@\n%@",tipString,[Helper getCurrencyLocal:[ti floatValue]]]];
                                               } else {
                                                   [customNavigationBarView setRightBarButtonTitle:tipString];
                                               }
                                           } else {
                                               [customNavigationBarView setRightBarButtonTitle:@"TIP"];
                                           }
                                       }
                                   }
                                   else {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   }
                               }];
}


#pragma mark - UIScrollViewDelegate -

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
//    NSInteger count = (arrayOfCarTypes.count - 3) * screenWidth/3;
//    if(scrollView.contentOffset.x == 0 && scrollView.contentOffset.x < count) {
//        [rightArrow setHidden:NO];
//        [leftArrow setHidden:YES];
//    } else if(scrollView.contentOffset.x == count ) {
//        [rightArrow setHidden:YES];
//        [leftArrow setHidden:NO];
//    } else {
//        [rightArrow setHidden:NO];
//        [leftArrow setHidden:NO];
//    }
//}

#pragma mark - Download Images Methods -
-(void)downloadMapImages {
    arrayOfMapImages = [[NSMutableArray alloc]init];
    for (int i=0; i < arrayOfCarTypes.count; i++) {
        NSString *mapImageURL = flStrForStr(arrayOfMapIcons[i]);
        if (mapImageURL.length) {
            mapImageURL = [NSString stringWithFormat:@"%@", arrayOfMapIcons[i]];
            [arrayOfImages[i] sd_setImageWithURL:[NSURL URLWithString:mapImageURL]
                                placeholderImage:nil
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           if(!error && image) {
                                               image = [Helper imageWithImage:image scaledToSize:[Helper makeSize:image.size fitInSize:CGSizeMake(35, 40)]];
                                               [arrayOfMapImages addObject:image];
                                           } else {
                                               [arrayOfMapImages addObject:[UIImage new]];
                                           }
                                       }];
        }
    }
}

-(void)downloadSelectedVehicleTypeImages {
    for (int i=0; i < arrayOfCarTypes.count; i++) {
        NSString *selectedVehicleImageURL = flStrForStr(arrayOfCarTypes[i][@"vehicle_img"]);
        if (selectedVehicleImageURL.length) {
            selectedVehicleImageURL = [NSString stringWithFormat:@"%@",arrayOfCarTypes[i][@"vehicle_img"]];
            UIImageView *imageView = [UIImageView new];
            imageView.tag = i;
            [imageView sd_setImageWithURL:[NSURL URLWithString:selectedVehicleImageURL]
                                      placeholderImage:nil
                                             completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                 if(!error) {
                                                     UIButton *buttonCategory = arrayOfButtons[imageView.tag];
                                                     image = [Helper imageWithImage:image scaledToSize:[Helper makeSize:image.size fitInSize:CGSizeMake(36, 36)]];
                                                     [buttonCategory setImage:image forState:UIControlStateSelected];
                                                     [buttonCategory setImage:image forState:UIControlStateHighlighted];
                                                     [self setCarTypeButtonContentInsets:buttonCategory];
//                                                     CGFloat spacing = 6.0;
//                                                     CGSize imageSize = buttonCategory.imageView.frame.size;
//                                                     buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(- (imageSize.height + spacing), - imageSize.width, 0.0, 0.0);
//                                                     CGSize titleSize = buttonCategory.titleLabel.frame.size;
//                                                     buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, - (titleSize.height + spacing), -titleSize.width);
                                                 }
                                             }];
        }
    }
}

-(void)downloadUnSelectedVehicleTypeImages {
    for (int i=0; i < arrayOfCarTypes.count; i++) {
        NSString *unSelectedVehicleImageURL = flStrForStr(arrayOfCarTypes[i][@"vehicle_img_off"]);
        if (unSelectedVehicleImageURL.length) {
            NSString *unSelectedVehicleImageURL = [NSString stringWithFormat:@"%@",arrayOfCarTypes[i][@"vehicle_img_off"]];
            UIImageView *imageView = [UIImageView new];
            imageView.tag = i;
            [imageView sd_setImageWithURL:[NSURL URLWithString:unSelectedVehicleImageURL]
                                        placeholderImage:nil
                                               completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                   if(!error) {
                                                       UIButton *buttonCategory = arrayOfButtons[imageView.tag];
                                                       image = [Helper imageWithImage:image scaledToSize:[Helper makeSize:image.size fitInSize:CGSizeMake(36, 36)]];
                                                       [buttonCategory setImage:image forState:UIControlStateNormal];
                                                       [self setCarTypeButtonContentInsets:buttonCategory];
                                                   }
                                               }];
        }
    }
}

-(void)setCarTypeButtonContentInsets:(UIButton *)buttonCategory
{
    // the space between the image and text
       CGFloat spacing = 6.0;
    
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = CGSizeMake(35, 35); //buttonCategory.imageView.image.size;
    buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - spacing, 0.0); //Bottom: -(imageSize.height + spacing)
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = [buttonCategory.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: buttonCategory.titleLabel.font}];
    buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(spacing, 0.0, 0.0, - titleSize.width);// Top: (titleSize.height + spacing)
    
    // increase the content height to avoid clipping
    CGFloat edgeOffset = fabs(titleSize.height - imageSize.height) / 2.0;
    buttonCategory.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
}




#pragma mark - Activity Indicator Custom Methods -

-(void)startSpinitAgain {
    [[MyAppTimerClass sharedInstance] startSpinTimer];
    [self showActivityIndicator];
}

-(void)showActivityIndicator {
    centerPickUpObj.timeLabel.hidden = YES;
    centerPickUpObj.activityIndicator.hidden = NO;
    [centerPickUpObj.activityIndicator startAnimating];
}

-(void)hideAcitvityIndicator {
    centerPickUpObj.timeLabel.hidden = NO;
    [centerPickUpObj.activityIndicator stopAnimating];
    centerPickUpObj.activityIndicator.hidden = YES;
}

-(void)updateTheNearestDriverinSpinitCircle:(id)distance {
    [[MyAppTimerClass sharedInstance] stopSpinTimer];
    NSString *str = [NSString stringWithFormat:@"%@",distance];
    if([str rangeOfString: NSLocalizedString(@"No Drivers", @"No Drivers")].location != NSNotFound) {
        [Helper setToLabel:centerPickUpObj.timeLabel Text:str WithFont:fontNormal FSize:7 Color:[UIColor whiteColor]];
    } else {
        [Helper setToLabel:centerPickUpObj.timeLabel Text:str WithFont:fontNormal FSize:8 Color:[UIColor whiteColor]];
    }
    [self hideAcitvityIndicator];
}


#pragma mark - Booking Request Methods -

-(void)sendAppointmentRequestForLiveBooking {
    walletAmount = [[[Utility walletBalance] stringByReplacingCharactersInRange:[[Utility walletBalance] rangeOfString:currencyStr] withString:@""] doubleValue];
    if(kPaymentType) {
        if (! (paymentTypesForLiveBooking == 1 || paymentTypesForLiveBooking == 2) ) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Please select your mode of payment.", @"Please select your mode of payment.")];
            return;
        }
    } else {
        if (!(paymentTypesForLiveBooking == 1) && kCardOrCash ) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Please add your card details.", @"Please add your card details.")];
            return;
        }
    }
    if (walletAmount > 0.00 && arrayOfDriverID.count) {
        NSString *str1 = NSLocalizedString(@"Your current wallet amount is ", @"Your current wallet amount is ");
        NSString *str2 = [Helper getCurrencyLocal: walletAmount];
        NSString *str3 = NSLocalizedString(@"Do you want to use it for your current booking?",@"Do you want to use it for your current booking?");
        NSString *messageString = [NSString stringWithFormat:@"%@ %@. %@", str1, str2, str3];
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:messageString];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Yes", @"Yes"), NSLocalizedString(@"No", @"No"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                selectedPaymentType = 0;
            } else {
                if (paymentTypesForLiveBooking == 1) {
                    selectedPaymentType = 2;
                } else {
                    selectedPaymentType = 1;
                }
            }
            [self sendDriverBooking];
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
    } else if(walletAmount < [Utility bookingAmountAllowUpto] && arrayOfDriverID.count){
        NSString *str1 = NSLocalizedString(@"Your current wallet amount is ", @"Your current wallet amount is ");
        NSString *str2 = [Helper getCurrencyLocal: walletAmount];
        NSString *str3 = NSLocalizedString(@"Please settle the due amount on your wallet to proceed with a new booking",@"Please settle the due amount on your wallet to proceed with a new booking");
        NSString *messageString = [NSString stringWithFormat:@"%@ %@. %@", str1, str2, str3];
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:messageString];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Ok", @"Ok"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
                vc.isComingFromMapVC = NO;
                vc.amountToSettle = [NSString stringWithFormat:@"%@ %0.02f", currencyStr, -(walletAmount)];
                vc.callback = ^(NSString *cardIde , NSString *type, NSString *last4){
                    walletAmount = [last4 doubleValue];
                };
                vc.isComingFromMapVC = YES;
                _isSelectinLocation = YES;
                UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
                [self presentViewController:navBar animated:YES completion:nil];
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
    } else {
        [self sendDriverBooking];
    }
}

/**
 Checking validations before call Live Booking service
 */
-(void) sendDriverBooking {
    if(kWithDispatch) {
        if (isLaterSelected) {
            PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
            if ( [reachability isNetworkAvailable]) {
                [self callServiceToBookDriver];
            } else {
                [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"Network Connection Unavailable", @"Network Connection Unavailable") On:self.view];
            }
        } else if (arrayOfDriverID.count) {
            isRequestingButtonClicked = YES;
            PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
            if ( [reachability isNetworkAvailable]) {
                [self callServiceToBookDriver];
            } else {
                [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"Network Connection Unavailable", @"Network Connection Unavailable") On:self.view];
            }
        } else {
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Driver Unavailable.", @"Driver Unavailable.")];
        }
    } else {
        if (arrayOfDriverID.count) {
            isRequestingButtonClicked = YES;
            PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
            if ( [reachability isNetworkAvailable]) {
                [self callServiceToBookDriver];
            } else {
                [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"Network Connection Unavailable", @"Network Connection Unavailable") On:self.view];
            }
        } else {
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Driver Unavailable.", @"Driver Unavailable.")];
        }
    }
}


//get Elapsed Time
-(void)updateEachAndEverySeconds {
    remainingTimeForBooking -= 1;
    NSLog(@"Update Timer For Each Second = %ld", (long)remainingTimeForBooking);
    if(remainingTimeForBooking == 0) {
        [self stopTimer];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
        [self removeAddProgressView];
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Driver Unavailable.", @"Driver Unavailable.")];
        arrayOfMastersAround = [NSMutableArray new];
        [self clearTheMapBeforeChagingTheCarTypes];
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [self publishPubNubStream];
        [[MyAppTimerClass sharedInstance] startPublishTimer];
        [self stopBackgroundTask];
    }
}


-(void)startTimer{
    bookingTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                    target:self
                                                  selector:@selector(updateEachAndEverySeconds)
                                                  userInfo:nil
                                                   repeats:YES];
}

-(void)stopTimer {
    if(bookingTimer) {
        [bookingTimer invalidate];
        bookingTimer = nil;
    }
}

-(void)showScreenWhenDriverNotAcceptedBooking:(NSString *)message {
    dispatch_async(dispatch_get_main_queue(),^{
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:message];
    });
    [self bookingNotAccetedByAnyDriverScreenLayout];
    [self stopBackgroundTask];
}


#pragma mark - Booking Flow Methods -

-(void)bookingNotAccetedByAnyDriverScreenLayout {
    desLat = 0;
    desLong = 0;
    paymentTypesForLiveBooking = 0;
    dropOffAddressTextField.text = @"";
    dropOffAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
    [arrayOfMastersAround removeAllObjects];
    arrayOfMastersAround = nil;
    [arrayOfDriverID removeAllObjects];
    [arrayOfFirstOnlineDrivers removeAllObjects];
    arrayOfDriverID = nil;
    arrayOfFirstOnlineDrivers = nil;
    [centerPickUpObj.activityIndicator startAnimating];
    [[MyAppTimerClass sharedInstance] startSpinTimer];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
    [Helper setToLabel:centerPickUpObj.timeLabel Text:NSLocalizedString(@"No Drivers", @"No Drivers") WithFont:fontNormal FSize:7 Color:[UIColor whiteColor]];
    centerPickUpObj.timeLabel.hidden = YES;
    centerPickUpObj.activityIndicator.hidden = NO;
    [centerPickUpObj.activityIndicator startAnimating];

    UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
    [driverView removeFromSuperview];
    
    UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
    [driverMsgLabel removeFromSuperview];
    
    UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
    [currentPickUpMarkerView removeFromSuperview];
    
    UIView *currentLocationView =[self.view viewWithTag:customLocationViewTag];
    [currentLocationView removeFromSuperview];
    
    UIView *bottomView =[self.view viewWithTag:bottomViewWithCarTag];
    [bottomView removeFromSuperview];

    [confirmBottomViewObj hidePOPup];
    
    UIView *confirmBottomView = [self.view viewWithTag:confirmationBottomViewTag];
    [confirmBottomView removeFromSuperview];
    [customNavigationBarView addTitleButton];
    [customNavigationBarView hideLeftMenuButton:NO];
    if(kBookLater) {
        UIView *bookLaterView =[self.view viewWithTag:bookingOptionViewTag];
        [bookLaterView removeFromSuperview];
    }

    [self removeAddProgressView];
    isCustomMarkerSelected = NO;
    [self addPickUpLocationView];
    [self addCustomNavigationBar];
    NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUCarArrayKey]];
    arrayOfCarTypes = [carTypes mutableCopy];
    if (arrayOfCarTypes.count > 0) {
        [self addBottomView];
    }
    
    UIButton *nowBtn =  (UIButton *)[[self.view viewWithTag:bookingOptionViewTag] viewWithTag:nowBookingBtnTag];
    UIButton *laterBtn =  (UIButton *)[[self.view viewWithTag:bookingOptionViewTag] viewWithTag:laterBookingBtnTag];
    [nowBtn setSelected:NO];
    [laterBtn setSelected:NO];
    isNowSelected = NO;
    isLaterSelected = NO;
    centerPickUpObj = nil;

    [self createCenterView];
    
    if (!requestingView) {
        requestingView = [ProviderRequestingView sharedInstance];
    }
    [requestingView stopTimer];
    requestingView = nil;
    
    isRequestingButtonClicked = NO;
    [self getAddress:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude)];
    [self stopBackgroundTask];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NetworkLostWhileBooking" object:nil];

}

-(void)networkLostWhileBooking:(NSNotification *) notification {
    isRequestingButtonClicked = NO;
    [self stopTimer];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
    [self removeAddProgressView];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    arrayOfMastersAround = [NSMutableArray new];
    [self clearTheMapBeforeChagingTheCarTypes];
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    [self publishPubNubStream];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
    [self stopBackgroundTask];
    [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Could not connect to the Network", @"Could not connect to the Network")];
}

-(void)allDriversBusy:(NSNotification *)notification {
    [self stopTimer];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingApproximateFare];
    [self removeAddProgressView];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    arrayOfMastersAround = [NSMutableArray new];
    [self clearTheMapBeforeChagingTheCarTypes];
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    [self publishPubNubStream];
    [[MyAppTimerClass sharedInstance] startPublishTimer];
    [self stopBackgroundTask];
    [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@" :\n%@", notification.userInfo[@"aps"][@"alert"]]];
}

-(void)bookingAccetedBySomeDriverScreenLayout {
    [mapView_ clear];
    [self removeAddProgressView];
    [allMarkers removeAllObjects];
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
    [currentPickUpMarkerView removeFromSuperview];
    UIView *locationButtonsView = [self.view viewWithTag:loctionButtonsTag];
    [locationButtonsView removeFromSuperview];
    UIView *carTypeView =[self.view viewWithTag:bottomViewWithCarTag];
    [carTypeView removeFromSuperview];
    UIView *bookingOptionView =[self.view viewWithTag:bookingOptionViewTag];
    [bookingOptionView removeFromSuperview];
}


-(void)bookingConfirmed:(NSNotification*)notification {
    NSDictionary *userinfo = notification.userInfo;
    [Utility setDriverID:flStrForStr(userinfo[@"mid"])];
    [Utility setBookingDate:flStrForStr(userinfo[@"d"])];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    BOOL isAlreadyCalled = [self checkServiceCalledAtleastOnce];
    if (!isAlreadyCalled)
        [self publishA3ToGetBookingData];
}

-(void) updateDriverStatus {
    NSInteger bookingStatus = [Utility bookingStatus];
    if (!driverDetailsObj) {
        driverDetailsObj = [[DriverDetailsView alloc] init];
    }
    if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        [customNavigationBarView setTitle:NSLocalizedString(@"On my way to ZiRide", @"On my way to ZiRide") withColor:UIColorFromRGB(0xFFFFFF)];
    } else if (bookingStatus == kNotificationTypeBookingReachedLocation) {
        [customNavigationBarView setTitle:NSLocalizedString(@"I have arrived, You ready?", @"I have arrived, You ready?") withColor:UIColorFromRGB(0xFFFFFF)];
    } else if (bookingStatus == kNotificationTypeBookingStarted) {
        [customNavigationBarView setTitle:NSLocalizedString(@"Trip started, Relax and Enjoy the ride", @"Trip started, Relax and Enjoy the ride") withColor:UIColorFromRGB(0xFFFFFF)];
    } else {
        [driverDetailsObj removeFromSuperview];
        driverDetailsObj = nil;
    }
    if(bookingStatus == kNotificationTypeBookingOnMyWay  || bookingStatus == kNotificationTypeBookingReachedLocation) {
        driverDetailsObj.cancelView.hidden = NO;
    } else {
        driverDetailsObj.cancelView.hidden = YES;
    }
}


-(void)changeContentOfPresentController:(NSNotification *)notification {
    if ([notification.name isEqualToString:@"JOBCOMPLETED"]) {
        _isDriverOnTheWay = _isDriverArrived = isRequestingButtonClicked = isLaterSelected = NO;
        _isDriverArrived = NO;
        isRequestingButtonClicked = NO;
        isLaterSelected = NO;
        isNowSelected = NO;
        isInBooking = 0;
        sourceMarker = nil;
        destinationMarker = nil;
        bookedCarMarker = nil;
        UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
        [currentPickUpMarkerView removeFromSuperview];
        
        UIView *locationView = [self.view viewWithTag:customLocationViewTag];
        [locationView removeFromSuperview];
        
        self.dropOffAddressTextField.text = @"";
        [self addPickUpLocationView];
        [self createCenterView];
        [customNavigationBarView addTitleButton];
        [customNavigationBarView hideLeftMenuButton:NO];
        [self addSearchAddressView];
        if ([[Utility driverPubnubChannel] length])
            [self unSubsCribeToPubNubChannel:[Utility driverPubnubChannel]];
        [self clearUserDefaultsAfterBookingCompletion];
        desAddr = desAddrline2 = @"";
        dropOffAddressTextField.text = @"";
        dropOffAddressTextField.placeholder = NSLocalizedString(@"Location", @"Location");
        desLat = desLong = 0;
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
        NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUCarArrayKey]];
        arrayOfCarTypes = [carTypes mutableCopy];
        if (arrayOfCarTypes.count > 0) {
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = NO;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        UIEdgeInsets mapInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        mapView_.padding = mapInsets;
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [self publishPubNubStream];
        [[MyAppTimerClass sharedInstance] startPublishTimer];
    }
    NSInteger bookingStatus = [Utility bookingStatus];
    if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        if (!_isDriverOnTheWay) {
            [mapView_ clear];
            _isPathPlotted = NO;
            _isDriverOnTheWay = YES;
            [allMarkers removeAllObjects];
            waypoints_ = [[NSMutableArray alloc]init];
            waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
            [self subscribeOnlyBookedDriver];
            
            UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
            [currentPickUpMarkerView removeFromSuperview];
            
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = YES;
            
            UIView *locationView = [self.view viewWithTag:customLocationViewTag];
            [locationView removeFromSuperview];
            [self addPickUpAndDropOffLocationViewInBookingDetails];
            
            [self addCustomNavigationBar];
            [self bookingAccetedBySomeDriverScreenLayout];
            [self addCustomNavigationBar];
            [self createDriverArrivedView:bookingStatus];
            if(!([Utility pickUpLatitude] == 0 || [Utility pickUpLongitude] == 0)) {
                _currentLatitude = [Utility pickUpLatitude];
                _currentLongitude = [Utility pickUpLongitude];
                [self setStartLocationCoordinates:_currentLatitude Longitude:_currentLongitude :2];
                GMSCameraPosition *srcLocation = [GMSCameraPosition cameraWithLatitude: _currentLatitude longitude:_currentLongitude zoom:mapZoomLevel];
                [mapView_ setCamera:srcLocation];
            }
            [self sendRequestgetETAnDistance];
            [[MyAppTimerClass sharedInstance] startEtaNDisTimer];
        }
    } else if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        if (!_isDriverArrived) {
            _isDriverArrived = YES;
            UIView *currentPickUpMarkerView = [self.view viewWithTag:currentPickUpMarker];
            [currentPickUpMarkerView removeFromSuperview];
            
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = YES;
            
            UIView *locationViewBtn = [self.view viewWithTag:loctionButtonsTag];
            locationViewBtn.hidden = YES;
            
            UIView *locationView = [self.view viewWithTag:customLocationViewTag];
            [locationView removeFromSuperview];
            [self addPickUpAndDropOffLocationViewInBookingDetails];

            UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
            [driverMsgLabel removeFromSuperview];
            [self addCustomNavigationBar];
            [self createDriverArrivedView:bookingStatus];
            [self sendRequestgetETAnDistance];
            _isPathPlotted = NO;
            waypoints_ = [[NSMutableArray alloc]init];
            waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
            [self bookingAccetedBySomeDriverScreenLayout];
            desLat = [Utility dropOffLatitude];
            desLong = [Utility dropOffLongitude];
            if(!(desLong == 0 || desLong == 0))
                [self setStartLocationCoordinates: desLat Longitude:desLong :3];
            if (_currentLatitude == 0 || _currentLongitude == 0) {
                _currentLatitude =  [Utility pickUpLatitude];
                _currentLongitude = [Utility pickUpLongitude];
            }
            if(!(_currentLatitude == 0 || _currentLongitude == 0)) {
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
                sourceMarker = [GMSMarker markerWithPosition:position];
                sourceMarker.icon = [UIImage imageNamed:@"default_marker_p"];
                sourceMarker.map = mapView_;
                [self fitBoundsWithAllMarkers];
            }
            [self subscribeOnlyBookedDriver];
            if (!arrayOfpickerData) {
                arrayOfpickerData = [NSMutableArray array];
                selectedRowForTip = 0;
                for (NSInteger i = 5 ; i<=100; i+= 5) {
                    NSNumber *anumber = [NSNumber numberWithInteger:i];
                    NSString *aString = [NSString stringWithFormat:@"%@",[Helper getCurrencyLocal:[anumber floatValue]]];
                    [arrayOfpickerData addObject:aString];
                }
            }
        } else {
            [self addPickUpAndDropOffLocationViewInBookingDetails];
            [self addCustomNavigationBar];
            [self sendRequestgetETAnDistance];
            [self createDriverArrivedView:bookingStatus];
            if (!arrayOfpickerData) {
                arrayOfpickerData = [NSMutableArray array];
                selectedRowForTip = 0;
                for (NSInteger i = 5; i<=100; i+=5) {
                    NSNumber *anumber = [NSNumber numberWithInteger:i];
                    NSString *aString = [NSString stringWithFormat:@"%@", [Helper getCurrencyLocal:[anumber floatValue]]];
                    [arrayOfpickerData addObject:aString];
                }
            }

        }
    } else {
        [mapView_ animateToViewingAngle:0];
        UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
        [driverView removeFromSuperview];
        UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
        [driverMsgLabel removeFromSuperview];
        UIView *currentLocationView = [self.view viewWithTag:loctionButtonsTag];
        [currentLocationView removeFromSuperview];
        [self addLocationButtonsOnMap];
        [self createCenterView];
        
        UIView *locationView = [self.view viewWithTag:customLocationViewTag];
        [locationView removeFromSuperview];
        [self addPickUpLocationView];

        [self getCurrentLocation:nil];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        [self getAddress:position];
        [self createCenterView];
        [self getCurrentLocationFromGPS];
        NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey: kNSUCarArrayKey]];
        arrayOfCarTypes = [carTypes mutableCopy];
        if (arrayOfCarTypes.count > 0) {
            [self addBottomView];
            [self changeMapMarker:carTypesForLiveBooking-1];
            if (carTypesForLiveBookingServer == 0) {
                carTypesForLiveBookingServer = [arrayOfCarTypes[0][@"type_id"] integerValue];
            }
        }
        arrayOfMastersAround = [NSMutableArray new];
        [self clearTheMapBeforeChagingTheCarTypes];
        [self subscribeToPassengerChannel];
        [self publishPubNubStream];
        [self addCustomNavigationBar];
    }
}


#pragma mark - Custom Button Actions -

/**
 Choose Payment Type Button Action
 */
-(void)choosePaymentTypeButtonClicked {
    ChoosePaymentView* view = [[ChoosePaymentView alloc]init];
    switch (selectedPaymentType) {
        case 0:
            view.previousPaymentType = 0;
            break;
        case 1:
            view.previousPaymentType = 1;
            break;
        case 2:
            view.previousPaymentType = 2;
            break;
        default:
            view.previousPaymentType = 0;
            break;
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [view showPopUpOnView:window];
    view.onCompletion = ^(NSInteger paymentOption) {
        if (paymentOption == 0) {
            [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:NSLocalizedString(@"ZiRide Money", @"ZiRide Money") WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
            confirmBottomViewObj.paymentTypeImageView.image = [UIImage imageNamed:@"wallet_icon"];
            paymentTypesForLiveBooking = 2;
            selectedPaymentType = 0;
        } else if(paymentOption == 1) {
            [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:NSLocalizedString(@"Cash", @"Cash") WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
            confirmBottomViewObj.paymentTypeImageView.image = [UIImage imageNamed:@"cash_icon"];
            paymentTypesForLiveBooking = 2;
            selectedPaymentType = 1;
        } else if (paymentOption == 2) {
            NSArray *cardDetailArray = [Database getCardDetails];
            if(!cardDetailArray.count) {
                CardLoginViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CardLogin"];
                VC.isComingFromVC = kMapViewController;
                _isSelectinLocation = YES;
                VC.callback = ^(BOOL isCardAdded) {
                    self.navigationController.navigationBarHidden = YES;
                    if (isCardAdded) {
                        NSArray *cardsArray = [Database getCardDetails];
                        CardDetails *card = cardsArray[0];
                        NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
                        [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:[NSString stringWithFormat:@"%@ %@", @"XXXX", last4] WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
                        paymentTypesForLiveBooking = 1;
                        selectedPaymentType = 2;
                        confirmBottomViewObj.paymentTypeImageView.image = [Helper setPlaceholderToCardType:card.cardtype];
                    } else {
                        switch (selectedPaymentType) {
                            case 0: {
                                [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:NSLocalizedString(@"ZiRide Money", @"ZiRide Money") WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
                                confirmBottomViewObj.paymentTypeImageView.image = [UIImage imageNamed:@"wallet_icon"];
                                paymentTypesForLiveBooking = 2;
                            }
                                break;
                            case 1:{
                                [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:NSLocalizedString(@"Cash", @"Cash") WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
                                confirmBottomViewObj.paymentTypeImageView.image = [UIImage imageNamed:@"cash_icon"];
                                 paymentTypesForLiveBooking = 2;
                            }
                                break;
                            default:
                                break;
                        }
                    }
                };
                UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:VC];
                [self presentViewController:navBar animated:YES completion:nil];
            } else {
                CardDetails *card = cardDetailArray[0];
                NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
                [Helper setButton:confirmBottomViewObj.choosePaymentTypeBtn Text:[NSString stringWithFormat:@"%@ %@", @"XXXX", last4] WithFont:fontMedium FSize:14 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
                confirmBottomViewObj.paymentTypeImageView.image = [Helper setPlaceholderToCardType:card.cardtype];
                paymentTypesForLiveBooking = 1;
                selectedPaymentType = 2;
            }
        } else if(paymentOption == 3) {
            PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
            vc.isComingFromMapVC = YES;
            vc.callback = ^(NSString *cardIde , NSString *type, NSString *last4) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Money added to Wallet", @"Money added to Wallet")];
            };
            vc.isComingFromMapVC = YES;
            _isSelectinLocation = YES;
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
            [self presentViewController:navBar animated:YES completion:nil];
        } else {

        }
    };
}


/**
 Promo Code Button Action
 */
-(void)promoCodeButtonClicked {
    PromoCodeView* view = [[PromoCodeView alloc] init];
    view.headingText = NSLocalizedString(@"Apply Promo Code", @"Apply Promo Code");
    if ([[Utility promoCode] length]) {
        view.textViewPlaceHolder = [Utility promoCode];
    } else {
        view.textViewPlaceHolder = NSLocalizedString(@"Please enter your code here...", @"Please enter your code here...");
    }
    view.applyBtnTitleText = NSLocalizedString(@"SAVE", @"SAVE");
    view.userLatitude = _currentLatitude;
    view.userLongitude = _currentLongitude;
    view.isPromo = YES;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [view showPopUpWithDetailedDict:nil Onwindow: window];
    view.onCompletion = ^(NSInteger applyBtnClicked, NSString *returnText) {
        if (applyBtnClicked == 1) {
            if (returnText.length) {
                promocode = returnText;
                [self sendAServiceForFareCalculator];
            } else {
                promocode = @"";
            }
        } else {
            promocode = @"";
        }
        customNavigationBarView.rightbarButton.userInteractionEnabled = YES;
    };
}


/**
 *  when the search button on the home screen is clicked
 *
 *  @param sender sender of the button whose tag is 91, This method is to make sure the location is not changing after picking up the fixing the pickup location
 */
-(void)searchButtonClicked:(id)sender {
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    UIButton *mBtn = (UIButton *)sender;
    [self locationButtonClicked:mBtn];
}

/**
 Add notes to Driver
 */
-(void)addNotesToDriverButtonClicked {
    UIButton *noteToDriver = (UIButton *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:911];
    noteToDriver.userInteractionEnabled = NO;
    if(!arrayOfDriverID.count) {
        noteToDriver.userInteractionEnabled = YES;
        return;
    }
    PromoCodeView* view = [[PromoCodeView alloc]init];
    view.headingText = NSLocalizedString(@"Add Notes To Driver", @"Add Notes To Driver");
    view.textViewPlaceHolder = NSLocalizedString(@"Please enter your message here...", @"Please enter your message here...");
    view.applyBtnTitleText = NSLocalizedString(@"DONE", @"DONE");
    view.userLatitude = _currentLatitude;
    view.userLongitude = _currentLongitude;
    view.isPromo = NO;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [view showPopUpWithDetailedDict:nil Onwindow: window];
    view.onCompletion = ^(NSInteger applyBtnClicked, NSString *returnText) {
        if (applyBtnClicked == 1) {
            if (returnText.length) {
                driverNotes = returnText;
            } else {
                driverNotes = @"";
            }
        } else {
            driverNotes = @"";
        }
        noteToDriver.userInteractionEnabled = YES;
    };
}

/**
 Location Button Action

 @param sender Button clicked by User
 */
- (void)locationButtonClicked:(UIButton *)sender {
    PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
    if(sender.tag == 155 || sender.tag == 91) {
        pickController.locationType = kSourceAddress;
        pickController.isComingFromMapVCFareButton = NO;
    } else if(sender.tag == 175) {
        pickController.locationType = kDestinationAddress;
        pickController.isComingFromMapVCFareButton = NO;
    } else {
        pickController.locationType = kDestinationAddress;
        pickController.isComingFromMapVCFareButton = NO;
    }
    isChangeLocationChangingInBooking = YES;
    pickController.typeID = carTypesForLiveBookingServer;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
    pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
    pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType) {
        _isAddressManuallyPicked = YES;
        NSString *addressText = flStrForStr(dict[@"address1"]);
        if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound) {
            addressText = [addressText stringByAppendingString:@", "];
            addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
        } else {
            addressText = flStrForStr(dict[@"address2"]);
        }
        if(locationType == 1) {
            UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            isUpdatedDropOffAddress = YES;
            textAddress.text = addressText;
            srcAddr = flStrForStr(dict[@"address1"]);
            srcAddrline2 = flStrForStr(dict[@"address2"]);
            srcLat = [dict[@"lat"] floatValue];
            srcLong = [dict[@"lng"] floatValue];
            pickUpAddressTextField.text = addressText;
            pickUpAddressTextField.textColor = UIColorFromRGB(0x666666);
            zipCodeOnMap = flStrForStr(dict[@"zipCode"]);
            if (isCustomMarkerSelected) {
                UILabel *pickupLabel = (UILabel *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:1010];
                if([dict[@"locationType"] length])
                    pickupLabel.text = [flStrForStr(dict[@"locationType"]) capitalizedString];
                else
                    pickupLabel.text = NSLocalizedString(@"Pickup Location", @"Pickup Location");
            } else {
                UILabel *pickupLabel = (UILabel *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:1010];
                if([dict[@"locationType"] length])
                    pickupLabel.text = [flStrForStr(dict[@"locationType"]) capitalizedString];
                else
                    pickupLabel.text = NSLocalizedString(@"Pickup Location", @"Pickup Location");
                
                UIButton *btn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:97];
                if ([dict[@"isFavourite"] intValue]) {
                    btn.selected = YES;
                } else {
                    btn.selected = NO;
                }
            }
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
            CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:srcLat longitude:srcLong];
            [self checkDistanceWithPreviousLocation:newLocation];
            if (isCustomMarkerSelected && !isRequestingButtonClicked) {
                [self sendAServiceForFareCalculator];
            }
        } else if(sender.tag == 175) {
            desAddr = flStrForStr(dict[@"address1"]);
            desAddrline2 = flStrForStr(dict[@"address2"]);
            desLat = [dict[@"lat"] floatValue];
            desLong = [dict[@"lng"] floatValue];
            UIView *customView = [self.view viewWithTag:170];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:171];
            textAddress.text = addressText;
            textAddress.textColor = UIColorFromRGB(0x333333);
            isUpdatedDropOffAddress = YES;
            [self sendServiceToUpdateDropAddress];
            [Utility setDropOffLatitude:desLat];
            [Utility setDropOffLongitude:desLong];
            _currentLatitude =  [Utility pickUpLatitude];
            _currentLongitude = [Utility pickUpLongitude];
            NSInteger bookingStatus = [Utility bookingStatus];
            if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
                _isPathPlotted = NO;
                [mapView_ clear];
                waypoints_ = [[NSMutableArray alloc]init];
                waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
                [self setStartLocationCoordinates: desLat Longitude:desLong :3];
                if(!(_currentLatitude == 0 || _currentLongitude == 0)) {
                    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
                    sourceMarker = [GMSMarker markerWithPosition:position];
                    sourceMarker.map = mapView_;
                    sourceMarker.icon = [UIImage imageNamed:@"default_marker_p"];
                    [self updateDestinationLocationWithLatitude:_currentLatitude Longitude:
                     _currentLongitude];
                    [self fitBoundsWithAllMarkers];
//                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude
//                                                                            longitude:_currentLongitude
//                                                                                 zoom:mapZoomLevel];
//                    
//                    [mapView_ setCamera:camera];
                }
            } else {
                if (!isUpdatingDropOff) {
                    [self sendAServiceForFareCalculator];
                } else {
                    isUpdatingDropOff = NO;
                }
            }
        } else if (sender.tag == 165) {
            isUpdatedDropOffAddress = YES;
            UIView *customView = [self.view viewWithTag:customLocationViewTag];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:161];
            textAddress.text = addressText;
            textAddress.textColor = UIColorFromRGB(0x666666);
            desAddr = flStrForStr(addressText);
            desAddrline2 = flStrForStr(@"");
            desLat = [dict[@"lat"] floatValue];
            desLong = [dict[@"lng"] floatValue];
            UILabel *dropOffLabel = (UILabel *) [[self.view viewWithTag:customLocationViewTag] viewWithTag:1650];
            if([dict[@"locationType"] length])
                dropOffLabel.text = [flStrForStr(dict[@"locationType"]) capitalizedString];
            else
                dropOffLabel.text = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
            NSInteger bookingStatus = [Utility bookingStatus];
            if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
                isUpdatedDropOffAddress = YES;
                [self sendServiceToUpdateDropAddress];
                [Utility setDropOffLatitude:desLat];
                [Utility setDropOffLongitude:desLong];
                _currentLatitude =  [Utility pickUpLatitude];
                _currentLongitude = [Utility pickUpLongitude];
                if (bookingStatus != kNotificationTypeBookingStarted) {
                    [self sendAServiceForFareCalculator];
                }
            }
            if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
                _isPathPlotted = NO;
                [mapView_ clear];
                waypoints_ = [[NSMutableArray alloc] init];
                waypointStrings_ = [[NSMutableArray alloc] initWithCapacity:2];
                [self setStartLocationCoordinates: desLat Longitude:desLong :3];
                if(!(_currentLatitude == 0 || _currentLongitude == 0)) {
                    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
                    sourceMarker = [GMSMarker markerWithPosition:position];
                    sourceMarker.icon = [UIImage imageNamed:@"default_marker_p"];
                    sourceMarker.map = mapView_;
                    [self updateDestinationLocationWithLatitude:_currentLatitude Longitude:_currentLongitude];
                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude
                                                                            longitude:_currentLongitude
                                                                                 zoom:mapZoomLevel];
                    [mapView_ setCamera:camera];
                }
            } else {
                if (!isUpdatingDropOff) {
                    [self sendAServiceForFareCalculator];
                } else {
                    isUpdatingDropOff = NO;
                }
            }
        }
    };
    _isSelectinLocation = YES;
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 This add location as favourite

 @param sender favourite button
 */
-(void)addLocationAsFavourite:(UIButton *)sender {
    UIButton *favBtn = sender;
    if (favBtn.tag == 97) {
        locationTypeForFavourite = kSourceAddress;
    } else {
        locationTypeForFavourite = kDestinationAddress;
    }
    if (favBtn.selected) {
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Are you sure you want to delete this favourite location?", @"Are you sure you want to delete this favourite location?")];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Yes", @"Yes"), NSLocalizedString(@"No", @"No"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                NSString *addressID = [Database getAddressIDForRemoveFromFavouriteAddress:srcAddr and:srcAddrline2];
                if (addressID.length) {
                    [self removeFromFavouriteAddress:addressID];
                } else {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Sorry, something went wrong.", @"Sorry, something went wrong.")];
                }
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
    } else {
        SaveLocationViewController *saveLocVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SaveLocationVC"];
        saveLocVC.dropoffAddressView.hidden = YES;
        saveLocVC.saveLocationView.hidden = NO;
        saveLocVC.addressChangable = NO;
        saveLocVC.isComingForDropAddress = NO;
        saveLocVC.navigationTitle = NSLocalizedString(@"Favourite Location", @"Favourite Location");
        saveLocVC.lastBtnName = NSLocalizedString(@"Save Location",@"Save Location");
        saveLocVC.address = flStrForStr(pickUpAddressTextField.text);
        saveLocVC.address1 = flStrForStr(srcAddr);
        saveLocVC.address2 = flStrForStr(srcAddrline2);
        saveLocVC.zipCode = flStrForStr(zipCodeOnMap);
        saveLocVC.latitude = srcLat;
        saveLocVC.longitude = srcLong;
        saveLocVC.locationType = locationTypeForFavourite;
        saveLocVC.onCompletion =^(NSInteger success, NSDictionary *dict){
            UIButton *btn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:97];
            if (success) {
                btn.selected = YES;
            } else {
                btn.selected = NO;
            }
        };
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:saveLocVC];
        [self presentViewController:navBar animated:YES completion:nil];
        isUpdatedDropOffAddress = YES;
    }
}


/**
 This will open SearchAddressVC to adddrop off address and then calculate Fare Estimate
 */
-(void)fareButtonClicked {
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    if(srcAddr == nil) {
        UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
        textAddress.textColor = UIColorFromRGB(0x333333);
        srcAddr = textAddress.text;
        srcAddrline2 = @"";
    }
    if(DropOffViewForConfirmScreen == nil || desLat == 0 || desLong == 0) {
        UIButton *dummyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        dummyBtn.tag = 165;
        [self locationButtonClicked:dummyBtn];
        if(desAddr == nil)
            desAddr = @"";
        desAddrline2 = @"";
    }
}


- (void)getCurrentLocation:(UIButton *)sender {
    previousLocation = [[CLLocation alloc] initWithLatitude:_currentLatitude longitude:_currentLongitude];
    CLLocation *location = mapView_.myLocation;
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    if (location) {
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
        [mapView_ animateWithCameraUpdate:zoomCamera];
        [self getAddress:CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude)];
    }
}


-(void)changeMapType:(UIButton *)sender {
    if (sender.isSelected == NO) {
        sender.selected = YES;
        mapView_.mapType = kGMSTypeSatellite;
    } else {
        sender.selected = NO;
        mapView_.mapType = kGMSTypeNormal;
    }
}

-(void)changeMapTrafficMode:(UIButton *)sender {
    if (sender.isSelected == NO) {
        sender.selected = YES;
        mapView_.trafficEnabled = YES;
    } else {
        sender.selected = NO;
        mapView_.trafficEnabled = NO;
    }
}

-(void) sendBooking:(id)sender {
    if (isLaterSelected && kWithDispatch) {
        if (paymentTypesForLiveBooking != 0) {
            [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"", @"")];
            [[MyAppTimerClass sharedInstance] stopPublishTimer];
            [self makeMyProgressBarMoving];
            [addProgressBarView setHidden:YES];
            [self sendDriverBooking];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please choose payement type", @"Please choose payement type")];
        }
    } else {
        if (arrayOfDriverID.count && paymentTypesForLiveBooking != 0) {
            if(paymentTypesForLiveBooking == 1) {
                arrDBResult = [[Database getCardDetails] mutableCopy];
                if (arrDBResult.count && arrayOfDriverID.count) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
                    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
                    paymentTypesForLiveBooking = 1;
                    if (filterdArray.count) {
                        CardDetails *card = filterdArray[0];
                        cardId = card.idCard;
                    } else {
                        CardDetails *card = filterdArray[0];
                        cardId = card.idCard;
                    }
                }
            }
            isBookingButtonClicked = YES;
            [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"", @"")];
            [[MyAppTimerClass sharedInstance] stopPublishTimer];
            [self makeMyProgressBarMoving];
            [addProgressBarView setHidden:YES];
            [self sendDriverBooking];
//            [self sendAppointmentRequestForLiveBooking];
        } else if(paymentTypesForLiveBooking == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please choose payement type", @"Please choose payement type")];
        }
    }
}

-(void)vehicleButtonAction:(UIButton *)sender {
    UIButton *mBtn = (UIButton *)sender;
    if(mBtn.selected == YES && !isVehicleBtnClickedForRateCard) {
        isVehicleBtnClickedForRateCard = YES;
        [self performSelector:@selector(rateCardButtonClicked) withObject:nil afterDelay:0.5];
    } else if(mBtn.selected == NO) {
        carTypesForLiveBooking = mBtn.tag - 201;
        carTypesForLiveBookingServer = [arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
        [self changeMapMarker:carTypesForLiveBooking-1];
        [self publishPubNubStream];
    }
    for (UIView *view in mBtn.superview.superview.subviews) {
        UIButton *button = [[view subviews] firstObject];
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    mBtn.selected = YES;
}

-(void)contactBtnAction:(id)sender {
    if([[Utility driverMobileNumber] length]) {
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:[Utility driverMobileNumber]]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:[Utility driverMobileNumber]]];
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Failure", @"Failure") Message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")];
        }
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Failure", @"Failure") Message:NSLocalizedString(@"Mobile number is not valid", @"Mobile number is not valid")];
    }
}

-(void)cancelBtnAction:(id) sender {
    [self checkCancellationTimeBeforeCancel];
}

-(void)shareBtnAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"Share", @"Share")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:NSLocalizedString(@"E-mail",@"E-mail"),
                                  NSLocalizedString(@"SMS",@"SMS"),
                                  NSLocalizedString(@"WhatsApp",@"WhatsApp"), nil];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}


#pragma mark - UIActionSheet Delegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        [self emailSharing];
    } else if (buttonIndex == 1) {
        [self shareViaSMS];
    } else if (buttonIndex == 2) {
        [self sendTextOnWhatsApp];
    }
}

-(void)shareViaSMS{
    if([MFMessageComposeViewController canSendText]){
        MFMessageComposeViewController *viewController = [[MFMessageComposeViewController alloc] init];
        viewController.messageComposeDelegate = self;
        NSString *shareUrl = shareUrlViaSMS;
        NSString *str1 = NSLocalizedString(@"Hi, You can track me live on ZiRide", @"Hi, You can track me live on ZiRide");
        viewController.body = [NSString stringWithFormat:@"%@\n %@",str1,shareUrl];
        [[viewController navigationBar] setTintColor: [UIColor blackColor]];
        viewController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        [self presentViewController:viewController animated:YES completion:NULL];
    }
    else{
        [Helper showAlertWithTitle:NSLocalizedString(@"Alert!", @"Alert!") Message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!")];
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed: {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:NSLocalizedString(@"Failed to send!", @"Failed to send!")];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)emailSharing {
    if ([MFMailComposeViewController canSendMail]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:fontBold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [[mailer navigationBar] setTintColor: [UIColor blackColor]];
        mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        NSString *str1 = NSLocalizedString(@"Hi, You can track me live on ZiRide", @"Hi, You can track me live on ZiRide");
        [mailer setSubject:NSLocalizedString(@"Track me live on ZiRide", @"Track me live on ZiRide")];
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        NSMutableString* message =[NSMutableString stringWithFormat:@"%@\n %@",str1,flStrForStr(shareUrlViaSMS)];
        [mailer setMessageBody:message isHTML:NO];
        [mailer setToRecipients:toRecipents];
        UIImage *image = [UIImage imageNamed:@"appIcon"];
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
        [self presentViewController:mailer animated:YES completion:^{
            [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        }];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Failure", @"Failure") Message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")];
    }
}
- (void)sendTextOnWhatsApp {
    NSString *str1 = NSLocalizedString(@"Hi, You can track me live on ZiRide", @"Hi, You can track me live on ZiRide");
    NSString* message =[NSString stringWithFormat:@"%@\n %@",str1,flStrForStr(shareUrlViaSMS)];
    message = [message stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    message = [message stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    message = [message stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    message = [message stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    message = [message stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    message = [message stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    message = [message stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    NSString *urlWhats = [NSString stringWithFormat:@"%@%@",whatsAppSendTextUrl,message];
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:whatsAppUrl]]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
    } else {
        [Helper showAlertWithTitle:@"Message" Message:@"Your device has no WhatsApp installed."];
    }
}


#pragma mark - MFMailComposeViewController Delegate -

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Google API's -

/**
 Calculate time and distance to arrange drivers
 
 @param driversLatAndLongArray drivers Lat and Long array
 */
- (void) calculateTimeAndDistanceToArrangeDriversArray:(NSArray *)driversLatAndLongArray andDriverIDsArray:(NSArray *)driverIDsArray {
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *driverLocations = [driversLatAndLongArray componentsJoinedByString:@"|"];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%f,%f&mode=driving&mode=transit&transit_mode=bus&key=%@", driverLocations, srcLat, srcLong, [Utility distanceMatrixAPIKey]];
    NSLog(@"Google Params = %@", strUrl);
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(calculatedTimeAndDistanceToArrangeDriver:)];
//    NSString *driverIDs = [driverIDsArray componentsJoinedByString:@","];
    //[self updateGoogleDistanceMatrixAPICountOnServer:strUrl andDriverID:driverIDs];
}



/**
 Calculate time and distance to arrange drivers
 
 @param driversLatAndLongArray drivers Lat and Long array
 */
- (void) calculateTimeAndDistanceToArrangeDrivers:(NSString *)driversLatAndLong andDriverID:(NSString *)driverID {
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%f,%f&mode=driving&mode=transit&transit_mode=bus&key=%@", driversLatAndLong, srcLat, srcLong, [Utility distanceMatrixAPIKey]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(calculatedTimeAndDistanceToArrangeDriver:)];
    [self updateGoogleDistanceMatrixAPICountOnServer:strUrl andDriverID:driverID];
}

/**
 Calculated Time and Distance To Arrange Driver Google Response
 
 @param response reponse dictionary
 */
- (void)calculatedTimeAndDistanceToArrangeDriver:(NSDictionary *)response {
    [self hideAcitvityIndicator];
    if (!response) {
    } else {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response[@"ItemsList"]];
        if (!checkStatus) {
            if ([[Utility googleKeysArray] count]) {
                NSMutableArray *googleKeysArray = [[Utility googleKeysArray] mutableCopy];
                NSString *distanceMatrixKey = [[Utility googleKeysArray] firstObject];
                [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
                [googleKeysArray removeObjectAtIndex:0];
                [Utility setGoogleKeysArray:googleKeysArray];
                [self publishPubNubStream];
            } else {
                [self getKeysService];
            }
        } else {
            NSArray *distanceArr = [[NSArray alloc] initWithArray:response[@"ItemsList"][@"rows"]];
            if (([[response[@"ItemsList"] allKeys] containsObject:@"ZERO_RESULTS"] && [response[@"ItemsList"][@"status"] isEqualToString:@"ZERO_RESULTS"]) || ([[response[@"ItemsList"] allKeys] containsObject:@"REQUEST_DENIED"] && [response[@"ItemsList"][@"status"] isEqualToString:@"REQUEST_DENIED"])) {
                return;
            }
            NSLog(@"Google Response = %@", response);
            if (distanceArr.count && distanceArr.count == arrayOfFirstOnlineDriversButton.count) {
                for (int i = 0; i < arrayOfFirstOnlineDriversButton.count; i++) {
                    if (distanceArr.count && [Utility maximumETA] > 0 && [distanceArr[i][@"elements"][0][@"duration"][@"value"] integerValue] > [Utility maximumETA]) {
                        UIButton *button = arrayOfFirstOnlineDriversButton[i];
                        NSString *buttonTitle = button.titleLabel.text;
                        NSString *searchString = @"\n\n\n";
                        NSRange range1 = [buttonTitle rangeOfString:searchString];
                        NSString *noCabs = NSLocalizedString(@"NO CABS", @"NO CABS");
                        [Helper setButton:button Text:[noCabs stringByAppendingString:[buttonTitle substringFromIndex:range1.location]] WithFont:fontSemiBold FSize:10 TitleColor:UIColorFromRGB(0x9E9E9E) ShadowColor:nil];
                        [button setTitleColor:UIColorFromRGB(0x9E9E9E) forState:UIControlStateNormal];
                        [button setTitleColor:UIColorFromRGB(0x484848) forState:UIControlStateSelected];
                        [self setCarTypeButtonContentInsets:button];
                        [mapView_ clear];
                        if (button  == arrayOfButtons[carTypesForLiveBooking - 1] && isCustomMarkerSelected) {
                            [customNavigationBarView setTitle:[NSString stringWithFormat:@"%@, %@", arrayOfCarTypes[carTypesForLiveBooking - 1][@"type_name"], [noCabs capitalizedString]] withColor:UIColorFromRGB(0xFFFFFF)];
                            confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = NO;
                            confirmBottomViewObj.requestBookingBtn.selected = YES;
                        }
                    } else {
                        int timeInMin = [self convertTimeInMin:[distanceArr[i][@"elements"][0][@"duration"][@"value"] intValue]];
                        NSString *time = [NSString stringWithFormat:@"%d %@", timeInMin, NSLocalizedString(@"MIN", @"MIN")];
                        UIButton *button = arrayOfFirstOnlineDriversButton[i];
                        NSString *buttonTitle = button.titleLabel.text;
                        NSString *searchString = @"\n\n\n";
                        NSRange range1 = [buttonTitle rangeOfString:searchString];
                        [Helper setButton:button Text:[time stringByAppendingString:[buttonTitle substringFromIndex:range1.location]] WithFont:fontSemiBold FSize:10 TitleColor:UIColorFromRGB(0x9E9E9E) ShadowColor:nil];
                        [button setTitleColor:UIColorFromRGB(0x9E9E9E) forState:UIControlStateNormal];
                        [button setTitleColor:UIColorFromRGB(0x484848) forState:UIControlStateSelected];
                        [self setCarTypeButtonContentInsets:button];
                        if (button  == arrayOfButtons[carTypesForLiveBooking - 1] && isCustomMarkerSelected) {
                            [customNavigationBarView setTitle:[NSString stringWithFormat:@"%@, %@", arrayOfCarTypes[carTypesForLiveBooking - 1][@"type_name"], [time capitalizedString]] withColor:UIColorFromRGB(0xFFFFFF)];
                            confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = YES;
                            confirmBottomViewObj.requestBookingBtn.selected = NO;
                        }
                    }
                }
                for (int i = 0; i < arrayOfFirstOfflineDriversButton.count; i++) {
                    UIButton *button = arrayOfFirstOfflineDriversButton[i];
                    NSString *buttonTitle = button.titleLabel.text;
                    NSString *searchString = @"\n\n\n";
                    NSRange range1 = [buttonTitle rangeOfString:searchString];
                    NSString *noCabs = NSLocalizedString(@"NO CABS", @"NO CABS");
                    [Helper setButton:button Text:[noCabs stringByAppendingString:[buttonTitle substringFromIndex:range1.location]] WithFont:fontSemiBold FSize:10 TitleColor:UIColorFromRGB(0x9E9E9E) ShadowColor:nil];
                    [button setTitleColor:UIColorFromRGB(0x9E9E9E) forState:UIControlStateNormal];
                    [button setTitleColor:UIColorFromRGB(0x484848) forState:UIControlStateSelected];
                    [self setCarTypeButtonContentInsets:button];
                    if (button  == arrayOfButtons[carTypesForLiveBooking - 1] && isCustomMarkerSelected) {
                        [customNavigationBarView setTitle:[NSString stringWithFormat:@"%@, %@", arrayOfCarTypes[carTypesForLiveBooking - 1][@"type_name"], [noCabs capitalizedString]] withColor:UIColorFromRGB(0xFFFFFF)];
                        confirmBottomViewObj.requestBookingBtn.userInteractionEnabled = NO;
                        confirmBottomViewObj.requestBookingBtn.selected = YES;
                        [mapView_ clear];
                    }
                }
            } else {
                [self updateTheNearestDriverinSpinitCircle:NSLocalizedString(@"All Busy", @"All Busy")];
            }
        }
    }
}


/**
 Send Request for ETA and Distance
 */
- (void) sendRequestgetETAnDistance {
    NSInteger bookingStatus = [Utility bookingStatus];
    double lDesLat  = _driverCurLat;
    double lDesLong = _driverCurLong;
    if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        _currentLatitude =  [Utility pickUpLatitude];
        _currentLongitude = [Utility pickUpLongitude];
        if (lDesLat == 0 || lDesLong == 0) {
            lDesLat = [Utility driverLatitude];
            lDesLong = [Utility driverLongitude];
        }
    }
    if (_currentLatitude == 0 || _currentLongitude == 0 || lDesLat == 0 || lDesLong == 0) {
        return;
    }
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&mode=transit&transit_mode=bus&api=%@",lDesLat,lDesLong,_currentLatitude,_currentLongitude, [Utility distanceMatrixAPIKey]];
    NSURL *url = [NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(googleResponseForETAnDistance:)];
    [self updateGoogleDistanceMatrixAPICountOnServer:strUrl andDriverID:nil];
}


/**
 Google Reponse for ETA and Distance while Booking and On the Way state
 
 @param response response dictionary
 */
- (void)googleResponseForETAnDistance:(NSDictionary *)response {
    if (!response) {
    } else {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response[@"ItemsList"]];
        if (!checkStatus) {
            if ([[Utility googleKeysArray] count]) {
                NSMutableArray *googleKeysArray = [[Utility googleKeysArray] mutableCopy];
                NSString *distanceMatrixKey = [[Utility googleKeysArray] firstObject];
                [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
                [googleKeysArray removeObjectAtIndex:0];
                [Utility setGoogleKeysArray:googleKeysArray];
                [self publishPubNubStream];
            } else {
                [self getKeysService];
            }
        } else {
            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
            if ([distance doubleValue] < 500) {
                distance = [NSString stringWithFormat:@"%@ %@", distance, NSLocalizedString(@"Meters", @"Meters")];
            } else {
                distance = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kDistanceMetric, kDistanceParameter];
            }
            NSString *str1= NSLocalizedString(@"On my way to ZiRide", @"On my way to ZiRide");
            NSString *str2= NSLocalizedString(@"away", @"away");
            driverDetailsObj.driverStatusLabel.text = [NSString stringWithFormat:@"%@ %@ %@", str1, distance, str2];
            int timeInSec =  [response[@"ItemsList"][@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
            int timeInMin = [self convertTimeInMin:timeInSec];
            NSString *str = NSLocalizedString(@"min", @"min");
            if(driverDetailsObj) {
                driverDetailsObj.timeLeftLabel.text = [NSString stringWithFormat:@"%@ %@", [NSString stringWithFormat:@"%d %@", timeInMin, str], NSLocalizedString(@"away", @"away")];
            }

        }
    }
}

/**
 Contains Keys to check Google Reponse for ETA and Distance is coorect or not
 
 @param key key what to compare
 @param dict dict data
 @return YES if success
 NO if fail
 */
- (BOOL)containsKey: (NSString *)key dictionary:(NSDictionary *) dict {
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    if (retVal) {
        if ([dict[@"status"] isEqualToString:@"OK"]){
            retVal = YES;
        } else {
            retVal = NO;
        }
    }
    return retVal;
}

/**
 Convert Time in Minutes
 
 @param timeInSec time in seconds
 @return time in Minutes
 */
-(int) convertTimeInMin:(int) timeInSec {
    int min;
    if (timeInSec < 60 && timeInSec <= 0){
        min = 1;
    } else if (timeInSec > 60) {
        min = timeInSec/60;
        int remainingTime = timeInSec%60;
        if (remainingTime < 60 && remainingTime >= 30)
            min++;
    } else {
        min = 1;
    }
    return min;
}

- (void)animateInBottomView {
    if(kBookLater) {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview = bottomView1.frame;
        frameTopview.origin.y = self.view.bounds.size.height-120;
        
        UIView *bottomView2 = [self.view viewWithTag:bookingOptionViewTag];
        CGRect frameTopview2 = bottomView2.frame;
        frameTopview2.origin.y = self.view.bounds.size.height - 50;
        
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview;
            bottomView2.frame = frameTopview2;
        }];
    } else {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview = bottomView1.frame;
        frameTopview.origin.y = self.view.bounds.size.height-70;
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview;
        }];
    }
}

- (void)animateOutBottomView {
    if(kBookLater) {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview1 = bottomView1.frame;
        frameTopview1.origin.y = self.view.bounds.size.height-70;
        UIView *bottomView2 = [self.view viewWithTag:bookingOptionViewTag];
        CGRect frameTopview2 = bottomView2.frame;
        frameTopview2.origin.y = self.view.bounds.size.height;
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview1;
            bottomView2.frame = frameTopview2;
        }];
    } else {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview1 = bottomView1.frame;
        frameTopview1.origin.y = self.view.bounds.size.height;
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview1;
        }];
    }
}

-(void)clearTheMapBeforeChagingTheCarTypes {
    allMarkers = [NSMutableDictionary new];
    arrayOfDriverID = [NSMutableArray new];
    arrayOfFirstOnlineDrivers = [NSMutableArray new];
    arrayOfFirstOnlineDriversButton = [NSMutableArray new];
    arrayOfFirstOfflineDriversButton = [NSMutableArray new];
    if (![Utility isUserInBooking]) {
        [mapView_ clear];
    }
}

-(void) refreshDriverArrays {
    arrayOfDriverID = [NSMutableArray new];
    arrayOfFirstOnlineDrivers = [NSMutableArray new];
    arrayOfFirstOnlineDriversButton = [NSMutableArray new];
    arrayOfFirstOfflineDriversButton = [NSMutableArray new];
}

-(void)changeMapMarker:(NSInteger)type {
    [allMarkers removeAllObjects];
    [mapView_ clear];
    drivers = [[NSMutableArray alloc] init];
    if (arrayOfMastersAround.count <= type && arrayOfCarTypes.count <= type) {
        carTypesForLiveBooking = 1;
        return;
    }
    if (arrayOfMastersAround.count) {
        [drivers addObjectsFromArray:arrayOfMastersAround[type][@"mas"]];
    }
    if (drivers.count > 0) {
        [self addCustomMarkerFor];
    } else {
        [self hideAcitvityIndicator];
    }
}


#pragma  mark - Custom Methods  -

-(void)checkDistanceWithPreviousLocation:(CLLocation *)newLocation {
    if(previousLocation && newLocation) {
        CLLocationDistance distance = [newLocation distanceFromLocation:previousLocation];
        NSInteger thresholdDistance;
        switch ([Utility operatingAreaType]) {
            case kOperatingArea:
                thresholdDistance = [Utility radiusForOperatingArea];
                break;
            case kNotOperatingArea:
                thresholdDistance = [Utility radiusForNotOperatingArea];
                break;
            default:
                break;
        }
        if (distance > thresholdDistance) {
            if ([Utility operatingAreaType] == kOperatingArea) {
                [self refreshDriverArrays];
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
                [self publishPubNubStream];
                [[MyAppTimerClass sharedInstance] startPublishTimer];
            } else  if ([Utility operatingAreaType] == kNotOperatingArea) {
                [self serviceToNotifyUserIsComingFromNotOperatingArea];
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
            }
        }
        if (distance > 1000 && [Utility operatingAreaType] == kOperatingArea) {
            [self sendAServiceForFareCalculator];
        }
        previousLocation = nil;
    }
}

-(void)goToPaymentController {
    PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
    vc.callback = ^(NSString *cardIde , NSString *type, NSString *last4){
        UIView *v = [self.view viewWithTag:501];
        UIButton *btn = (UIButton *)[v viewWithTag:502];
        [btn setImage:[Helper setPlaceholderToCardType:type] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
        NSString *str1 =  NSLocalizedString(@"Ends with", @"Ends with");
        last4 = [last4 substringFromIndex: [last4 length] - 4];
        [btn setTitle:[NSString stringWithFormat:@"%@ %@", str1, last4] forState:UIControlStateNormal];
        cardId = cardIde;
        paymentTypesForLiveBooking = 1;
    };
    vc.isComingFromMapVC = YES;
    _isSelectinLocation = YES;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 *  This methods get called after calling Handletap method,it brings the main view to its original position
 */

- (void)closeAnimation {
    [UIView animateWithDuration:0.5f animations:^{
        self.view.transform = CGAffineTransformIdentity;
    }];
}

- (void)closeTransparentView{
    UIView *viewToClose = [self.view viewWithTag:50];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    [viewToClose removeFromSuperview];
    [UIView commitAnimations];
}

- (void)changeCurrentLocation:(NSDictionary *)dictAddress {
    NSString *latitude = [dictAddress objectForKey:@"lat"];
    NSString *longitude = [dictAddress objectForKey:@"lng"];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
}


#pragma mark - Animations -

/**
 This method will call when user touches to map screen
 */
- (void)startAnimation {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    NSInteger bookingStatus = [Utility bookingStatus];
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    if(isCustomMarkerSelected == YES) {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 30;
            PikUpViewForConfirmScreen.frame = rectPick;
            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 74;
            DropOffViewForConfirmScreen.frame = rectDrop;
        } else {
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 30;
            PikUpViewForConfirmScreen.frame = rect;
        }
    } else if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        
    } else {
        UIView *customNaviagtionBar = [self.view viewWithTag:78];
        CGRect rect2 = customNaviagtionBar.frame;
        rect2.origin.y = -210;
        customNaviagtionBar.frame = rect2;

        UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = - 210;
        customLocationView.frame = rect;
        customLocationView.alpha = 0;
//        [customNavigationBarView hideLeftMenuButtonInMapVC:YES];
        [customNavigationBarView.rightbarButton setHidden:YES];
        
//        UIView *carTypeView = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect rect3 = scroller.frame;
        rect3.origin.y = screenHeight + rect3.size.height;
        scroller.frame = rect3;
        scroller.alpha = 0;
        
        CGRect rect7 = shadowViewForScroller.frame;
        rect7.origin.y = screenHeight + rect3.size.height;
        shadowViewForScroller.frame = rect7;
        shadowViewForScroller.alpha = 0;

        if (kBookLater) {
            UIView *bookingTypeView = [self.view viewWithTag:bookingOptionViewTag];
            CGRect rect4 = bookingTypeView.frame;
            rect4.origin.y = screenHeight + rect4.size.height;
            bookingTypeView.frame = rect4;
            bookingTypeView.alpha = 0;
        }
        
        UIView *locationBtns = [self.view viewWithTag:loctionButtonsTag];
        CGRect rect5 = locationBtns.frame;
        rect5.origin.x = screenWidth;
        locationBtns.frame = rect5;
        locationBtns.alpha = 0;
        
        UIView *searchAddressView = [self.view viewWithTag:searchAddressViewTag];
        CGRect rect6 = searchAddressView.frame;
        rect6.origin.y =  44;
        searchAddressView.frame = rect6;
        searchAddressView.alpha = 1;
    }
    [UIView commitAnimations];
    [centerPickUpObj collapse];
}

/**
 This method will call when user touches to map screen
 */
- (void)endAnimation {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    NSInteger bookingStatus = [Utility bookingStatus];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    if(isCustomMarkerSelected == YES) {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 74;
            PikUpViewForConfirmScreen.frame = rectPick;
            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 118;
            DropOffViewForConfirmScreen.frame = rectDrop;
        } else {
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 74;
            PikUpViewForConfirmScreen.frame = rect;
        }
    }  else if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        
    } else {
        UIView *customNaviagtionBar = [self.view viewWithTag:78];
        CGRect rect2 = customNaviagtionBar.frame;
        rect2.origin.y = 0;

        UIView *customLocationView = [self.view viewWithTag:customLocationViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = 74;
        customLocationView.frame = rect;
        customLocationView.alpha = 1;
//        [customNavigationBarView hideLeftMenuButtonInMapVC:NO];
        [customNavigationBarView.rightbarButton setHidden:NO];
        customNaviagtionBar.frame = rect2;
        
//        UIView *carTypeView = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect rect3 = scroller.frame;
        if (kBookLater) {
            rect3.origin.y = screenHeight - 135;
        } else {
            rect3.origin.y = screenHeight - 85;
        }
        scroller.frame = rect3;
        scroller.alpha = 1;
        
        CGRect rect7 = shadowViewForScroller.frame;
        if (kBookLater) {
            rect7.origin.y = screenHeight - 135;
        } else {
            rect7.origin.y = screenHeight - 85;
        }
        shadowViewForScroller.frame = rect7;
        shadowViewForScroller.alpha = 1;

        if (kBookLater) {
            UIView *bookingTypeView = [self.view viewWithTag:bookingOptionViewTag];
            CGRect rect4 = bookingTypeView.frame;
            rect4.origin.y = screenHeight - 50;
            bookingTypeView.frame = rect4;
            bookingTypeView.alpha = 1;
        }
        
        UIView *locationBtns = [self.view viewWithTag:loctionButtonsTag];
        CGRect rect5 = locationBtns.frame;
        rect5.origin.x = screenWidth - 45;
        locationBtns.frame = rect5;
        locationBtns.alpha = 1;

        UIView *searchAddressView = [self.view viewWithTag:searchAddressViewTag];
        CGRect rect6 = searchAddressView.frame;
        rect6.origin.y =  74;
        searchAddressView.frame = rect6;
        searchAddressView.alpha = 0;
    }
    [UIView commitAnimations];
    [centerPickUpObj expand];
}

#pragma mark - UINavigation - 

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"pickup"]) {
        PickUpViewController *pickController = [segue destinationViewController];
        pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
        pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
        pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType){
            pickUpAddressTextField.text = dict[@"formatted_address"];
            NSString *latitude = [dict valueForKeyPath:@"geometry.location.lat"];
            NSString *longitude = [dict valueForKeyPath:@"geometry.location.lng"];
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setValue:latitude forKey:@"Latitude"];
            [mDict setValue:longitude forKey:@"Longitude"];
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
        };
    }
}

#pragma mark - UITextField Delegate -

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSInteger bookingStatus = [Utility bookingStatus];
    if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        if (!_isUpdatedLocation) {
            [_locationManager stopUpdatingLocation];
            _isUpdatedLocation = YES;
            [allMarkers removeAllObjects];
            [mapView_ clear];
        }
    } else {
        if (!_isUpdatedLocation) {
            _isUpdatedLocation = YES;
            [_locationManager stopUpdatingLocation];
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                    longitude:newLocation.coordinate.longitude
                                                                         zoom:mapZoomLevel];
            [mapView_ setCamera:camera];
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
            _currentLatitude = newLocation.coordinate.latitude;
            _currentLongitude =  newLocation.coordinate.longitude;
            [self getAddress:position];
            MyAppTimerClass *obj = [MyAppTimerClass sharedInstance];
            [obj startPublishTimer];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        [self gotoLocationServicesMessageViewController];
    }
}

-(void)locationServicesChanged:(NSNotification*)notification {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController {
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

-(void) getCurrentLocationFromGPS {
    if ([CLLocationManager locationServicesEnabled]) {
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
                [self.locationManager requestWhenInUseAuthorization];
            }
        }
        [_locationManager startUpdatingLocation];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Location Service", @"Location Service") Message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.")];
    }
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        if (response && response.firstResult) {
            GMSAddress *address = response.firstResult;
            NSMutableArray *addressArr = [address.lines mutableCopy];
            if ([addressArr containsObject:@""])
                [addressArr removeObject:@""];
            NSString *addressText = [addressArr componentsJoinedByString:@", "];
            pickUpAddressTextField.text =  addressText;
            UITextField *pickUpAddress= (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed))
                pickUpAddress.text = addressText;
            pickUpAddress.text = addressText;
            pickUpAddress.textColor = UIColorFromRGB(0x333333);
            srcLat = response.firstResult.coordinate.latitude;
            srcLong = response.firstResult.coordinate.longitude;
            srcAddr = addressArr[0];
            srcAddrline2 = addressText;
            zipCodeOnMap = flStrForStr(address.postalCode);
            UIButton *btn = [(UIButton *) [self.view viewWithTag:customLocationViewTag] viewWithTag:97];
            UILabel *pickupLabel = (UILabel *)[[self.view viewWithTag:customLocationViewTag] viewWithTag:1010];
            BOOL isFav = [Database checkAddressIsFavouriteInDatabase:srcAddr and:srcAddrline2];
            if (isFav) {
                NSArray *arr = [Database getFavouriteAddressFromDataBase:srcAddr and:srcAddrline2];
                SourceAddress *add = arr[0];
                pickupLabel.text = [flStrForStr(add.locationType) capitalizedString];
                btn.selected = YES;
            } else {
                btn.selected = NO;
                pickupLabel.text = NSLocalizedString(@"Pickup Location", @"Pickup Location");
            }
        } else {
            UITextField *pickUpAddress= (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed)) {
                pickUpAddress.text = NSLocalizedString(@"Fetching Location...", @"Fetching Location...");
            }
            pickUpAddress.text = NSLocalizedString(@"Fetching Location...", @"Fetching Location...");
            pickUpAddressTextField.text = NSLocalizedString(@"Fetching Location...", @"Fetching Location...");
            srcLat = _currentLatitude;
            srcLong = _currentLongitude;
            srcAddr = flStrForObj(@"");
            srcAddrline2 = flStrForObj(@"");
            pickUpAddress.textColor = UIColorFromRGB(0x333333);
        }
        if ([Utility isNeedToPublishMessageOnPubnub]) {
            [Utility setIsNeedToPublishMessageOnPubnub:NO];
            arrayOfMastersAround = [NSMutableArray new];
            [self clearTheMapBeforeChagingTheCarTypes];
            [self publishPubNubStream];
        } else {
            CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:_currentLatitude longitude:_currentLongitude];
            [self checkDistanceWithPreviousLocation:newLocation];
        }
    };
    [geocoder_ reverseGeocodeCoordinate:coordinate completionHandler:handler];
}

#pragma mark - Map Markers methods
/**
 *  Plots Marker on Map
 *
 *  @param medicalSpecialist doctor or nurse
 */
- (void)addCustomMarkerFor {
    if (!allMarkers) {
        allMarkers = [[NSMutableDictionary alloc] init];
    }
    for (int i = 0; i < drivers.count; i++) {
        NSDictionary *dict = drivers[i];
        float latitude = [dict[@"lt"] floatValue];
        float longitude = [dict[@"lg"] floatValue];
        if (!allMarkers[dict[@"chn"]]) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            marker.tappable = YES;
            marker.flat = YES;
            marker.groundAnchor = CGPointMake(0.5f, 0.5f);
            marker.userData =  dict;
            if (arrayOfMapImages.count && arrayOfMapImages.count == arrayOfCarTypes.count) {
                marker.icon = [arrayOfMapImages objectAtIndex:carTypesForLiveBooking-1];
                marker.map = mapView_;
                [allMarkers setObject:marker forKey:dict[@"chn"]];
            } else {
                NSString *markerURL;
                 markerURL = [NSString stringWithFormat:@"%@", [Utility vehicleMapImageURL]];
                if(!markerURL.length && arrayOfCarTypes.count) {
                    markerURL = [NSString stringWithFormat:@"%@", arrayOfCarTypes[carTypesForLiveBooking-1][@"MapIcon"]];
                }
                if ([PMDReachabilityWrapper sharedInstance].isNetworkAvailable) {
                    [self downloadDriverCarImage:markerURL markerName:marker withData:(NSDictionary *) dict];
                }
            }
        } else {
            GMSMarker *marker = allMarkers[dict[@"chn"]];
            CLLocationCoordinate2D lastPosition =  marker.position;
            CLLocationCoordinate2D newPosition = CLLocationCoordinate2DMake(latitude, longitude);
            CLLocationDirection heading = GMSGeometryHeading(lastPosition, newPosition);
            marker.position = newPosition;
            
            [CATransaction begin];
            [CATransaction setAnimationDuration:5.0];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            [CATransaction commit];
            if (heading > 0)
                marker.rotation = heading;
        }
    }
    [drivers removeAllObjects];
}

-(void) downloadDriverCarImage:(NSString *) markerURL markerName:(GMSMarker *)marker withData:(NSDictionary *)dict {
    UIImageView *markerImageView;
    if (!markerImageView) {
        markerImageView = [[UIImageView alloc] init];
    }
    if (markerURL.length) {
        [markerImageView sd_setImageWithURL:[NSURL URLWithString:markerURL]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                      if(image){
                                          image = [Helper imageWithImage:image scaledToSize:[Helper makeSize:image.size fitInSize:CGSizeMake(50, 50)]];
                                          marker.icon = image;
                                          marker.map = mapView_;
                                          [allMarkers setObject:marker forKey:dict[@"chn"]];
                                          return;
                                      }
                                  }];
    }
}

-(void)removeMarker:(GMSMarker*)marker {
    marker.map = mapView_;
    marker.map = nil;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERONTHEWAY" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERREACHED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBSTARTED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBCOMPLETED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBookingConfirmationNameKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLocationServicesChangedNameKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NetworkLostWhileBooking" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AllDriversBusy" object:nil];
}



@end
