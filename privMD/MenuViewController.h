//
//  ViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XDKAirMenuController.h"

@interface MenuViewController : UIViewController

@property (nonatomic, strong) XDKAirMenuController *airMenuController;
@property (weak, nonatomic) IBOutlet UIView *containerOutlet;
@end
