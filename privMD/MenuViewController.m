//
//  ViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "MenuViewController.h"
#import "PickUpViewController.h"
#import "PaymentViewController.h"

@interface MenuViewController ()<XDKAirMenuDelegate>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation MenuViewController


- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];//UIColorFromRGB(0x15B8EC)];
    [_tableView setBackgroundColor:[UIColor whiteColor]];//UIColorFromRGB(0x15B8EC)];
    if (IS_IOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        self.containerOutlet.frame = CGRectMake(95, 0, screenWidth - 95, _containerOutlet.frame.size.height);
        self.airMenuController.isMenuOnRight = TRUE;
    }
    
    
    self.airMenuController = [XDKAirMenuController sharedMenu];
    self.airMenuController.airDelegate = self;
    [self.view addSubview:self.airMenuController.view];
    [self addChildViewController:self.airMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UINavigation -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"TableViewSegue"]) {
        self.tableView = ((UITableViewController*)segue.destinationViewController).tableView;
    }
}


#pragma mark - XDKAirMenuDelegate

- (UIViewController*)airMenu:(XDKAirMenuController*)airMenu viewControllerAtIndexPath:(NSIndexPath*)indexPath {
    UIStoryboard *storyboard = self.storyboard;
    UIViewController *vc = nil;
    vc.view.autoresizesSubviews = TRUE;
    vc.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    if (indexPath.row == 0) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
        return vc;
    } else if (indexPath.row == 1) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController1"];
        return vc;
    } else if (indexPath.row == 2) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"appointmentViewController"];
        return vc;
    } else if (indexPath.row == 3) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"paymentVC"];
        return vc;
    } else if(indexPath.row == 4) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"supportTableVC"];
        return vc;
    } else if(indexPath.row == 5) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"inviteVC"];
        return vc;
    } else if(indexPath.row == 6) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
        return vc;
    } else if (indexPath.row == 7) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
        return vc;
    } else {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController1"];
        return vc;
    }
}

#pragma mark Webservice Handler(Request) -

- (UITableView*)tableViewForAirMenu:(XDKAirMenuController*)airMenu {
    return self.tableView;
}

@end
