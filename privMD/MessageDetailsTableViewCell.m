//
//  MessageDetailsTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "MessageDetailsTableViewCell.h"

@implementation MessageDetailsTableViewCell
@synthesize driverImageView;
@synthesize driverNameLabel;
@synthesize timeDifferenceLabel;
@synthesize messageLabel;
@synthesize lineView;

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = screenWidth;
    [self subviews][0].frame = frame;
    
    self.driverImageView.frame = CGRectMake(10, 25, 50, 50);
    self.driverNameLabel.frame = CGRectMake(70, 15, screenWidth - 120 - 70, 50);
    self.timeDifferenceLabel.frame = CGRectMake(CGRectGetMaxX(self.driverNameLabel.frame) + 10, 15, 100, 30);
    self.messageLabel.frame = CGRectMake(70, CGRectGetMaxY(self.driverNameLabel.frame) + 10, screenWidth - 70 - 10, 30);

    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.driverImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverImageView.frame = frame;
        
        frame = self.driverNameLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverNameLabel.frame = frame;
        
        frame = self.timeDifferenceLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.timeDifferenceLabel.frame = frame;
        self.timeDifferenceLabel.textAlignment = NSTextAlignmentLeft;

        frame = self.messageLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.messageLabel.frame = frame;
    } else {
        self.timeDifferenceLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
