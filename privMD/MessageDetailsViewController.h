//
//  MessageDetailsViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *messageTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *tripDetailView;
@property (weak, nonatomic) IBOutlet UILabel *forYourTripOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripDateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tripDetailsImageView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UITextView *replytextView;
@property (weak, nonatomic) IBOutlet UIView *horizontalLineView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

- (IBAction)submitBtnAction:(id)sender;

@property(nonatomic, strong) NSString *navigationTitle;
@property(nonatomic, strong) NSString *ticketID;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *submitterID;
@property(nonatomic, strong) NSString *createdDateAndTime;


@end
