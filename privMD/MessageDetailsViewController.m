//
//  MessageDetailsViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "MessageDetailsViewController.h"
#import "MessageDetailsTableViewCell.h"
#import "SplashViewController.h"
#import "UILabel+DynamicHeight.h"
#import "User.h"

@interface MessageDetailsViewController () {
    NSMutableArray *messages;
    NSString *cellIdentifier;
    MessageDetailsTableViewCell *cell;
    float oldHeightOfReplyTextView;
}
@end

@implementation MessageDetailsViewController
@synthesize navigationTitle, ticketID, status, submitterID, createdDateAndTime;
@synthesize statusLabel, headerView;
@synthesize tripDetailView, forYourTripOnLabel, tripDateAndTimeLabel, tripDetailsImageView;
@synthesize footerView, lineView, replytextView, horizontalLineView, submitBtn;
@synthesize messageTableView;


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    cellIdentifier = @"MessageDetailsCell";
    replytextView.textColor = UIColorFromRGB(0xA1A6BB);
    replytextView.text = NSLocalizedString(@"Type your reply here...", @"Type your reply here...");
    footerView.hidden = YES;
    oldHeightOfReplyTextView = 40;
}

-(void) viewWillAppear:(BOOL)animated {
    [self handleKeyboard];
}

-(void)viewDidAppear:(BOOL)animated {
    self.navigationItem.title = navigationTitle;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:fontNormal size:15]}];
    [self getSelectedMessageDetails];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark - Custom Methods -
/**
 Set Frame Of Table
 */
- (void)setFrameOfTableView {
    CGRect frameOftable = messageTableView.frame;
    frameOftable.origin.y = 0;
    frameOftable.size.width = CGRectGetWidth(self.view.frame);
    frameOftable.size.height = CGRectGetMinY(footerView.frame);
    messageTableView.frame = frameOftable;
    
    frameOftable = headerView.frame;
    frameOftable.origin.x = 0;
    frameOftable.origin.y = 0;
    frameOftable.size.width = CGRectGetWidth(self.view.frame);
    frameOftable.size.height = 30;
    headerView.frame = frameOftable;
    
    frameOftable = statusLabel.frame;
    frameOftable.origin.x = 0;
    frameOftable.origin.y = 0;
    frameOftable.size.width = CGRectGetWidth(self.view.frame);
    frameOftable.size.height = 30;
    statusLabel.frame = frameOftable;
    statusLabel.text = [NSString stringWithFormat:@"%@ (%@)", status, createdDateAndTime];
}

/**
 Set frame for Footer View
 */
- (void)setFrameOfFooterView {
    CGRect frameOfFooter = footerView.frame;
    if([status isEqualToString:@"New"] || [status isEqualToString:@"Open"] || [status isEqualToString:@"Pending"] || [status isEqualToString:@"Solved"]) {
        frameOfFooter.origin.y = CGRectGetHeight(self.view.frame) - (oldHeightOfReplyTextView + 10);
        frameOfFooter.size.height = oldHeightOfReplyTextView + 10;
    } else {
        frameOfFooter.origin.y = CGRectGetHeight(self.view.frame);
        frameOfFooter.size.height = 0;
    }
    footerView.frame = frameOfFooter;
    [self setFrameOfFooterSubViews];
}

/**
 Set frame for Footer view subview TextView, line and Submit Button
 */
-(void) setFrameOfFooterSubViews {
    CGRect frameOfFooter = submitBtn.frame;
    frameOfFooter.origin.x = CGRectGetWidth(self.view.frame) - 75 - 10;
    frameOfFooter.origin.y = (10 + oldHeightOfReplyTextView - frameOfFooter.size.height)/2;
    frameOfFooter.size.width = 75;
    frameOfFooter.size.height = 40;
    submitBtn.frame = frameOfFooter;
    frameOfFooter = replytextView.frame;
    frameOfFooter.origin.x = 10;
    frameOfFooter.origin.y = 5;
    frameOfFooter.size.width = CGRectGetWidth(self.view.frame) - 15 - 75 - 5;
    frameOfFooter.size.height = oldHeightOfReplyTextView;
    replytextView.frame = frameOfFooter;
    frameOfFooter = horizontalLineView.frame;
    frameOfFooter.origin.x = CGRectGetWidth(self.view.frame) - 2.5 - 75 - 10;
    frameOfFooter.origin.y = 10;
    frameOfFooter.size.width = 1;
    frameOfFooter.size.height = replytextView.frame.size.height - 10;
    horizontalLineView.frame = frameOfFooter;
    [self setFrameOfTableView];
    [self updateUI];
}

/**
 Set Frame for TextView while typing
 
 @param remainder Textview height
 */
- (void)setLaterFrameOfTextView:(float)remainder {
    CGRect frameOfFooter = footerView.frame;
    frameOfFooter.origin.y -= remainder;
    frameOfFooter.size.height += remainder;
    footerView.frame = frameOfFooter;
    [self setFrameOfFooterSubViews];
}

-(void)updateUI {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.replytextView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.replytextView.frame = frame;
        replytextView.textAlignment = NSTextAlignmentRight;

        frame = self.submitBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.submitBtn.frame = frame;
    } else {
        replytextView.textAlignment = NSTextAlignmentLeft;
    }
}

#pragma mark - UINavigation Custom Method -

/**
 Create Left Navigation Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

#pragma mark - CustomNavigation Button

/**
 Navigation left button action method
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - WebService -

-(void) getSelectedMessageDetails {
    NSDictionary *params = @{
                             @"ent_action":@"GetCommets",
                             @"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_ticketId":ticketID,
                             @"ent_date_time":[Helper getCurrentNetworkDateTime]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ZendeskOperation"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getSelectedMessageDetailsResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..." , @"Loading...")];
}

-(void)getSelectedMessageDetailsResponse:(NSDictionary *)responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:responseDict[@"errMsg"]];
        } else {
            messages = [responseDict[@"response"][@"comments"] mutableCopy];
            [messageTableView reloadData];
            [self setFrameOfFooterView];
            
            if ([status isEqualToString:@"New"]) {
                footerView.hidden = NO;
                statusLabel.backgroundColor = UIColorFromRGB(0xFFD112);
            } else if ([status isEqualToString:@"Open"]) {
                footerView.hidden = NO;
                statusLabel.backgroundColor = UIColorFromRGB(0xEE4F4F);
            } else if ([status isEqualToString:@"Pending"]) {
                footerView.hidden = NO;
                statusLabel.backgroundColor = UIColorFromRGB(0x0DB5E8);
            } else if ([status isEqualToString:@"Solved"]) {
                footerView.hidden = NO;
                statusLabel.backgroundColor = UIColorFromRGB(0x3CB878);
            } else if ([status isEqualToString:@"Closed"]) {
                footerView.hidden = YES;
                statusLabel.backgroundColor = UIColorFromRGB(0x808080);
            } else {
                footerView.hidden = YES;
                statusLabel.backgroundColor = UIColorFromRGB(0x3CB878);
            }
            footerView.backgroundColor = UIColorFromRGB(0xE5E5E5);
        }
    }
}

-(void) replyMessageOnZendesk:(NSString *) repliedText {
    NSDictionary *params = @{
                             @"ent_action":@"RespondToTicket",
                             @"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_ticketId":ticketID,
                             @"ent_text": repliedText,
                             @"ent_submitter_id":submitterID,
                             @"ent_date_time":[Helper getCurrentNetworkDateTime]
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ZendeskOperation"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self replyMessageOnZendeskResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..." , @"Loading...")];
}

-(void) replyMessageOnZendeskResponse:(NSDictionary *)responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:responseDict[@"errMsg"]];
            [self setFrameOfFooterView];
        } else {
            NSDictionary *dict = @{ @"body": responseDict[@"response"][@"audit"][@"events"][0][@"body"],
                                    @"created_at": responseDict[@"response"][@"audit"][@"created_at"],
                                    @"user":[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserName]
            };
            [messages addObject:dict];
            [messageTableView reloadData];
            oldHeightOfReplyTextView = 40;
            [self setFrameOfFooterView];
        }
    }
}


# pragma mark - Cell Setup

- (void)setUpCell:(MessageDetailsTableViewCell *)cell1 atIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    NSString *name;
    NSMutableDictionary *dict = [messages[indexPath.row] mutableCopy];
    if ([dict [@"via"][@"channel"] isEqualToString:@"email"] || [messages[indexPath.row][@"via"][@"channel"] isEqualToString:@"api"]) {
        name = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserName];
    } else if([[dict allKeys] containsObject:@"user"]) {
        name = flStrForStr(dict[@"user"]);
    } else {
        name = NSLocalizedString(@"ZiRide Guest Support", @"ZiRide Guest Support");
    }
    [Helper setToLabel:cell1.driverNameLabel Text:name WithFont:fontBold FSize:14 Color:UIColorFromRGB(0x222328)];
    NSDate *date = [Helper getDateAndTimeFromString:flStrForStr(messages[indexPath.row][@"created_at"])];
    [Helper setToLabel:cell1.timeDifferenceLabel Text:[Helper relativeDateStringForDate:date] WithFont:fontNormal FSize:10 Color:UIColorFromRGB(0xA1A6BB)];
    [Helper setToLabel:cell1.messageLabel Text:flStrForStr(messages[indexPath.row][@"body"]) WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0xA1A6BB)];
    
    CGFloat driverNameLabelHeight = [cell1.driverNameLabel measureHeightLabel];
    CGRect frame = cell1.driverNameLabel.frame;
    frame.origin.y = 15;
    frame.size.width = screenWidth - 70 - 120;
    frame.size.height = driverNameLabelHeight;
    cell1.driverNameLabel.frame = frame;

    frame = cell1.timeDifferenceLabel.frame;
    frame.origin.y = 15;
    frame.size.width = 100;
    frame.size.height = driverNameLabelHeight;
    cell1.timeDifferenceLabel.frame = frame;


    CGFloat messageLabelHeight = [cell1.messageLabel measureHeightLabel];
    frame = cell1.messageLabel.frame;
    frame.origin.y = 15 + [cell1.driverNameLabel measureHeightLabel] + 10;
    frame.size.width = screenWidth - 70 - 10;
    frame.size.height = messageLabelHeight;
    cell1.messageLabel.frame = frame;

    frame = cell1.lineView.frame;
    frame.origin.x = 0;
    frame.origin.y = 15 + [cell1.driverNameLabel measureHeightLabel] + 10 + [cell.messageLabel measureHeightLabel] + 15 - 1;
    frame.size.width = screenWidth;
    frame.size.height = 1;
    cell1.lineView.frame = frame;
}

# pragma mark - UITableView DataSource & Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil) {
        cell = (MessageDetailsTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    tableView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(messages.count) {
    static MessageDetailsTableViewCell *cell2 = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell2 = [messageTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    });
    [self setUpCell:cell2 atIndexPath:indexPath];
    CGFloat height = [cell2.driverNameLabel measureHeightLabel] + [cell2.messageLabel measureHeightLabel] + 40;
    return height;
    } else {
        return  0;
    }
}


#pragma mark - TextView Delegate methods -

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text  isEqualToString:NSLocalizedString(@"Type your reply here...", @"Type your reply here...")]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    if(textView.text.length == 0) {
        textView.textColor = UIColorFromRGB(0xA1A6BB);
        textView.text = NSLocalizedString(@"Type your reply here...", @"Type your reply here...");
        [textView resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if(textView.text.length == 0) {
        [textView setTextColor:UIColorFromRGB(0xA1A6BB)];
        textView.text = NSLocalizedString(@"Type your reply here...", @"Type your reply here...");
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0) {
            textView.text = @"";
        }
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - Keyboard Notification -

/**
 *  Handle Keyboard Show & Hide
 */
- (void)handleKeyboard {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    //set notification for when a key is pressed.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector: @selector(keyPressed:)
                                                 name: UITextViewTextDidChangeNotification
                                               object: nil];

}

/**
 *  Keyboard Shown
 */
- (void)keyboardWillShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float height = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:replytextView andKeyboardHeight:height];
}

/**
 *  Keyboard will be hidden
 */
- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextView *)textView andKeyboardHeight:(float)height {
    
    float textfieldMaxY = CGRectGetMaxY(textView.frame);
    UIView *view = [textView superview];
    
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    
    float remainder = CGRectGetHeight(self.view.window.frame) - (textfieldMaxY + height + 20);
    if (remainder >= 0) {
    }
    else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             [self setLaterFrameOfTextView:- (remainder)];
                         }];
    }
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    [self setFrameOfFooterView];
}

- (void) keyPressed: (NSNotification*) notification {
    UILabel *label = [[UILabel alloc] initWithFrame:replytextView.frame];
    label.font = [UIFont fontWithName:fontNormal size:14];
    label.text = replytextView.text;
    NSInteger newSizeH= [label measureHeightLabel];
    if (newSizeH > 21 && newSizeH <= 83) {
        float difference = newSizeH - oldHeightOfReplyTextView + 15;
        if (difference > 0) {
            oldHeightOfReplyTextView += 20;
            [self setLaterFrameOfTextView:difference];
        }
    } else if (newSizeH > 83) {
        replytextView.scrollEnabled = YES;
    } else {
        oldHeightOfReplyTextView = 40;
    }
}


#pragma mark - UIButton Action -

- (IBAction)submitBtnAction:(id)sender {
    [replytextView resignFirstResponder];
    if (replytextView.text.length && ![replytextView.text isEqualToString:NSLocalizedString(@"Type your reply here...", @"Type your reply here...")]) {
        [self replyMessageOnZendesk:replytextView.text];
        [replytextView setTextColor:UIColorFromRGB(0xA1A6BB)];
        replytextView.text = NSLocalizedString(@"Type your reply here...", @"Type your reply here...");
    }
}





@end
