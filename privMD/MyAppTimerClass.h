//
//  MyAppTimerClass.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyAppTimerClass : NSObject

@property(nonatomic,weak) NSTimer *pubnubStreamTimer;
@property(nonatomic,weak) NSTimer *etanDisTimer;
@property(nonatomic,weak) NSTimer *spinTimer;

+ (id)sharedInstance;

-(void)startPublishTimer;

-(void)stopPublishTimer;

-(void)startSpinTimer;

-(void)stopSpinTimer;

-(void)startEtaNDisTimer;

-(void)stopEtaNDisTimer;

@end
