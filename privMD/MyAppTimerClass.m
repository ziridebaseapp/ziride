//
//  MyAppTimerClass.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


#import "MyAppTimerClass.h"
#import "MapViewController.h"
#import "Utility.h"

static MyAppTimerClass *myAppTimer =nil;
@implementation MyAppTimerClass

@synthesize pubnubStreamTimer, spinTimer, etanDisTimer;

+ (id)sharedInstance {
    if (!myAppTimer) {
        myAppTimer = [[self alloc]init];
    }
    return myAppTimer;
}

-(void)startPublishTimer {
    if (self.pubnubStreamTimer == nil) {
        NSInteger timeInterval;
        if([MapViewController getSharedInstance].addProgressBarView != nil) {
            timeInterval = [Utility pubnubPublishOnBookingTimeInterval];
        } else {
            timeInterval = [Utility pubnubPublishTimeInterval];
        }
        pubnubStreamTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(publishPubNubStream:) userInfo:nil repeats:YES];
    }
}

-(void)stopPublishTimer {
    if ([self.pubnubStreamTimer isValid]) {
        [self.pubnubStreamTimer invalidate];
    }
}

-(void)publishPubNubStream:(NSTimer *)myTimer {
    MapViewController *obj = [MapViewController getSharedInstance];
    if(obj) {
        [obj publishPubNubStream];
    }
}

-(void)startEtaNDisTimer {
    if (self.etanDisTimer == nil) {
        etanDisTimer = [NSTimer scheduledTimerWithTimeInterval:[Utility googleMatrixTimeInterval] target:self selector:@selector(sendEtanDis:) userInfo:nil repeats:YES];
    }
}
-(void)stopEtaNDisTimer  {
    if ([self.etanDisTimer isValid]) {
        [self.etanDisTimer invalidate];
    }
}

-(void)sendEtanDis:(NSTimer *)myTimer {
    MapViewController *obj = [MapViewController getSharedInstance];
    if(obj)
    {
        [obj sendRequestgetETAnDistance];
    }
}

-(void)startSpinTimer {
    if (self.spinTimer == nil) {
        spinTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(hideActivityIndicator:) userInfo:nil repeats:NO];
    }
}

-(void)stopSpinTimer {
    if ([self.spinTimer isValid]) {
        [self.spinTimer invalidate];
    }
}

-(void)hideActivityIndicator:(NSTimer *)myTimer {
    MapViewController *obj = [MapViewController getSharedInstance];
    if(obj) {
        [obj hideAcitvityIndicator];
    }
}

@end
