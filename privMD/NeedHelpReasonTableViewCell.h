//
//  NeedHelpReasonTableViewCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedHelpReasonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@end
