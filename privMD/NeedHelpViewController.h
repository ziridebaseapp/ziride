//
//  NeedHelpViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedHelpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *upperView;

@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourRatingLabel;

@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *ratingStatusLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *greenImageView;
@property (weak, nonatomic) IBOutlet UIImageView *redImageView;
@property (weak, nonatomic) IBOutlet UIView *horizontalLineView;

@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLabel;

@property(strong, nonatomic) NSDictionary *invoiceData;
@end
