//
//  NeedHelpViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NeedHelpViewController.h"
#import "SupportTableViewCell.h"
#import "SupportWebViewController.h"
#import "SupportDetailTableViewController.h"

@interface NeedHelpViewController ()<CustomNavigationBarDelegate> {
    NSMutableArray *listOfItemsArray;
    NSMutableArray *detailsOfListArray;
    NSString *navTitle;
    NSString *webUrlLink;
}
@end

@implementation NeedHelpViewController
@synthesize invoiceData;
@synthesize driverImageView, dropAddressLabel, pickUpAddressLabel, ratingView, ratingStatusLabel, invoiceAmountLabel, dateLabel, carModelLabel;

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    driverImageView.layer.cornerRadius =  self.driverImageView.frame.size.width/2;
    driverImageView.clipsToBounds = YES;
    driverImageView.layer.borderWidth = 2;
    driverImageView.layer.borderColor = UIColorFromRGB(0xffffff).CGColor;
    [self updateUI];

    CGRect frame = self.tableView.frame;
    frame.origin.x = 15;
    frame.size.width = [UIScreen mainScreen].bounds.size.width-30;
    self.tableView.frame = frame;

    listOfItemsArray = [NSMutableArray array];
    detailsOfListArray = [NSMutableArray array];
    [self addCustomNavigationBar];
    [self sendRequestToGetNeedHelp];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark- Custom Methods

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Select An Issue", @"Select An Issue")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation Left Button Action

 @param sender LeftBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - Custom Methods -

/**
 Update UI
 */
-(void) updateUI {
    pickUpAddressLabel.text = flStrForStr(invoiceData[@"addrLine1"]);
    dropAddressLabel.text = flStrForStr(invoiceData[@"dropLine1"]);
    dateLabel.text = [Helper getDateInString:flStrForStr(invoiceData[@"apntDt"])];
    NSString *amount =  [Helper getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
    invoiceAmountLabel.text = amount;
    self.invoiceAmountLabel.textAlignment = NSTextAlignmentRight;
    carModelLabel.text = flStrForStr(invoiceData[@"model"]);
    ratingView.value = [invoiceData[@"r"] integerValue];
    ratingView.baseColor = UIColorFromRGB(0xDEDEDE);
    ratingView.highlightColor = UIColorFromRGB(0x019F6E);
    ratingView.userInteractionEnabled = NO;
    ratingStatusLabel.text = @"";
    NSString *driverImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (driverImageURL.length) {
        driverImageURL = [NSString stringWithFormat:@"%@",flStrForStr(invoiceData[@"pPic"])];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:driverImageURL]
                                placeholderImage:[UIImage imageNamed:@"driverImage"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                       }];
    }
    CGFloat screenWidth = self.view.frame.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.driverImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.driverImageView.frame = frame;

        frame = self.dateLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dateLabel.frame = frame;
        
        frame = self.invoiceAmountLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.invoiceAmountLabel.frame = frame;
        self.invoiceAmountLabel.textAlignment = NSTextAlignmentLeft;
        
        frame = self.carModelLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.carModelLabel.frame = frame;
        
        frame = self.yourRatingLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.yourRatingLabel.frame = frame;
        
        frame = self.ratingView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.ratingView.frame = frame;

        frame = self.ratingStatusLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.ratingStatusLabel.frame = frame;
        
        frame = self.greenImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.greenImageView.frame = frame;
        
        frame = self.horizontalLineView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.horizontalLineView.frame = frame;
        
        frame = self.redImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.redImageView.frame = frame;

        frame = self.pickUpAddressLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.pickUpAddressLabel.frame = frame;
        
        frame = self.dropAddressLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dropAddressLabel.frame = frame;
    }

}



#pragma mark - WebService -

/**
 Send Requset for Get Need Help API
 */
-(void)sendRequestToGetNeedHelp {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    NSDictionary *params = @{@"ent_sid":flStrForStr([Utility userID]),
                             @"ent_lan":[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_bookingId":flStrForStr(invoiceData[@"bid"])
                            };
    [networHandler composeRequestWithMethod:@"helptext"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response) {
                                   if (success) {
                                       [self getNeedHelpResponse:response];
                                   } else {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   }
                               }];
}

/**
 Get Need Help response
 
 @param response return type void and takes no argument
 */
-(void) getNeedHelpResponse:(NSDictionary *) response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil)
        return;
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([response [@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
    } else {
        listOfItemsArray = [response[@"HelpedTextArray"] mutableCopy];
        [self.tableView reloadData];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listOfItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier=@"supportCell";
    SupportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil) {
        cell = (SupportTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:1];
    }
    cell.titleLabel.text = listOfItemsArray[indexPath.row][@"tag"];
    cell.accessoryImage.image = [UIImage imageNamed:@"help_next_arrow_icon_off"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    detailsOfListArray = listOfItemsArray[indexPath.row][@"childs"];
    if (detailsOfListArray.count == 0) {
        if ([listOfItemsArray[indexPath.row][@"link"] length]) {
            SupportWebViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportWeb"];
            VC.title = NSLocalizedString(@"Tell Us More", @"Tell Us More");
            webUrlLink = [NSString stringWithFormat:@"%@%@",listOfItemsArray[indexPath.row][@"link"], @"&frm_booking=0"];
            VC.weburl = webUrlLink;
            [Helper checkForPush:self.navigationController.view];
            [self.navigationController pushViewController:VC animated:NO];
        }
    } else {
        SupportDetailTableViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportDetailTable"];
        VC.title = NSLocalizedString(@"Tell Us More", @"Tell Us More");
        VC.detailsArray = [detailsOfListArray mutableCopy];
        VC.isComingFromNeedHelp = YES;
        [Helper checkForPush:self.navigationController.view];
        [self.navigationController pushViewController:VC animated:NO];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
