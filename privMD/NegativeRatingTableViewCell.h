//
//  NegativeRatingTableViewCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NegativeRatingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellBgImage;
@property (weak, nonatomic) IBOutlet UILabel *negatingRatingReasonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nextImageView;
@end
