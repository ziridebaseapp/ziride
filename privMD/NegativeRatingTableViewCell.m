//
//  NegativeRatingTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NegativeRatingTableViewCell.h"

@implementation NegativeRatingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        self.nextImageView.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        self.nextImageView.transform = CGAffineTransformMakeScale(1, 1);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
