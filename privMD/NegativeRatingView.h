//
//  NegativeRatingView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NegativeRatingView : UIView {
    NSMutableArray *listOfItemsArray;
    NSMutableArray *detailsOfListArray;
    
    NSInteger selectedSection, previousSelectedSection;
    BOOL updateTableView;
    UIButton *selectedSectionButton;
    
    NSString *navTitle;
    NSString *webUrlLink;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *tableUpperView;

@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceAmountLabel;

@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *ratingStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *pickupAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropoffAddressLabel;

@property (weak, nonatomic) IBOutlet UITableView *helpTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;


@property (strong, nonatomic) NSDictionary *invoiceData;
@property (nonatomic) NSInteger ratingValue;
@property (nonatomic) NSInteger isComingFromNeedHelp;

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
-(void)hidePOPup;

@property (nonatomic, copy) void(^onCompletion)(NSInteger closeClicked);

@end
