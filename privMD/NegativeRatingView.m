//
//  NegativeRatingView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NegativeRatingView.h"
#import "NegativeRatingTableViewCell.h"
#import "SupportDetailTableViewController.h"
#import "SupportWebViewController.h"
#import "PopUpWebView.h"

@implementation NegativeRatingView
@synthesize invoiceData, ratingValue, isComingFromNeedHelp, onCompletion;
@synthesize helpTableView, tableUpperView, tableViewHeight, firstView, driverImageView, carModelLabel, dateLabel, invoiceAmountLabel, ratingStatusLabel, ratingView;
@synthesize secondView, pickupAddressLabel, dropoffAddressLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        self.ratingStatusLabel.textAlignment = NSTextAlignmentLeft;
        self.invoiceAmountLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.ratingStatusLabel.textAlignment = NSTextAlignmentRight;
        self.invoiceAmountLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"NegativeRatingView" owner:self options:nil] firstObject];
    UINib *cellNib = [UINib nibWithNibName:@"NegativeRatingTableCell" bundle:nil];
    [helpTableView registerNib:cellNib forCellReuseIdentifier:@"NegativeRatingTableCell"];
    return self;
}


-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window {
    listOfItemsArray = [NSMutableArray array];
    detailsOfListArray = [NSMutableArray array];
    invoiceData = [dict mutableCopy];
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    self.contentView.alpha = 0.3;
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    [self updateUI];
    [self sendRequestToGetNeedHelp];
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];

}

-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(void)sendRequestToGetNeedHelp {
    [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    NSDictionary *params = @{@"ent_sid":flStrForStr([Utility userID]),
                             @"ent_lan":[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_bookingId":flStrForStr(invoiceData[@"bid"])
                             };
    [networHandler composeRequestWithMethod:@"helptext"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response) {
                                   if (success) {
                                       [self getNeedHelpResponse:response];
                                   } else {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   }
                               }];
}

/**
 Get Need Help response
 
 @param response return type void and takes no argument
 */
-(void) getNeedHelpResponse:(NSDictionary *) response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil)
        return;
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([response [@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
    } else {
        listOfItemsArray = [response[@"HelpedTextArray"] mutableCopy];
        [helpTableView reloadData];
        self.tableViewHeight.constant = helpTableView.contentSize.height;
        [self layoutIfNeeded];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];    }
}

-(void) setRatingViewInInvoice {
    ratingView.value = ratingValue;
    ratingView.stepInterval = 1.0;
    ratingView.markFont = [UIFont systemFontOfSize:25];
    ratingView.baseColor = UIColorFromRGB(0xDEDEDE);
    ratingView.highlightColor = UIColorFromRGB(0x019F6E);
    ratingView.userInteractionEnabled = NO;
}

-(void) updateUI {
    switch (ratingValue) {
        case 1:
            ratingStatusLabel.text = NSLocalizedString(@"Terrible", @"Terrible");
            break;
        case 2:
            ratingStatusLabel.text = NSLocalizedString(@"Bad", @"Bad");
            break;
        default:
            break;
    }
    [self setRatingViewInInvoice];
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    NSString *strImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (strImageURL.length) {
        strImageURL = [NSString stringWithFormat:@"%@",invoiceData[@"pPic"]];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      if (error || !image){
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      } else {
                                          driverImageView.image = image;
                                      }
                                  }];
    } else {
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    }
    dateLabel.text = [Helper getDateInString:flStrForStr(invoiceData[@"apptDt"])];
    carModelLabel.text = flStrForStr(invoiceData[@"model"]);
    NSString *amount =  [Helper getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
    self.invoiceAmountLabel.text = amount;
    pickupAddressLabel.text = flStrForObj(invoiceData[@"addr1"]);
    dropoffAddressLabel.text = flStrForObj(invoiceData[@"dropAddr1"]);
}

- (IBAction)tapGestureRecognizerAction:(id)sender {
    CGPoint location = [sender locationInView:self];
    id tappedView = [self hitTest:location withEvent:nil];
    if ([tappedView isEqual:self]) {
        onCompletion(1);
        [self hidePOPup];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listOfItemsArray.count;;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == selectedSection) {
        return detailsOfListArray.count;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, helpTableView.frame.size.width, 40)];
    UIButton *sectionSelctorBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, helpTableView.frame.size.width, 40)];
    sectionSelctorBtn.tag = 1000*section+1;
    UIImageView *cellBgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, helpTableView.frame.size.width, 40)];
    if(section == 0) {
        cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top"];
    } else {
        cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_middle"];
    }
    UIImageView *nextBtnImageView = [[UIImageView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    UIImage *nextBtnImage = [[UIImage alloc] init];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    array = listOfItemsArray[section][@"childs"];
    if (array.count) {
        [sectionSelctorBtn addTarget:self action:@selector(sectionSelctorBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        if (selectedSection == section && selectedSectionButton.isSelected) {
            nextBtnImage = [UIImage imageNamed:@"help_up_arrow_icon_off"];
            nextBtnImageView.image = nextBtnImage;
        } else {
            nextBtnImage = [UIImage imageNamed:@"help_down_arrow_icon_off"];
            nextBtnImageView.image = nextBtnImage;
        }
        [Helper setToLabel:label Text:flStrForStr(listOfItemsArray[section][@"tag"]) WithFont:fontBold FSize:12 Color:UIColorFromRGB(0x222328)];
    } else {
        [sectionSelctorBtn addTarget:self action:@selector(sectionSelctorBtnActionForOpenWebView:) forControlEvents:UIControlEventTouchUpInside];
        nextBtnImage = [UIImage imageNamed:@"help_next_arrow_icon_off"];
        nextBtnImageView.image = nextBtnImage;
        if ([Helper isCurrentLanguageRTL]) {
            nextBtnImageView.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            nextBtnImageView.transform = CGAffineTransformMakeScale(1, 1);
        }

        [Helper setToLabel:label Text:flStrForStr(listOfItemsArray[section][@"tag"]) WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0x222328)];
    }
    [view addSubview:sectionSelctorBtn];
    [view addSubview: nextBtnImageView];
    [view addSubview:label];
    [view addSubview:cellBgImage];
    if ([Helper isCurrentLanguageRTL]) {
        nextBtnImageView.frame = CGRectMake(5, (40 - nextBtnImage.size.height)/2, nextBtnImage.size.width, nextBtnImage.size.height);
        label.frame = CGRectMake(45, 5, helpTableView.frame.size.width - 10 - 40 - 5, 30);
    } else {
        label.frame = CGRectMake(5, 5, helpTableView.frame.size.width - 10 - nextBtnImage.size.width - 5, 30);
        nextBtnImageView.frame = CGRectMake(CGRectGetMaxX(label.frame) + 5, (40 - nextBtnImage.size.height)/2, nextBtnImage.size.width, nextBtnImage.size.height);
    }
    return view;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"NegativeRatingTableCell" owner:self options:nil];
    [helpTableView setAllowsSelection:YES];
    NegativeRatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NegativeRatingTableCell"];
    if(!cell) {
        cell = [cellArray lastObject];
    }
    cell.negatingRatingReasonLabel.text = detailsOfListArray[indexPath.row][@"tag"];
    cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_middle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PopUpWebView* popView = [[PopUpWebView alloc]init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    webUrlLink = [NSString stringWithFormat:@"%@%@",detailsOfListArray[indexPath.row][@"link"],@"&frm_booking=1"];
    popView.invoiceData = invoiceData;
    popView.rating = [NSString stringWithFormat:@"%ld", (long)ratingValue];
    [popView showPopUpWithURL:webUrlLink Onwindow:window];
    popView.onCompletion = ^(NSInteger success) {
        if(success == 1) {
            onCompletion(2);
            [self hidePOPup];
        }
    };
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)sectionSelctorBtnAction:(id)sender {
    UIButton *sectionSectorBtn = (UIButton *) sender;
    if ((selectedSectionButton != nil) && (selectedSectionButton.tag -1)/1000 == (sectionSectorBtn.tag -1)/1000) {
        sectionSectorBtn = selectedSectionButton;
    } else {
        selectedSectionButton = sectionSectorBtn;
    }
    selectedSection = (sectionSectorBtn.tag-1)/1000;
    if(sectionSectorBtn.isSelected) {
        selectedSectionButton.selected = NO;
        detailsOfListArray = [[NSMutableArray alloc] init];
    } else {
        selectedSectionButton.selected = YES;
        detailsOfListArray = [[NSMutableArray alloc] init];
        detailsOfListArray = listOfItemsArray[selectedSection][@"childs"];
    }
    [helpTableView setAllowsSelection:YES];
    [helpTableView reloadData];
    self.tableViewHeight.constant = helpTableView.contentSize.height;
    [self layoutIfNeeded];
}

-(void)sectionSelctorBtnActionForOpenWebView:(id)sender {
    UIButton *sectionBtn = (UIButton *)sender;
    if (![listOfItemsArray[(sectionBtn.tag-1)/1000][@"link"] length]) {
        return;
    }
    PopUpWebView *popView = [[PopUpWebView alloc]init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    webUrlLink = [NSString stringWithFormat:@"%@%@",listOfItemsArray[(sectionBtn.tag-1)/1000][@"link"],@"&frm_booking=1"];
    popView.invoiceData = invoiceData;
    popView.rating = [NSString stringWithFormat:@"%ld", (long)ratingValue];
    [popView showPopUpWithURL:webUrlLink Onwindow:window];
    popView.onCompletion = ^(NSInteger success) {
        if(success == 1) {
            onCompletion(2);
            [self hidePOPup];
        }
    };
}



@end
