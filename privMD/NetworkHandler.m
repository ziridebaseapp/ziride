//
//  NetworkHandler.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NetworkHandler.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "NetworkStatusShowingView.h"
#define _AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES_ 1

@interface NetworkHandler ()
@property(nonatomic,strong) AFHTTPRequestOperationManager *manager;
@property(nonatomic,strong) NSString *lastMethod;
@end

@implementation NetworkHandler
static NetworkHandler *networkHandler;

+ (id)sharedInstance {
    if (!networkHandler) {
        networkHandler  = [[self alloc] init];
    }
    return networkHandler;
}

-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock {
    _manager = [AFHTTPRequestOperationManager manager];
    for (NSOperation *operation in _manager.operationQueue.operations) {
        [operation cancel];
    }
    __block  NSString *postUrl = [self getBaseString:method];
    __weak AFHTTPRequestOperationManager *nm = _manager;
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
//    [_manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"your_username" password:@"your_password"];
        _manager.securityPolicy.allowInvalidCertificates = YES;
        _manager.securityPolicy.validatesDomainName = NO;
        [nm POST:postUrl parameters:paramas success:^(AFHTTPRequestOperation *operation, id responseObject) {
            completionBlock(YES,responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Network Error", @"Network Error") Message:[error localizedDescription]];
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            completionBlock(NO,nil);
        }];
}

-(void)composeRequestForNodeAPIMethod:(NSString*)method paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock {
    _manager = [AFHTTPRequestOperationManager manager];
    for (NSOperation *operation in _manager.operationQueue.operations) {
        [operation cancel];
    }
    __block  NSString *postUrl = [self getBaseURLForNodeAPI:method];
    __weak AFHTTPRequestOperationManager *nm = _manager;
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    _manager.securityPolicy.allowInvalidCertificates = YES;
    _manager.securityPolicy.validatesDomainName = NO;
    [nm POST:postUrl parameters:paramas success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completionBlock(YES,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Network Error", @"Network Error") Message:[error localizedDescription]];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        completionBlock(NO,nil);
    }];
}

-(void)composeRequestWithURL:(NSString*)url paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock {
    _manager = [AFHTTPRequestOperationManager manager];
    for (NSOperation *operation in _manager.operationQueue.operations) {
        [operation cancel];
    }
    __weak AFHTTPRequestOperationManager *nm = _manager;
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    [nm POST:url parameters:paramas success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completionBlock(YES,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Network Error", @"Network Error") Message:[error localizedDescription]];
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        completionBlock(NO,nil);
        
    }];
}

-(void)cancelRequestOperation{
    for (NSOperation *operation in _manager.operationQueue.operations) {
        [operation cancel];
    }
}

-(NSString*)getBaseString:(NSString*)method {
    return [NSString stringWithFormat:@"%@%@", BASE_URL, method];
}

-(NSString*)getBaseURLForNodeAPI:(NSString*)method {
    return [NSString stringWithFormat:@"%@%@", BASE_NODE_URL, method];
}

-(NSString*)paramDictionaryToString:(NSDictionary*)params {
    NSMutableString *request = [[NSMutableString alloc] init];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [request appendFormat:@"&%@=%@", key, obj];
    }];
    NSString *finalRequest = request;
    if ([request hasPrefix:@"&"]) {
        finalRequest = [request substringFromIndex:1];
    }
    return finalRequest;
}


@end
