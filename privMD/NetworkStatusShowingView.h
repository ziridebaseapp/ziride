//
//  NetworkStatusShowingView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkStatusShowingView : UIView
{
    NSTimer *timer;
}
+ (id)sharedInstance ;
+(void)removeViewShowingNetworkStatus;

@end
