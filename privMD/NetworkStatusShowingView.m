//
//  NetworkStatusShowingView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NetworkStatusShowingView.h"

@implementation NetworkStatusShowingView

static NetworkStatusShowingView *showNetworkStatusView = nil;

+ (id)sharedInstance {
    if (!showNetworkStatusView) {
        showNetworkStatusView  = [[self alloc] init];
    }
    return showNetworkStatusView;
}

-(id)init {
    self = [super init];
    if (self) {
        [self addViewToShowNetworkStatus];
    }
    return self;
}

-(void)addViewToShowNetworkStatus {
    if ([[[UIApplication sharedApplication] keyWindow] viewWithTag:1000] == nil) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
        UIView *showStatus = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
        showStatus.tag = 1000;
        showStatus.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.7];
        UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
        NSString *str1 = NSLocalizedString(@"We can't reach our network right now.", @"We can't reach our network right now.");
        NSString *str2 = NSLocalizedString(@"Please check your connection", @"Please check your connection");
        [Helper setToLabel:msg Text:[NSString stringWithFormat:@"%@\n%@", str1, str2] WithFont:fontNormal FSize:13 Color:[UIColor whiteColor]];
        msg.backgroundColor = [UIColor clearColor];
        msg.textAlignment = NSTextAlignmentCenter;
        msg.numberOfLines = 0;
        [showStatus addSubview:msg];
        [frontWindow addSubview:showStatus];
    }
}

-(void)hide {
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                     }
                     completion:^(BOOL finished){
                         UIView *showStatus = [[[UIApplication sharedApplication] keyWindow] viewWithTag:1000];
                         [showStatus removeFromSuperview];
                         showNetworkStatusView = nil;
                     }];
}


+(void)removeViewShowingNetworkStatus {
    if ([[[UIApplication sharedApplication] keyWindow] viewWithTag:1000] != nil) {
        UIView *showStatus = [[[UIApplication sharedApplication] keyWindow] viewWithTag:1000];
        [showStatus removeFromSuperview];
        showNetworkStatusView = nil;
    }
}


@end
