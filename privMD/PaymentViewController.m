//
//  PaymentViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PaymentViewController.h"
#import "XDKAirMenuController.h"
#import "Database.h"
#import "CardDetails.h"
#import "CustomNavigationBar.h"
#import "SplashViewController.h"
#import "PaymentTableViewCell.h"
#import "WalletHistoryViewController.h"
#import "User.h"
#import "CardLoginViewController.h"

@interface PaymentViewController ()<CustomNavigationBarDelegate> {
    float keyboardHeight;
    NSString *currencyStr;
    UITapGestureRecognizer *tap;
    PaymentTableViewCell *cell;
    NSInteger defaultCardIndex;
    NSString *amountToAdd;
}

@property(strong,nonatomic) UIButton *addPaymentbutton;
@property (strong,nonatomic)  UITextField *activeTextField;

@end

@implementation PaymentViewController
@synthesize addPaymentbutton, isComingFromMapVC;
@synthesize paymentTableView;
@synthesize callback;
@synthesize addMoneytextField, addMoneyBtn1, addMoneyBtn2, addMoneyBtn3;
@synthesize walletView, walletHeadingView, walletHeadingLabel;
@synthesize currentBalanceStatusLabel, currentBalanceAmountLabel;
@synthesize activeTextField;
@synthesize amountToSettle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}
#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    amountToAdd = @"0";
    currencyStr =  [Helper getCurrencyLocal:0.00];
    NSRange removeRange = [currencyStr rangeOfString:@"0.00"];
    currencyStr = [currencyStr stringByReplacingCharactersInRange:removeRange withString:@""];
    [self setAmountButtonTitleWithCurrencyAndTextfieldPlaceholder];
    [self tableViewReloadAndAdjustUI];
    [self updateUI];
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    [self getWalletCurrentMoney];
}

-(void)viewDidAppear:(BOOL)animated {
    if (amountToSettle.length) {
        self.addMoneytextField.userInteractionEnabled = NO;
        addMoneyBtn1.userInteractionEnabled = NO;
        addMoneyBtn2.userInteractionEnabled = NO;
        addMoneyBtn3.userInteractionEnabled = NO;
        self.addMoneytextField.text = amountToSettle;
        self.currentBalanceAmountLabel.text = [NSString stringWithFormat:@"- %@", amountToSettle];
    } else {
        addMoneytextField.text = @"";
        addMoneytextField.placeholder = [NSString stringWithFormat:@"%@ 0.00", currencyStr];
        [addMoneyBtn1 setSelected:NO];
        [addMoneyBtn2 setSelected:NO];
        [addMoneyBtn2 setSelected:NO];
        self.addMoneytextField.userInteractionEnabled = YES;
        addMoneyBtn1.userInteractionEnabled = YES;
        addMoneyBtn2.userInteractionEnabled = YES;
        addMoneyBtn3.userInteractionEnabled = YES;
    }
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    if (context!=nil) {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    }
    [self tableViewReloadAndAdjustUI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITapGestureRecognizer Delagate -
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


#pragma mark - Custom Methods -
-(void) updateUI {
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = self.view.frame.size.width;
        CGRect frame = self.walletHeadingLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.walletHeadingLabel.frame = frame;
        
        frame = self.walletIconImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.walletIconImageView.frame = frame;
        self.walletIconImageView.transform = CGAffineTransformMakeScale(-1, 1);
        
        frame = self.currentBalanceStatusLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.currentBalanceStatusLabel.frame = frame;
        self.currentBalanceStatusLabel.textAlignment = NSTextAlignmentRight;
        
        frame = self.currentBalanceAmountLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.currentBalanceAmountLabel.frame = frame;
        self.currentBalanceAmountLabel.textAlignment = NSTextAlignmentLeft;
        
        frame = self.nextArrow.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.nextArrow.frame = frame;
        self.nextArrow.transform = CGAffineTransformMakeScale(-1, 1);
        
        frame = self.addMoneyBtn1.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.addMoneyBtn1.frame = frame;
        
        frame = self.addMoneyBtn3.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.addMoneyBtn3.frame = frame;
    } else {
        self.walletIconImageView.transform = CGAffineTransformMakeScale(1, 1);
        self.currentBalanceStatusLabel.textAlignment = NSTextAlignmentLeft;
        self.currentBalanceAmountLabel.textAlignment = NSTextAlignmentRight;
        self.nextArrow.transform = CGAffineTransformMakeScale(1, 1);
    }
}

-(void) setAmountButtonTitleWithCurrencyAndTextfieldPlaceholder {
    addMoneyBtn1.titleLabel.numberOfLines = 0;
    addMoneyBtn2.titleLabel.numberOfLines = 0;
    addMoneyBtn3.titleLabel.numberOfLines = 0;

    UIFont *font1 = [UIFont fontWithName:fontNormal size:10.0f];
    UIFont *font2 = [UIFont fontWithName:fontNormal  size:18.0f];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSForegroundColorAttributeName:UIColorFromRGB(0x555555)};
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font2,
                            NSForegroundColorAttributeName:UIColorFromRGB(0x555555)};
    
    NSDictionary *dict3 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSForegroundColorAttributeName:UIColorFromRGB(0xFFFFFF)};
    NSDictionary *dict4 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font2,
                            NSForegroundColorAttributeName:UIColorFromRGB(0xFFFFFF)};

    NSMutableAttributedString *attBtn1TitleForNormal = [[NSMutableAttributedString alloc] init];
    [attBtn1TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict1]];
    [attBtn1TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton1 attributes:dict2]];
    
    NSMutableAttributedString *attBtn1TitleForSelected = [[NSMutableAttributedString alloc] init];
    [attBtn1TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict3]];
    [attBtn1TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton1 attributes:dict4]];

    [addMoneyBtn1 setAttributedTitle:attBtn1TitleForNormal forState:UIControlStateNormal];
    [addMoneyBtn1 setAttributedTitle:attBtn1TitleForSelected forState:UIControlStateHighlighted];
    [addMoneyBtn1 setAttributedTitle:attBtn1TitleForSelected forState:UIControlStateSelected];

    
    NSMutableAttributedString *attBtn2TitleForNormal = [[NSMutableAttributedString alloc] init];
    [attBtn2TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict1]];
    [attBtn2TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton2 attributes:dict2]];
    
    NSMutableAttributedString *attBtn2TitleForSelected = [[NSMutableAttributedString alloc] init];
    [attBtn2TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict3]];
    [attBtn2TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton2 attributes:dict4]];
    
    [addMoneyBtn2 setAttributedTitle:attBtn2TitleForNormal forState:UIControlStateNormal];
    [addMoneyBtn2 setAttributedTitle:attBtn2TitleForSelected forState:UIControlStateHighlighted];
    [addMoneyBtn2 setAttributedTitle:attBtn2TitleForSelected forState:UIControlStateSelected];


    NSMutableAttributedString *attBtn3TitleForNormal = [[NSMutableAttributedString alloc] init];
    [attBtn3TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict1]];
    [attBtn3TitleForNormal appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton3 attributes:dict2]];
    
    NSMutableAttributedString *attBtn3TitleForSelected = [[NSMutableAttributedString alloc] init];
    [attBtn3TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",currencyStr] attributes:dict3]];
    [attBtn3TitleForSelected appendAttributedString:[[NSAttributedString alloc] initWithString:kWalletButton3 attributes:dict4]];

    [addMoneyBtn3 setAttributedTitle:attBtn3TitleForNormal forState:UIControlStateNormal];
    [addMoneyBtn3 setAttributedTitle:attBtn3TitleForSelected forState:UIControlStateHighlighted];
    [addMoneyBtn3 setAttributedTitle:attBtn3TitleForSelected forState:UIControlStateSelected];
    
    self.addMoneyView.layer.borderWidth = 1;
    self.addMoneyView.layer.cornerRadius = 5;
    self.addMoneyView.layer.masksToBounds = YES;
    self.addMoneyView.layer.borderColor = UIColorFromRGB(0x019F6E).CGColor;
    addMoneytextField.placeholder = [NSString stringWithFormat:@"%@ 0.00", currencyStr];
}

-(void)cardDetailsButtonClicked:(id)sender {
    UIButton *mBtn = (UIButton *)sender;
    isGoingDelete = YES;
    _dict = _arrayContainingCardInfo[mBtn.tag];
}

-(void)cardDetailsClicked:(NSDictionary *)getDict {
    _dict =  [getDict mutableCopy];
}

-(void)addPayment {
    CardLoginViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CardLogin"];
    VC.isComingFromVC = kPaymentViewController;
    VC.callback = ^(BOOL isCardAdded) {
        if (!isCardAdded) {

        }
    };
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

-(void) getCardDetailsFromService {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
        [self sendServiceToGetCardDetail];
    } else {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}

- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)cancelButtonPressedAccount {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"My Wallet", @"My Wallet")];
    if(isComingFromMapVC == YES) {
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setleftBarButtonImage:[UIImage imageNamed:@"payment_cancel_btn_on"] :[UIImage imageNamed:@"payment_cancel_btn_off"]];
    }
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if(isComingFromMapVC == YES)
        [self cancelButtonPressedAccount];
    else
        [self menuButtonPressedAccount];
}

-(void) tableViewReloadAndAdjustUI {
    [paymentTableView reloadData];
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height - 44;
    
    self.mainScrollView.frame = CGRectMake(0, 44, screenWidth, screenHeight);
    CGRect frame = walletView.frame;
    
    paymentTableView.frame = CGRectMake(0, 0, screenWidth, paymentTableView.contentSize.height);
    frame.origin.y = paymentTableView.frame.size.height;

    frame.origin.x = 0;
    frame.size.width = screenWidth;
    frame.size.height = walletView.frame.size.height;
    walletView.frame = frame;
    self.mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(walletView.frame) + 10);
}


#pragma mark - WebService call

-(void)sendServiceToGetCardDetail {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getCardDetails:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void)getCardDetails:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7)) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
        } else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            _arrayContainingCardInfo = dictResponse[@"cards"];
            [self addInDataBase];
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            [self tableViewReloadAndAdjustUI];
            NSString *currentBal = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"walletBal"]) floatValue]];
            [addMoneytextField resignFirstResponder];
            currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
            if ([dictResponse[@"walletBal"] integerValue] <= 0) {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0xeb1313)];
            } else {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0x666666)];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [self tableViewReloadAndAdjustUI];
            
        } else if([response[@"errFlag"] integerValue] == 1 && [response[@"errNum"] integerValue] == 51) {
            NSString *currentBal = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"walletBal"]) floatValue]];
            [addMoneytextField resignFirstResponder];
            currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
            if ([dictResponse[@"walletBal"] integerValue] <= 0) {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0xeb1313)];
            } else {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0x666666)];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [self tableViewReloadAndAdjustUI];
        }
    }
}

-(void)sendServiceToDeleteCard:(NSString *)cardID {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_cc_id":cardID,
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"removeCard"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getDeleteCardResponse:response cardIDToDelete:cardID];
                                   }
                               }];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void)getDeleteCardResponse:(NSDictionary *)response cardIDToDelete:(NSString *)cardID {
    if (!response) {
        [[ProgressIndicator sharedInstance]  hideProgressIndicator];
    } else {
        [[ProgressIndicator sharedInstance]  hideProgressIndicator];
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            [Database DeleteCard:cardID];
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            [self tableViewReloadAndAdjustUI];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}


-(void) getWalletCurrentMoney {
    NSString *sid = flStrForStr([Utility userID]);
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr(sid),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Current Balance...", @"Checking Current Balance...")];
}

-(void) getWalletCurrentMoneyResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        [Utility setWalletBalance:currentWalletAmount];
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentWalletAmount];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0x000000)];
        } else {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:fontNormal FSize:12 Color:UIColorFromRGB(0x000000)];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
    }
}


-(void) addMoneyToWallet:(NSString *)amount withCardID:(NSString *)cardID {
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_amount":flStrForStr(amount),
                             @"ent_token": flStrForStr(cardID),
                             @"ent_date_time":[Helper getCurrentNetworkDateTime]
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"AddMoneyToWallet"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self addMoneyToWalletResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Money...", @"Adding Money...")];
}

-(void) addMoneyToWalletResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil){
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: NO];
        NSString *currentBal = [Helper getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        addMoneytextField.text = @"";
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        [Utility setWalletBalance:currentBal];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0xeb1313)];
        } else {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:fontNormal FSize:14 Color:UIColorFromRGB(0x666666)];
        }
        if (isComingFromMapVC) {
            if (callback) {
                [self cancelButtonPressedAccount];
                callback(@"Wallet Money Added", @"OK", responseDict[@"amount"]);
            }
        }else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
    }
}


#pragma mark -UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (kPaymentType) {
        return arrDBResult.count + 1;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 45)];
    headerView.backgroundColor = UIColorFromRGB(0xF1F2F7);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 7.5, [UIScreen mainScreen].bounds.size.width - 20, 30)];
    [Helper setToLabel:headerLabel Text:NSLocalizedString(@"SAVED CARDS", @"SAVED CARDS") WithFont:fontBold FSize:13 Color:UIColorFromRGB(0x222328)];
    if ([Helper isCurrentLanguageRTL])
        headerLabel.textAlignment = NSTextAlignmentRight;
    else
        headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"paymentTableViewCell";
    cell = (PaymentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil) {
        cell =[[PaymentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if(indexPath.row == arrDBResult.count) {
        [Helper setToLabel:cell.textLabel Text:NSLocalizedString(@"Add Credit Card", @"Add Credit Card") WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x1E96EA)];
        cell.imageView.image = [UIImage imageNamed:@"addcard"];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"help_next_arrow_icon_off"]];
    } else {
        CardDetails *card = arrDBResult[indexPath.row];
        NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
        NSString *findRangeForString = [NSString stringWithFormat:@"%@ %@", @"XXXX", last4];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:findRangeForString attributes:nil];
        NSRange range1 = [findRangeForString rangeOfString:@"XXXX"];
        NSRange range2 = [findRangeForString rangeOfString: last4];
        [attributedString addAttribute: NSForegroundColorAttributeName
                                 value: UIColorFromRGB(0xA1A6BB)
                                 range: range1];
        [attributedString addAttribute: NSFontAttributeName
                                 value:  [UIFont fontWithName:fontNormal size:13]
                                 range: range1];
        
        [attributedString addAttribute: NSForegroundColorAttributeName
                                 value: UIColorFromRGB(0x000000)
                                 range: range2];
        
        [attributedString addAttribute: NSFontAttributeName
                                 value:  [UIFont fontWithName:fontNormal size:13]
                                 range: range2];
        
        cell.textLabel.attributedText = attributedString;
        cell.imageView.image = [Helper setPlaceholderToCardType:card.cardtype];
        if ([card.isDefault integerValue] == 1) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
            defaultCardIndex = indexPath.row + 1;
        } else {
            cell.accessoryView = nil;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    if ([Helper isCurrentLanguageRTL]) {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        if (indexPath.row == arrDBResult.count) {
            cell.accessoryView.transform = CGAffineTransformMakeScale(-1, 1);
            cell.imageView.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            cell.accessoryView.transform = CGAffineTransformMakeScale(1, 1);
            cell.imageView.transform = CGAffineTransformMakeScale(1, 1);
        }
    } else {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.accessoryView.transform = CGAffineTransformMakeScale(1, 1);
        cell.imageView.transform = CGAffineTransformMakeScale(1, 1);
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == arrDBResult.count) {
        [self addPayment];
    } else {
        cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        CardDetails *card;
        NSString *cardID;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
        if (defaultCardIndex > 0 && indexPath.row != defaultCardIndex - 1) {
            card = arrDBResult[defaultCardIndex - 1];
            cardID = card.idCard;
            [Database updateCardDetailsForCardId:cardID andStatus:@"0"];
            NSIndexPath *defaultIndex = [NSIndexPath indexPathForRow:defaultCardIndex-1 inSection:0];
            [self tableView:paymentTableView didDeselectRowAtIndexPath:defaultIndex];
        }
        defaultCardIndex = indexPath.row + 1;
        card = arrDBResult[defaultCardIndex - 1];
        cardID = card.idCard;
        [Database updateCardDetailsForCardId:cardID andStatus:@"1"];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView cellForRowAtIndexPath:indexPath];
    if(indexPath.row == arrDBResult.count) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"help_next_arrow_icon_off"]];
    } else {
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == arrDBResult.count) {
        return NO;
    } else {
        return YES;
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:NSLocalizedString(@"Delete", @"Delete") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        if (indexPath.row == defaultCardIndex - 1) {
            defaultCardIndex = 0;
        }
        CardDetails *card = arrDBResult[indexPath.row];
        [self sendServiceToDeleteCard:card.idCard];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}

-(void)addInDataBase {
    NSString *defaultCardID;
    arrDBResult = [[Database getCardDetails] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filterdArray.count) {
        CardDetails *card = filterdArray[0];
        defaultCardID = card.idCard;
    }
    [Database DeleteAllCard];
    [arrDBResult removeAllObjects];
    [_arrayContainingCardInfo enumerateObjectsUsingBlock:^(id dict, NSUInteger index, BOOL *stop) {
        NSMutableDictionary *cardDict = [dict mutableCopy];
        if ((defaultCardID.length && [cardDict[@"id"] isEqualToString:defaultCardID]) || (!defaultCardID.length && index == 0)) {
            [cardDict setObject:@"1" forKey:@"isDefault"];
        } else {
            [cardDict setObject:@"0" forKey:@"isDefault"];
        }
        [Database makeDataBaseEntry:cardDict];
        if (index >= _arrayContainingCardInfo.count) {
            *stop = YES;
        }
    }];
}


#pragma mark - UIButton Actions -

- (IBAction)openHistoryAction:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        return;
    }
    [self.view endEditing:YES];
    WalletHistoryViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletHistory"];
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

- (IBAction)addMoneyBtn1Action:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        return;
    }
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, kWalletButton1];
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn1 afterDelay:0];
}

- (IBAction)addMoneyBtn2Action:(id)sender {
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, kWalletButton2];
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn2 afterDelay:0];
}

- (IBAction)addMoneyBtn3Action:(id)sender {
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, kWalletButton3];
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn3 afterDelay:0];
}


-(void) setSelectedButton:(UIButton *)sender {
    if ([sender isEqual:addMoneyBtn1]) {
        [addMoneyBtn1 setSelected: YES];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: NO];
    } else if([sender isEqual:addMoneyBtn2]) {
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: YES];
        [addMoneyBtn3 setSelected: NO];
    } else if ([sender isEqual:addMoneyBtn3]) {
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: YES];
    }
}

- (IBAction)addCreditMoneyBtnAction:(id)sender {
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        return;
    }
    NSString *textFiledText = flStrForObj(addMoneytextField.text);
    NSString *amount = @"";
    NSRange replaceRange = [textFiledText rangeOfString:currencyStr];
    if (replaceRange.location != NSNotFound){
        amount = [textFiledText stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    NSInteger number = [amount integerValue];
    if (number == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"First enter amount", @"First enter amount")];
    } else if(number < kWalletMiniumCheck && amountToSettle.length == 0) {
        NSString *minimumValue = [NSString stringWithFormat:@"%@",[Helper getCurrencyLocal: (kWalletMiniumCheck - 1)]];
        NSString *message = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"The amount need to be more than", @"The amount need to be more than"), minimumValue];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:message];
    } else if(!arrDBResult.count){
        addMoneytextField.text = @"";
        addMoneytextField.placeholder = [NSString stringWithFormat:@"%@ 0.00", currencyStr];
        [addMoneyBtn1 setSelected:NO];
        [addMoneyBtn2 setSelected:NO];
        [addMoneyBtn2 setSelected:NO];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please add your credit card first and then try again.", @"Please add your credit card first and then try again.")];
    } else {
        [addMoneytextField resignFirstResponder];
        amountToAdd = amount;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
        NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {
            
            UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Are you sure that do you want to do transaction with above selected card?", @"Are you sure that do you want to do transaction with above selected card?")];
            RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
            [alertView setContainerView:containerView];
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Confirm", @"Confirm"), NSLocalizedString(@"Change", @"Change"), nil]];
            [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
                if (buttonIndex == 0) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
                    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
                    CardDetails *card = filterdArray[0];
                    [self addMoneyToWallet:amountToAdd withCardID:(NSString *)card.idCard];
                }
                [alertView close];
            }];
            [alertView setUseMotionEffects:true];
            [alertView show];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please selected your credit card first and then try again.", @"Please selected your credit card first and then try again.")];
        }
    }
}


#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if([XDKAirMenuController sharedMenu].isMenuOpened){
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        return NO;
    }
    activeTextField = textField;
    textField.text = currencyStr;
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] > 0) {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable]) {
            if ([textField isEqual:addMoneytextField]) {
            }
        } else {
            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
        }
    }
    self.activeTextField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *typedText = textField.text;
    typedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (typedText.length <= currencyStr.length) {
        addMoneytextField.text = @"";
        addMoneytextField.placeholder = [NSString stringWithFormat:@"%@ 0.00", currencyStr];
        return NO;
    } else if (typedText.length >= currencyStr.length+7) {
        [textField resignFirstResponder];
        return NO;
    }
    if (typedText.length) {
        NSString *amount;
        NSRange replaceRange = [typedText rangeOfString:currencyStr];
        if (replaceRange.location != NSNotFound){
            amount = [typedText stringByReplacingCharactersInRange:replaceRange withString:@""];
        }
        NSArray *items = @[kWalletButton1, kWalletButton2, kWalletButton3];
        int item = 3;
        if ([items indexOfObject:amount] < 3) {
            item = [items indexOfObject:amount];
        }
        switch (item) {
            case 0:
                [addMoneyBtn1 setSelected:YES];
                break;
            case 1:
                [addMoneyBtn2 setSelected:YES];
                break;
            case 2:
                [addMoneyBtn3 setSelected:YES];
                break;
            default:
                [addMoneyBtn1 setSelected: NO];
                [addMoneyBtn2 setSelected: NO];
                [addMoneyBtn3 setSelected: NO];
                break;
        }
    }
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.walletView addGestureRecognizer:tap];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self.walletView removeGestureRecognizer:tap];
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 50;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height);
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.mainScrollView.contentOffset = CGPointMake(0, -remainder);
                     }];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    self.mainScrollView.contentOffset = CGPointMake(0, -20);
    [self tableViewReloadAndAdjustUI];
}


@end
