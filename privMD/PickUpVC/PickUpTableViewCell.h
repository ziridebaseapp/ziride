//
//  PickUpTableViewCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickUpTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *locationType;
@property (weak, nonatomic) IBOutlet UILabel *addressLine1;
@property (weak, nonatomic) IBOutlet UILabel *addressLine2;
@property (weak, nonatomic) IBOutlet UIButton *favouriteBtn;

@end
