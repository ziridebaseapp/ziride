//
//  PickUpTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PickUpTableViewCell.h"

@implementation PickUpTableViewCell
@synthesize locationType;
@synthesize addressLine1;
@synthesize addressLine2;
@synthesize favouriteBtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame = self.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    [self subviews][0].frame = frame;

    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGRect frame = self.locationType.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.locationType.frame = frame;
        self.locationType.textAlignment = NSTextAlignmentRight;
        
        frame = self.addressLine1.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.addressLine1.frame = frame;
        self.addressLine1.textAlignment = NSTextAlignmentRight;

        frame = self.addressLine2.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.addressLine2.frame = frame;
        self.addressLine2.textAlignment = NSTextAlignmentRight;

        frame = self.favouriteBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.favouriteBtn.frame = frame;
    } else {
        self.locationType.textAlignment = NSTextAlignmentLeft;
        self.addressLine1.textAlignment = NSTextAlignmentLeft;
        self.addressLine1.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
