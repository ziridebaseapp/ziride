//
//  PickUpViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickUpViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
	NSManagedObjectContext *context;
}
@property(nonatomic,assign) BOOL isComingFromMapVCFareButton;
@property(nonatomic,assign) NSInteger typeID;
@property (nonatomic, copy)   void (^onCompletion)(NSDictionary *suburb,NSInteger locationtype);
@property(strong, nonatomic)  NSString *searchString;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarController;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *manualAddressView;
@property (weak, nonatomic) IBOutlet UIImageView *pickAddressFromMapImageView;
@property (weak, nonatomic) IBOutlet UILabel *selectAddressLabel;

- (IBAction)pickAddressFromMabBtnAction:(id)sender;



@property (strong, nonatomic) NSMutableArray *mAddress;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (assign, nonatomic) NSInteger locationType;
-(void)dismissViewController:(UIButton *)sender;
- (IBAction)favouriteBtnAction:(id)sender;

@end
