//
//  PickUpViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PickUpViewController.h"
#import "PickUpTableViewCell.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"
#import "Database.h"
#import "SourceAddress.h"
#import "MapViewController.h"
#import "SplashViewController.h"
#import "SaveLocationViewController.h"
#import "RPCustomIOSAlertView.h"
#import "UILabel+DynamicHeight.h"

@interface PickUpViewController ()

@property(nonatomic,assign) BOOL isSearchResultCome;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;
@property NSMutableArray *favAddressArray;
@property NSMutableArray *recentAddressArray;

@end


@implementation PickUpViewController
@synthesize onCompletion;
@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearchResultCome = NO;
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    self.searchBarController.searchBarStyle = UISearchBarStyleProminent;
    [self createNavLeftButton];
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    [self getAddressFromDatabase];
}
-(void)viewDidAppear:(BOOL)animated {
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [_searchBarController becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom Navigation
/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


/**
 Dismiss View

 @param sender self
 */
- (void)dismissViewController:(UIButton *)sender {
    MapViewController *obj = [MapViewController getSharedInstance];
    obj.isSelectinLocation = NO;
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
}

#pragma mark - Custom Methods -
/**
 Get address from local Database
 */
-(void) getAddressFromDatabase {
    self.navigationItem.title = NSLocalizedString(@"Search Address", @"Search Address");
    if(locationType == kSourceAddress) {
        _searchBarController.placeholder = NSLocalizedString(@"Pickup Location", @"Pickup Location");
    } else {
        _searchBarController.placeholder = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
    }
    if (context!=nil) {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    }
    [self.navigationItem setHidesBackButton:YES animated:YES];
    if (arrDBResult.count != 0) {
        _mAddress = [arrDBResult mutableCopy];
        _favAddressArray = [[NSMutableArray alloc] init];
        _recentAddressArray = [[NSMutableArray alloc] init];
        for (int i=0; i<arrDBResult.count; i++) {
            id address =  arrDBResult[i];
            if (!isSearchResultCome ) {
                SourceAddress *add = (SourceAddress*)address;
                if ([add.isFavourite intValue] == 1) {
                    [_favAddressArray addObject:add];
                } else {
                    if (locationType == kSourceAddress && [add.addressType intValue] == 1) {
                        [_recentAddressArray addObject:add];
                    } else if (locationType == kDestinationAddress && [add.addressType intValue] == 2){
                        [_recentAddressArray addObject:add];
                    }
                }
            }
        }
    } else {
        _mAddress = [NSMutableArray array];
    }
    [self.tblView reloadData];
}

/**
 Go back from you came this class

 @param dictionary dictionary
 */
-(void)gotoViewController:(NSDictionary *)dictionary {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UIButton Action -

/**
 Favourite Button Action

 @param sender favBtn
 */
- (IBAction)favouriteBtnAction:(id)sender {
    SourceAddress *address;
    UIButton *favBtn = sender;
    if (isSearchResultCome) {
        return;
    } else {
        if (favBtn.tag < 1000) {
            if (_favAddressArray.count) {
                address = (SourceAddress *)_favAddressArray[favBtn.tag];
            } else {
                address = (SourceAddress *)_recentAddressArray[favBtn.tag];
            }
        } else {
            address = (SourceAddress *)_recentAddressArray[favBtn.tag-1000];
        }
    }
    if (favBtn.selected) {
        UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Are you sure you want to delete this favourite location?", @"Are you sure you want to delete this favourite location?")];
        RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
        [alertView setContainerView:containerView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Yes", @"Yes"), NSLocalizedString(@"No", @"No"), nil]];
        [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
            if (buttonIndex == 0) {
                if (address.addressID.length) {
                    [self removeFromFavouriteAddress:address.addressID];
                } else {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Sorry, something went wrong.", @"Sorry, something went wrong.")];
                }
            }
            [alertView close];
        }];
        [alertView setUseMotionEffects:true];
        [alertView show];
    } else {
        SaveLocationViewController *saveLocVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SaveLocationVC"];
        if (!isSearchResultCome) {
            SourceAddress *add = (SourceAddress*) address;
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            NSString *sourceAddress = flStrForStr(add.srcAddress);
            NSString *sourceAddress1 = flStrForStr(add.srcAddress2);
            if ([sourceAddress1 rangeOfString:sourceAddress].location == NSNotFound) {
                if(sourceAddress.length)
                    [arr addObject:sourceAddress];
                if (sourceAddress1.length)
                    [arr addObject:sourceAddress1];
            } else {
                if (sourceAddress1.length)
                    [arr addObject:sourceAddress1];
            }
            NSString *addressText = [arr componentsJoinedByString:@", "];
            saveLocVC.navigationTitle = NSLocalizedString(@"Favourite Location", @"Favourite Location");
            saveLocVC.lastBtnName = NSLocalizedString(@"Save Location", @"Save Location");
            saveLocVC.dropoffAddressView.hidden = YES;
            saveLocVC.saveLocationView.hidden = NO;
            saveLocVC.addressChangable = NO;
            saveLocVC.isComingForDropAddress = NO;
            saveLocVC.address = flStrForStr(addressText);
            saveLocVC.address1 = flStrForStr(add.srcAddress);
            saveLocVC.address2 = flStrForStr(add.srcAddress2);
            saveLocVC.zipCode = flStrForStr(add.zipCode);
            saveLocVC.latitude = [add.srcLatitude floatValue];
            saveLocVC.longitude = [add.srcLongitude floatValue];
            saveLocVC.locationType = [add.addressType intValue];
        }
        saveLocVC.onCompletion = ^(NSInteger success, NSDictionary *dict) {
            if (success) {
                [self getAddressFromDatabase];
            }
        };
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:saveLocVC];
        [self presentViewController:navBar animated:YES completion:nil];
    }
}


/**
 Pick Location from map

 @param sender pickAddressFromMapBtn
 */
- (IBAction)pickAddressFromMabBtnAction:(id)sender {
    if (locationType == kSourceAddress) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        SaveLocationViewController *saveLocVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SaveLocationVC"];
        saveLocVC.dropoffAddressView.hidden = NO;
        saveLocVC.saveLocationView.hidden = YES;
        saveLocVC.navigationTitle = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
        saveLocVC.lastBtnName = NSLocalizedString(@"Confirm Location", @"Confirm Location");
        saveLocVC.locationType = locationType;
        saveLocVC.isComingForDropAddress = YES;
        saveLocVC.addressChangable = YES;
        saveLocVC.latitude = [[[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] doubleValue];
        saveLocVC.longitude = [[[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] doubleValue];
        saveLocVC.onCompletion =^(NSInteger success, NSDictionary *dict){
            if (onCompletion) {
                onCompletion(dict,locationType);
            }
            [self gotoViewController:dict];
        };
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:saveLocVC];
        [self presentViewController:navBar animated:YES completion:nil];
    }
}


#pragma mark - WebServices -

/**
 Remove favourite address from server

 @param addressID addressID
 */
-(void)removeFromFavouriteAddress:(NSString *) addressID {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_sid":[Utility userID],
                             @"ent_action":@"2",
                             @"ent_aid":flStrForStr(addressID),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"favourateAdresss"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                       }  else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
                                           [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                           [XDKAirMenuController relese];
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                                           SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
                                           self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
                                           [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
                                           User *user = [[User alloc] init];
                                           [user deleteUserSavedData];
                                       } else if ([[response objectForKey:@"errFlag"] intValue] == 1) {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
                                       } else {
                                           BOOL isFavouriteRemoved = [Database updateAddressWithAddressID:addressID asFavouriteTag:0];
                                           if(isFavouriteRemoved) {
                                               [self getAddressFromDatabase];
                                           }
                                       }
                                   }
                               }];
}

#pragma mark - UITableView DataSource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (arrDBResult.count) {
        int numberOfSection = 0;
        if (_favAddressArray.count) {
            numberOfSection++;
        }
        if (_recentAddressArray.count) {
            numberOfSection++;
        }
        return numberOfSection;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (arrDBResult.count) {
        if (section == 0) {
            if (_favAddressArray.count) {
                return _favAddressArray.count;
            } else {
                return _recentAddressArray.count;
            }
        } else {
            return _recentAddressArray.count;
        }
    } else {
        return _mAddress.count;
    }
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier=@"AddressCell";
    PickUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil) {
        cell = (PickUpTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[UIColor clearColor];
    }
    
    id address;
    if (!isSearchResultCome && arrDBResult.count) {
        if (indexPath.section == 0) {
            if(_favAddressArray.count) {
                address  =  _favAddressArray[indexPath.row];
            } else {
                address  =  _recentAddressArray[indexPath.row];
            }
        } else {
            address  =  _recentAddressArray[indexPath.row];
        }
    } else {
        address = _mAddress[indexPath.row];
    }
    if (isSearchResultCome) {
        NSDictionary *searchResult = [_mAddress objectAtIndex:indexPath.row];
        cell.locationType.text = @"";
        cell.addressLine1.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
        cell.addressLine2.text = searchResult[@"description"];
        cell.favouriteBtn.tag = ((indexPath.section * 1000) + indexPath.row);
        cell.favouriteBtn.selected = NO;
    } else if (!isSearchResultCome ) {
        SourceAddress *add = (SourceAddress*)address;
        cell.locationType.text = flStrForStr(add.locationType);
        cell.addressLine1.text = flStrForStr(add.srcAddress);
        cell.addressLine2.text = flStrForStr(add.srcAddress2);
        cell.favouriteBtn.tag = ((indexPath.section * 1000) + indexPath.row);
        if ([add.isFavourite intValue] == 1) {
            cell.favouriteBtn.selected = YES;
        } else {
            cell.favouriteBtn.selected = NO;
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  90;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (arrDBResult.count) {
        if (section == 0) {
            if (_favAddressArray.count) {
                return NSLocalizedString(@"  Saved Locations", @"  Saved Locations");
            } else {
                return NSLocalizedString(@"  Recent Locations", @"  Recent Locations");
            }
        } else {
            return NSLocalizedString(@"  Recent Locations", @"  Recent Locations");
        }
    } else {
        return NSLocalizedString(@"  Result", @"  Result");
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(isSearchResultCome == YES ) {
        NSString* addressType = @"";
        if (locationType == kSourceAddress) {
            addressType = @"1";
        } else {
            addressType = @"2";
        }
        NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.row];
        NSString *placeID = [searchResult objectForKey:@"place_id"];
        [self.searchBarController resignFirstResponder];
        [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place) {
             NSMutableDictionary *placeDict = [place mutableCopy];
             [placeDict setObject:[NSNumber numberWithInt:0]  forKey:@"isFavourite"];
             [placeDict setObject:@""  forKey:@"locationType"];
             [placeDict setObject:[NSNumber numberWithInt:[addressType intValue]] forKey:@"addressType"];
             [Database addSourceAddressInDataBase:placeDict];
             NSDictionary *dict = [NSDictionary dictionary];
             if (onCompletion) {
                 NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
                 NSString *add2 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"formatted_address"])];;
                 NSString *late = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                 NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                 NSString *keyId = [NSString stringWithFormat:@"%@",[place valueForKey:@"place_id"]];
                 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"types CONTAINS[cd] %@",@"postal_code"];
                 NSArray *filterdArray = [[[[place valueForKey:@"address_components"] mutableCopy] filteredArrayUsingPredicate:predicate] mutableCopy];
                 NSString *zipCode = @"";
                 if(filterdArray.count > 0) {
                     zipCode = filterdArray[0][@"long_name"];
                 }
                 if ([add2 rangeOfString:add1].location != NSNotFound) {
                     NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:[add2 componentsSeparatedByString:@", "]];
                     if (arr.count && [arr containsObject:add1]) {
                         [arr removeObject:add1];
                         add2 = [arr componentsJoinedByString:@", "];
                     }
                 }
                 dict = @{ @"address1":add1,
                           @"address2":add2,
                           @"lat":late,
                           @"lng":longi,
                           @"keyId":keyId,
                           @"zipCode":zipCode,
                           @"isFavourite":@"0",
                           @"locationType":@"",
                           @"addressType":addressType,
                           };
                 onCompletion(dict,locationType);
             }
             [self gotoViewController:dict];
         }];
    }
    else{
        NSDictionary *dict = [NSDictionary dictionary];
        int addressType = 0;
        if (locationType == kSourceAddress) {
            addressType = 1;
        } else {
            addressType = 2;
        }
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
        SourceAddress *add;
        if (arrDBResult.count) {
            if (indexPath.section == 0) {
                if (_favAddressArray.count) {
                    add  =  (SourceAddress *)_favAddressArray[indexPath.row];
                } else {
                    add  =  (SourceAddress *)_recentAddressArray[indexPath.row];
                }
            } else {
                add  =  (SourceAddress *)_recentAddressArray[indexPath.row];
            }
        }
        dict = @{ @"address1": add.srcAddress,
                  @"address2":add.srcAddress2,
                  @"lat":add.srcLatitude,
                  @"lng":add.srcLongitude,
                  @"keyId":flStrForStr(add.keyId),
                  @"zipCode":flStrForStr(add.zipCode),
                  @"isFavourite":[NSNumber numberWithInt:[add.isFavourite intValue]],
                  @"locationType":flStrForStr(add.locationType),
                  @"addressType":[NSNumber numberWithInt:addressType],
                  };
        if (onCompletion) {
            onCompletion(dict,locationType);
        }
        [self gotoViewController:dict];
    }
}

#pragma mark -
#pragma mark Search Bar Delegates
#pragma mark - Autocomplete SearchBar methods

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBarController resignFirstResponder];
    [arrDBResult removeAllObjects];
    [_favAddressArray removeAllObjects];
    [_recentAddressArray removeAllObjects];
    [self.tblView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSString *searchWordProtection = [self.searchBarController.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length != 0) {
        [self runScript];
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    self.substring = [NSString stringWithString:self.searchBarController.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}

- (void)runScript {
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring {
    [self.mAddress removeAllObjects];
    [arrDBResult removeAllObjects];
    [_favAddressArray removeAllObjects];
    [_recentAddressArray removeAllObjects];
    [self.tblView reloadData];
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            [self.mAddress addObjectsFromArray:results];
            isSearchResultCome = YES;
            NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
            [self.pastSearchResults addObject:searchResult];
            [arrDBResult removeAllObjects];
            [_favAddressArray removeAllObjects];
            [_recentAddressArray removeAllObjects];
            [self.tblView reloadData];
        }];
    } else {
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.mAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [arrDBResult removeAllObjects];
                [_favAddressArray removeAllObjects];
                [_recentAddressArray removeAllObjects];
                [self.tblView reloadData];
            }
        }
    }
}


#pragma mark - Google API Requests -

-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length != 0) {
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500&amplanguage=en&key=%@",searchWord,latitude,longitude,kPlaceAPIKey];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!data){
                NSDictionary *userInfo = @{@"error":@"Data Nil"};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                NSLog(@"Error = %@", newError);
                return;
            }
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    NSLog(@"Error = %@", newError);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            } else {
                NSArray *results = [jSONresult valueForKey:@"predictions"];
                complete(results);
            }
        }];
        
        [task resume];
    }
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",place,kPlaceAPIKey];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
}



@end
