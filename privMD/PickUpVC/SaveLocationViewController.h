//
//  SaveLocationViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SaveLocationViewController : UIViewController

@property (nonatomic, copy)   void (^onCompletion)(NSInteger success, NSDictionary *dict);
@property (weak, nonatomic) IBOutlet UIView *topView;



@property (weak, nonatomic) IBOutlet UIImageView *customMarkerImage;
@property (weak, nonatomic) IBOutlet UIImageView *crossMarkImage;
@property (weak, nonatomic) IBOutlet UIView *bottonView;
@property (weak, nonatomic) IBOutlet UIView *saveLocationView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *favStarImageView;
@property (weak, nonatomic) IBOutlet UITextField *locationtypeTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveLocationBtn;

@property (weak, nonatomic) IBOutlet UIView *dropoffAddressView;
@property (weak, nonatomic) IBOutlet UILabel *dropoffLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *horizontalLineView;
@property (weak, nonatomic) IBOutlet UIButton *favouriteLocBtn;

@property (strong, nonatomic) GMSMapView *mapView_;
@property (nonatomic) BOOL addressChangable;
@property (nonatomic) BOOL isComingForDropAddress;


- (IBAction)favouriteLocBtnAction:(id)sender;


@property (nonatomic, strong) NSString *navigationTitle;
@property (nonatomic, strong) NSString *lastBtnName;

@property (nonatomic) NSInteger locationType;
@property (nonatomic) NSInteger addressType;


- (IBAction)saveLocationbtnAction:(id)sender;

@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *address1;
@property(strong, nonatomic) NSString *address2;
@property(strong, nonatomic) NSString *zipCode;
@property(nonatomic) float latitude;
@property(nonatomic) float longitude;

@end
