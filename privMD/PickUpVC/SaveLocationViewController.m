//
//  SaveLocationViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SaveLocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Database.h"
#import "SplashViewController.h"
#import "PubNubWrapper.h"
#import "User.h"

@interface SaveLocationViewController ()<CustomNavigationBarDelegate, GMSMapViewDelegate> {
    GMSMapView *mapView_;
    GMSGeocoder *geocoder_;
    UITextField *activeTextField;
    CGFloat keyboardHeight;
    BOOL changingAddress;
    BOOL favBtnClicked;
}
@end

@implementation SaveLocationViewController
@synthesize  topView, bottonView;
@synthesize  latitude, longitude;
@synthesize addressLabel;
@synthesize address;
@synthesize saveLocationBtn;
@synthesize customMarkerImage, crossMarkImage;
@synthesize address1, address2, zipCode;
@synthesize onCompletion;
@synthesize locationType,addressType;
@synthesize mapView_;


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.navigationTitle;
    [Helper setButton:self.saveLocationBtn Text:self.lastBtnName WithFont:fontNormal FSize:15 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    [self createNavLeftButton];
    [self addMapOnView];
    if (self.isComingForDropAddress) {
        [self getAddress:CLLocationCoordinate2DMake(latitude,longitude)];
        self.dropoffAddressView.hidden = NO;
        self.saveLocationView.hidden = YES;
    }else {
        self.dropoffAddressView.hidden = YES;
        self.saveLocationView.hidden = NO;
    }
    addressLabel.text = address;
    changingAddress = NO;
    [self updateUI];
}

-(void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark- Custom Methods

/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


/**
 Naviagtion Left Button Action

 @param sender leftBtn
 */
-(void)cancelBtnClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (onCompletion) {
            onCompletion(0, [NSDictionary new]);
        }
    }];
}


#pragma mark - Custom Methods -

/**
 Update UI
 */
-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.addressLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.addressLabel.frame = frame;
        
        frame = self.favStarImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.favStarImageView.frame = frame;
        
        frame = self.locationtypeTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.locationtypeTextField.frame = frame;
        
        frame = self.dropoffLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dropoffLabel.frame = frame;
        
        frame = self.dropAddressLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dropAddressLabel.frame = frame;
        
        frame = self.horizontalLineView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.horizontalLineView.frame = frame;
        
        frame = self.favouriteLocBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.favouriteLocBtn.frame = frame;
    }
}

/**
 Add Map on View
 */
-(void) addMapOnView {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude  longitude:longitude zoom:16];
    topView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height-bottonView.frame.size.height-64);
    mapView_ = [GMSMapView mapWithFrame:topView.frame camera:camera];
    mapView_.settings.compassButton = YES;
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.userInteractionEnabled = self.addressChangable;
    [mapView_ clear];
    [topView addSubview:mapView_];
    CGRect frame = bottonView.frame;
    frame.origin.y = screenSize.height - bottonView.frame.size.height - 64;
    bottonView.frame = frame;
    geocoder_ = [[GMSGeocoder alloc] init];
    if(self.addressChangable) {
        self.bottonView.frame = CGRectMake(0, screenSize.height - 130 - 64, screenSize.width, 130);
        self.topView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height - 130 - 64);
        UIImage *markerImage = [UIImage imageNamed:@"centerMarker"];
        UIImage *crossImage = [UIImage imageNamed:@"home_cross_icon"];
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        if (self.isComingForDropAddress) {
            if (favBtnClicked) {
                customMarkerImage.frame = CGRectMake(screenSize.width/2 - markerImage.size.width/2, mapView_.frame.size.height/2 - markerImage.size.height, markerImage.size.width, markerImage.size.height);
                favBtnClicked = NO;
            } else {
                customMarkerImage.frame = CGRectMake(screenSize.width/2 - markerImage.size.width/2, mapView_.frame.size.height/2 - (markerImage.size.height-15)/2, markerImage.size.width, markerImage.size.height);
            }
        }
        crossMarkImage.frame = CGRectMake(screenSize.width/2 - crossImage.size.width/2, mapView_.frame.size.height/2 - 15/2, crossImage.size.width, crossImage.size.height);

        [topView bringSubviewToFront:customMarkerImage];
        [topView bringSubviewToFront:crossMarkImage];
        [self addLocationButtonsOnMap];
    } else {
        UIImage *markerImage = [UIImage imageNamed:@"centerMarker"];
        customMarkerImage.frame = CGRectMake(screenSize.width/2 - markerImage.size.width/2, mapView_.frame.size.height/2 - (markerImage.size.height-7.5)/2, markerImage.size.width, markerImage.size.height);

        [topView bringSubviewToFront:customMarkerImage];

    }
}

/**
 Start Merker animation
 */
-(void) startAnimation {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    CGRect frame = self.customMarkerImage.frame;
    UIImage *markerImage = [UIImage imageNamed:@"centerMarker"];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    frame = CGRectMake(screenSize.width/2 - markerImage.size.width/2, mapView_.frame.size.height/2 - markerImage.size.height - 25, markerImage.size.width, markerImage.size.height);
    self.customMarkerImage.frame = frame;
    crossMarkImage.hidden = NO;
    [UIView commitAnimations];
}

/**
 End Merker animation
 */
-(void) endAnimation {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    CGRect frame = customMarkerImage.frame;
    UIImage *markerImage = [UIImage imageNamed:@"centerMarker"];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    frame = CGRectMake(screenSize.width/2 - markerImage.size.width/2, mapView_.frame.size.height/2 - markerImage.size.height, markerImage.size.width, markerImage.size.height);
    customMarkerImage.frame = frame;
    crossMarkImage.hidden = YES;
    [UIView commitAnimations];
}

#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture {
    changingAddress = YES;
    [self startAnimation];
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position {
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    if (changingAddress) {
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        latitude = coor.latitude;
        longitude = coor.longitude;
        [self getAddress:coor];
        [self endAnimation];
    }
    changingAddress = NO;
}

/**
 Get address for given coordinates

 @param coordinate coordinate
 */
- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        if (response && response.firstResult) {
            GMSAddress *address3 = response.firstResult;
            NSMutableArray *addressArr = [address3.lines mutableCopy];
            if ([addressArr containsObject:@""])
                [addressArr removeObject:@""];
            NSString *addressText = [NSString stringWithFormat:@"%@",flStrForStr([address3.lines componentsJoinedByString:@", "])];
            latitude = response.firstResult.coordinate.latitude;
            longitude = response.firstResult.coordinate.longitude;
            address = addressText;
            address1 = flStrForStr(addressArr[0]);
            address2 = flStrForStr(addressText);
            zipCode = flStrForStr(address3.postalCode);
            dispatch_async(dispatch_get_main_queue(),^{
                self.dropAddressLabel.text = addressText;
                BOOL isFav = [Database checkAddressIsFavouriteInDatabase:address1 and:address2];
                if (isFav) {
                    self.favouriteLocBtn.selected = YES;
                } else {
                    self.favouriteLocBtn.selected = NO;
                }
            });
        } else {
            latitude = 0;
            longitude = 0;
            address = @"";
            dispatch_async(dispatch_get_main_queue(),^{
                self.favouriteLocBtn.selected = NO;
                self.dropAddressLabel.text = NSLocalizedString(@"Fetching Location...", @"Fetching Location...");
            });
        }
    };
    [geocoder_ reverseGeocodeCoordinate:coordinate completionHandler:handler];
}

/**
 Add location button on Map
 */
-(void)addLocationButtonsOnMap {
    UIView *vb = [self.topView viewWithTag:100];
    NSArray *arr = self.topView.subviews;
    if (![arr containsObject:vb]) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(screenSize.width-45, mapView_.frame.size.height/2 - 140/2, 40, 140)];
        customView.tag = 100;
        [customView setHidden:NO];
        customView.backgroundColor = [UIColor clearColor];
        [mapView_ addSubview:customView];
        
        UIButton *trafficBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        trafficBtn.frame = CGRectMake(0, 5, 40, 40);
        trafficBtn.tag = 103;
        [Helper setButton:trafficBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [trafficBtn setImage:[UIImage imageNamed:@"home_traffic_icon_off"] forState:UIControlStateNormal];
        [trafficBtn setImage:[UIImage imageNamed:@"home_traffic_icon_on"] forState:UIControlStateSelected];
        [trafficBtn addTarget:self action:@selector(changeMapTrafficMode:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:trafficBtn];

        UIButton *sateliteViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sateliteViewBtn.frame = CGRectMake(0, 50, 40, 40);
        sateliteViewBtn.tag = 101;
        [Helper setButton:sateliteViewBtn Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [sateliteViewBtn setImage:[UIImage imageNamed:@"home_satelight_icon_off"] forState:UIControlStateNormal];
        [sateliteViewBtn setImage:[UIImage imageNamed:@"home_satelight_icon_on"] forState:UIControlStateSelected];
        [sateliteViewBtn addTarget:self action:@selector(changeMapType:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:sateliteViewBtn];
        
        UIButton *currentLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        currentLoc.frame = CGRectMake(0, 95, 40, 40);
        currentLoc.tag = 102;
        [Helper setButton:currentLoc Text:@"" WithFont:fontNormal FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [currentLoc setImage:[UIImage imageNamed:@"home_gps_icon_on"] forState:UIControlStateNormal];
        [currentLoc setImage:[UIImage imageNamed:@"home_gps_icon_off"] forState:UIControlStateSelected];
        [currentLoc addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:currentLoc];
    } else {
        [self.topView bringSubviewToFront:vb];
    }
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        UIView *locationButtonView = [self.topView viewWithTag:100];
        CGRect frame = locationButtonView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        locationButtonView.frame = frame;
    }
}


#pragma mark - Custon Button Actions -
/**
 Get current Location from Map

 @param sender currentLocationBtn
 */
- (void)getCurrentLocation:(UIButton *)sender {
    CLLocation *location = mapView_.myLocation;
    latitude = location.coordinate.latitude;
    longitude = location.coordinate.longitude;
    if (location) {
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(latitude, longitude) zoom:16];
        [mapView_ animateWithCameraUpdate:zoomCamera];
        [self getAddress:CLLocationCoordinate2DMake(latitude,longitude)];
    }
}

/**
 Change map type

 @param sender satelliteBtn
 */
-(void)changeMapType:(UIButton *)sender {
    if (sender.isSelected == NO) {
        sender.selected = YES;
        mapView_.mapType = kGMSTypeSatellite;
    } else {
        sender.selected = NO;
        mapView_.mapType = kGMSTypeNormal;
    }
}

/**
 Change traffic mode

 @param sender trafficbtn
 */
-(void)changeMapTrafficMode:(UIButton *)sender {
    if (sender.isSelected == NO) {
        sender.selected = YES;
        mapView_.trafficEnabled = YES;
    } else {
        sender.selected = NO;
        mapView_.trafficEnabled = NO;
    }
}
#pragma mark - UIButton Action -

/**
 Save location button action
 
 @param sender saveLocBtn
 */
- (IBAction)saveLocationbtnAction:(id)sender {
    if (!self.dropoffAddressView.isHidden) {
        NSDictionary *dict = @{
                               @"address1":flStrForStr(address1),
                               @"address2":flStrForStr(address2),
                               @"zipCode":flStrForStr(zipCode),
                               @"lat":[NSNumber numberWithFloat:latitude],
                               @"lng":[NSNumber numberWithFloat:longitude],
                               @"locationType":flStrForStr(self.locationtypeTextField.text),
                               @"isFavourite":[NSNumber numberWithInt:1],
                               @"addressType":[NSNumber numberWithInteger:locationType]
                               };
        [self dismissViewControllerAnimated:YES completion:^{
            if (onCompletion) {
                onCompletion(1, dict);
            }
        }];
    } else {
        [self.locationtypeTextField resignFirstResponder];
        if (!self.locationtypeTextField.text.length) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please add your location like home, office etc...", @"Please add your location like home, office etc...")];
        } else {
            NSArray *getAllFavAddress = [Database getSourceAddressFromDataBase];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"locationType CONTAINS[cd] %@",flStrForStr(self.locationtypeTextField.text)];
            NSArray *filterdArray = [[getAllFavAddress filteredArrayUsingPredicate:predicate] mutableCopy];
            if (filterdArray.count) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"This name already exists, try something else.", @"This name already exists, try something else.")];
                return;
            }
            NSDictionary *dict = @{
                                   @"address1":flStrForStr(address1),
                                   @"address2":flStrForStr(address2),
                                   @"zipCode":flStrForStr(zipCode),
                                   @"lat":[NSNumber numberWithFloat:latitude],
                                   @"lng":[NSNumber numberWithFloat:longitude],
                                   @"locationType":flStrForStr(self.locationtypeTextField.text),
                                   @"isFavourite":[NSNumber numberWithInt:1],
                                   @"addressType":[NSNumber numberWithInteger:locationType],
                                   @"place_id":@"",
                                   };
            [self addAddressToFavourite:dict];
        }
    }
}

/**
 Favourite Location Button Action
 
 @param sender favLocationBtn
 */
- (IBAction)favouriteLocBtnAction:(id)sender {
    UIButton *favLocationBtn = (UIButton *)sender;
    if (favLocationBtn.selected) {
        return;
    }
    favLocationBtn.selected = YES;
    self.navigationTitle = NSLocalizedString(@"Favourite Location", @"Favourite Location");
    self.lastBtnName = NSLocalizedString(@"Save Location", @"Save Location");
    self.addressLabel.text = flStrForStr(self.dropAddressLabel.text);
    self.navigationItem.title = self.navigationTitle;
    [Helper setButton:self.saveLocationBtn Text:self.lastBtnName WithFont:fontNormal FSize:15 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    self.dropoffAddressView.hidden = YES;
    self.saveLocationView.hidden = NO;
    self.addressChangable = NO;
    [mapView_ removeFromSuperview];
    [self addMapOnView];
}

#pragma -marks Keyboard Appear and Hide Methods
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame);
    UIView *view = [textfield superview];
    while (view && view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.window.frame) - (textfieldMaxY + height);
    if (remainder >= 0) {
    }
    else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             CGSize screenSize = [UIScreen mainScreen].bounds.size;
                             self.view.frame = CGRectMake(0, remainder, screenSize.width, screenSize.height);
                         }];
    }
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    self.view.frame = CGRectMake(0, 64, screenSize.width, screenSize.height-64);
}

#pragma UITextFields Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length > 0) {
    }
    activeTextField = nil;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length > 0) {
    }
    return YES;
}



#pragma mark - WebServices -

/**
 Add favourite Address

 @param dict address data
 */
-(void)addAddressToFavourite:(NSDictionary *)dict {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_sid":[Utility userID],
                             @"ent_action":@"1",
                             @"ent_address1":flStrForStr(dict[@"address1"]),
                             @"ent_address2":flStrForStr(dict[@"address2"]),
                             @"ent_title":flStrForStr(dict[@"locationType"]),
                             @"ent_lat":flStrForStr(dict[@"lat"]),
                             @"ent_long":flStrForStr(dict[@"lng"]),
                             @"ent_zipCode":flStrForStr(dict[@"zipCode"]),
                             @"ent_addressType":flStrForStr(dict[@"addressType"])
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"favourateAdresss"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                       }  else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
                                       } else if ([[response objectForKey:@"errFlag"] intValue] == 1) {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
                                       } else {
                                           NSMutableDictionary *dummyDict = [dict mutableCopy];
                                           [dummyDict setObject:flStrForStr(response[@"data"]) forKey:@"addressID"];
                                           [Database addFavouriteSourceAddress:dummyDict];
                                           if (self.isComingForDropAddress) {
                                               self.navigationTitle = NSLocalizedString(@"Dropoff Location", @"Dropoff Location");
                                               self.lastBtnName = NSLocalizedString(@"Confirm Location", @"Confirm Location");
                                               self.navigationItem.title = self.navigationTitle;
                                               [Helper setButton:self.saveLocationBtn Text:self.lastBtnName WithFont:fontBold FSize:15 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
                                               self.dropoffAddressView.hidden = NO;
                                               self.saveLocationView.hidden = YES;
                                               self.addressChangable = YES;
                                               self.favouriteLocBtn.selected = YES;
                                               [mapView_ removeFromSuperview];
                                               favBtnClicked = YES;
                                               self.locationtypeTextField.text = @"";
                                               [self addMapOnView];
                                               return;
                                           }
                                           [self dismissViewControllerAnimated:YES completion:^{
                                               if (onCompletion) {
                                                   onCompletion(1, [NSDictionary new]);
                                               }
                                           }];
                                       }
                                   }
                               }];
}

@end
