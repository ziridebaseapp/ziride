//
//  PopUpWebView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpWebView : UIView <UIWebViewDelegate> {
    BOOL isRequestSubmitted;
}
@property(nonatomic,strong) NSString *weburl;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property(strong,nonatomic) UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) NSDictionary *invoiceData;
@property (strong, nonatomic) NSString *rating;
- (IBAction)backBtnAction:(id)sender;

-(void)showPopUpWithURL:(NSString *)webURL Onwindow:(UIWindow *)window;
@property (nonatomic, copy) void(^onCompletion)(NSInteger closeClicked);

@end
