//
//  PopUpWebView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PopUpWebView.h"

@implementation PopUpWebView
@synthesize weburl;
@synthesize webView;
@synthesize invoiceData, rating;

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"PopUpWebView" owner:self options:nil] firstObject];
    return self;
}


-(void)showPopUpWithURL:(NSString *)webURL Onwindow:(UIWindow *)window {
    webView.delegate= self;
    NSURL *url = [NSURL URLWithString:webURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished)
     {
     }];
}

- (IBAction)backBtnAction:(id)sender {
    _onCompletion(0);
    [self hidePOPup];
}


- (void)bookingComplete {
    _onCompletion(1);
    [self hidePOPup];
}

-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

#pragma mark - WebView Delegates

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *requestUrl = [request.URL absoluteString];
    if ([requestUrl rangeOfString:@"submit_help"].location != NSNotFound) {
        isRequestSubmitted = YES;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if(isRequestSubmitted){
        isRequestSubmitted = NO;
        [self sendRequestForReviewSubmit];
    }
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}


-(void)sendRequestForReviewSubmit{
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mid": [Utility driverID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             @"ent_rating_num":rating,
                             @"ent_review_msg":@"",
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseSubmitReviewResponse:response];
                                   else
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil){
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [self hidePOPup];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            _onCompletion(1);
            [self hidePOPup];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error")  Message:[response objectForKey:@"errMsg"]];
        }
    }
}

@end
