//
//  PostiveRatingView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostiveRatingView : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *viewTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *whatWentWellLabel;
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;
@property (weak, nonatomic) IBOutlet UIButton *pickupBtn;
@property (weak, nonatomic) IBOutlet UIButton *drivingBtn;
@property (weak, nonatomic) IBOutlet UIButton *comfortBtn;
@property (weak, nonatomic) IBOutlet UIButton *carQualityBtn;
@property (weak, nonatomic) IBOutlet UIButton *otherBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

- (IBAction)serviceBtnAction:(id)sender;
- (IBAction)pickupBtnAction:(id)sender;
- (IBAction)drivingBtnAction:(id)sender;
- (IBAction)comfortBtnAction:(id)sender;
- (IBAction)carQualityBtnAction:(id)sender;
- (IBAction)otherBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;
- (IBAction)tapGestureRecognizerAction:(id)sender;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger closeClicked);
@property (strong, nonatomic) NSDictionary *invoiceData;
@property (nonatomic) NSInteger ratingValue;
@property (nonatomic,strong) UIWindow *window;



-(void)showPopUpWithDetailedDict:(NSDictionary *)dict;





@end
