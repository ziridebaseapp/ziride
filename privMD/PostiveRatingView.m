//
//  PostiveRatingView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PostiveRatingView.h"

@implementation PostiveRatingView
@synthesize viewTitleLabel, lineView;
@synthesize driverImageView, driverName, ratingView;
@synthesize whatWentWellLabel;
@synthesize serviceBtn, pickupBtn, drivingBtn, comfortBtn, carQualityBtn, otherBtn;
@synthesize submitBtn;
@synthesize onCompletion, invoiceData, ratingValue, window;


- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"PositiveRatingView" owner:self options:nil] firstObject];
    return self;
}


-(void)showPopUpWithDetailedDict:(NSDictionary *)dict{
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    self.submitBtn.layer.cornerRadius = 5.0f;
    self.submitBtn.layer.masksToBounds = YES;
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    driverName.text = flStrForObj(invoiceData[@"fname"]).capitalizedString;
    NSString *strImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (strImageURL.length) {
        strImageURL = [NSString stringWithFormat:@"%@",invoiceData[@"pPic"]];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      if (error || !image)
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      else
                                          driverImageView.image = image;
                                  }];
    } else
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    
    [self setRatingViewInInvoice];
    invoiceData = [dict mutableCopy];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(void) setRatingViewInInvoice {
    ratingView.value = ratingValue;
    ratingView.stepInterval = 1.0;
    ratingView.markFont = [UIFont systemFontOfSize:30];
    ratingView.baseColor = UIColorFromRGB(0xDEDEDE);
    ratingView.highlightColor = UIColorFromRGB(0x019F6E);
}


- (IBAction)tapGestureRecognizerAction:(id)sender {
    CGPoint location = [sender locationInView:self];
    id tappedView = [self hitTest:location withEvent:nil];
    if ([tappedView isEqual:self]) {
        onCompletion(2);
        [self hidePOPup];
    }
}

- (IBAction)serviceBtnAction:(id)sender {
    serviceBtn.selected = !serviceBtn.isSelected;
}

- (IBAction)pickupBtnAction:(id)sender {
    pickupBtn.selected = !pickupBtn.isSelected;
}

- (IBAction)drivingBtnAction:(id)sender {
    drivingBtn.selected = !drivingBtn.isSelected;
}

- (IBAction)comfortBtnAction:(id)sender {
    comfortBtn.selected = !comfortBtn.isSelected;
}

- (IBAction)carQualityBtnAction:(id)sender {
    carQualityBtn.selected = !carQualityBtn.isSelected;
}

- (IBAction)otherBtnAction:(id)sender {
    otherBtn.selected = !otherBtn.isSelected;
}

- (IBAction)submitBtnAction:(id)sender {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (serviceBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Good Service", @"Good Service")];
    }
    if (pickupBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Came correctly on Pickup Location", @"Came correctly on Pickup Location")];
    }
    if (drivingBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Smooth and Nice Driving", @"Smooth and Nice Driving")];
    }
    if (comfortBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Nice comfortable car", @"Nice comfortable car")];
    }
    if (carQualityBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Good quality car", @"Good quality car")];
    }
    if (otherBtn.isSelected) {
        [array addObject:NSLocalizedString(@"Other reason liked", @"Other reason liked")];
    }
    NSString *reviews = flStrForStr([array componentsJoinedByString:@", "]);
    [self sendRequestForReviewSubmit:flStrForStr(reviews)];
}


-(void)sendRequestForReviewSubmit:(NSString *)review{
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    NSString *rateValue = [NSString stringWithFormat:@"%ld",(long)ratingValue];
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mid": [Utility driverID],
                             @"ent_booking_id":[Utility bookingID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             @"ent_rating_num":flStrForStr(rateValue),
                             @"ent_review_msg":review,
                             @"ent_promo":flStrForStr([Utility promoCode])
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self parseSubmitReviewResponse:response];
                                   else
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil){
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [self hidePOPup];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
            [Utility setPromoCode:flStrForStr(response[@"isPromoValid"])];
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(response[@"walletAmt"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            onCompletion(1);
            [self hidePOPup];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error")  Message:[response objectForKey:@"errMsg"]];
        }
    }
}

@end
