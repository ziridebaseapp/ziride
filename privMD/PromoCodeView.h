//
//  PromoCodeView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoCodeView : UIView<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headingView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIView *textViewReferenceView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *applyBtn;
@property (strong, nonatomic) NSString *textViewPlaceHolder;
@property (strong, nonatomic) NSString *headingText;
@property (strong, nonatomic) NSString *applyBtnTitleText;
@property (strong, nonatomic) NSString *cancelBtnTitleText;
@property (assign,nonatomic) double userLatitude;
@property (assign,nonatomic) double userLongitude;
@property (assign, nonatomic) NSInteger isPromo;
@property (assign, nonatomic) NSInteger isPromoCheckForFirstTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopConstant;

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger applyBtnClicked, NSString *text);

- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)applyBtnAction:(id)sender;


@end
