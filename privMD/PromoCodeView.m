//
//  PromoCodeView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PromoCodeView.h"
#import "MapViewController.h"
#import "Utility.h"

@implementation PromoCodeView
@synthesize textViewPlaceHolder;
@synthesize onCompletion;
@synthesize headingText;
@synthesize applyBtnTitleText, cancelBtnTitleText;
@synthesize isPromo, isPromoCheckForFirstTime;


-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"PromoCodeView" owner:self options:nil] firstObject];
    return self;
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    if ([Helper isCurrentLanguageRTL]) {
        self.textView.textAlignment = NSTextAlignmentRight;
    } else {
        self.textView.textAlignment = NSTextAlignmentLeft  ;
    }
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    self.applyBtn.layer.cornerRadius = 4.0f;
    self.applyBtn.layer.masksToBounds = YES;
    [Helper setButton:self.applyBtn Text:applyBtnTitleText WithFont:fontBold FSize:13 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    [Helper setToLabel:self.headingLabel Text:headingText WithFont:fontNormal FSize:15 Color:[UIColor blackColor]];
    self.textView.textColor = [UIColor lightGrayColor];
    self.textView.delegate = self;
    self.textView.text = textViewPlaceHolder;
    self.textView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    self.textView.userInteractionEnabled = YES;
    [self handleKeyboard];
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                     }
                     completion:^(BOOL finished){
                         if (isPromo && [[Utility promoCode] length]) {
                             isPromoCheckForFirstTime = 1;
                             self.textView.textColor = [UIColor blackColor];
                             textViewPlaceHolder = NSLocalizedString(@"Please enter your code here...", @"Please enter your code here...");
                             [self sendAServiceForPromoCode:[Utility promoCode]];
                         } else {
                             [self.textView becomeFirstResponder];
                         }
                     }];
}

-(void)hidePOPup {
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [self removeKeyboardNotofication];
                     }];
}

- (IBAction)applyBtnAction:(id)sender {
    [_textView resignFirstResponder];
    [self moveViewDown];
    NSString *message;
    NSString *oldPromoCode = [Utility promoCode];
    if (isPromo && oldPromoCode.length && [self.textView.text isEqualToString:oldPromoCode]) {
        onCompletion(1, self.textView.text);
        [self hidePOPup];
        return;
    } else if ([self.textView.text isEqualToString:textViewPlaceHolder]) {
        message = @"";
    } else {
        message = self.textView.text;
    }
    if(message.length) {
        if (isPromo) {
            [self sendAServiceForPromoCode:message];
        } else {
            onCompletion(1, message);
            [self hidePOPup];
        }
    }
}

- (IBAction)cancelBtnAction:(id)sender{
    if ([cancelBtnTitleText isEqualToString:NSLocalizedString(@"REMOVE", @"REMOVE")]) {
        [self sendAServiceForPromoCode:@""];
        return;
    }
    onCompletion(2, @"");
    [self removeKeyboardNotofication];
    [self hidePOPup];
}


- (void)removeKeyboardNotofication {
    [self.textView resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)sendAServiceForPromoCode:(NSString *)code {
    if (isPromoCheckForFirstTime) {
        [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:NSLocalizedString(@"", @"")];
    } else if ([cancelBtnTitleText isEqualToString:NSLocalizedString(@"REMOVE", @"REMOVE")]) {
        [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:NSLocalizedString(@"Removing...", @"Removing...")];
    } else {
        [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:NSLocalizedString(@"Verifying...", @"Verifying...")];
    }
    code = [NSString stringWithFormat:@"%@",[[code stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString]];
    
    NSDictionary *params = @{
                             @"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_coupon":flStrForStr(code),
                             @"ent_lat":[NSNumber numberWithDouble:_userLatitude],
                             @"ent_long":[NSNumber numberWithDouble:_userLongitude],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkCoupon"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       if (response == nil)
                                           return;
                                       else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
                                       } else {
                                           if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
                                               if ([cancelBtnTitleText isEqualToString:NSLocalizedString(@"REMOVE", @"REMOVE")]) {
                                                   cancelBtnTitleText = NSLocalizedString(@"CANCEL", @"CANCEL");
                                                   self.textView.userInteractionEnabled = YES;
                                                   [Helper setButton:self.cancelBtn Text:cancelBtnTitleText WithFont:fontBold FSize:13 TitleColor:[UIColor lightGrayColor] ShadowColor:nil];
                                                   self.textView.text = textViewPlaceHolder;
                                                   self.textView.textColor = [UIColor lightGrayColor];
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPromoCode];
                                                   [[MapViewController getSharedInstance] sendAServiceForFareCalculator];
                                                   return;
                                               }
                                               if (isPromoCheckForFirstTime == 1) {
                                                   isPromoCheckForFirstTime = 0;
                                                   self.textView.userInteractionEnabled = NO;
                                                   self.textView.text = code;
                                                   cancelBtnTitleText = NSLocalizedString(@"REMOVE", @"REMOVE");
                                                   [Helper setButton:self.cancelBtn Text:cancelBtnTitleText WithFont:fontBold FSize:13 TitleColor:[UIColor lightGrayColor] ShadowColor:nil];
                                               } else {
                                                   [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                                   if (code.length) {
                                                       [Utility setPromoCode:code];
                                                   }
                                                   onCompletion(1, code);
                                                   [self hidePOPup];
                                               }
                                           } else {
                                               if ([cancelBtnTitleText isEqualToString:NSLocalizedString(@"REMOVE", @"REMOVE")]) {
                                                   cancelBtnTitleText = NSLocalizedString(@"CANCEL", @"CANCEL");
                                                   self.textView.userInteractionEnabled = YES;
                                                   [Helper setButton:self.cancelBtn Text:cancelBtnTitleText WithFont:fontBold FSize:13 TitleColor:[UIColor lightGrayColor] ShadowColor:nil];
                                                   self.textView.text = textViewPlaceHolder;
                                                   self.textView.textColor = [UIColor lightGrayColor];
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPromoCode];
                                                   return;
                                               }
                                               if (isPromoCheckForFirstTime == 1) {
                                                   isPromoCheckForFirstTime = 0;
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPromoCode];
                                                   self.textView.text = textViewPlaceHolder;
                                                   self.textView.textColor = [UIColor lightGrayColor];
                                                   [self.textView becomeFirstResponder];
                                               } else {
                                                   self.textView.textColor = [UIColor lightGrayColor];
                                                   self.textView.text = textViewPlaceHolder;
                                                   [self.textView resignFirstResponder];
                                                   [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                               }
                                           }
                                       }
                                   }
                               }];
}

#pragma mark - TextView Delegate methods -

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if (textView.textColor == [UIColor lightGrayColor]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    if(textView.text.length == 0) {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = textViewPlaceHolder;
        [textView resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView hasText])
        textView.text = textView.text;
    else {
        [textView setTextColor:[UIColor lightGrayColor]];
        textView.text = textViewPlaceHolder;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0) {
            textView.text = @"";
        }
        [textView resignFirstResponder];
        if(![textView.text isEqualToString:textViewPlaceHolder] && textView.text.length && isPromo) {
            [self sendAServiceForPromoCode:textView.text];
        }
        return NO;
    }
    return YES;
}

/**
 *  Handle Keyboard Show & Hide
 */
- (void)handleKeyboard {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

/**
 *  Keyboard Shown
 */
- (void)keyboardWillShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float height = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUpWithKeyboardHeight:height];
}

/**
 *  Keyboard will be hidden
 */
- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self moveViewDown];
}

- (void)moveViewUpWithKeyboardHeight:(float)height {
    float heightOfTextfield = CGRectGetMaxY(self.textView.frame) + CGRectGetMinY([self.textView superview].frame) + self.contentView.frame.origin.y + height + 80;
    float windowHeight = self.frame.size.height;
    if ((heightOfTextfield - windowHeight) > 0) {
        [UIView animateWithDuration:0.4
                         animations:^{
                             CGRect frame = self.contentView.frame;
                             frame.origin.y -= (heightOfTextfield - windowHeight);
                             self.contentView.frame = frame;
                         }];
    }
}

- (void)moveViewDown {
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.contentView.center = [self.contentView superview].center;
                     }];
}

@end
