//
//  ProviderBookingView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PulsingHaloLayer.h"
#import "CustomSliderView.h"

@interface ProviderRequestingView : UIView <CustomSliderViewDelegate> {
    NSInteger count;
    PulsingHaloLayer *layer;
}

+ (id)sharedInstance;

@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapImageWidthConstant;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property (weak, nonatomic) IBOutlet UIView *sliderView;

@property (weak, nonatomic) IBOutlet UIView *progressBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *widthUpdatingView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressWidthConstant;
@property (weak, nonatomic) IBOutlet UILabel *requestingCarLabel;

@property (weak, nonatomic) NSTimer *timerObj;
@property (assign, nonatomic) float totalDuration;
@property (assign, nonatomic) float timeRemaining;
@property (assign, nonatomic) float changingInterval;


-(void)setPulsingAnimation;
-(void)updateUI;
-(void)stopTimer;

@end
