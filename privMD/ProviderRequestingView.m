//
//  ProviderBookingView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ProviderRequestingView.h"
#import "MapViewController.h"


#define kMaxRadius 320
#define kMaxDuration 15

static ProviderRequestingView *requestingView = nil;


@implementation ProviderRequestingView
@synthesize bottomView, sliderView, customSliderView;
@synthesize progressBackgroundView, progressWidthConstant, widthUpdatingView, requestingCarLabel;


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self =  [[[NSBundle mainBundle] loadNibNamed:@"ProviderRequestingView" owner:self options:nil]objectAtIndex:0];
        self.mapImageWidthConstant.constant = [UIScreen mainScreen].bounds.size.width - 60;
        [self layoutIfNeeded];
    }
    return self;
}


+ (id)sharedInstance {
    if (!requestingView) {
        requestingView  = [[self alloc] initWithFrame:CGRectZero];
        requestingView.frame = [[UIScreen mainScreen]bounds];
    }
    [requestingView setPulsingAnimation];
    return requestingView;
}

/**
 Update UI
 */
-(void) updateUI {
    sliderView.layer.cornerRadius = sliderView.frame.size.height/2;
    sliderView.layer.masksToBounds = YES;
    sliderView.layer.borderColor = [UIColor whiteColor].CGColor;
    sliderView.layer.borderWidth = 2;
    customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] firstObject];
    customSliderView.delegate = self;
    CGRect frame = customSliderView.frame;
    frame.size.width = sliderView.frame.size.width;
    frame.size.height = sliderView.frame.size.height;
    customSliderView.frame = frame;
    customSliderView.sliderImage.image = [UIImage imageNamed:@"cancelRequest_off"];
    customSliderView.sliderTitle.text = NSLocalizedString(@"Slide to cancel", @"Slide to cancel");
    customSliderView.sliderTitle.textColor = UIColorFromRGB(0x707070);
    [sliderView addSubview:customSliderView];
    [self staticMapImageWithLatitude];
    self.mapImageView.layer.cornerRadius = self.mapImageView.frame.size.width/2;
    self.mapImageView.layer.masksToBounds = YES;
    self.mapImageView.layer.borderColor = UIColorFromRGB(0x019F6E).CGColor;
    self.mapImageView.layer.borderWidth = 5.0f;
    [self layoutIfNeeded];
    [self startProgress];
}


/**
 Set Pulsing Animation
 */
-(void)setPulsingAnimation {
    layer = [PulsingHaloLayer layer];
    layer.position = CGPointMake(self.center.x, self.center.y+6);
    [self.topView.layer insertSublayer:layer below:self.mapImageView.layer];
    layer.haloLayerNumber = 5;
    layer.radius = 0.7 * self.mapImageView.frame.size.width;
    layer.animationDuration = 0.5 * kMaxDuration;
    [layer setBackgroundColor:UIColorFromRGB(0x019F6E).CGColor];
    [layer start];
}


-(void)staticMapImageWithLatitude {
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:blue|%f,%f&%@&sensor=true&scale=2", [Utility pickUpLatitude],  [Utility pickUpLongitude], [NSString stringWithFormat:@"zoom=14&size=%dx%d",3*(int)self.mapImageView.frame.size.width, 3*(int)self.mapImageView.frame.size.height]];//=color:blue
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self.mapImageView sd_setImageWithURL:mapUrl
                         placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (!error) {
                                    }
                                    else {
                                    }
                                }];
}

/**
 Slider Action
 */
-(void)sliderAction {
    [layer setBackgroundColor:UIColorFromRGB(0xC0392B).CGColor];
    [self stopTimer];
    [[MapViewController getSharedInstance] sendAppointmentRequestForLiveBookingCancellation];
}


/**
 Stop Timer
 */
-(void)stopTimer {
    if(self.timerObj) {
        [self.timerObj invalidate];
        self.timerObj = nil;
    }
}

/**
 Start progerss bar
 */
- (void)startProgress {
    self.timeRemaining = self.totalDuration;
    self.changingInterval = 0.001;
    progressWidthConstant.constant = 0;
    [self.widthUpdatingView layoutIfNeeded];
    if(self.timerObj) {
        [self.timerObj invalidate];
        self.timerObj = nil;
    }
    self.timerObj = [NSTimer scheduledTimerWithTimeInterval:self.changingInterval
                                                     target:self
                                                   selector:@selector(updateProgressBarWidth)
                                                   userInfo:nil
                                                    repeats:YES];
}

/**
 Update progress bar width
 */
-(void)updateProgressBarWidth {
    if (self.timeRemaining <= 0.000) {
        [self.timerObj invalidate];
        requestingView = nil;
    } else {
        float widthOf = CGRectGetWidth(progressBackgroundView.frame);
        [UIView animateWithDuration:self.changingInterval
                         animations:^{
                             progressWidthConstant.constant += widthOf * self.changingInterval/self.totalDuration;
                             [self.widthUpdatingView layoutIfNeeded];
                        }];
        self.timeRemaining -= self.changingInterval;
    }
}
@end
