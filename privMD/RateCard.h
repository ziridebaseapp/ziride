//
//  RateCard.h
//  iServe_Customer
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateCard : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *labelVehicleModel;
@property (weak, nonatomic) IBOutlet UILabel *labelBaseFare;
@property (weak, nonatomic) IBOutlet UILabel *labelDistanceRate;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeRate;
@property (weak, nonatomic) IBOutlet UILabel *label1st;
@property (weak, nonatomic) IBOutlet UILabel *label2nd;
@property (weak, nonatomic) IBOutlet UILabel *label3rd;

@property NSInteger bookingType;

-(void)showPopUpWithDictionary:(NSDictionary *)vehicleData Onwindow:(UIWindow *) window;
-(void)hidePopUp;
@end
