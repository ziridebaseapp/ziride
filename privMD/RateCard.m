//
//  RateCard.m
//  iServe_Customer
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RateCard.h"
#import "Helper.h"

@implementation RateCard


- (id)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"RateCard"  owner:self options:nil] firstObject];
    return self;
}


/**
 Show Rate Card Pop Up View on Application Window
 
 @param vehicleData Selected Car Type Data
 @param window Application Window
 */
-(void)showPopUpWithDictionary:(NSDictionary *)vehicleData Onwindow:(UIWindow *) window {
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                     }
                     completion:^(BOOL finished){
                         self.label1st.text = NSLocalizedString(@"Base Fare", @"Base Fare");
                         self.label2nd.text = NSLocalizedString(@"Ride Rate", @"Ride Rate");
                         self.label3rd.text = NSLocalizedString(@"Ride Time Rate", @"Ride Time Rate");
                         
                         self.labelBaseFare.text = [Helper getCurrencyLocal:[vehicleData[@"basefare"] floatValue]];
                         self.labelTimeRate.text =  [NSString stringWithFormat:@"%@/Min",[Helper getCurrencyLocal:[vehicleData[@"price_per_min"] floatValue]]];
                         self.labelDistanceRate.text = [NSString stringWithFormat:@"%@/Km",[Helper getCurrencyLocal:[vehicleData[@"price_per_km"] floatValue]]] ;
                         self.contentView.layer.cornerRadius = 3.0;
                         self.contentView.layer.masksToBounds = YES;
                         [self layoutIfNeeded];
                     }];

}

/**
 Hide Rate Card Pop up View
 */
-(void)hidePopUp {
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

/**
 Got It Button Action

 @param sender Got It Button
 */
- (IBAction)gotItBtnAction:(id)sender {
    [self hidePopUp];
}

/**
 Tap Gesture recognizer Action
 
 @param sender Tap Gesture
 */
- (IBAction)gestureRecognizer:(id)sender {
    [self hidePopUp];
}

@end
