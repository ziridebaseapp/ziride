//
//  ReceiptCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReceiptCell.h"

@implementation ReceiptCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([Helper isCurrentLanguageRTL]) {
        self.itemLabel.textAlignment = NSTextAlignmentRight;
        self.priceLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.itemLabel.textAlignment = NSTextAlignmentLeft;
        self.priceLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
