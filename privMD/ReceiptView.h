//
//  ReceiptView.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiptCell.h"


@interface ReceiptView : UIView<UITableViewDelegate, UITableViewDataSource> {
    NSDictionary *invoiceData;
    ReceiptCell *cell;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *receiptLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstant;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

- (IBAction)closeBtnAction:(id)sender;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
@property (nonatomic, copy)   void (^onCompletion)(NSInteger closeClicked);

@property (nonatomic) NSInteger numberOfRows;
@property (strong, nonatomic) NSMutableArray *titleArray;
@property (strong, nonatomic) NSMutableArray *priceArray;
@property (strong, nonatomic) NSMutableArray *lineArray;


@end
