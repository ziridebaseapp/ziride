//
//  ReceiptView.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReceiptView.h"
#import "ReceiptCell.h"

@implementation ReceiptView
@synthesize onCompletion;
@synthesize numberOfRows, titleArray, priceArray, lineArray;
@synthesize tableView, tableViewHeightConstant;

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptView" owner:self options:nil] firstObject];
    UINib *cellNib = [UINib nibWithNibName:@"ReceiptCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell"];
    return self;
}

-(NSInteger) calculateNumberOfRows {
    numberOfRows = 0;
    NSString *distance = NSLocalizedString(@"Distance Fare", @"Distance Fare");
    distance = [NSString stringWithFormat:@"%@ (%@ %@)", distance, flStrForStr(invoiceData[@"dis"]), kDistanceParameter];
    NSString *waitingTime = NSLocalizedString(@"Waiting time", @"Waiting time");
    if ([flStrForStr(invoiceData[@"waitingTime"]) intValue] != 0){
        NSString *timeForDisplay = [Helper timeFormatted:[flStrForStr(invoiceData[@"waitingTime"]) intValue]];
        waitingTime = [NSString stringWithFormat:@"%@ (%@)", waitingTime, timeForDisplay];
    }
    NSString *duration = NSLocalizedString(@"Duration", @"Duration");
    if ([flStrForStr(invoiceData[@"dur"]) intValue] != 0){
        NSString *timeForDisplay = [Helper timeFormatted:[flStrForStr(invoiceData[@"dur"]) intValue]];
        duration = [NSString stringWithFormat:@"%@ (%@)", duration, timeForDisplay];
    }

    titleArray = [[NSMutableArray alloc] initWithObjects: NSLocalizedString(@"Base Fare", @"Base Fare"), distance, waitingTime, duration, nil];
    priceArray = [[NSMutableArray alloc] initWithObjects:invoiceData[@"baseFee"], invoiceData[@"distanceFee"], invoiceData[@"waitingFee"], invoiceData[@"timeFee"], nil];
    if([invoiceData[@"airportFee"] floatValue] > 0){
        if([invoiceData[@"extraChargeTitle"] length]) {
            [titleArray addObject:flStrForStr(invoiceData[@"extraChargeTitle"])];
        } else {
            [titleArray addObject:NSLocalizedString(@"Airport Fare", @"Airport Fare")];
        }
        [priceArray addObject:invoiceData[@"airportFee"]];
        lineArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"1", nil];
        numberOfRows++;
    } else {
        lineArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"1", nil];
    }
                                              
    [titleArray addObject:NSLocalizedString(@"Trip fare", @"Trip fare")];
    [priceArray addObject:invoiceData[@"subTotal"]];
    [lineArray addObject:@"1"];
    
    if([invoiceData[@"discountVal"] floatValue] > 0){
        NSString *discountLabel = NSLocalizedString(@"Promo Code", @"Promo Code");
        if([flStrForStr(invoiceData[@"code"]) length]) {
            if ([invoiceData[@"discountType"] integerValue] == 2) {
                discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ %% )",discountLabel, flStrForStr(invoiceData[@"code"]), flStrForStr(invoiceData[@"discountPercentage"])];
            } else if ([invoiceData[@"discountType"] integerValue] == 1) {
                NSString *discountWithCurrency = [Helper getCurrencyLocal:[flStrForStr(invoiceData[@"discountVal"]) floatValue]];
                discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ )", discountLabel, flStrForStr(invoiceData[@"code"]), discountWithCurrency];
            }
        }
        [titleArray addObject:discountLabel];
        [priceArray addObject:invoiceData[@"discountVal"]];
        numberOfRows++;
    }
    
    if ([invoiceData[@"sumOfLastDue"] floatValue] > 0) {
        [titleArray addObject:NSLocalizedString(@"Dues from previous trips", @"Dues from previous trips")];
        [priceArray addObject:invoiceData[@"sumOfLastDue"]];
        [lineArray addObject:@"0"];
        [lineArray addObject:@"1"];
        numberOfRows++;
    } else if([invoiceData[@"discountVal"] integerValue] > 0) {
        [lineArray addObject:@"1"];
    }
    
    if ([invoiceData[@"duePayment"] floatValue] > 0) {
        [titleArray addObject:NSLocalizedString(@"Dues payment", @"Dues payment")];
        [priceArray addObject:invoiceData[@"duePayment"]];
        [lineArray addObject:@"1"];
        numberOfRows++;
    }

    [titleArray addObject:NSLocalizedString(@"Payment Method:", @"Payment Method:")];
    [priceArray addObject:@" "];
    [lineArray addObject:@"0"];
    
    if([flStrForStr(invoiceData[@"payType"]) integerValue] == 1 && [invoiceData[@"cardDeduct"] floatValue] > 0) {
        NSString *card = flStrForStr(invoiceData[@"cardlastDigit"]);
        if (card.length) {
            NSString *last4 = [card substringFromIndex: [card length] - 4];
            [titleArray addObject:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Paid by ****", @"Paid by ****"), last4]];
        } else {
            [titleArray addObject:NSLocalizedString(@"Paid by card", @"Paid by card")];
        }
        [priceArray addObject:flStrForStr(invoiceData[@"cardDeduct"])];
        numberOfRows++;
    } else if([flStrForStr(invoiceData[@"payType"]) integerValue] == 2 && [invoiceData[@"CashCollected"] floatValue] > 0) {
        [titleArray addObject:NSLocalizedString(@"Cash collected", @"Cash collected")];
        [priceArray addObject:flStrForStr(invoiceData[@"CashCollected"])];
        numberOfRows++;
    }
    
    if([invoiceData[@"walletDeducted"] floatValue] != 0) {
        [titleArray addObject:NSLocalizedString(@"Paid from Wallet", @"Paid from Wallet")];
        [priceArray addObject:invoiceData[@"walletDeducted"]];
        if([invoiceData[@"CashCollected"] floatValue] > 0 || [invoiceData[@"cardDeduct"] floatValue] > 0){
            [lineArray addObject:@"0"];
        }
        [lineArray addObject:@"1"];
        numberOfRows++;
    } else {
        [lineArray addObject:@"1"];
    }

    if([invoiceData[@"walletDebitCredit"] integerValue] != 0){
        [titleArray addObject:NSLocalizedString(@"Wallet transaction", @"Wallet transaction")];
        [priceArray addObject:invoiceData[@"walletDebitCredit"]];
        [lineArray addObject:@"1"];
        numberOfRows++;
    }
    numberOfRows +=6;
    return numberOfRows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (invoiceData == nil) {
        numberOfRows = 0;
        titleArray = [[NSMutableArray alloc] init];
        priceArray = [[NSMutableArray alloc] init];
        lineArray = [[NSMutableArray alloc] init];
        return numberOfRows;
    } else {
        return numberOfRows;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
    cell = (ReceiptCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ReceiptCell"];
    if(!cell) {
        cell = [cellArray firstObject];
    }
    NSString *price = [Helper getCurrencyLocal:[flStrForStr(priceArray[indexPath.row]) floatValue]];
    [Helper setToLabel:cell.itemLabel Text:titleArray[indexPath.row] WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x000000)];
    if([titleArray[indexPath.row] isEqualToString:NSLocalizedString(@"Payment Method:", @"Payment Method:")]) {
        price = @" ";
        [Helper setToLabel:cell.itemLabel Text:titleArray[indexPath.row] WithFont:fontBold FSize:13 Color:UIColorFromRGB(0x000000)];
    }
    [Helper setToLabel:cell.priceLabel Text:price WithFont:fontNormal FSize:13 Color:UIColorFromRGB(0x000000)];
    if ([lineArray[indexPath.row] integerValue] == 1) {
        cell.lineLabel.hidden = NO;
    } else {
        cell.lineLabel.hidden = YES;
    }
    return cell;
}


- (IBAction)closeBtnAction:(id)sender {
    onCompletion(1);
    [self hidePOPup];
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window {
    invoiceData = [dict mutableCopy];
    self.frame = window.frame;
    [window addSubview:self];
    self.contentView.alpha = 0.3;
    NSString *headingLabel = NSLocalizedString(@"RECEIPT", @"RECEIPT");
    NSString *bookingID = NSLocalizedString(@"BOOKING ID : ", @"BOOKING ID : ");
    bookingID = [NSString stringWithFormat:@"%@%@", bookingID, flStrForStr(invoiceData[@"bid"])];
    NSMutableAttributedString *headingAttributedLabel = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", headingLabel, bookingID]];
    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0x363D4E)
                                   range:NSMakeRange(0,[headingLabel length])];
    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0xa1A6BB)
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];
    [headingAttributedLabel addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:fontNormal size:15.0]
                                   range:NSMakeRange(0, [headingLabel length])];
    [headingAttributedLabel addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:fontNormal size:12.0]
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];
    [self.receiptLabel setAttributedText:headingAttributedLabel];
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    self.closeBtn.layer.cornerRadius = 4.0f;
    self.closeBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.closeBtn.layer.borderWidth = 1.0f;

    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                         [self calculateNumberOfRows];
                         [self.tableView reloadData];
                         CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
                         if(self.tableView.contentSize.height > screenHeight - 140)
                             tableViewHeightConstant.constant = screenHeight - 140;
                         else
                             tableViewHeightConstant.constant = self.tableView.contentSize.height;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

@end
