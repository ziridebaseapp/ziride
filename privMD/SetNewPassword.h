//
//  SetNewPassword.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetNewPassword : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *enterNewPasswordTextField;

@property(strong, nonatomic) NSString *phoneNumber;

@end
