//
//  SetNewPassword.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SetNewPassword.h"
@interface SetNewPassword () <CustomNavigationBarDelegate>

@end

@implementation SetNewPassword

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [_enterNewPasswordTextField becomeFirstResponder];
}


#pragma mark - Custom Navigation Bar -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Set New Password", @"Set New Password")];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [self.view addSubview:customNavigationBarView];
}

/**
 Navigation Left Button

 @param sender leftBarBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender {
    [Helper popLikeDismissVC:self.navigationController.view];
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark - UIButton Action -

/**
 Change Password Button Action

 @param sender changePassBtn
 */
- (IBAction)changePasswordButtonAction:(id)sender {
    [self.view endEditing:YES];
    if(![_enterNewPasswordTextField.text length]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Enter New Password", @"Please Enter New Password")];
        [_enterNewPasswordTextField becomeFirstResponder];
        return;
    } else if (![_confirmPasswordTextField.text length]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Confirm a Password", @"Please Confirm a Password")];
        [_confirmPasswordTextField becomeFirstResponder];
        return;
    } else if (![_enterNewPasswordTextField.text isEqualToString:_confirmPasswordTextField.text]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Both The Password Didnot Match. Please try Again!", @"Both The Password Didnot Match. Please try Again!")];
        _enterNewPasswordTextField.text = @"";
        _confirmPasswordTextField.text  = @"";
        [_enterNewPasswordTextField becomeFirstResponder];
        return;
    } else {
        [self updatePasswordForUser];
    }
}

#pragma mark - GestureRecognizer Delegate -

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - TextField Delegate -

-(BOOL)textFieldShouldReturn:(UITextField*)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [[[textField superview] superview]  viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self changePasswordButtonAction:nil];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - Webservice -

/**
 Update Password API call
 */
-(void)updatePasswordForUser {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Updating...", @"Updating...")];
    NSDictionary *params = @{
                             @"ent_mobile":flStrForStr(self.phoneNumber),
                             @"ent_pass":flStrForStr(self.confirmPasswordTextField.text),
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_user_type": @"2",
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updatePasswordForUser"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getResponseForVerifyOTP:response];
                               }];
}

/**
 Update Password API response

 @param response response Data
 */
-(void)getResponseForVerifyOTP:(NSDictionary*)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else {
        if ([response[@"errFlag"] integerValue] == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your new password has been updated. Please try Logging In!", @"Your new password has been updated. Please try Logging In!")];
            [Helper popLikeDismissVC:self.navigationController.view];
            [self.navigationController popToRootViewControllerAnimated:NO];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}

@end
