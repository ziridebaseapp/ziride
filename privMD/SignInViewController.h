//
//  SignInViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginButton.h>
#import "FBLoginHandler.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface SignInViewController : UIViewController<FBLoginHandlerDelegate, GIDSignInDelegate, GIDSignInUIDelegate, UITextFieldDelegate,UITextViewDelegate> {
    NSDictionary *itemList;
    BOOL checkLoginCredentials;
}
@property (weak, nonatomic) IBOutlet UIImageView *countryFlagImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
    @property (weak, nonatomic) IBOutlet UIButton *countryCodeButton;

@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *googleBtn;


- (IBAction)countryCode:(id)sender;
- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)facebookLoginBtnAction:(id)sender;
- (IBAction)googlePlusLoginBtnAction:(id)sender;
- (IBAction)forgotPasswordButtonClicked:(id)sender;

@end
