//
//  SignInViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "MenuViewController.h"
#import "Database.h"
#import "CountryPicker.h"
#import "CountryNameTableViewController.h"
#import "EnterPhoneNumberViewController.h"
#import "Database.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "SignUpViewController.h"
#import "AmazonTransfer.h"
#import <Stripe/Stripe.h>

@interface SignInViewController () {
    BOOL isForgotPassword;
    NSMutableDictionary *userData;
}
@end

@implementation SignInViewController

@synthesize emailTextField, passwordTextField;
@synthesize countryFlagImageView, countryCodeLabel;
@synthesize signinButton;
@synthesize facebookBtn, googleBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBarHidden = NO;
    [self setCountryFlagImageView];
    [self setPlaceHolderForTextFileds];
    [self updateUI];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    [self createNavView];
    [self createNavLeftButton];
}

-(void)viewDidDisappear:(BOOL)animatekd {
    if (userData) {
        self.navigationController.navigationBarHidden = NO;
        return;
    } else if(isForgotPassword) {
        
    } else {
        self.navigationController.navigationBarHidden = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITapGesture Recogniser -

/**
 This is to dismiss keybord from screeen using tap gesture on screen
 */
-(void)dismissKeyboard {
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}


#pragma mark - NavigationBar Custom Methods -

/**
 This creates navigation title
 */
-(void)createNavView {
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80, 0, 160, 44)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 160, 44)];
    navTitle.text = NSLocalizedString(@"LogIn", @"LogIn");
    navTitle.textColor = UIColorFromRGB(0xFFFFFF);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:fontNormal size:20];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}

/**
 This creates left navigation button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    }else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Left navigation button action method
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma  mark - Custom Methods - 

/**
 This sets country code and flag when View will appear.
 */
-(void) setCountryFlagImageView {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode1 = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode1];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode1];
    countryCodeLabel.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    countryFlagImageView.image = [UIImage imageNamed:imagePath];
}

/**
 This sets textfields place holder properties.
 */
-(void) setPlaceHolderForTextFileds {
    [emailTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
}

-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width;
    facebookBtn.frame = CGRectMake(10, 290, (screenWidth - 20)/2 - 10, 50);
    googleBtn.frame = CGRectMake((screenWidth - 20)/2 + 20, 290, (screenWidth - 20)/2 - 10, 50);
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.countryFlagImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryFlagImageView.frame = frame;
        
        frame = self.countryCodeLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryCodeLabel.frame = frame;
        
        frame = self.countryCodeButton.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryCodeButton.frame = frame;

        frame = self.emailTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.emailTextField.frame = frame;
        
        frame = self.passwordTextField.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.passwordTextField.frame = frame;
        
        frame = self.forgotPasswordButton.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.forgotPasswordButton.frame = frame;
        
        frame = self.facebookBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.facebookBtn.frame = frame;

        frame = self.googleBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.googleBtn.frame = frame;
    }
    
    [signinButton setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0x019F6E)] forState:UIControlStateNormal];
    [signinButton setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0xC7C7C7)] forState:UIControlStateHighlighted];
    [signinButton setBackgroundImage:[Helper imageWithColor:UIColorFromRGB(0xC7C7C7)] forState:UIControlStateSelected];
    signinButton.layer.cornerRadius = 5.0f;
    signinButton.layer.masksToBounds = YES;
    [Helper addBottomShadowOnButton: signinButton];

    facebookBtn.layer.cornerRadius = 5.0f;
    facebookBtn.layer.masksToBounds = YES;
    [Helper addBottomShadowOnButton: facebookBtn];

    googleBtn.layer.cornerRadius = 5.0f;
    googleBtn.layer.masksToBounds = YES;
    [Helper addBottomShadowOnButton: googleBtn];
}

#pragma mark - TextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == emailTextField) {
        if (textField.text.length > 0) {
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            if ([textField.text rangeOfString:@"\\@"].location == NSNotFound && [textField.text rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                BOOL isValid = [Helper mobileNumberValidate:textField.text];
                if (!isValid) {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your mobile number is invalid.", @"Your mobile number is invalid.")];
                    emailTextField.text = @"";
                    [emailTextField becomeFirstResponder];
                } else {
                    [passwordTextField becomeFirstResponder];
                }
            } else {
                BOOL isValid = [Helper emailValidationCheck:emailTextField.text];
                if (!isValid) {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your e-mail ID is invalid.", @"Your e-mail ID is invalid.")];
                    emailTextField.text = @"";
                    [emailTextField becomeFirstResponder];
                } else {
                    [passwordTextField becomeFirstResponder];
                }
            }
        }
    } else {
        [passwordTextField resignFirstResponder];
        [self checkAllValidationBeforeSignIn];
    }
    return YES;
}


#pragma mark - UIButton Action -

/**
 This presents the Country Picker Screen

 @param sender countryCode or countryFlag Button
 */

- (IBAction)countryCode:(id)sender {
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName) {
        countryFlagImageView.image = flagimg;
        countryCodeLabel.text = [NSString stringWithFormat:@"+%@", code];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 This is Sign In button

 @param sender signInBtn
 */

- (IBAction)signInButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    [self checkAllValidationBeforeSignIn];
}

/**
 This is Facebook button
 
 @param sender facebookBtn
 */
- (IBAction)facebookLoginBtnAction:(id)sender {
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler setDelegate:self];
    [handler loginWithFacebook:self];
}

/**
 This is Google Login button
 
 @param sender googleBtn
 */

- (IBAction)googlePlusLoginBtnAction:(id)sender
{
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
    [signIn signIn];
}

/**
 This is Forgot Password Login button
 
 @param sender forgotPassBtn
 */

- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    isForgotPassword = YES;
    self.navigationController.navigationBarHidden = YES;
    EnterPhoneNumberViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"EnterPhoneNumber"];
    [Helper pushLikePresentVC:self.navigationController.view];
    [self.navigationController pushViewController:viewcontroller animated:NO];
}


#pragma mark - Custom Methods -

/**
 This check all validation before Sign In
 */

-(void)checkAllValidationBeforeSignIn {
    NSString *email = emailTextField.text;
    NSString *password = passwordTextField.text;
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if((unsigned long)email.length == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your email-ID or mobile number", @"Please enter your email-ID or mobile number")];
    }
//    else if([email rangeOfCharacterFromSet:notDigits].location == NSNotFound && [Helper mobileNumberValidate:email] == 0) {
//        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid mobile number", @"Invalid mobile number")];
//        emailTextField.text = @"";
//    }
    else if((unsigned long)password.length == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter a Password", @"Please enter a Password")];
    } else {
        checkLoginCredentials = YES;
        NSString *email = @"";
        if ([emailTextField.text rangeOfString:@"\\@"].location == NSNotFound && [emailTextField.text rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
            email = [NSString stringWithFormat:@"%@", emailTextField.text];
//            email = [NSString stringWithFormat:@"%@%@", countryCodeLabel.text, emailTextField.text];
        } else {
            email = emailTextField.text;
        }
        [self sendServiceForLogin:email password:passwordTextField.text loginType:@"1"];
    }
}

#pragma mark - WebServices -

/**
 This is API for Sign In

 @param mobile    Mobile number entered by user
 @param password Password entered by user
 @param type     Login type if Normal SignIn - 1, Facebook SignIn - 2, Google SignIn - 3
 */
-(void)sendServiceForLogin:(NSString *)mobile password:(NSString *)password loginType:(NSString *)type {
    NSDictionary *params = @{
                             KDALoginEmail:mobile,
                             KDALoginPassword:password,
                             KDALoginDevideId:[Utility deviceID],
                             KDALoginPushToken:[Utility pushToken],
                             KDASignUpLatitude:[Utility currentLatitude],
                             KDASignUpLongitude:[Utility currentLongitude],
                             KDASignUpCity:[Utility currentCity],
                             KDALoginDeviceType:@"1",
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             KDALoginUpDateTime:[Helper getCurrentNetworkDateTime],
                             @"ent_login_type": type,
                             @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                             @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                             @"ent_dev_model":[Helper getModelName]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientLogin
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self loginResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Logging in..." , @"Logging in...")];
}

/**
 Sign In Service Response

 @param dictionary response data
 */
-(void)loginResponse:(NSDictionary *)dictionary {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    itemList = [dictionary mutableCopy];
    if(!dictionary)
        return;
    else {
        if ([[itemList objectForKey:@"errFlag"]integerValue] == 0 || ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115)) {
            [Utility setSessionToken:flStrForStr(itemList[@"token"])];
            
            [Utility setPubnubPublishKey:flStrForStr(itemList[@"pub"])];
            [Utility setPubnubSubscribeKey:flStrForStr(itemList[@"sub"])];
            
            [Utility setUserPubnubChannel:flStrForStr(itemList[@"chn"])];
            [Utility setServerPubnubChannel:flStrForStr(itemList[@"serverChn"])];
            
            [Utility setUserID:flStrForStr(itemList[@"sid"])];
            [Utility setUserName:flStrForStr(itemList[@"name"])];
            [Utility setUserEmailID:flStrForStr(itemList[@"email"])];
            [Utility setUserProfileImage:flStrForStr(itemList[@"profilePic"])];
            [Utility setUserReferralCode:flStrForStr(itemList[@"coupon"])];
            [Utility setUserShareMessage:flStrForStr(itemList[@"shareMessage"])];
            [Utility setPromoCode:flStrForStr(itemList[@"promoCode"])];
            [Utility setWalletBalance:[Helper getCurrencyLocal:[flStrForObj(itemList[@"walletAmt"]) floatValue]]];
            
            NSDictionary *configData = [dictionary[@"configData"] mutableCopy];
            [Utility setGoogleMatrixTimeInterval:[configData[@"DistanceMatrixInterval"] integerValue]];
            [Utility setPubnubPublishTimeInterval:[configData[@"PubnubIntervalHome"] integerValue]];
            [Utility setPubnubPublishOnBookingTimeInterval:[configData[@"PubnubIntervalDispatch"] integerValue]];
            [Utility setRadiusForOperatingArea:[configData[@"RadiusOperating"] integerValue]];
            [Utility setRadiusForNotOperatingArea:[configData[@"RadiusNotOperating"] integerValue]];
            [Utility setMaximumETA:[configData[@"ETAThreshold"] integerValue]*60];
            [Utility setBookingAmountAllowUpto:[configData[@"BookingAmountAllowUpto"] doubleValue]];
            
            NSMutableArray *googleKeysArray = [configData[@"GoogleKeysArray"] mutableCopy];
            NSString *distanceMatrixKey = [googleKeysArray firstObject];
            [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
            [googleKeysArray removeObjectAtIndex:0];
            [Utility setGoogleKeysArray:googleKeysArray];

            [Utility setPaymentGatewayKey:flStrForStr(itemList[@"stipeKey"])];
            if([Utility paymentGatewayKey]) {
                [Stripe setDefaultPublishableKey:[Utility paymentGatewayKey]];
            }

            NSMutableArray *cardDetails = [itemList[@"cards"] mutableCopy];
            if (cardDetails.count) {
                [self addCardsInDatabase:cardDetails];
            }
            NSMutableArray *favAddress = [itemList[@"addresses"] mutableCopy];
            if (favAddress.count) {
                [self addFavouriteAddressInDatabase:favAddress];
            }
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            if (!carTypes || !carTypes.count){
                [Utility setOpratingTypeArea:kNotOperatingArea];
            } else {
                [Utility setOpratingTypeArea:kOperatingArea];
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:kNSUCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            [Utility setIsNeedToPublishMessageOnPubnub:YES];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            if ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:itemList[@"errMsg"]];
            }
        } else if ([dictionary [@"errFlag"] integerValue] == 1) {
            if ([dictionary[@"errNum"] integerValue] == 8 && userData != nil) {
                SignUpViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUp"];
                if (userData != nil) {
                    VC.userData = userData;
                }
                [Helper checkForPush:self.navigationController.view];
                [self.navigationController pushViewController:VC animated:NO];
            } else {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictionary[@"errMsg"]];
            }
            checkLoginCredentials = NO;
        }
    }
}

#pragma mark - facebookDelegates

- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    [self.view endEditing:YES];
    if (userInfo == nil) {
        return;
    }
    userData = [userInfo mutableCopy];
    NSString *email = flStrForStr(userInfo[@"email"]);
    NSString *password = flStrForStr(userInfo[@"id"]);
    NSString *imageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", userData[@"id"]];
    [userData setObject:imageURL forKey:@"imageURL"];
    [userData setObject:@"2" forKey:@"loginType"];
    [self sendServiceForLogin:email password:password loginType:@"2"];
}

- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[error localizedDescription]];
}

-(void)didUserCancelLogin {
    
}

#pragma mark - Google+ Delegate

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//completed sign In
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (user == nil) {
        return;
    }
    // Perform any operations on signed in user here.
    NSString *url = [NSString stringWithFormat:@"%@", [user.profile imageURLWithDimension:70]];
    NSDictionary *dict = @{@"name":flStrForStr(user.profile.name),
                           @"last_name":flStrForStr(user.profile.familyName),
                           @"email":flStrForStr(user.profile.email),
                           @"id":flStrForStr(user.userID),
                           @"imageURL":flStrForStr(url),
                           @"loginType":@"3"
                           };
    userData = [dict mutableCopy];
    [self sendServiceForLogin:user.profile.email password:user.userID loginType:@"3"];
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
}


#pragma mark - Database Methods -

/**
 Add card in Local DB

 @param cards Card array got from Sign In Response
 */
-(void)addCardsInDatabase:(NSMutableArray *)cards {
    [Database DeleteAllCard];
    [cards enumerateObjectsUsingBlock:^(NSMutableDictionary *cardDict, NSUInteger index, BOOL *stop) {
        if (index == 0) {
            [cardDict setObject:@"1" forKey:@"isDefault"];
        } else {
            [cardDict setObject:@"0" forKey:@"isDefault"];
        }
        [Database makeDataBaseEntry:cardDict];
        if (index >= cards.count) {
            *stop = YES;
        }
    }];
}

/**
 Add favourite Addresses in Local DB
 
 @param addresses favourite Addresses array got from Sign In Response
 */
-(void)addFavouriteAddressInDatabase:(NSMutableArray *)addresses {
    [Database deleteAllSourceAddress];
    [addresses enumerateObjectsUsingBlock:^(NSMutableDictionary *addressDict, NSUInteger index, BOOL *stop) {
        [Database addFavouriteSourceAddress:addressDict];
        if (index >= addresses.count) {
            *stop = YES;
        }
    }];
}





@end
