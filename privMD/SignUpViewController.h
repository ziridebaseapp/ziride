//
//  SignUpViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPButton.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate, CLLocationManagerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate> {
    NSMutableArray *array;
    BOOL checkSignupCredentials;
    BOOL isTnCButtonSelected;
}

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@property (strong, nonatomic) IBOutlet UIButton *profileButton;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *phoneNoCountryFlag;
@property (weak, nonatomic) IBOutlet UILabel *phoneNoCountryCode;
@property (strong, nonatomic) IBOutlet UITextField *phoneNoTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *referalCodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *creatingLabel;
@property (strong, nonatomic) IBOutlet UIButton *tncCheckButton;
@property (weak, nonatomic) IBOutlet RPButton *nextBtn;

- (IBAction)countryCode:(id)sender;
- (IBAction)nextButtonClick:(id)sender;
- (IBAction)checkButtonClicked:(id)sender;
- (IBAction)profileButtonClicked:(id)sender;
- (IBAction)maleBtnAction:(id)sender;
- (IBAction)femaleBtnAction:(id)sender;

@property (strong, nonatomic) UIImage *pickedImage;
@property (weak, nonatomic) NSDictionary *userData;
@property (strong, nonatomic) UIButton *navNextButton;
@property (nonatomic, strong) NSMutableArray *helperCity;
@property (strong, nonatomic) NSArray *saveSignUpDetails;
@property (strong,nonatomic)  UITextField *activeTextField;
@property (nonatomic, strong) NSMutableArray *helperCountry;



@end
