//
//  SignUpViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
#import "VerifyiMobileViewController.h"
#import "CardLoginViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "MenuViewController.h"
#import "CountryPicker.h"
#import "CountryNameTableViewController.h"
#import "LocationServicesMessageVC.h"




@interface SignUpViewController () {
    float initialOffset_Y;
    float keyboardHeight;
    NSTextContainer *textContainer;
    NSLayoutManager *layoutManager;
    NSTextStorage *textStorage;
    BOOL emailVerified;
    BOOL referralVerified;
    BOOL isComingFromvalidateBeforeSignUp;
}

@property(nonatomic,strong) CLLocationManager *locationManager;
@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (nonatomic, retain) UIToolbar *keyboardToolbar;

typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;


@end

@implementation SignUpViewController
@synthesize mainScrollView;
@synthesize activeTextField, keyboardToolbar;
@synthesize view1, view2, view3;
@synthesize lineView, label1, label2;
@synthesize firstNameTextField, phoneNoTextField, emailTextField, passwordTextField, confirmPasswordTextField, referalCodeTextField;
@synthesize phoneNoCountryCode, phoneNoCountryFlag;
@synthesize nextBtn;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncCheckButton;
@synthesize creatingLabel;
@synthesize locationManager;
@synthesize userData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

# pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    keyboardHeight = 260;
    isTnCButtonSelected = NO;
    
    [self createNavLeftButton];
    [self setCountryFlagAndCode];
    [self setTermsNConditionsLink];
    [self setTextFiledsPlaceHolders];
    [self setRegisterStepsFrameAndProfileImageCente];
    [self updateUI];

    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBarHidden = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [mainScrollView addGestureRecognizer:tapGesture];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    textContainer.size = creatingLabel.frame.size;
}

-(void)viewWillAppear:(BOOL)animated {
    [self createNavView];
    if (userData != nil && !view2.isHidden) {
        firstNameTextField.text = flStrForStr(userData[@"name"]); //flStrForStr(userData[@"last_name"]);
        NSString *email = flStrForStr(userData[@"email"]);
        if (email.length) {
            emailTextField.text = userData[@"email"];
        } else {
            emailTextField.text = @"";
        }
        passwordTextField.text = flStrForStr(userData[@"id"]);
        NSString *imageURL = flStrForStr(userData[@"imageURL"]);
        if (imageURL) {
            [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                     placeholderImage:[UIImage imageNamed:@"camera"]
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl){
                                                if(!error && image){
                                                    _pickedImage = [Helper imageWithImage:image scaledToSize:CGSizeMake(100, 100)];
                                                    profileImageView.image = _pickedImage;
                                                } else {
                                                    profileImageView.image = [UIImage imageNamed:@"camera"];;
                                                    _pickedImage = nil;
                                                }
                                            }];

        }
        profileButton.userInteractionEnabled = NO;
        keyboardToolbar = nil;
        [phoneNoTextField becomeFirstResponder];
        view2.hidden = YES;
        CGRect frame = view3.frame;
        frame.origin.y -= view2.frame.size.height;
        view3.frame = frame;
    } else {
        profileButton.userInteractionEnabled = YES;
    }
    initialOffset_Y = mainScrollView.contentOffset.y;
    [self performSelector:@selector(setStyleCircleForImage:) withObject:profileImageView afterDelay:0.0];
}

-(void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    if([[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] && [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] == 0) {
        [self getCurrentLocationFromGPS];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void) viewDidDisappear:(BOOL)animated {
    self.navigationItem.titleView  = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Location Custom Methods -

-(void)getCurrentLocationFromGPS {
    if ([CLLocationManager locationServicesEnabled]) {
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {                 [self.locationManager requestWhenInUseAuthorization];
            }
        }
        [locationManager startUpdatingLocation];
    }
    else
        [self gotoLocationServicesMessageViewController];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:KNUCurrentLat];
    [[NSUserDefaults standardUserDefaults]setObject:log forKey:KNUCurrentLong];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if([[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] && [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] == 0)
        [self getCurrentLocationFromGPS];
    else
        [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
        [self gotoLocationServicesMessageViewController];
}

-(void)locationServicesChanged:(NSNotification*)notification {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        [self gotoLocationServicesMessageViewController];
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController {
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

#pragma mark - NavigationBar Custom Methods -

/**
 Create navigation view
 */
-(void)createNavView {
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80, 0, 160, 44)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 160, 44)];
    navTitle.text = NSLocalizedString(@"Register", @"Register");
    navTitle.textColor = UIColorFromRGB(0xFFFFFF);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:fontNormal size:20];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}


/**
 Create navigation left button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation left button action
 */
- (void)cancelButtonClicked {
    if (userData) {
        userData = nil;
    }
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UITapGesture Recogniser -

/**
 Dismiss keyboard
 */
-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [firstNameTextField resignFirstResponder];
    [phoneNoTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [referalCodeTextField resignFirstResponder];
}

#pragma  mark - Custom Methods -

    
-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.phoneNoCountryFlag.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.phoneNoCountryFlag.frame = frame;
        
        frame = self.phoneNoCountryCode.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.phoneNoCountryCode.frame = frame;
        
        frame = self.countryBtn.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.countryBtn.frame = frame;

        frame = self.phoneNoTextField.frame;
        frame.size.width = screenWidth - 45 - phoneNoCountryCode.frame.size.width - phoneNoCountryFlag.frame.size.width;
        frame.origin.x = 15;
        self.phoneNoTextField.frame = frame;
        
        frame = self.label1.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.label1.frame = frame;

        frame = self.label2.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.label2.frame = frame;

        frame = self.tncCheckButton.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.tncCheckButton.frame = frame;
        
        frame = self.creatingLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.creatingLabel.frame = frame;
    } else {
        CGRect frame = self.phoneNoTextField.frame;
        frame.size.width = screenWidth - 45 - phoneNoCountryCode.frame.size.width - phoneNoCountryFlag.frame.size.width;
        frame.origin.x = 75;
        self.phoneNoTextField.frame = frame;
        tncCheckButton.transform = CGAffineTransformMakeScale(1, 1);
    }
}


/**
 Update Profile Image and Button at center and aslo align step labels
 */
-(void)setRegisterStepsFrameAndProfileImageCente {
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    
    mainScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight - 60);
    CGRect frame = view1.frame;
    frame.size.width = screenWidth;
    view1.frame = frame;
    frame = view2.frame;
    frame.size.width = screenWidth;
    view2.frame = frame;
    frame = view3.frame;
    frame.size.width = screenWidth;
    view3.frame = frame;
    
    lineView.frame = CGRectMake(15, 20, screenWidth - 30, 1);
    label1.frame = CGRectMake(12.5, 10, 20, 20);
    label2.frame = CGRectMake(screenWidth - 32.5 , 10, 20, 20);

    label1.layer.cornerRadius = 10;
    label1.layer.masksToBounds  = YES;
    label2.layer.cornerRadius = 10;
    label2.layer.masksToBounds  = YES;
    
    profileImageView.frame = CGRectMake((screenWidth - 100)/2, 50, 100, 100);
    profileButton.frame = CGRectMake((screenWidth - 100)/2, 50, 100, 100);
    mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(view3.frame) + 20 );
    
    self.nextBtn.frame = CGRectMake(15, CGRectGetMaxY(mainScrollView.frame) + 10, screenWidth - 30, 40);
    [self.view bringSubviewToFront:self.nextBtn];
}

/**
 Set place holder property of each textfield
 */
-(void) setTextFiledsPlaceHolders {
    [firstNameTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [phoneNoTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [emailTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [confirmPasswordTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
    [referalCodeTextField setValue:UIColorFromRGB(0xA1A6BB) forKeyPath:@"_placeholderLabel.textColor"];
}

/**
 Set country code and flag when first time view appears
 */
-(void) setCountryFlagAndCode {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    phoneNoCountryCode.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    phoneNoCountryFlag.image = [UIImage imageNamed:imagePath];
}

/**
 Set terms and condition link label
 */

-(void)setTermsNConditionsLink {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"By creating account you agree with our Terms & Conditions.", @"By creating account you agree with our Terms & Conditions.") attributes:nil];
    NSString *findRangeForString = NSLocalizedString(@"By creating account you agree with our Terms & Conditions.", @"By creating account you agree with our Terms & Conditions.");
    NSRange linkRange = [findRangeForString rangeOfString:NSLocalizedString(@"Terms & Conditions.", @"Terms & Conditions.")];
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName :UIColorFromRGB(0x019F6E), NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedString setAttributes:linkAttributes range:linkRange];
    
    creatingLabel.attributedText = attributedString;
    creatingLabel.userInteractionEnabled = YES;
    [creatingLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    layoutManager = [[NSLayoutManager alloc] init];
    textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    textStorage = [[NSTextStorage alloc] initWithAttributedString:attributedString];
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = creatingLabel.lineBreakMode;
    textContainer.maximumNumberOfLines = creatingLabel.numberOfLines;
}

/**
 Adding action on terms and condition label

 @param tapGesture tap on "Terms & Conditions."
 */
- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGSize labelSize = tapGesture.view.bounds.size;
    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x, locationOfTouchInLabel.y - textContainerOffset.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer inTextContainer:textContainer fractionOfDistanceBetweenInsertionPoints:nil];
    NSString *findRangeForString = NSLocalizedString(@"By creating account you agree with our Terms & Conditions.", @"By creating account you agree with our Terms & Conditions.");
    NSRange linkRange = [findRangeForString rangeOfString:NSLocalizedString(@"Terms & Conditions.", @"Terms & Conditions.") ];
    if (NSLocationInRange(indexOfCharacter, linkRange)) {
        [self.view endEditing:YES];
        TermsnConditionViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsNCondition"];
        VC.isCommingFromSignUp = YES;
        [Helper checkForPush:self.navigationController.view];
        [self.navigationController pushViewController:VC animated:NO];
    }
}

/**
 Set profile image view circular

 @param imgview profile imageView
 */
-(void)setStyleCircleForImage:(UIImageView *)imgview
{
    imgview.layer.cornerRadius = imgview.frame.size.width/2.0;
    imgview.clipsToBounds=YES;
}

/**
 Validate all data entered by user before Sign Up API call
 */
-(void)validationBeforeSignUp {
    NSString *signupFirstName = firstNameTextField.text;
    NSString *signupEmail = emailTextField.text;
    NSString *signupPassword = passwordTextField.text;
    NSString *signupConfirmPassword = confirmPasswordTextField.text;
    NSString *signupPhoneno = phoneNoTextField.text;
    NSString *signupReferralCode = referalCodeTextField.text;
    if ((unsigned long)signupFirstName.length == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your Full Name", @"Please enter your Full Name")];
        nextBtn.userInteractionEnabled = YES;
    } else if ((unsigned long)signupPhoneno.length == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your Phone Number", @"Please enter your Phone Number")];
        nextBtn.userInteractionEnabled = YES;
    } else if ((unsigned long)signupEmail.length == 0 && userData == nil) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your e-mail ID", @"Please enter your e-mail ID")];
        nextBtn.userInteractionEnabled = YES;
    } else if ((unsigned long)signupEmail.length > 0 && [Helper emailValidationCheck:signupEmail] == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your e-mail ID is invalid", @"Your e-mail ID is invalid")];
        nextBtn.userInteractionEnabled = YES;
    }  else if(!emailVerified) {
        [self validateEmailAndPostalCode];
        nextBtn.userInteractionEnabled = YES;
    } else if(userData == nil) {
        if ((unsigned long)signupPassword.length == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter a Password", @"Please enter a Password")];
            [passwordTextField becomeFirstResponder];
            nextBtn.userInteractionEnabled = YES;
        } else if ((unsigned long)signupPassword.length <= 5) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password must be minimum 6 charatesrs. Try again", @"Your password must be minimum 6 charatesrs. Try again")];
            [passwordTextField becomeFirstResponder];
            nextBtn.userInteractionEnabled = YES;
        } else if ((unsigned long)signupConfirmPassword.length == 0) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please confirm a Password", @"Please confirm a Password")];
            [confirmPasswordTextField becomeFirstResponder];
            nextBtn.userInteractionEnabled = YES;
        } else if ((unsigned long)signupConfirmPassword.length <= 5) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password must be minimum 6 charatesrs. Try again", @"Your password must be minimum 6 charatesrs. Try again")];
            [confirmPasswordTextField becomeFirstResponder];
            nextBtn.userInteractionEnabled = YES;
        } else if (![signupPassword isEqualToString:signupConfirmPassword]) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password doesn't matches. try again", @"Your password doesn't matches. try again")];
            [confirmPasswordTextField becomeFirstResponder];
            nextBtn.userInteractionEnabled = YES;
        } else if ([signupReferralCode length] && !referralVerified) {
            [self validateRefferalCode];
            nextBtn.userInteractionEnabled = YES;
        } else if (!isTnCButtonSelected) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select our Terms and Condition", @"Please select our Terms and Condition")];
            nextBtn.userInteractionEnabled = YES;
        } else {
            saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text, emailTextField.text, phoneNoTextField.text, passwordTextField.text, referalCodeTextField.text, @"1", phoneNoCountryCode.text, nil];
            NSString *mobNo = [phoneNoCountryCode.text stringByAppendingString:phoneNoTextField.text];
            [self getVerificationCode:mobNo];
        }
    } else {
        if ([signupReferralCode length] && !referralVerified) {
            [self validateRefferalCode];
            nextBtn.userInteractionEnabled = YES;
        } else if (!isTnCButtonSelected) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select our Terms and Condition", @"Please select our Terms and Condition")];
            nextBtn.userInteractionEnabled = YES;
        } else {
            saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text, emailTextField.text, phoneNoTextField.text, passwordTextField.text, referalCodeTextField.text, userData[@"loginType"], phoneNoCountryCode.text, nil];
            NSString *mobNo = [phoneNoCountryCode.text stringByAppendingString:phoneNoTextField.text];
            [self getVerificationCode:mobNo];
        }
    }
}

#pragma mark - WebService Call -

/**
 Verify mobile number
 */
- (void) validatemobilePhoneNumber {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Verifying your Mobile Number...", @"Validating Mobile Number...")];
    NSString *mobNo = [phoneNoCountryCode.text stringByAppendingString:phoneNoTextField.text];
    NSDictionary *params = @{
                             @"ent_mobile":mobNo,
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentNetworkDateTime],
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkMobile"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                       } else if ([response objectForKey:@"Error"]) {
                                           phoneNoTextField.text = @"";
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
                                       } else {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
                                               if (isComingFromvalidateBeforeSignUp) {
                                                   [self validationBeforeSignUp];
                                                   nextBtn.userInteractionEnabled = YES;
                                               } else {
                                                   [emailTextField becomeFirstResponder];
                                                   nextBtn.userInteractionEnabled = YES;
                                               }
                                           } else {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                               phoneNoTextField.text = @"";
                                               keyboardToolbar = nil;
                                               [self keyboardWillBeHidden:nil];
                                               [phoneNoTextField becomeFirstResponder];
                                           }
                                       }
                                   }
                               }];
}

/**
 Verify e-mail ID which entered by user is valid or not
 */
- (void) validateEmailAndPostalCode {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Verifying your e-mail ID...", @"Verifying your e-mail ID...")];
    NSDictionary *params = @{
                             @"ent_email":emailTextField.text,
                             @"zip_code":@"560024",
                             @"ent_user_type":@"2",
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             KDASignUpDateTime:[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"validateEmailZip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                           nextBtn.userInteractionEnabled = YES;
                                       } else {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
                                               emailVerified = YES;
                                               if (isComingFromvalidateBeforeSignUp) {
                                                   [self validationBeforeSignUp];
                                                   nextBtn.userInteractionEnabled = NO;
                                               } else {
                                                   [passwordTextField becomeFirstResponder];
                                                   nextBtn.userInteractionEnabled = YES;
                                               }
                                           } else {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
                                               self.emailTextField.text = @"";
                                               [self.emailTextField becomeFirstResponder];
                                               nextBtn.userInteractionEnabled = YES;
                                           }
                                       }
                                   }
                               }];
}

/**
 Verify Refferal Code API
 */
- (void) validateRefferalCode {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Verifying your Referral Code...", @"Verifying your Referral Code...")];
    NSDictionary *params = @{
                             @"ent_coupon":referalCodeTextField.text,
                             @"ent_lat":[Utility currentLatitude],
                             @"ent_long":[Utility currentLongitude],
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       if (!response) {
                                       } else if ([response objectForKey:@"Error"]) {
                                           referalCodeTextField.text = @"";
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
                                       } else {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
                                               referralVerified = YES;
                                               if (isComingFromvalidateBeforeSignUp) {
                                                   [self validationBeforeSignUp];
                                               } else {
                                                   keyboardToolbar = nil;
                                                   [referalCodeTextField resignFirstResponder];
                                               }
                                           } else {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
                                               referalCodeTextField.text = @"";
                                               [referalCodeTextField becomeFirstResponder];
                                           }
                                       }
                                   }
                               }];
}

/**
 Get verification service API

 @param phoneNumber Mobile Number entered by user
 */
-(void)getVerificationCode:(NSString *)phoneNumber {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    NSDictionary *params = @{
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mobile":phoneNumber,
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       nextBtn.userInteractionEnabled = YES;
                                       if (!response) {
                                       } else if ([response objectForKey:@"Error"]) {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
                                       } else {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
                                               VerifyiMobileViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyMobile"];
                                               VC.myNumber = [saveSignUpDetails objectAtIndex:2];
                                               VC.getSignupDetails = [saveSignUpDetails mutableCopy];
                                               VC.pickedImage = _pickedImage;
                                               [Helper checkForPush:self.navigationController.view];
                                               [self.navigationController pushViewController:VC animated:NO];
                                           }
                                           else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 113) {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                               keyboardToolbar = nil;
                                               [phoneNoTextField becomeFirstResponder];
                                           } else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 108) {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response [@"test"][@"response"][@"error"]];
                                               keyboardToolbar = nil;
                                               [phoneNoTextField becomeFirstResponder];
                                           } else {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                           }
                                       }
                                   }
                               }];
}



#pragma mark - TextFieldDelegates -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    if(keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)] ;
        [keyboardToolbar setBarStyle:UIBarStyleDefault];
        [keyboardToolbar sizeToFit];
        UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        NSArray *itemsArray;
        if([textField isEqual:phoneNoTextField]) {
            UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", @"Next") style:UIBarButtonItemStyleDone target:self action:@selector(textFieldShouldReturn:)];
            nextButton.tintColor = UIColorFromRGB(0x019F6E);
            itemsArray = [NSArray arrayWithObjects:flexButton,nextButton, nil];
        }
        [keyboardToolbar setItems:itemsArray];
    }
    if([textField isEqual:phoneNoTextField]) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        [phoneNoTextField setInputAccessoryView:keyboardToolbar];
        [UIView commitAnimations];
    } else if([textField isEqual:emailTextField]) {
        emailVerified = NO;
        isComingFromvalidateBeforeSignUp = NO;
        emailTextField.text = @"";
    } else if([textField isEqual:referalCodeTextField]) {
        referralVerified = NO;
        isComingFromvalidateBeforeSignUp = NO;
        referalCodeTextField.text = @"";
    }
    nextBtn.userInteractionEnabled = YES;
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == firstNameTextField) {
        return ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]].location != NSNotFound || [string rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound || [string isEqualToString:@""]);
    }
    else
        return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if ([textField isKindOfClass:[UIBarButtonItem class]]) {
        textField = activeTextField;
    }
    if ([textField isEqual:firstNameTextField] || [textField isEqual:phoneNoTextField] || [textField isEqual:emailTextField] || [textField isEqual:passwordTextField] || [textField isEqual:confirmPasswordTextField] || [textField isEqual:referalCodeTextField]) {
        if ([textField isEqual:firstNameTextField] ) {
            keyboardToolbar = nil;
            [phoneNoTextField becomeFirstResponder];
        } else if ([textField isEqual:phoneNoTextField]) {
            if([textField.text length] > 0) {
                PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
                if ([reachability isNetworkAvailable]) {
                    [self dismissKeyboard];
                    [self validatemobilePhoneNumber];
                } else {
                    [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
                }
            } else {
                keyboardHeight = 220;
                [emailTextField becomeFirstResponder];
            }
        } else if ([textField isEqual:emailTextField]) {
            if([textField.text length] > 0) {
                if ([Helper emailValidationCheck:emailTextField.text]) {
                    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
                    if ([reachability isNetworkAvailable]) {
                        [self validateEmailAndPostalCode];
                    } else {
                        [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
                    }
                } else {
                    emailTextField.text = @"";
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter valid e-mail ID", @"Please enter valid e-mail ID")];
                    [emailTextField becomeFirstResponder];
                }
            } else {
                [passwordTextField becomeFirstResponder];
            }
        } else if ([textField isEqual:passwordTextField]  && userData == nil){
            NSString *password = passwordTextField.text;
            if (password.length == 0) {
                [confirmPasswordTextField becomeFirstResponder];
            } else if(password.length <= 5) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password must be minimum 6 charatesrs. Try again", @"Your password must be minimum 6 charatesrs. Try again")];
                [passwordTextField becomeFirstResponder];
            } else if(password.length > 5) {
                [confirmPasswordTextField becomeFirstResponder];
            }
        } else if ([textField isEqual:confirmPasswordTextField]  && userData == nil) {
            NSString *password = passwordTextField.text;
            NSString *confirmPassword = confirmPasswordTextField.text;
            if (confirmPassword.length == 0) {
                [referalCodeTextField becomeFirstResponder];
            } else if (confirmPassword.length <= 5) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password must be minimum 6 charatesrs. Try again", @"Your password must be minimum 6 charatesrs. Try again")];
                [confirmPasswordTextField becomeFirstResponder];
            } else if(confirmPassword.length > 5 && ![password isEqualToString:confirmPassword]) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your password doesn't matches. Try again", @"Your password doesn't matches. Try again")];
                confirmPasswordTextField.text = @"";
                [confirmPasswordTextField becomeFirstResponder];
            } else if([password isEqualToString:confirmPassword]) {
                [referalCodeTextField becomeFirstResponder];
            }
        } else if ([textField isEqual:referalCodeTextField]) {
            if([referalCodeTextField.text length]) {
                PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
                if ([reachability isNetworkAvailable]) {
                    [self validateRefferalCode];
                } else {
                    [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
                }
            } else {
                keyboardToolbar = nil;
                [referalCodeTextField resignFirstResponder];
            }
        }
    }
    return YES;
}

#pragma mark - UIButton Action -

/**
 Country code Button Action

 @param sender country code or country flag button
 */
- (IBAction)countryCode:(id)sender {
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName) {
        self.phoneNoCountryFlag.image = flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        [Helper setToLabel:phoneNoCountryCode Text:countryCode WithFont:fontNormal FSize:15 Color:UIColorFromRGB(0x1D1D26)];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 Terms and Condition selected Button

 @param sender check mark button
 */
- (IBAction)checkButtonClicked:(id)sender {
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES;
    if(mBut.isSelected) {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_off"] forState:UIControlStateNormal];
    } else {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_on"] forState:UIControlStateSelected];
    }
}

/**
 Male Button Action

 @param sender male button
 */
- (IBAction)maleBtnAction:(id)sender {
    //maleBtn.selected = YES;
   // femaleBtn.selected = NO;
}

/**
 Female Button Action

 @param sender female button
 */
- (IBAction)femaleBtnAction:(id)sender {
  //  maleBtn.selected = NO;
  //  femaleBtn.selected = YES;
}


/**
 Profile Button Action

 @param sender profile button
 */
- (IBAction)profileButtonClicked:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (_pickedImage != nil) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Profile Photo", @"Profile Photo")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"Camera", @"Camera"),
                                                           NSLocalizedString(@"Library", @"Library"),
                                                           NSLocalizedString(@"Remove Profile Photo", @"Remove Profile Photo"), nil];
        actionSheet.tag = 200;
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Profile Picture", @"Profile Picture")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"Camera", @"Camera"),
                                                           NSLocalizedString(@"Library", @"Library"),nil];
        actionSheet.tag = 100;
    }
    [actionSheet showInView:self.view];
}

/**
 Next Button Action

 @param sender next button
 */
- (IBAction) nextButtonClick:(id)sender {
    nextBtn.userInteractionEnabled = NO;
    isComingFromvalidateBeforeSignUp = YES;
    [self dismissKeyboard];
    [self validationBeforeSignUp];
}


#pragma mark - UIActionSheet Delegates -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100) {
        switch (buttonIndex) {
            case 0: {
                [self cameraButtonClicked];
                break;
            } case 1: {
                [self libraryButtonClicked];
                break;
            }
            default:
                break;
        }
    }
    else  if (actionSheet.tag == 200) {
        switch (buttonIndex) {
            case 0: {
                [self cameraButtonClicked];
                break;
            }
            case 1: {
                [self libraryButtonClicked];
                break;
            }
            case 2: {
                [self deSelectProfileImage];
                break;
            }
            default:
                break;
        }
    }
}

#pragma mark - UIactionSheet Action Methods -

/**
 Remove selected image
 */
-(void)deSelectProfileImage {
    profileImageView.image = [UIImage imageNamed:@"camera"];;
    _pickedImage = nil;
}

-(void)cameraButtonClicked {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate =self;
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            imagePicker.allowsEditing = YES;
            [self presentViewController:imagePicker animated:YES completion:nil];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Camera is not available", @"Camera is not available")];
        }
    });
}

-(void)libraryButtonClicked {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate =self;
        picker.allowsEditing = YES;
        picker.navigationBar.barTintColor = [UIColor whiteColor];
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        } else {
            [self presentViewController:picker animated:YES completion:nil];
        }
    });
}

#pragma mark - UIImagePickerController Delegate -

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    profileImageView.image = _pickedImage;
    _pickedImage = [Helper imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    NSLog(@"Did Finish Picking Image");
}


#pragma mark - Keyboard Notifications -

/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [keyboardToolbar removeFromSuperview];
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 20;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0.00000000000001) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(view3.frame) + 20 + 64 + height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    CGFloat screenWidth = self.view.frame.size.width;
    mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(view3.frame) + 20 + 64);
}


@end


