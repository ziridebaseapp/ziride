//
//  SplashViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SplashViewController : UIViewController<CLLocationManagerDelegate> {
    CLLocationManager *clmanager;
}
@property(nonatomic,weak) NSTimer *pubnubStreamTimer;

@end
