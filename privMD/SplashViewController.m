//
//  SplashViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SplashViewController.h"
#import "HelpViewController.h"
#import "MenuViewController.h"
#import "MapViewController.h"
#import "MyAppTimerClass.h"
#import "NetworkHandler.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "LocationServicesMessageVC.h"
#import <AVFoundation/AVFoundation.h>
#import <Google/Analytics.h>
#import "Utility.h"
#import <Stripe/Stripe.h>

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

static NSBundle *bundle = nil;

@interface SplashViewController () <PubNubWrapperDelegate> {
    PubNubWrapper *pubnub;
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    if([[UIDevice currentDevice].systemVersion floatValue] > 9.0) {
        [self setHomeViewSematics];
        [self setHomeViewSematics];
    }
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
    int screenHeight = self.view.frame.size.height;
    NSString *imageName = [NSString stringWithFormat:@"Default-%d", screenHeight];
    imageview.image = [UIImage imageNamed:imageName];
    [self.view addSubview:imageview];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    if(IS_SIMULATOR) {
        [[NSUserDefaults standardUserDefaults] setObject:@"13.028869"
                                                  forKey:kNSUCurrentLat];
        [[NSUserDefaults standardUserDefaults] setObject:@"77.589638"
                                                  forKey:kNSUCurrentLong];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    if([[UIDevice currentDevice].systemVersion floatValue] > 9.0) {
        [self getDirection];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    if([[UIDevice currentDevice].systemVersion floatValue] > 9.0) {
        if([[Utility sessionToken] length]) {
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(splashLoading) userInfo:Nil repeats:NO];
        } else {
            [self removeSplash];
        }
        if([[[FIRInstanceID instanceID] token] length]) {
            [Utility setPromoCode:[[FIRInstanceID instanceID] token]];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    pubnub = nil;
}

/**
 Splash Loading
 */
-(void)splashLoading {
    pubnub = [PubNubWrapper sharedInstance];
    [self checkSession];
}

- (void)setHomeViewSematics {
    if ([Helper isCurrentLanguageRTL]) {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    } else {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
}

#pragma mark - Webservices -

-(void) checkSession {
    NSDictionary *params = @{
                             @"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_user_type":@"2",
                             @"ent_date_time":[Helper getCurrentDateTime]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkSession"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self checkSessionResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..." , @"Loading...")];
}

-(void)checkSessionResponse:(NSDictionary *)response {
    if (response == nil) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [self unSubscribeToPassengerChannel];
        User *user = [[User alloc] init];
        [user deleteUserSavedData];
        [self removeSplash];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
        } else {
            [self subscribeToPassengerChannel];
            [Utility setPromoCode:flStrForStr(response[@"promoCode"])];
            NSDictionary *configData = [response[@"configData"] mutableCopy];
            if ([Helper checkForUpdateAppVersion:configData[@"versions"][@"iosCust"]]) {
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
                return;
            }
            [Utility setGoogleMatrixTimeInterval:[configData[@"DistanceMatrixInterval"] integerValue]];
            [Utility setPubnubPublishTimeInterval:[configData[@"PubnubIntervalHome"] integerValue]];
            [Utility setPubnubPublishOnBookingTimeInterval:[configData[@"PubnubIntervalDispatch"] integerValue]];
            [Utility setRadiusForOperatingArea:[configData[@"RadiusOperating"] integerValue]];
            [Utility setRadiusForNotOperatingArea:[configData[@"RadiusNotOperating"] integerValue]];
            [Utility setMaximumETA:[configData[@"ETAThreshold"] integerValue]*60];
            [Utility setBookingAmountAllowUpto:[configData[@"BookingAmountAllowUpto"] doubleValue]];
            [Utility setDistanceThresholdForFareEstimate:[configData[@"distanceThresholdForFareEstimate"] integerValue]];
            
            [Utility setPaymentGatewayKey:flStrForStr(configData[@"stipeKey"])];
            if([Utility paymentGatewayKey]) {
                [Stripe setDefaultPublishableKey:[Utility paymentGatewayKey]];
            }

            NSMutableArray *googleKeysArray = [configData[@"GoogleKeysArray"] mutableCopy];
            NSString *distanceMatrixKey = [googleKeysArray firstObject];
            [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
            [googleKeysArray removeObjectAtIndex:0];
            [Utility setGoogleKeysArray:googleKeysArray];
            [self getDriversFromService];
        }
    }
}


-(void) getDriversFromService {
    NSDictionary *params = @{
                             @"sid": [Utility userID],
                             @"lt":[Utility currentLatitude],
                             @"lg":[Utility currentLongitude]
                             };
    NSLog(@"Get Drivers Params = %@", params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestForNodeAPIMethod:@"getDrivers"
                                          paramas:params
                                     onComplition:^(BOOL succeeded, NSDictionary *response) {
                                         NSLog(@"Get Drivers Response = %@", response);
                                         [self getDriversResponse:response];
                                     }];
}

-(void)getDriversResponse:(NSDictionary *)response {
    if (response == nil) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [self unSubscribeToPassengerChannel];
        User *user = [[User alloc] init];
        [user deleteUserSavedData];
        [self removeSplash];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
        } else {
            if ([response[@"bookingStatus"] integerValue] == 1) {
                [self getBookingDataFromService];
            } else if ([response[@"bookingStatus"] integerValue] == 2 && [response[@"ExpiredTime"] integerValue] > 0) {
                [Utility setSplashData:response];
                if ([[response objectForKey:@"flag"] intValue] == 1) {
                    [Utility setOpratingTypeArea:kNotOperatingArea];
                    [self serviceToNotifyUserIsComingFromNotOperatingArea];
                } else {
                    [Utility setOpratingTypeArea:kOperatingArea];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingData];
                    [self removeSplash];
                }
            } else {
                [Utility setSplashData:response];
                if ([[response objectForKey:@"flag"] intValue] == 1) {
                    [Utility setOpratingTypeArea:kNotOperatingArea];
                    [self serviceToNotifyUserIsComingFromNotOperatingArea];
                } else {
                    [Utility setOpratingTypeArea:kOperatingArea];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUBookingData];
                    [self removeSplash];
                }
            }
        }
    }
}

-(void) getBookingDataFromService {
    NSDictionary *params = @{
                             @"sid": [Utility userID],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestForNodeAPIMethod:@"getSlvBookingData"
                                          paramas:params
                                     onComplition:^(BOOL succeeded, NSDictionary *response) {
                                         [self bookingDataResponse:response];
                                     }];
}

-(void)bookingDataResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [self unSubscribeToPassengerChannel];
        User *user = [[User alloc] init];
        [user deleteUserSavedData];
        [self removeSplash];
    } else {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0) {
            if ([response[@"status"] intValue] == 3) {
                //Rejected
                [self removeSplash];
            } else {
                NSDictionary *dictionary =  response[@"data"];
                NSInteger newBookingStatus = [flStrForStr(dictionary[@"status"]) integerValue];
                [Utility setBookingData:dictionary];
                [Utility setPickUpLatitude:[dictionary[@"pickLat"] doubleValue]];
                [Utility setPickUpLongitude:[dictionary[@"pickLong"] doubleValue]];
                [Utility setDropOffLatitude:[dictionary[@"desLat"] doubleValue]];
                [Utility setDropOffLongitude:[dictionary[@"desLong"] doubleValue]];
                [Utility setDriverLatitude:[dictionary[@"driverLat"] doubleValue]];
                [Utility setDriverLongitude:[dictionary[@"driverLong"] doubleValue]];
                
                [Utility setPickUpLocationAddressLine1:flStrForStr(dictionary[@"addr1"])];
                [Utility setPickUpLocationAddressLine2:flStrForStr(dictionary[@"addr2"])];
                [Utility setDropOffLocationAddressLine1:flStrForStr(dictionary[@"dropAddr1"])];
                [Utility setPickUpLocationAddressLine2:flStrForStr(dictionary[@"dropAddr2"])];
                
                if (newBookingStatus == 5) {
                    newBookingStatus = 10;
                } else if (newBookingStatus == 4) {
                    newBookingStatus = 11;
                }
                if (newBookingStatus == 6 || newBookingStatus == 7 || newBookingStatus == 8 || (newBookingStatus == 9 )) {
                    [Utility setBookingStatus:newBookingStatus];
                    [Utility setBookingID:flStrForStr(dictionary[@"bid"])];
                    [Utility setBookingDate:flStrForStr(dictionary[@"apptDt"])];
                    
                    [Utility setDriverID:flStrForStr(dictionary[@"mid"])];
                    [Utility setDriverEmailID:flStrForStr(dictionary[@"email"])];
                    [Utility setDriverPubnubChannel:flStrForStr(dictionary[@"chn"])];
                    [Utility setDriverMobileNumber:flStrForStr(dictionary[@"mobile"])];
                    [Utility setDriverProfileImageURL:flStrForStr(dictionary[@"pPic"])];
                    [Utility setDriverRating:flStrForStr(dictionary[@"r"])];
                    [Utility setDriverName:[NSString stringWithFormat:@"%@ %@",flStrForStr(dictionary[@"fName"]), flStrForStr(dictionary[@"lName"])]];
                    
                    [Utility setVehicleColor:flStrForStr(dictionary[@"color"])];
                    [Utility setVehicleMakeName:flStrForStr(dictionary[@"make"])];
                    [Utility setVehicleModelName:flStrForStr(dictionary[@"model"])];
                    [Utility setVehicleImageURL:flStrForStr(dictionary[@"carImage"])];
                    [Utility setVehicleNumberPlate:flStrForStr(dictionary[@"plateNo"])];
                    [Utility setVehicleMapImageURL:flStrForStr(dictionary[@"carMapImage"])];
                    
                    [Utility setIsUserInBooking:YES];
                    
                    if(newBookingStatus == 9)
                        [self unSubscribeOnlyBookedDriver];
                    else if(newBookingStatus == 6 || newBookingStatus == 7 || newBookingStatus == 8)
                        [self subscribeOnlyBookedDriver];
                    [(AppDelegate*)[[UIApplication sharedApplication] delegate] noPushForceChangingController:dictionary :(int)newBookingStatus];
                } else {
                    [(AppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:dictionary :(int)newBookingStatus];
                }
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
                [self removeSplash];
            }
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
        }
    }
}

/**
 Send serivce for not operating area
 
 @param index index Number
 */
-(void)serviceToNotifyUserIsComingFromNotOperatingArea {
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params =  @{ @"ent_sess_token":[Utility sessionToken],
                               @"ent_dev_id": [Utility deviceID],
                               @"ent_lat":[NSNumber numberWithFloat:[[Utility currentLatitude] floatValue]],
                               @"ent_long":[NSNumber numberWithFloat:[[Utility currentLongitude] floatValue]],
                               @"ent_user_type":@"2"
                               };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getWorkplaces"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 [self notifyOperatingAreaResponse:response];
                                 [self removeSplash];
                             }
                         }];
}

/**
 Get Notify for Not Operating Area response
 
 @param response response data
 */
-(void)notifyOperatingAreaResponse:(NSDictionary *)response {
    if (!response) {
        return;
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 99 || [response[@"errNum"] intValue] == 101 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
        [self removeSplash];
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
        } else {
            if ([dictResponse[@"types"] count]) {
                [Utility setOpratingTypeArea:kOperatingArea];
            } else {
                [Utility setOpratingTypeArea:kNotOperatingArea];
            }
        }
    }
}

#pragma mark - Pubnub Custom Methods -

/**
 Susbscribe Passenger channel
 */
-(void)subscribeToPassengerChannel {
    pubnub = [PubNubWrapper sharedInstance];
    [pubnub subscribeToUserChannel];
    [pubnub setDelegate:self];
}

/**
 Unsubscribe Passenger Channel
 */
-(void) unSubscribeToPassengerChannel {
    pubnub = [PubNubWrapper sharedInstance];
    [pubnub unsubscribeFromMyChannel];
    [pubnub setDelegate:self];
}

/**
 This is to subscribe given channel
 
 @param channel channel name
 */
-(void)subsCribeToPubNubChannel:(NSString*)channel {
    pubnub = [PubNubWrapper sharedInstance];
    [pubnub subscribeToChannel:channel];
    [pubnub setDelegate:self];
}

/**
 This is to subscribe given channel
 
 @param channel channel name
 */
-(void)unSubsCribeToPubNubChannel:(NSString *)channel {
    pubnub = [PubNubWrapper sharedInstance];
    [pubnub unsubscribeFromChannel:channel];
    [pubnub setDelegate:self];
}

/**
 Subscribe Only Booked Driver
 */
-(void)subscribeOnlyBookedDriver {
    if ([[Utility driverPubnubChannel] length])
        [self subsCribeToPubNubChannel:[Utility driverPubnubChannel]];
}

/**
 Unsubscribe Only Booked Driver
 */
-(void) unSubscribeOnlyBookedDriver {
    if ([[Utility driverPubnubChannel] length])
        [self unSubsCribeToPubNubChannel:[Utility driverPubnubChannel]];
}


/**
 This removes splash VC and go to next If session is there then open Home screen else Help VC
 */
-(void)removeSplash {
    if([[Utility sessionToken] length]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else {
        HelpViewController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
        [[self navigationController ] pushViewController:help animated:NO];
        [self.view bringSubviewToFront:self.movieView];
    }
}


/**
 *  All Directions Method
 */
#pragma mark-CLLocation Delegate Method
- (void) getDirection {
    clmanager = [[CLLocationManager alloc] init];
    clmanager.delegate = self;
    clmanager.distanceFilter = kCLDistanceFilterNone;
    clmanager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([clmanager respondsToSelector:@selector(requestWhenInUseAuthorization)])//requestAlwaysAuthorization
        [clmanager requestWhenInUseAuthorization];//requestAlwaysAuthorization
    [clmanager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



/*
 To Get Updated lattitude & longitude
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    [Utility setCurrentLatitude:lat];
    [Utility setCurrentLongitude:log];
    if(location.coordinate.latitude == 0 && location.coordinate.longitude == 0)
        [self getDirection];
    else
    {
        [self requestForGoogleGeocoding :lat:log];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [clmanager stopUpdatingLocation];
    }
}

/*
 To print error msg of location manager
 @param error msg.
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        [self gotoLocationServicesMessageViewController];
    } else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorNetwork) {
    } else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
    }
}

-(void)locationServicesChanged:(NSNotification*)notification {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [self gotoLocationServicesMessageViewController];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController {
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

/*
 Get Google Geocoding
 
 @Params lattitude, longitude
 */
-(void)requestForGoogleGeocoding :(NSString*)lattitude :(NSString*)longitude {
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    [handler setRequestType:eLatLongparser];
    NSString *string = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true",lattitude,longitude];
    NSURL *url = [NSURL URLWithString:string];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(googleReverseGeocodingResponse:)];
}

/**
 Google Reverse Geocoding Response
 
 @param _response geo coding response
 */
-(void)googleReverseGeocodingResponse:(NSDictionary*)_response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    NSDictionary *dict  = [_response objectForKey:@"ItemsList"];
    if(!_response)
        return;
    else {
        [Utility setCurrentCity:flStrForStr(dict[@"locality"])];
        [Utility setCurrentCountry:flStrForStr(dict[@"country"])];
    }
}




@end
