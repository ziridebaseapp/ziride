//
//  SupportDetailTableViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportDetailTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NSMutableArray *detailsArray;
@property(strong,nonatomic) NSString *titleNav;
@property (weak, nonatomic) IBOutlet UITableView *supportDetailsTable;
@property(nonatomic) BOOL isComingFromNeedHelp;

@end
