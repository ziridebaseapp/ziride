//
//  SupportDetailTableViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SupportDetailTableViewController.h"
#import "SupportWebViewController.h"
#import "SupportTableViewCell.h"
#import "UILabel+DynamicHeight.h"
#import "GenericUtility.h"

@interface SupportDetailTableViewController () {
    NSString *navTitle;
    NSString *webUrlLink;
    SupportTableViewCell *cell;
    NSString *cellIdentifier;
}
@end

@implementation SupportDetailTableViewController

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    cellIdentifier = @"supportCell";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationItem setHidesBackButton:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - Custom Navigation Bar -

/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation Left Button Action
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _detailsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_detailsArray.count) {
        static SupportTableViewCell *cell2 = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell2 = [_supportDetailsTable dequeueReusableCellWithIdentifier:cellIdentifier];
        });
        [self setUpCell:cell2 atIndexPath:indexPath];
        CGFloat height = [cell2.titleLabel measureHeightLabel] + 2;
        if (height < 50) {
            return 50;
        }
        return height;
    } else {
        return  0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil) {
        cell = (SupportTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    [self setUpCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"supportWebViewVC" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

# pragma mark - Cell Setup

/**
 Set up Cell

 @param cell1 Support TableView Cell
 @param indexPath IndexPath
 */
- (void)setUpCell:(SupportTableViewCell *)cell1 atIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    cell1.titleLabel.text = _detailsArray[indexPath.row][@"tag"];
    CGFloat titleLabelHeight = [cell1.titleLabel measureHeightLabel];
    CGRect frame = cell1.titleLabel.frame;
    frame.origin.x = 30;
    frame.origin.y = (cell1.frame.size.height - titleLabelHeight)/2;
    frame.size.width = screenWidth - 60;
    frame.size.height = titleLabelHeight;
    cell1.titleLabel.frame = frame;
    
    cell1.lineView.backgroundColor = UIColorFromRGB(0xE4E7F0);
    frame = cell1.lineView.frame;
    frame.origin.x = 0;
    frame.origin.y = cell1.frame.size.height - 1;
    frame.size.width = screenWidth;
    frame.size.height = 1;
    cell1.lineView.frame = frame;
    [cell1 bringSubviewToFront:cell1.lineView];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"supportWebViewVC"]) {
        SupportWebViewController *webView = (SupportWebViewController*)[segue destinationViewController];
        NSIndexPath *path = (NSIndexPath *)sender;
        if (self.isComingFromNeedHelp) {
            webUrlLink = [NSString stringWithFormat:@"%@%@",_detailsArray[path.row][@"link"], @"&frm_booking=0"];
            webView.title = NSLocalizedString(@"Tell Us More", @"Tell Us More");
            webView.weburl = webUrlLink;
        } else {
        webUrlLink = [NSString stringWithFormat:@"%@",_detailsArray[path.row][@"link"]];
        webView.title = NSLocalizedString(@"Learn More", @"Learn More");
        webView.weburl = webUrlLink;
        }
    }
}



@end
