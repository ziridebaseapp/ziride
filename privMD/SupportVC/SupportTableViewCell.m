//
//  SupportTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SupportTableViewCell.h"

@implementation SupportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    
    frame = [self subviews][0].frame;
    frame.size.width = screenWidth;
    [self subviews][0].frame = frame;
    
    if ([Helper isCurrentLanguageRTL]) {
        self.titleLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
