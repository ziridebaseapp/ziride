//
//  SupportViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SupportViewController.h"
#import "SupportDetailTableViewController.h"
#import "SupportWebViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "SupportTableViewCell.h"
#import "UILabel+DynamicHeight.h"

@interface SupportViewController ()<CustomNavigationBarDelegate> {
    NSMutableArray *listOfItemsArray;
    NSMutableArray *detailsOfListArray;
    NSString *navTitle;
    NSString *webUrlLink;
    SupportTableViewCell *cell;
    NSString *cellIdentifier;
}
@property (weak, nonatomic) IBOutlet UITableView *supportTable;

@end

@implementation SupportViewController

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    cellIdentifier = @"supportCell";
    listOfItemsArray = [NSMutableArray array];
    detailsOfListArray = [NSMutableArray array];
    [self addCustomNavigationBar];
    [self sendRequestToGetSupportDetails];
    [_supportTable reloadData];
}

-(void)sendRequestToGetSupportDetails {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{
                             @"ent_lan":[NSNumber numberWithInteger:[Utility selectedLangaugeID]]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"support"
                                    paramas:params
                                    onComplition:^(BOOL success, NSDictionary *response) {
                                   if (success) {
                                       [self getSupportDetailsResponse:response];
                                   } else {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   }
                               }];
}
/**
 Get Support Details response
 
 @param response return type void and takes no argument
 */
-(void) getSupportDetailsResponse:(NSDictionary *) response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (response == nil)
        return;
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([response [@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(response[@"errMsg"])];
    } else {
        listOfItemsArray = [response[@"support"]mutableCopy];
        [_supportTable reloadData];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom Navigation Bar -

/**
 Add custom Navigation Bar
 */
- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Support", @"Support")];
    [self.view addSubview:customNavigationBarView];
}


/**
 Navigation Left Button action

 @param sender leftBtn
 */
-(void)leftBarButtonClicked:(UIButton *)sender{
    
    [self menuButtonPressedAccount];
}

/**
 Menu Button Pressed Action
 */
- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listOfItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil) {
        cell = (SupportTableViewCell *)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    [self setUpCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(listOfItemsArray.count) {
        static SupportTableViewCell *cell2 = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell2 = [_supportTable dequeueReusableCellWithIdentifier:cellIdentifier];
        });
        [self setUpCell:cell2 atIndexPath:indexPath];
        CGFloat height = [cell2.titleLabel measureHeightLabel] + 2;
        if (height<50) {
            return 50;
        }
        return height;
    } else {
        return  0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    detailsOfListArray = listOfItemsArray[indexPath.row][@"childs"];
    if (detailsOfListArray.count == 0) {
        SupportWebViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportWeb"];
        webUrlLink = [NSString stringWithFormat:@"%@",listOfItemsArray[indexPath.row][@"link"]];
        VC.title = NSLocalizedString(@"Learn More", @"Learn More");
        VC.weburl = webUrlLink;
        [Helper checkForPush:self.navigationController.view];
        [self.navigationController pushViewController:VC animated:NO];
    } else {
        SupportDetailTableViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportDetailTable"];
        VC.detailsArray = [detailsOfListArray mutableCopy];
        VC.title = flStrForStr(listOfItemsArray[indexPath.row][@"tag"]);
        [Helper checkForPush:self.navigationController.view];
        [self.navigationController pushViewController:VC animated:NO];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Cell Setup

/**
 Set up cell

 @param cell1 Support TableView Cell
 @param indexPath IndexPath 
 */
- (void)setUpCell:(SupportTableViewCell *)cell1 atIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    cell1.titleLabel.text = listOfItemsArray[indexPath.row][@"tag"];
    CGFloat titleLabelHeight = [cell1.titleLabel measureHeightLabel];
    CGRect frame = cell1.titleLabel.frame;
    frame.origin.x = 30;
    frame.origin.y = (cell1.frame.size.height - titleLabelHeight)/2;
    frame.size.width = screenWidth - 60;
    frame.size.height = titleLabelHeight;
    cell1.titleLabel.frame = frame;
    
    cell1.lineView.backgroundColor = UIColorFromRGB(0xE4E7F0);
    frame = cell1.lineView.frame;
    frame.origin.x = 0;
    frame.origin.y = cell1.frame.size.height - 1;
    frame.size.width = screenWidth;
    frame.size.height = 1;
    cell1.lineView.frame = frame;
    [cell1 bringSubviewToFront:cell1.lineView];
}


@end
