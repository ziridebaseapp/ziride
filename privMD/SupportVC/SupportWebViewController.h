//
//  SupportWebViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportWebViewController : UIViewController<UIWebViewDelegate>

@property(nonatomic,strong) NSString *weburl;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property(strong,nonatomic) UIButton *backButton;
@property (nonatomic) NSInteger isComingFromVC;


@end
