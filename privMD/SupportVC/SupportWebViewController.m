//
//  SupportWebViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SupportWebViewController.h"

@interface SupportWebViewController () {
    BOOL isRequestSubmitted;
}

@end

@implementation SupportWebViewController
@synthesize webView,weburl;
@synthesize backButton;
@synthesize isComingFromVC;

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    isRequestSubmitted = NO;
    [self createNavLeftButton];
    webView.delegate= self;
    NSURL *url = [NSURL URLWithString:weburl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [webView loadRequest:requestObj];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationItem setHidesBackButton:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.webView stopLoading];
    self.webView.delegate = nil;
    self.navigationController.navigationBarHidden = YES;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *requestUrl = [request.URL absoluteString];
    if ([requestUrl rangeOfString:@"submit_help"].location != NSNotFound) {
        isRequestSubmitted = YES;
    }
    return YES;
}

#pragma mark - Custom Navigation Bar -

/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation Left Button Action
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UIWebView Delegates -

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if(isRequestSubmitted){
        isRequestSubmitted = NO;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"IssueRaised"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}


@end
