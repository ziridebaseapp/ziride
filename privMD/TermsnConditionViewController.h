//
//  TermsnConditionViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsnConditionViewController : UIViewController
@property (nonatomic) BOOL isCommingFromSignUp;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIButton *link1Button;
@property (strong, nonatomic) IBOutlet UIButton *link2Button;

@end
