//
//  TermsnConditionViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "TermsnConditionViewController.h"
#import "WebViewController.h"
@interface TermsnConditionViewController ()

@end

@implementation TermsnConditionViewController
@synthesize backgroundView;
@synthesize link1Button;
@synthesize link2Button;
@synthesize isCommingFromSignUp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    if (isCommingFromSignUp) {
        self.navigationItem.title = NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions");
    } else {
        self.navigationItem.title = NSLocalizedString(@"Legal", @"Legal");
    }
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = UIColorFromRGB(0xE4E7F0).CGColor;
    backgroundView.layer.cornerRadius = 4.0;
    backgroundView.layer.masksToBounds = YES;

    [link1Button setTitle:NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions") forState:UIControlStateNormal];
    [link1Button setTitle:NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions") forState:UIControlStateSelected];
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateNormal];
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateSelected];
    [link1Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [link2Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];    
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation Left Button Action
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}



#pragma mark - Custom Button Action -

/**
 This button is to open webview dependes

 @param sender linkBtn
 */
-(void)linkButtonClicked :(id)sender {
    WebViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebView"];
    UIButton *mBtn = (UIButton *)sender;
    if (mBtn.tag == 100) {
        VC.title = NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions");
        VC.weburl = termsAndCondition;
    }
    else if (mBtn.tag == 200) {
        VC.title = NSLocalizedString(@"Privacy Policy", @"Privacy Policy");
        VC.weburl = privacyPolicy;
    }
    [Helper checkForPush:self.navigationController.view];
    [self.navigationController pushViewController:VC animated:NO];
}

@end
