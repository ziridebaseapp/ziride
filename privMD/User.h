//
//  User.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserDelegate;
@interface User : NSObject
@property(nonatomic,weak)id <UserDelegate> delegate;
-(void)logout;
-(void)deleteUserSavedData;
- (void)updateUserSessionToken;

@end

@protocol UserDelegate <NSObject>

@optional

-(void)userDidLogoutSucessfully:(BOOL)sucess;
-(void)userDidFailedToLogout:(NSError*)error;
-(void)userDidUpdateSessionSucessfully:(BOOL)sucess;
-(void)userDidUpdateSessionUnSucessfully:(BOOL)sucess;

@end
