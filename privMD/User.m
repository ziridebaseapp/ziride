//
//  User.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "User.h"
#import "Database.h"
#import "PMDReachabilityWrapper.h"
#import "NetworkHandler.h"
#import "PubNubWrapper.h"

@implementation User
@synthesize delegate;

- (void)logout {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_user_type":@"2",
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodPassengerLogout
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       if (success)
                                           [self userLogoutResponse:response];
                                   }];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No Network", @"No Network")];
    }
}

- (void) userLogoutResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        [self deleteUserSavedData];
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            if (delegate && [delegate respondsToSelector:@selector(userDidFailedToLogout:)]) {
                [delegate userDidFailedToLogout:nil];
            }
        } else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
                [delegate userDidLogoutSucessfully:YES];
            }
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

-(void)deleteUserSavedData {
    NSString *deviceID = [Utility deviceID];
    NSString *pushToken = [Utility pushToken];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [ud synchronize];
    
    [Utility setDeviceID:deviceID];
    [Utility setPushToken:pushToken];
    
    [Database DeleteAllCard];
    [Database deleteAllSourceAddress];
    
    PubNubWrapper *pubNub = [PubNubWrapper sharedInstance];
    pubNub.delegate = nil;
}

- (void)updateUserSessionToken {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_user_type":@"2",
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateSession"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self updateSessionTokenResponse:response];
                                   else
                                       [self updateSessionTokenResponse:response];
                               }];
}

-(void)updateSessionTokenResponse:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0 && [[dictResponse objectForKey:@"errNum"] intValue] == 73) {
            if (delegate && [delegate respondsToSelector:@selector(userDidUpdateSessionSucessfully:)]) {
                [delegate userDidUpdateSessionSucessfully:YES];
            }
        } else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0 && [[dictResponse objectForKey:@"errNum"] intValue] == 89) {
            [[NSUserDefaults standardUserDefaults]setObject:[response objectForKey:@"token"] forKey:KDAcheckUserSessionToken];;
            [[NSUserDefaults standardUserDefaults]synchronize];
            if (delegate && [delegate respondsToSelector:@selector(userDidUpdateSessionSucessfully:)]) {
                [delegate userDidUpdateSessionSucessfully:YES];
            }
        } else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            if (delegate && [delegate respondsToSelector:@selector(userDidUpdateSessionUnSucessfully:)]) {
                [delegate userDidUpdateSessionUnSucessfully:NO];
            }
        }
    }
}

@end
