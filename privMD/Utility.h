//
//  Utility.h
//  PQDev
//
//  Created by 3Embed on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

typedef enum {
    kOperatingArea = 1,
    kNotOperatingArea = 2,
}OperatingAreaType;




+(void) setSessionToken:(NSString *)sessionToken;
+(void) setPushToken:(NSString *)pushToken;
+(void) setDeviceID:(NSString *)deviceID;
+(void) setCurrentLatitude:(NSString *)currentLatitude;
+(void) setCurrentLongitude:(NSString *)currentLongitude;
+(void) setCurrentCity:(NSString *)currentCity;
+(void) setCurrentCountry:(NSString *)currentCountry;
+(void) setSelectedLangaugeID:(NSInteger)selectedLangaugeID;

+(void) setUserID:(NSString *)userID;
+(void) setUserName:(NSString *)userName;
+(void) setUserEmailID:(NSString *)userEmailID;
+(void) setUserProfileImage:(NSString *)userProfileImage;
+(void) setWalletBalance:(NSString *)walletBalance;
+(void) setUserReferralCode:(NSString *)userReferralCode;
+(void) setUserShareMessage:(NSString *)userShareMessage;
+(void) setPubnubPublishKey:(NSString *)pubnubPublishKey;
+(void) setPubnubSubscribeKey:(NSString *)pubnubSubscribeKey;
+(void) setPaymentGatewayKey:(NSString *)paymentGatewayKey;
+(void) setUserPubnubChannel:(NSString *)userPubnubChannel;
+(void) setServerPubnubChannel:(NSString *)serverPubnubChannel;
+(void) setPresensePubnubChannel:(NSString *)presensePubnubChannel;
+(void) setDriverPubnubChannel:(NSString *)driverPubnubChannel;
+(void) setDistanceMatrixAPIKey:(NSString *)distanceMatrixAPIKey;

+(void) setIsNeedToPublishMessageOnPubnub:(BOOL)publish;

+(void) setSplashData:(NSDictionary *)splashData;

+(void) setDriverID:(NSString *)driverID;
+(void) setDriverEmailID:(NSString *)driverEmailID;
+(void) setDriverName:(NSString *)driverName;
+(void) setDriverMobileNumber:(NSString *)driverMobileNumber;
+(void) setDriverProfileImageURL:(NSString *)driverProfileImageURL;
+(void) setDriverRating:(NSString *)driverRating;
+(void) setDriverLatitude:(double)driverLatitude;
+(void) setDriverLongitude:(double)driverLongitude;

+(void) setBookingID:(NSString *)bookingID;
+(void) setBookingData:(NSDictionary *)bookingData;
+(void) setBookingDate:(NSString *)bookingDate;
+(void) setBookingStatus:(NSInteger)bookingStatus;
+(void) setIsUserInBooking:(BOOL)isUserInBooking;
+(void) setPromoCode:(NSString *)promoCode;
+(void) setBookingApproximateFare:(NSString *)bookingApproximateFare;

+(void) setVehicleImageURL:(NSString *)vehicleImageURL;
+(void) setVehicleMapImageURL:(NSString *)vehicleMapImageURL;
+(void) setVehicleMakeName:(NSString *)vehicleMakelName;
+(void) setVehicleModelName:(NSString *)vehicleModelName;
+(void) setVehicleNumberPlate:(NSString *)vehicleNumberPlate;
+(void) setVehicleColor:(NSString *)vehicleColor;

+(void) setFavouritePickUpLocation:(NSString *)favouritePickUpLocation;
+(void) setPickUpLocationAddressLine1:(NSString *)pickUpLocationAddressLine1;
+(void) setPickUpLocationAddressLine2:(NSString *)pickUpLocationAddressLine2;
+(void) setPickUpLatitude:(double)pickUpLatitude;
+(void) setPickUpLongitude:(double)pickUpLongitude;

+(void) setFavouriteDropOffLocation:(NSString *)favouriteDropOffLocation;
+(void) setDropOffLocationAddressLine1:(NSString *)dropOffLocationAddressLine1;
+(void) setDropOffLocationAddressLine2:(NSString *)dropOffLocationAddressLine2;
+(void) setDropOffLatitude:(double)dropOffLatitude;
+(void) setDropOffLongitude:(double)dropOffLongitude;

+(void) setMaximumETA:(NSInteger)maximumETA;
+(void) setBookingAmountAllowUpto:(double)bookingAmountAllowUpto;

+(void) setPubnubPublishTimeInterval:(NSInteger)pubnubPublishTimeInterval;
+(void) setPubnubPublishOnBookingTimeInterval:(NSInteger)pubnubPublishOnBookingTimeInterval;
+(void) setGoogleMatrixTimeInterval:(NSInteger)googleMatrixTimeInterval;
+(void) setRadiusForNotOperatingArea:(NSInteger)radiusForNotOperatingArea;
+(void) setRadiusForOperatingArea:(NSInteger)radiusForOperatingArea;
+(void) setDistanceThresholdForFareEstimate:(NSInteger)distanceThresholdForFareEstimate;
+(void) setOpratingTypeArea:(OperatingAreaType)operatingAreaType;
+(void) setGoogleKeysArray:(NSArray *)googleKeysArray;

    
+(NSString *)sessionToken;
+(NSString *)pushToken;
+(NSString *)deviceID;
+(NSString *)currentLatitude;
+(NSString *)currentLongitude;
+(NSString *)currentCity;
+(NSString *)currentCountry;
+(NSInteger)selectedLangaugeID;
+(NSString *)userID;
+(NSString *)userName;
+(NSString *)userEmailID;
+(NSString *)userProfileImage;
+(NSString *)walletBalance;
+(NSString *)userReferralCode;
+(NSString *)userShareMessage;
+(NSString *)pubnubPublishKey;
+(NSString *)pubnubSubscribeKey;
+(NSString *)paymentGatewayKey;
+(NSString *)userPubnubChannel;
+(NSString *)serverPubnubChannel;
+(NSString *)presensePubnubChannel;
+(NSString *)driverPubnubChannel;
+(NSString *)distanceMatrixAPIKey;

+(BOOL)isNeedToPublishMessageOnPubnub;

+(NSString *)driverID;
+(NSString *)driverEmailID;
+(NSString *)driverName;
+(NSString *)driverRating;
+(NSString *)driverMobileNumber;
+(NSString *)driverProfileImageURL;
+(double)driverLatitude;
+(double)driverLongitude;

+(NSDictionary *)splashData;

+(NSString *)bookingID;
+(NSDictionary *)bookingData;
+(NSString *)bookingDate;
+(NSInteger)bookingStatus;
+(BOOL)isUserInBooking;
+(NSString *)promoCode;
+(NSString *)bookingApproximateFare;

+(NSString *)vehicleImageURL;
+(NSString *)vehicleMapImageURL;
+(NSString *)vehicleMakelName;
+(NSString *)vehicleModelName;
+(NSString *)vehicleNumberPlate;
+(NSString *)vehicleColor;

+(NSString *)favouritePickUpLocation;
+(NSString *)pickUpLocationAddressLine1;
+(NSString *)pickUpLocationAddressLine2;
+(double)pickUpLatitude;
+(double)pickUpLongitude;

+(NSString *)favouriteDropOffLocation;
+(NSString *)dropOffLocationAddressLine1;
+(NSString *)dropOffLocationAddressLine2;
+(double)dropOffLatitude;
+(double)dropOffLongitude;

+(NSInteger)pubnubPublishTimeInterval;
+(NSInteger)pubnubPublishOnBookingTimeInterval;
+(NSInteger)googleMatrixTimeInterval;
+(NSInteger)radiusForNotOperatingArea;
+(NSInteger)radiusForOperatingArea;
+(NSInteger)distanceForOperatingArea;
+(OperatingAreaType)operatingAreaType;
+(NSArray *)googleKeysArray;
+(NSInteger)maximumETA;
+(double)bookingAmountAllowUpto;

@end
