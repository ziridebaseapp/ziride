//
//  Utility.m
//  PQDev
//
//  Created by 3Embed on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(instancetype) sharedInstance {
    static Utility *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Utility alloc] init];
    });
    return sharedInstance;
}

+(void) setSessionToken:(NSString *)sessionToken {
    [[NSUserDefaults standardUserDefaults] setObject:sessionToken forKey:kNSUUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setDeviceID:(NSString *)deviceID {
    [[NSUserDefaults standardUserDefaults] setObject:deviceID forKey:kNSUDeviceID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPushToken:(NSString *)pushToken {
    [[NSUserDefaults standardUserDefaults] setObject:pushToken forKey:kNSUPushToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setCurrentLatitude:(NSString *)currentLatitude {
    [[NSUserDefaults standardUserDefaults] setObject:currentLatitude forKey:kNSUCurrentLat];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setCurrentLongitude:(NSString *)currentLongitude {
    [[NSUserDefaults standardUserDefaults] setObject:currentLongitude forKey:kNSUCurrentLong];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setCurrentCity:(NSString *)currentCity {
    [[NSUserDefaults standardUserDefaults] setObject:currentCity forKey:kNSUserCurrentCity];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setCurrentCountry:(NSString *)currentCountry {
    [[NSUserDefaults standardUserDefaults] setObject:currentCountry forKey:kNSUserCurrentCountry];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserID:(NSString *)userID {
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:kNSUUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserName:(NSString *)userName {
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:kNSUUserName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserEmailID:(NSString *)userEmailID {
    [[NSUserDefaults standardUserDefaults] setObject:userEmailID forKey:kNSUUserEmailID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setWalletBalance:(NSString *)walletBalance {
    if (walletBalance.length == 0) {
        walletBalance = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:walletBalance forKey:kNSUWalletBalance];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserProfileImage:(NSString *)userProfileImage {
    [[NSUserDefaults standardUserDefaults] setObject:userProfileImage forKey:kNSUUserProfileImage];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserReferralCode:(NSString *)userReferralCode {
    [[NSUserDefaults standardUserDefaults] setObject:userReferralCode forKey:kNSUUserReferralCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(void) setUserShareMessage:(NSString *)userShareMessage {
    [[NSUserDefaults standardUserDefaults] setObject:userShareMessage forKey:kNSUUserShareMessage];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setSelectedLangaugeID:(NSInteger)selectedLangaugeID {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedLangaugeID forKey:kNSUSelectedLangaugeID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPubnubPublishKey:(NSString *)pubnubPublishKey {
    [[NSUserDefaults standardUserDefaults] setObject:pubnubPublishKey forKey:KNSUPubnubPublishKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPubnubSubscribeKey:(NSString *)pubnubSubscribeKey {
    [[NSUserDefaults standardUserDefaults] setObject:pubnubSubscribeKey forKey:kNSUPubnubSubscribeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPaymentGatewayKey:(NSString *)paymentGatewayKey {
    [[NSUserDefaults standardUserDefaults] setObject:paymentGatewayKey forKey:kNSUPaymentGatewayKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setUserPubnubChannel:(NSString *)userPubnubChannel {
    [[NSUserDefaults standardUserDefaults] setObject:userPubnubChannel forKey:kNSUUserPubnubChannel];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setServerPubnubChannel:(NSString *)serverPubnubChannel {
    [[NSUserDefaults standardUserDefaults] setObject:serverPubnubChannel forKey:kNSUServerPubnubChannel];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPresensePubnubChannel:(NSString *)presensePubnubChannel {
    [[NSUserDefaults standardUserDefaults] setObject:presensePubnubChannel forKey:kNSUPresensePubnubChannel];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverPubnubChannel:(NSString *)driverPubnubChannel {
    [[NSUserDefaults standardUserDefaults] setObject:driverPubnubChannel forKey:kNSUDriverPubnubChannel];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void)setDistanceMatrixAPIKey:(NSString *)distanceMatrixAPIKey {
    if (distanceMatrixAPIKey.length == 0) {
        distanceMatrixAPIKey = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:distanceMatrixAPIKey forKey:kNSUDistancematrixAPIKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setIsNeedToPublishMessageOnPubnub:(BOOL)publish {
    [[NSUserDefaults standardUserDefaults] setBool:publish forKey:kNSUIsNeedToPublishMessageOnPubnub];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setSplashData:(NSDictionary *)splashData{
    if (splashData == nil) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:splashData forKey:kNSUSplashData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverID:(NSString *)driverID {
    if ([driverID integerValue] == 0) {
        driverID = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverID forKey:kNSUDriverID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverEmailID:(NSString *)driverEmailID {
    if (driverEmailID.length == 0) {
        driverEmailID = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverEmailID forKey:kNSUDriverEmailID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverName:(NSString *)driverName {
    if (driverName.length == 0) {
        driverName = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverName forKey:kNSUDriverName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverMobileNumber:(NSString *)driverMobileNumber {
    if (driverMobileNumber.length == 0) {
        driverMobileNumber = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverMobileNumber forKey:kNSUDriverMobileNumber];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverProfileImageURL:(NSString *)driverProfileImageURL {
    if (driverProfileImageURL.length == 0) {
        driverProfileImageURL = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverProfileImageURL forKey:kNSUDriverProfileImageURL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverRating:(NSString *)driverRating {
    if (driverRating.length == 0) {
        driverRating = @"0";
    }
    [[NSUserDefaults standardUserDefaults] setObject:driverRating forKey:kNSUDriverRating];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setDriverLatitude:(double)driverLatitude {
    [[NSUserDefaults standardUserDefaults] setDouble:driverLatitude forKey:kNSUDriverLatitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDriverLongitude:(double)driverLongitude {
    [[NSUserDefaults standardUserDefaults] setDouble:driverLongitude forKey:kNSUDriverLongitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingID:(NSString *)bookingID {
    if (bookingID.length == 0) {
        bookingID = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:bookingID forKey:kNSUBookingID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingData:(NSDictionary *)bookingData {
    if (bookingData == nil) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:bookingData forKey:kNSUBookingData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingDate:(NSString *)bookingDate {
    if (bookingDate.length == 0) {
        bookingDate = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:bookingDate forKey:kNSUBookingDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingStatus:(NSInteger)bookingStatus {
    [[NSUserDefaults standardUserDefaults] setInteger:bookingStatus forKey:kNSUBookingStatus];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setIsUserInBooking:(BOOL)isUserInBooking {
    [[NSUserDefaults standardUserDefaults] setBool:isUserInBooking forKey:kNSUIsUserInBooking];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPromoCode:(NSString *)promoCode {
    if (promoCode.length == 0) {
        promoCode = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:promoCode forKey:kNSUPromoCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingApproximateFare:(NSString *)bookingApproximateFare {
    if (bookingApproximateFare.length == 0) {
        bookingApproximateFare = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:bookingApproximateFare forKey:kNSUBookingApproximateFare];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleImageURL:(NSString *)vehicleImageURL {
    if (vehicleImageURL.length == 0) {
        vehicleImageURL = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleImageURL forKey:kNSUVehicleImageURL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleMapImageURL:(NSString *)vehicleMapImageURL {
    if (vehicleMapImageURL.length == 0) {
        vehicleMapImageURL = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleMapImageURL forKey:kNSUVehicleMapImageURL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleMakeName:(NSString *)vehicleMakelName {
    if (vehicleMakelName.length == 0) {
        vehicleMakelName = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleMakelName forKey:kNSUVehicleMakeName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleModelName:(NSString *)vehicleModelName {
    if (vehicleModelName.length == 0) {
        vehicleModelName = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleModelName forKey:kNSUVehicleModelName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleNumberPlate:(NSString *)vehicleNumberPlate {
    if (vehicleNumberPlate.length == 0) {
        vehicleNumberPlate = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleNumberPlate forKey:kNSUVehicleNumberPlate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setVehicleColor:(NSString *)vehicleColor {
    if (vehicleColor.length == 0) {
        vehicleColor = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:vehicleColor forKey:kNSUVehicleColor];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setFavouritePickUpLocation:(NSString *)favouritePickUpLocation {
    if (favouritePickUpLocation.length == 0) {
        favouritePickUpLocation = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:favouritePickUpLocation forKey:kNSUFavouritePickUpLocation];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setPickUpLocationAddressLine1:(NSString *)pickUpLocationAddressLine1 {
    if (pickUpLocationAddressLine1.length == 0) {
        pickUpLocationAddressLine1 = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:pickUpLocationAddressLine1 forKey:kNSUPickUpLocationAddressLine1];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPickUpLocationAddressLine2:(NSString *)pickUpLocationAddressLine2 {
    if (pickUpLocationAddressLine2.length == 0) {
        pickUpLocationAddressLine2 = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:pickUpLocationAddressLine2 forKey:kNSUPickUpLocationAddressLine2];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPickUpLatitude:(double)pickUpLatitude {
    [[NSUserDefaults standardUserDefaults] setDouble:pickUpLatitude forKey:kNSUPickUpLatitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPickUpLongitude:(double)pickUpLongitude {
    [[NSUserDefaults standardUserDefaults] setDouble:pickUpLongitude forKey:kNSUPickUpLongitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setFavouriteDropOffLocation:(NSString *)favouriteDropOffLocation {
    if (favouriteDropOffLocation.length == 0) {
        favouriteDropOffLocation = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:favouriteDropOffLocation forKey:kNSUFavouriteDropOffLocation];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDropOffLocationAddressLine1:(NSString *)dropOffLocationAddressLine1 {
    if (dropOffLocationAddressLine1.length == 0) {
        dropOffLocationAddressLine1 = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:dropOffLocationAddressLine1 forKey:kNSUDropOffLocationAddressLine1];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDropOffLocationAddressLine2:(NSString *)dropOffLocationAddressLine2 {
    if (dropOffLocationAddressLine2.length == 0) {
        dropOffLocationAddressLine2 = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:dropOffLocationAddressLine2 forKey:kNSUDropOffLocationAddressLine2];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDropOffLatitude:(double)dropOffLatitude {
    [[NSUserDefaults standardUserDefaults] setDouble:dropOffLatitude forKey:kNSUDropOffLatitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDropOffLongitude:(double)dropOffLongitude {
    [[NSUserDefaults standardUserDefaults] setDouble:dropOffLongitude forKey:kNSUDropOffLongitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setPubnubPublishTimeInterval:(NSInteger)pubnubPublishTimeInterval {
    [[NSUserDefaults standardUserDefaults] setInteger:pubnubPublishTimeInterval forKey:kNSUPubnubPublishTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setPubnubPublishOnBookingTimeInterval:(NSInteger)pubnubPublishOnBookingTimeInterval {
    [[NSUserDefaults standardUserDefaults] setInteger:pubnubPublishOnBookingTimeInterval forKey:kNSUPubnubPublishOnBookingTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setGoogleMatrixTimeInterval:(NSInteger)googleMatrixTimeInterval {
    [[NSUserDefaults standardUserDefaults] setInteger:googleMatrixTimeInterval forKey:kNSUGoogleMatrixTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setRadiusForNotOperatingArea:(NSInteger)radiusForNotOperatingArea {
    [[NSUserDefaults standardUserDefaults] setInteger:radiusForNotOperatingArea forKey:kNSURadiusForNotOperatingArea];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setRadiusForOperatingArea:(NSInteger)radiusForOperatingArea {
    [[NSUserDefaults standardUserDefaults] setInteger:radiusForOperatingArea forKey:kNSURadiusForOperatingArea];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setDistanceThresholdForFareEstimate:(NSInteger)distanceThresholdForFareEstimate {
    [[NSUserDefaults standardUserDefaults] setInteger:distanceThresholdForFareEstimate forKey:kNSUDistanceThresholdForFareEstimate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setOpratingTypeArea:(OperatingAreaType)operatingAreaType {
    [[NSUserDefaults standardUserDefaults] setInteger:operatingAreaType forKey:kNSUOperatingAreaType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+(void) setGoogleKeysArray:(NSArray *)googleKeysArray {
    [[NSUserDefaults standardUserDefaults] setObject:googleKeysArray forKey:kNSUGoogleKeysArray];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setMaximumETA:(NSInteger)maximumETA {
    [[NSUserDefaults standardUserDefaults] setInteger:maximumETA forKey:kNSUMaximumETA];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setBookingAmountAllowUpto:(double)bookingAmountAllowUpto {
    [[NSUserDefaults standardUserDefaults] setDouble:bookingAmountAllowUpto forKey:kNSUBookingAmountAllowUpto];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



+(NSString *)sessionToken {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserSessionToken] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserSessionToken];
    }
    return @"";
}

+(NSString *)deviceID {
    if (IS_SIMULATOR) {
        return kNSUTestDeviceidKey;
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDeviceID] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDeviceID];
    }
    return @"";
}

+(NSString *)pushToken {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPushToken] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPushToken];
    }
    return @"";
}

+(NSString *)currentLatitude {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUCurrentLat] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUCurrentLat];
    }
    return @"";
}

+(NSString *)currentLongitude {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUCurrentLong] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUCurrentLong];
    }
    return @"";
}

+(NSString *)currentCity {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUserCurrentCity] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUserCurrentCity];
    }
    return @"";
}

+(NSString *)currentCountry {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUserCurrentCountry] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUserCurrentCountry];
    }
    return @"";
}

+(NSInteger)selectedLangaugeID {
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUSelectedLangaugeID];
}


+(NSString *)userID {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserID] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserID];
    }
    return @"";
}

+(NSString *)userName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserName] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserName];
    }
    return @"";
}

+(NSString *)userEmailID {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserEmailID] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserEmailID];
    }
    return @"";
}

+(NSString *)userProfileImage {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserProfileImage] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserProfileImage];
    }
    return @"";
}

+(NSString *)walletBalance {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUWalletBalance] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUWalletBalance];
    }
    return @"";
}

+(NSString *)userReferralCode {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserReferralCode] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserReferralCode];
    }
    return @"";
}

+(NSString *)userShareMessage {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserShareMessage] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserShareMessage];
    }
    return @"";
}


+(NSString *)pubnubPublishKey {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KNSUPubnubPublishKey] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:KNSUPubnubPublishKey];
    }
    return @"";
}

+(NSString *)pubnubSubscribeKey {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPubnubSubscribeKey] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPubnubSubscribeKey];
    }
    return @"";
}

+(NSString *)paymentGatewayKey {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPaymentGatewayKey] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPaymentGatewayKey];
    }
    return @"";
}

+(NSString *)userPubnubChannel {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel];
    }
    return @"";
}

+(NSString *)serverPubnubChannel {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUServerPubnubChannel] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUServerPubnubChannel];
    }
    return @"";
}

+(NSString *)presensePubnubChannel {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPresensePubnubChannel] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPresensePubnubChannel];
    }
    return @"";
}

+(NSString *)driverPubnubChannel {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel];
    }
    return @"";
}

+(NSString *)distanceMatrixAPIKey {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDistancematrixAPIKey] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDistancematrixAPIKey];
    }
    return @"";
}

+(BOOL)isNeedToPublishMessageOnPubnub {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsNeedToPublishMessageOnPubnub];
}

+(NSDictionary *)splashData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUSplashData] != nil) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUSplashData];
    }
    return nil;
}

+(NSString *)driverID {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverID] integerValue] > 0) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverID];
    }
    return @"";
}

+(NSString *)driverEmailID {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverEmailID] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverEmailID];
    }
    return @"";
}

+(NSString *)driverName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverName] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverName];
    }
    return @"";
}

+(NSString *)driverMobileNumber {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverMobileNumber] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverMobileNumber];
    }
    return @"";
}

+(NSString *)driverProfileImageURL {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverProfileImageURL] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverProfileImageURL];
    }
    return @"";
}

+(NSString *)driverRating {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverRating] floatValue] > 0) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverRating];
    }
    return @"0";
}


+(double)driverLatitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUDriverLatitude];
}

+(double)driverLongitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUDriverLongitude];
}

+(NSString *)bookingID {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingID] integerValue] > 0) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingID];
    }
    
    return @"";
}

+(NSDictionary *)bookingData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingData] != nil) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingData];
    }
    return nil;
}


+(NSString *)bookingDate {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingDate] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingDate];
    }
    return @"";
}

+(NSInteger)bookingStatus {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUBookingStatus] > 0) {
        return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUBookingStatus];
    }
    return 0;
}

+(NSString *)promoCode {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPromoCode] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPromoCode];
    }
    return @"";
}

+(NSString *)bookingApproximateFare {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingApproximateFare] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUBookingApproximateFare];
    }
    return @"";
}

+(BOOL)isUserInBooking {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsUserInBooking];
}

+(NSString *)vehicleImageURL {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleImageURL] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleImageURL];
    }
    return @"";
}

+(NSString *)vehicleMapImageURL {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleMapImageURL] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleMapImageURL];
    }
    return @"";
}

+(NSString *)vehicleMakelName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleMakeName] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleMakeName];
    }
    return @"";
}

+(NSString *)vehicleModelName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleModelName] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleModelName];
    }
    return @"";
}

+(NSString *)vehicleNumberPlate {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleNumberPlate] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleNumberPlate];
    }
    return @"";
}

+(NSString *)vehicleColor {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleColor] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUVehicleColor];
    }
    return @"";
}

+(NSString *)favouritePickUpLocation {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUFavouritePickUpLocation] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUFavouritePickUpLocation];
    }
    return @"";
}

+(NSString *)pickUpLocationAddressLine1 {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPickUpLocationAddressLine1] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPickUpLocationAddressLine1];
    }
    return @"";
}

+(NSString *)pickUpLocationAddressLine2 {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPickUpLocationAddressLine2] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPickUpLocationAddressLine2];
    }
    return @"";
}

+(double)pickUpLatitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUPickUpLatitude];
}

+(double)pickUpLongitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUPickUpLongitude];
}

+(NSString *)favouriteDropOffLocation {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUFavouriteDropOffLocation] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUFavouriteDropOffLocation];
    }
    return @"";
}

+(NSString *)dropOffLocationAddressLine1 {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDropOffLocationAddressLine1] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDropOffLocationAddressLine1];
    }
    return @"";
}

+(NSString *)dropOffLocationAddressLine2 {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDropOffLocationAddressLine2] length]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDropOffLocationAddressLine2];
    }
    return @"";
}

+(double)dropOffLatitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUDropOffLatitude];
}

+(double)dropOffLongitude {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kNSUDropOffLongitude];
}

+(NSInteger)pubnubPublishTimeInterval {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUPubnubPublishTimeInterval] <= 0) {
        return  5;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUPubnubPublishTimeInterval];
}

+(NSInteger)pubnubPublishOnBookingTimeInterval {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUPubnubPublishOnBookingTimeInterval] <= 0) {
        return  5;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUPubnubPublishOnBookingTimeInterval];
}

+(NSInteger)googleMatrixTimeInterval {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUGoogleMatrixTimeInterval] <= 0) {
        return  5;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUGoogleMatrixTimeInterval];
}

+(NSInteger)radiusForNotOperatingArea {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSURadiusForNotOperatingArea] <= 0) {
        return  5;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSURadiusForNotOperatingArea];
}

+(NSInteger)radiusForOperatingArea {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUGoogleMatrixTimeInterval] <= 0) {
        return  5;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSURadiusForOperatingArea];
}

+(NSInteger)distanceForOperatingArea{
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUDistanceThresholdForFareEstimate] <= 0) {
        return  500;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUDistanceThresholdForFareEstimate];
}

+(OperatingAreaType)operatingAreaType {
    return (OperatingAreaType)[[NSUserDefaults standardUserDefaults] integerForKey:kNSUOperatingAreaType];
}

+(NSArray *)googleKeysArray {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUGoogleKeysArray] != nil) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kNSUGoogleKeysArray];
    }
    return nil;
}

+(NSInteger)maximumETA {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUMaximumETA] == 0) {
        return  0;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUMaximumETA];
}

+(double)bookingAmountAllowUpto {
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:kNSUBookingAmountAllowUpto] == 0) {
        return  0.0;
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:kNSUBookingAmountAllowUpto];
}


@end
