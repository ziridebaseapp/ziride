//
//  VerifyiMobileViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RPButton.h"
#import "AmazonTransfer.h"
#import "UploadImagesToAmazonServer.h"

@interface VerifyiMobileViewController : UIViewController <UploadImageOnAWSDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *supportView;
@property (weak, nonatomic) IBOutlet UIView *coloredLineView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *textFleildsView;
@property (strong, nonatomic) IBOutlet UITextField *firstOtpField;
@property (strong, nonatomic) IBOutlet UITextField *secondOtpField;
@property (strong, nonatomic) IBOutlet UITextField *thirdOtpField;
@property (strong, nonatomic) IBOutlet UITextField *fourthOtpField;
@property (weak, nonatomic) IBOutlet UITextField *fifthOtpField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)resendBtnAction:(id)sender;
- (IBAction)nextBtnAction:(id)sender;

@property (strong,nonatomic)  UITextField *activeTextField;
@property (strong, nonatomic)  NSArray *getSignupDetails;
@property (strong,nonatomic)   NSArray *getInfoDetails;
@property (strong, nonatomic) UIImage *pickedImage;
@property (strong, nonatomic) NSString *myNumber;

@end
