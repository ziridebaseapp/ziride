//
//  VerifyiMobileViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.//

#import "VerifyiMobileViewController.h"
#import "MenuViewController.h"
#import <Stripe/Stripe.h>

@interface VerifyiMobileViewController () {
    float initialOffset_Y;
    float keyboardHeight;
}
@property (nonatomic, retain) UIToolbar *keyboardToolbar;

@end

@implementation VerifyiMobileViewController
@synthesize mainScrollView, supportView;
@synthesize activeTextField, keyboardToolbar;
@synthesize messageLabel, coloredLineView, lineView, label1, label2, label3;
@synthesize textFleildsView, firstOtpField, secondOtpField, thirdOtpField, fourthOtpField, fifthOtpField;
@synthesize nextBtn;


#pragma mark - UILife Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBarHidden = NO;
    [self createNavView];
    [self createNavLeftButton];
    [self setRegisterStepsFrame];
    [self updateUI];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void) viewWillAppear:(BOOL)animated {
    NSString *phoneNumber = _myNumber;
    if ([Helper isCurrentLanguageRTL]) {
        phoneNumber = [NSString stringWithFormat:@"%@+", [phoneNumber substringFromIndex:1]];
    }
    messageLabel.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"A verification code has been sent via SMS to the phone number “", @"A verification code has been sent via SMS to the phone number “"), phoneNumber, NSLocalizedString(@"” , please enter the number here", @"” , please enter the number here")];
    self.navigationItem.hidesBackButton = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    mainScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64);
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(nextBtn.frame) + 64)];
    initialOffset_Y = mainScrollView.contentOffset.y;
    keyboardHeight = 260;
    [firstOtpField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    self.navigationItem.titleView  = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITapGesture Recogniser -

/**
 Dismiss keyboard
 */
-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [firstOtpField resignFirstResponder];
    [secondOtpField resignFirstResponder];
    [thirdOtpField resignFirstResponder];
    [fourthOtpField resignFirstResponder];
    [fifthOtpField resignFirstResponder];
}

#pragma mark - NavigationBar Custom Methods -

/**
 Creates navigation view
 */
-(void)createNavView {
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80, 0, 160, 44)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 160, 44)];
    navTitle.text = NSLocalizedString(@"Register", @"Register");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:fontNormal size:15];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}

/**
 Creates navigation left button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation left button action
 */
-(void)cancelButtonClicked {
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma  mark - Custom Methods -

/**
 Sets steps label in align and adjust scrollview frame and content size
 */
-(void) setRegisterStepsFrame {
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    mainScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    supportView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    coloredLineView.frame = CGRectMake(15, 20, screenWidth - 30, 1);
    label1.frame = CGRectMake(12.5, 10, 20, 20);
    label2.frame = CGRectMake(screenWidth - 32.5 , 10, 20, 20);
    label1.layer.cornerRadius = 10;
    label1.layer.masksToBounds  = YES;
    label2.layer.cornerRadius = 10;
    label2.layer.masksToBounds  = YES;
    CGRect frame = textFleildsView.frame;
    frame.size.width = 320;
    frame.origin.x = (screenWidth - 320)/2;
    textFleildsView.frame = frame;
}

-(void) updateUI {
    if ([Helper isCurrentLanguageRTL]) {
        CGFloat screenWidth = self.view.frame.size.width;        
        CGRect frame = self.coloredLineView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.coloredLineView.frame = frame;
        
        frame = self.lineView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.lineView.frame = frame;

        
        frame = self.label1.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.label1.frame = frame;
        
        frame = self.label2.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.label2.frame = frame;
    }
}

/**
 Clear all textfields
 */
-(void) clearAllTextfields {
    firstOtpField.text = @"";
    secondOtpField.text = @"";
    thirdOtpField.text = @"";
    fourthOtpField.text = @"";
    fifthOtpField.text = @"";
}

/**
 Resend OTP API Call

 @param sender resend button
 */
- (IBAction)resendBtnAction:(id)sender {
    [self.view endEditing:YES];
    [self dismissKeyboard];
    [self clearAllTextfields];
    [self getVerificationCode];
}


#pragma mark - UITextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    textField.text = @"";
    if(keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)] ;
        [keyboardToolbar setBarStyle:UIBarStyleDefault];
        [keyboardToolbar sizeToFit];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        NSArray *itemsArray;
        if(textField.tag != 5) {
            UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", @"Next")  style:UIBarButtonItemStyleDone target:self action:@selector(textFieldShouldReturn:)];
            nextButton.tintColor = UIColorFromRGB(0x019F6E);
            itemsArray = [NSArray arrayWithObjects:flexButton,nextButton, nil];
        } else {
            UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(textFieldShouldReturn:)];
            doneButton.tintColor = UIColorFromRGB(0x019F6E);
            itemsArray = [NSArray arrayWithObjects:flexButton,doneButton, nil];
        }
        [keyboardToolbar setItems:itemsArray];
        [UIView commitAnimations];
    }
    [textField setInputAccessoryView:keyboardToolbar];
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isKindOfClass:[UIBarButtonItem class]]) {
        textField = activeTextField;
    }
    [self findNextResponder:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL shouldProcess = NO; //default to reject
    BOOL shouldMoveToNextField = NO; //default to remaining on the current field
    NSUInteger insertStringLength = [string length];
    if(insertStringLength == 0){ //backspace
        shouldProcess = YES; //Process if the backspace character was pressed
    } else {
        if([[textField text] length] == 0) {
            shouldProcess = YES; //Process if there is only 1 character right now
        }
    }
    //here we deal with the UITextField on our own
    if(shouldProcess){
        //grab a mutable copy of what's currently in the UITextField
        NSMutableString* mstring = [[textField text] mutableCopy];
        if([mstring length] == 0){
            //nothing in the field yet so append the replacement string
            [mstring appendString:string];
            shouldMoveToNextField = YES;
        } else{
            //adding a char or deleting?
            if(insertStringLength > 0){
                [mstring insertString:string atIndex:range.location];
            } else {
                //delete case - the length of replacement string is zero for a delete
                [mstring deleteCharactersInRange:range];
            }
        }
        //set the text now
        [textField setText:mstring];
        if (shouldMoveToNextField) {
            //MOVE TO NEXT INPUT FIELD HERE
            [self findNextResponder:textField];
        }
    }
    //always return no since we are manually changing the text field
    return NO;
}

-(void)findNextResponder:(UITextField *)textField {
    switch (textField.tag) {
        case 1:
            if(textField == firstOtpField) {
                [secondOtpField becomeFirstResponder];
            }
            break;
        case 2:
            if(textField == secondOtpField) {
                [thirdOtpField becomeFirstResponder];
            }
            break;
        case 3:
            if (textField == thirdOtpField) {
                [fourthOtpField becomeFirstResponder];
            }
            break;
        case 4:
            if (textField == fourthOtpField) {
                keyboardToolbar = nil;
                [fifthOtpField becomeFirstResponder];
            }
            break;
        case 5:
            if (textField == fifthOtpField) {
                [textField resignFirstResponder];
                [self nextBtnAction:nil];
            }
            break;
        default:
            [textField resignFirstResponder];
            break;
    }
}
#pragma mark -UIButton Action -

/**
 Next Button to Sign Up

 @param sender Next Button
 */
- (IBAction)nextBtnAction:(id)sender {
    nextBtn.userInteractionEnabled = NO;
    [self dismissKeyboard];
    if([firstOtpField.text length] && [secondOtpField.text length] && [thirdOtpField.text length] && [fourthOtpField.text length] && [fifthOtpField.text length]) {
        [self validateUserMobileNumber];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please fill the OTP!",@"Please fill the OTP!")];
        nextBtn.userInteractionEnabled = YES;
    }
}

#pragma mark - WebServices -

/**
 API for get verification code
 */
-(void)getVerificationCode {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    NSDictionary *params = @{
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_mobile":_myNumber,
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self validateVerificationCodeResponse:response];
                               }];
}

/**
 Response for Get verification code API

 @param response response data
 */
-(void)validateVerificationCodeResponse :(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response [@"errMsg"]];
    }
}

/**
 Verify mobile number with entered OTP
 */
- (void) validateUserMobileNumber {
    if([firstOtpField.text length] && [secondOtpField.text length] && [thirdOtpField.text length] && [fourthOtpField.text length] && [fifthOtpField.text length]) {
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Fill the OTP!",@"Please Fill the OTP!")];
        return;
    }
    NSString *otpNumber = [NSString stringWithFormat:@"%@%@%@%@%@", firstOtpField.text, secondOtpField.text,thirdOtpField.text, fourthOtpField.text, fifthOtpField.text];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Verifying Number...", @"Verifying Number...")];
    NSDictionary *params = @{
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_phone":flStrForStr(_myNumber),
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_code":flStrForStr(otpNumber)
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyPhone"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       if (!response) {
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       } else if ([response[@"errFlag"]integerValue] == 0) {
                                           if(_pickedImage) {
                                               [UploadImagesToAmazonServer sharedInstance].delegate = self;
                                               [[UploadImagesToAmazonServer sharedInstance] uploadProfileImageToServer:_pickedImage andMobile:_myNumber andType:1];
                                           } else {
                                               [self sendServiceToSignUpUser];
                                           }
                                       } else if ([response[@"errFlag"]integerValue] == 1) {
                                           [self clearAllTextfields];
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response [@"errMsg"]];
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                           nextBtn.userInteractionEnabled = YES;
                                       }
                                   }
                               }];
}


/**
 API call for Sign UP
 */
-(void)sendServiceToSignUpUser {
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Registering...", @"Registering...")];
    NSDictionary *params = @{KDASignUpFirstName:[_getSignupDetails objectAtIndex:0],
                             KDASignUpLastName:@"",
                             KDASignUpEmail:[_getSignupDetails objectAtIndex:1],
                             KDASignUpMobile:[_getSignupDetails objectAtIndex:2],
                             KDASignUpPassword:[_getSignupDetails objectAtIndex:3],
                             KDASignUpReferralCode:[_getSignupDetails objectAtIndex:4],
                             KDASignUpDeviceId:[Utility deviceID],
                             KDASignUpAccessToken:@"",
                             KDASignUpCity:[Utility currentCity],
                             KDASignUpCountry:[Utility currentCountry],
                             KDASignUpPushToken:[Utility pushToken],
                             KDASignUpLatitude:[Utility currentLatitude],
                             KDASignUpLongitude:[Utility currentLongitude],
                             KDASignUpTandC:@"1",
                             KDASignUpPricing:@"1",
                             KDASignUpDeviceType:@"1",
                             KDASignUpLanguage:[NSNumber numberWithInteger:[Utility selectedLangaugeID]],
                             @"ent_login_type":[_getSignupDetails objectAtIndex:5],
                             @"ent_country_code":[_getSignupDetails objectAtIndex:6],
                             @"ent_gender": @"",
                             @"ent_dob":@"",
                             KDASignUpDateTime:[Helper getCurrentNetworkDateTime],
                             @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                             @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                             @"ent_propic":[Utility userProfileImage],
                             @"ent_dev_model":[Helper getModelName]
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientSignUp
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response) {
                                   if (success)
                                       [self signupResponse:response];
                               }];
}


/**
 Sign Up API response

 @param response response data
 */
-(void)signupResponse:(NSDictionary*)response {
    nextBtn.userInteractionEnabled = YES;
    if(response == nil) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        return;
    } else {
        NSDictionary *itemList = [[NSDictionary alloc]init];
        itemList = [response mutableCopy];
        if ([itemList[@"errFlag"] intValue] == 0 || ([itemList[@"errFlag"] intValue] == 1 && [itemList[@"errNum"] intValue] == 115)) {
            if ([itemList[@"errFlag"] integerValue] == 1) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:flStrForStr(itemList[@"errMsg"])];
            }
            [Utility setSessionToken:flStrForStr(itemList[@"token"])];
            
            [Utility setPubnubPublishKey:flStrForStr(itemList[@"pub"])];
            [Utility setPubnubSubscribeKey:flStrForStr(itemList[@"sub"])];
            
            [Utility setUserPubnubChannel:flStrForStr(itemList[@"chn"])];
            [Utility setServerPubnubChannel:flStrForStr(itemList[@"serverChn"])];
            
            [Utility setUserID:flStrForStr(itemList[@"sid"])];
            [Utility setUserName:flStrForStr(self.getSignupDetails[0])];
            [Utility setUserEmailID:flStrForStr(itemList[@"email"])];
            [Utility setUserProfileImage:flStrForStr(itemList[@"profilePic"])];
            [Utility setUserReferralCode:flStrForStr(itemList[@"coupon"])];
            [Utility setUserShareMessage:flStrForStr(itemList[@"shareMessage"])];
            
            [Utility setWalletBalance:[Helper getCurrencyLocal:[flStrForStr(itemList[@"walletAmt"]) floatValue]]];            
            NSDictionary *configData = [response[@"configData"] mutableCopy];
            
            [Utility setGoogleMatrixTimeInterval:[configData[@"DistanceMatrixInterval"] integerValue]];
            [Utility setPubnubPublishTimeInterval:[configData[@"PubnubIntervalHome"] integerValue]];
            [Utility setPubnubPublishOnBookingTimeInterval:[configData[@"PubnubIntervalDispatch"] integerValue]];
            [Utility setRadiusForOperatingArea:[configData[@"RadiusOperating"] integerValue]];
            [Utility setRadiusForNotOperatingArea:[configData[@"RadiusNotOperating"] integerValue]];
            [Utility setMaximumETA:[configData[@"ETAThreshold"] integerValue]*60];
            [Utility setBookingAmountAllowUpto:[configData[@"BookingAmountAllowUpto"] doubleValue]];
            
            NSMutableArray *googleKeysArray = [configData[@"GoogleKeysArray"] mutableCopy];
            NSString *distanceMatrixKey = [googleKeysArray firstObject];
            [Utility setDistanceMatrixAPIKey:flStrForStr(distanceMatrixKey)];
            [googleKeysArray removeObjectAtIndex:0];
            [Utility setGoogleKeysArray:googleKeysArray];
            
            [Utility setPaymentGatewayKey:flStrForStr(configData[@"stipeKey"])];
            if([Utility paymentGatewayKey]) {
                [Stripe setDefaultPublishableKey:[Utility paymentGatewayKey]];
            }

            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:kNSUIsNewSignUp];
            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc] initWithArray:itemList[@"types"]];
            if (!carTypes || !carTypes.count){
                [Utility setOpratingTypeArea:kNotOperatingArea];
            } else {
                [Utility setOpratingTypeArea:kOperatingArea];
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:kNSUCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            if([[Utility sessionToken] length] == 0) {
                return;
            }
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        }
        else {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:itemList[@"errMsg"]];
        }
    }
}


#pragma mark - UploadFileDelegate -

-(void)uploadFileDidUploadSuccessfullyWithUrl:(NSString*) imageURL{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (imageURL) {
        [[SDImageCache sharedImageCache] removeImageForKey:imageURL fromDisk:YES];
        [Utility setUserProfileImage:flStrForStr(imageURL)];
        [self sendServiceToSignUpUser];
    }
}

-(void)uploadFileDidFailedWithError:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [Helper showAlertWithTitle:NSLocalizedString(@"Oops!", @"Oops!") Message:NSLocalizedString(@"Your profile photo failed to update.", @"Your profile photo failed to update.")];
    [self sendServiceToSignUpUser];
}


#pragma mark - Keyboard Notifications -

/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [keyboardToolbar removeFromSuperview];
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 10;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(nextBtn.frame) + 64 + height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    CGFloat screenWidth = self.view.frame.size.width;
    mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(nextBtn.frame) + 64);
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}

@end
