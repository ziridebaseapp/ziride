//
//  WalletHistoryViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletHistoryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;
- (IBAction)allBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *moneyInBtn;
- (IBAction)moneyInBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *moneyOutBtn;
- (IBAction)moneyOutBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *slider;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIScrollView *tableBackgroundScrollView;
@property (weak, nonatomic) IBOutlet UITableView *allHistoryTableView;
@property (weak, nonatomic) IBOutlet UITableView *moneyInTableView;
@property (weak, nonatomic) IBOutlet UITableView *moneyOutTableView;

@end
