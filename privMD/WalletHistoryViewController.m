
//
//  WalletHistoryViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "WalletHistoryViewController.h"
#import "walletHistoryTableViewCell.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"
#import "User.h"

@interface WalletHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CustomNavigationBarDelegate> {
    int indexNumber;
    NSMutableArray *walletAllHistory;
    NSMutableArray *walletInHistory;
    NSMutableArray *walletOutHistory;
}

@end

@implementation WalletHistoryViewController
@synthesize tableBackgroundScrollView;
@synthesize allHistoryTableView, moneyInTableView, moneyOutTableView;
@synthesize allBtn, moneyInBtn, moneyOutBtn , slider;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    walletAllHistory = [[NSMutableArray alloc] init];
    walletInHistory = [[NSMutableArray alloc] init];
    walletOutHistory = [[NSMutableArray alloc] init];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    tableBackgroundScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight-self.footerView.frame.origin.y);
    [tableBackgroundScrollView setContentSize:CGSizeMake(screenWidth*3, screenHeight-self.footerView.frame.origin.y)];
    tableBackgroundScrollView.contentOffset = CGPointMake(0, 0);
    [self updateUI];
    [self.allBtn setSelected:YES];
    [self.moneyInBtn setSelected:NO];
    [self.moneyOutBtn setSelected:NO];

    indexNumber = 0;
    [self getWalletHistory:[NSString stringWithFormat:@"%d", indexNumber]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
}

- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Recent Transactions", @"Recent Transactions")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = NO;
    [Helper checkForPop:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Custom Method -

-(void) updateUI {
    CGFloat screenWidth = self.view.frame.size.width;
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = moneyOutBtn.frame;
        frame.origin.x = 0;
        frame.size.width = screenWidth/3;
        moneyOutBtn.frame = frame;
        
        frame = moneyInBtn.frame;
        frame.origin.x = screenWidth/3;
        frame.size.width = screenWidth/3;
        moneyInBtn.frame = frame;
        
        frame = allBtn.frame;
        frame.origin.x = 2*screenWidth/3;
        frame.size.width = screenWidth/3;
        allBtn.frame = frame;
        
        frame = slider.frame;
        frame.origin.x = 2*screenWidth/3;
        frame.size.width = screenWidth/3;
        slider.frame = frame;
        
        frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 2 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];

        moneyOutTableView.frame = CGRectMake(0, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
        moneyInTableView.frame = CGRectMake(screenWidth, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
        allHistoryTableView.frame = CGRectMake(screenWidth*2, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
        
    } else {
        CGRect frame = allBtn.frame;
        frame.origin.x = 0;
        frame.size.width = screenWidth/3;
        allBtn.frame = frame;
        
        frame = moneyInBtn.frame;
        frame.origin.x = screenWidth/3;
        frame.size.width = screenWidth/3;
        moneyInBtn.frame = frame;
        
        frame = moneyOutBtn.frame;
        frame.origin.x = 2*screenWidth/3;
        frame.size.width = screenWidth/3;
        moneyOutBtn.frame = frame;
        
        frame = slider.frame;
        frame.origin.x = 0;
        frame.size.width = screenWidth/3;
        slider.frame = frame;
        
        frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
        
        allHistoryTableView.frame = CGRectMake(0, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
        moneyInTableView.frame = CGRectMake(screenWidth, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
        moneyOutTableView.frame = CGRectMake(screenWidth*2, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
    }
}


#pragma mark - UIButton Action - 

- (IBAction)allBtnAction:(id)sender {
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 2 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
    } else {
        CGRect frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
    }
}

- (IBAction)moneyInBtnAction:(id)sender {
    CGRect frame = tableBackgroundScrollView.bounds;
    frame.origin.x = 1 * CGRectGetWidth(self.view.frame);
    [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
}

- (IBAction)moneyOutBtnAction:(id)sender {
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
    } else {
        CGRect frame = tableBackgroundScrollView.bounds;
        frame.origin.x = 2 * CGRectGetWidth(self.view.frame);
        [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark - UITableView DataSource - 

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == allHistoryTableView || tableView == moneyInTableView || tableView == moneyOutTableView) {
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == allHistoryTableView) {
        return  walletAllHistory.count;
    } else if (tableView == moneyInTableView) {
        return walletInHistory.count;
    } else if (tableView == moneyOutTableView) {
        return  walletOutHistory.count;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict;
    if (tableView == allHistoryTableView) {
        dict =  walletAllHistory[indexPath.row];
    } else if (tableView == moneyInTableView) {
        dict =  walletInHistory[indexPath.row];
    } else if (tableView == moneyOutTableView) {
        dict =  walletOutHistory[indexPath.row];
    }
    walletHistoryTableViewCell *cell = (walletHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"walletHistoryCell"];
    [cell updateCellUI:dict];
    cell.lineView.frame = CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1);
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == tableBackgroundScrollView) {
        CGFloat scrollX = scrollView.contentOffset.x;
        CGRect sliderFrame = self.slider.frame;
        [self.allBtn setSelected:NO];
        [self.moneyInBtn setSelected:NO];
        [self.moneyOutBtn setSelected:NO];
        
        CGFloat sliderWidth = [UIScreen mainScreen].bounds.size.width/3;
        if ([Helper isCurrentLanguageRTL]) {
            sliderFrame.origin.x = scrollX/3;
            self.slider.frame = sliderFrame;
            if (self.slider.frame.origin.x > 3*sliderWidth/2) {
                [self.allBtn setSelected:YES];
            } else if(self.slider.frame.origin.x > sliderWidth/2 && self.slider.frame.origin.x < 3*sliderWidth/2) {
                [self.moneyInBtn setSelected:YES];
            } else if (self.slider.frame.origin.x < sliderWidth/2) {
                [self.moneyOutBtn setSelected:YES];
            }
        } else {
            sliderFrame.origin.x = scrollX/3;
            self.slider.frame = sliderFrame;
            if (self.slider.frame.origin.x <  sliderWidth/2) {
                [self.allBtn setSelected:YES];
            } else if(self.slider.frame.origin.x > sliderWidth/2 && self.slider.frame.origin.x < 3*sliderWidth/2) {
                [self.moneyInBtn setSelected:YES];
            } else if (self.slider.frame.origin.x > 3*sliderWidth/2) {
                [self.moneyOutBtn setSelected:YES];
            }
        }
    }
}


#pragma mark - WebServices -

-(void) getWalletHistory:(NSString *)pageIndex {
    NSDictionary *params = @{@"ent_sess_token":[Utility sessionToken],
                             @"ent_dev_id":[Utility deviceID],
                             @"ent_sid":flStrForStr([Utility userID]),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_page_index":pageIndex
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getWalletHistory"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       indexNumber++;
                                       [self getWalletHistoryResponse:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void) getWalletHistoryResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if (responseDict[@"Error"]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:responseDict[@"Error"]];
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        walletAllHistory =  [responseDict[@"walletHistory"] mutableCopy];
        for (NSDictionary *dict  in walletAllHistory) {
            if ([flStrForStr(dict[@"type"]) integerValue] == 1) {
                [walletInHistory addObject:dict];
            } else if ([flStrForStr(dict[@"type"]) integerValue] == 2) {
                [walletOutHistory addObject:dict];
            }
        }
        [self.moneyInTableView reloadData];
        [self.moneyOutTableView reloadData];
        [self.allHistoryTableView reloadData];
    }
}


@end
