//
//  WebViewController.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate, NSURLConnectionDelegate>
@property(nonatomic,strong) NSString *weburl;
@property(strong,nonatomic) UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSInteger isComingFromVC;
@end
