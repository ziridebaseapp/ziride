//
//  WebViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "WebViewController.h"
#import "SplashViewController.h"
#import "Database.h"
#import "CardDetails.h"
#import "User.h"

@interface WebViewController () {
    BOOL isSuccessed;
    BOOL isRequestSubmitted;
}

@end

@implementation WebViewController

@synthesize webView,weburl;
@synthesize backButton;
@synthesize isComingFromVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UIView Life Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    isSuccessed = NO;
    isRequestSubmitted = NO;
    [self createNavLeftButton];
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    self.navigationController.navigationBarHidden = NO;
    webView.delegate= self;
    NSURL *url = [NSURL URLWithString:weburl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [webView loadRequest:requestObj];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@""];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.webView stopLoading];
    self.webView.delegate = nil;
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Custom Navigation 

/**
 Creates Navigation Left Button
 */
-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    if ([Helper isCurrentLanguageRTL]) {
        navCancelButton.transform = CGAffineTransformMakeScale(-1, 1);
    } else {
        navCancelButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    [navCancelButton setFrame:CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height)];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_off"] forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateSelected];
    [navCancelButton setImage:[UIImage imageNamed:@"back_icon_on"] forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 0;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 Navigation Left Button Action
 */
-(void)cancelButtonClicked {
    if (!isSuccessed) {
        [Helper checkForPop:self.navigationController.view];
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please wait for while", @"Please wait for while")];
    }
}


#pragma mark - NSURLConnectionDelegate -

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}

#pragma mark - Web View Delegate - 

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *requestUrl = [request.URL absoluteString];
    if(isComingFromVC == 2) {
        if ([requestUrl rangeOfString:@"success.php"].location != NSNotFound) {
            isSuccessed = YES;
        }
    } else if ([requestUrl rangeOfString:@"submit_help"].location != NSNotFound) {
        isRequestSubmitted = YES;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (isSuccessed) {
        isSuccessed = NO;
        [self getCardDetailsFromService];
    } else if(isRequestSubmitted){
        isRequestSubmitted = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHomeVC" object:nil userInfo:nil];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}



#pragma mark - Custom Methods -

/**
 Get cards details API service call if network check
 */
-(void) getCardDetailsFromService {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
        [self sendServiceToGetCardDetail];
    } else {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}

#pragma mark - WebService call -

/**
 Get cards details API service
 */
-(void)sendServiceToGetCardDetail {
    NSDictionary *params = @{@"ent_sess_token": [Utility sessionToken],
                             @"ent_dev_id": [Utility deviceID],
                             @"ent_date_time":[Helper getCurrentNetworkDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getCardDetails:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Loading...", @"Loading...")];
}

/**
 Get Cards details reponse

 @param response response data
 */
-(void)getCardDetails:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7)) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil userInfo:nil];
        } else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            [self addCardsInDataBase:dictResponse[@"cards"]];
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"amount"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        } else if([response[@"errFlag"] integerValue] == 1 && [response[@"errNum"] integerValue] == 51) {
            NSString *currentWalletAmount = [Helper getCurrencyLocal:[flStrForObj(dictResponse[@"amount"]) floatValue]];
            [Utility setWalletBalance:currentWalletAmount];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        }
        if ([Helper checkForUpdateAppVersion:response[@"versions"][@"iosCust"]]) {
            return;
        }
        isSuccessed = NO;
        [self performSelector:@selector(cancelButtonClicked) withObject:self afterDelay:0];
    }
}

#pragma mark - Database Methods - 

/**
 Add cards in Local Database

 @param newCards cards Array from service response
 */
-(void)addCardsInDataBase:(NSArray *)newCards {
    NSString *defaultCardID;
    NSArray *arrDBResult = [[Database getCardDetails] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filterdArray.count) {
        CardDetails *card = filterdArray[0];
        defaultCardID = card.idCard;
    }
    [Database DeleteAllCard];
    [newCards enumerateObjectsUsingBlock:^(id dict, NSUInteger index, BOOL *stop) {
        NSMutableDictionary *cardDict = [dict mutableCopy];
        if ((defaultCardID.length && [cardDict[@"id"] isEqualToString:defaultCardID]) || (!defaultCardID.length && index == 0)) {
            [cardDict setObject:@"1" forKey:@"isDefault"];
        } else {
            [cardDict setObject:@"0" forKey:@"isDefault"];
        }
        [Database makeDataBaseEntry:cardDict];
        if (index >= newCards.count) {
            *stop = YES;
        }
    }];
}

@end
