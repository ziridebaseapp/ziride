
//  XDKAirMenuController.m
//  XDKAirMenu
//
//  Created by Xavier De Koninck on 29/12/2013.
//  Copyright (c) 2013 XavierDeKoninck. All rights reserved.
//

#import "XDKAirMenuController.h"
#import "User.h"
#import "SplashViewController.h"
#import "PaymentViewController.h"
#import "RPCustomIOSAlertView.h"
#import "UILabel+DynamicHeight.h"
#import "ReceiptView.h"
#import "PopUpWebView.h"
#import "PostiveRatingView.h"
#import "NegativeRatingView.h"
#import "CancelledReasonView.h"
#import "CancelledPopUpView.h"
#import "surgePricePopUp.h"
#import "PromoCodeView.h"
#import "InvoiceView.h"
#import "RPPassCodeAuthenticator.h"

//#define WIDTH_OPENED (93.0f)
//#define MIN_SCALE_CONTROLLER (0.6f)
//#define MIN_SCALE_TABLEVIEW (0.8f)
//#define MIN_ALPHA_TABLEVIEW (0.01f)
//#define DELTA_OPENING (65.f)

#define WIDTH_OPENED (95.f)
#define MIN_SCALE_CONTROLLER (1.0f)
#define MIN_SCALE_TABLEVIEW (0.8f)
#define MIN_ALPHA_TABLEVIEW (0.01f)
#define DELTA_OPENING (50.f)

@interface XDKAirMenuController ()<UITableViewDelegate, UIGestureRecognizerDelegate,UITableViewDataSource,UserDelegate>

@property (nonatomic, strong) NSArray *images_off;
@property (nonatomic, strong) NSArray *images_on;
@property (nonatomic, strong) NSArray *menuTitle;
@property (nonatomic, assign) CGPoint startLocation;
@property (nonatomic, assign) CGPoint lastLocation;
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, assign) CGFloat widthOpened;
@property (nonatomic, assign) CGFloat minScaleController;
@property (nonatomic, assign) CGFloat minScaleTableView;
@property (nonatomic, assign) CGFloat minAlphaTableView;
@property UIPanGestureRecognizer *panGesture, *panGesture1;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicator;

@end

@implementation XDKAirMenuController
@synthesize panGesture,panGesture1;
@synthesize currentWalletAmount;
@synthesize activityIndicator;


static XDKAirMenuController *controller = nil;

+ (id)sharedMenu {
    if (!controller) {
        controller  = [[XDKAirMenuController alloc] init];
    }
    return controller;
}

-(void)sharedRelease {
    for (UIGestureRecognizer *recognizer in self.currentViewController.view.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        } else {
            recognizer.delegate = nil;
            [self.currentViewController.view removeGestureRecognizer:recognizer];
        }
    }
}

+(BOOL)relese {
    [controller.panGesture.view removeGestureRecognizer:controller.panGesture];
    [controller.panGesture1.view removeGestureRecognizer:controller.panGesture1];
    controller.panGesture.delegate = nil;
    controller.panGesture = nil;
    controller.panGesture1.delegate = nil;
    controller.panGesture1 = nil;
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    for (UIGestureRecognizer *recognizer in frontWindow.gestureRecognizers){
        [frontWindow removeGestureRecognizer:recognizer];
    }
    NSArray *gestrue =  [[[XDKAirMenuController sharedMenu] view] gestureRecognizers];
    for (UIGestureRecognizer *recognizer in gestrue) {
        [[[XDKAirMenuController sharedMenu] view] removeGestureRecognizer:recognizer];
    }
    [[self sharedMenu] sharedRelease];
    controller.airDelegate = Nil;
    controller = Nil;
    return YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _images_off = [[NSArray alloc]initWithObjects:@"menu_user", @"menu_home", @"menu_history", @"menu_wallet",  @"menu_support", @"menu_share", @"menu_about", nil];//@"menu_history",
    
    _images_on = [[NSArray alloc]initWithObjects:@"menu_user_on",  @"menu_home_on", @"menu_history_on", @"menu_wallet_on",  @"menu_support_on", @"menu_share_on", @"menu_about_on", nil];//@@"menu_history_on",
    
    _menuTitle = [[NSArray alloc]initWithObjects:NSLocalizedString(@"View Profile", @"View Profile"),
                  NSLocalizedString(@"Request ZiRide", @"Request ZiRide"),
                  NSLocalizedString(@"History", @"History"),
                  NSLocalizedString(@"My Wallet", @"My Wallet"),
                  NSLocalizedString(@"Support", @"Support"),
                  NSLocalizedString(@"Refer a Friend", @"Refer a Friend"),
                  NSLocalizedString(@"About", @"About"),
                  nil];
//     NSLocalizedString(@"Invoice", @"Invoice"),
    if ([self.airDelegate respondsToSelector:@selector(tableViewForAirMenu:)]) {
        self.tableView = [self.airDelegate tableViewForAirMenu:self];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    self.widthOpened = WIDTH_OPENED;
    self.minAlphaTableView = MIN_ALPHA_TABLEVIEW;
    self.minScaleTableView = MIN_SCALE_TABLEVIEW;
    self.minScaleController = MIN_SCALE_CONTROLLER;
    if ([self.airDelegate respondsToSelector:@selector(widthControllerForAirMenu:)])
        self.widthOpened = [self.airDelegate widthControllerForAirMenu:self];
    if ([self.airDelegate respondsToSelector:@selector(minAlphaTableViewForAirMenu:)])
        self.minAlphaTableView = [self.airDelegate minAlphaTableViewForAirMenu:self];
    if ([self.airDelegate respondsToSelector:@selector(minScaleTableViewForAirMenu:)])
        self.minScaleTableView = [self.airDelegate minScaleTableViewForAirMenu:self];
    if ([self.airDelegate respondsToSelector:@selector(minScaleControllerForAirMenu:)])
        self.minScaleController = [self.airDelegate minScaleControllerForAirMenu:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableViewReload) name:@"RELOADXDK" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHome) name:@"OpenHomeVC" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogoutSucessfullyFromPush) name:@"Logout" object:nil];
    _isMenuOpened = FALSE;
    [self openViewControllerAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
}


-(void)tableViewReload {
    [self.tableView beginUpdates];
    NSIndexPath *indexPath0 = [NSIndexPath indexPathForRow:0 inSection:0];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:3 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath0, indexPath1, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

-(void)openHome {
    _isMenuOpened = FALSE;
    [self openMenuAnimated];
    [self openViewControllerAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
}

-(void)userDidLogoutSucessfullyFromPush {
    for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
        if([view isKindOfClass:[InvoiceView class]] || [view isKindOfClass:[ReceiptView class]] || [view isKindOfClass:[PostiveRatingView class]] || [view isKindOfClass:[NegativeRatingView class]] || [view isKindOfClass:[PopUpWebView class]] || [view isKindOfClass:[PromoCodeView class]] || [view isKindOfClass:[CancelledReasonView class]] || [view isKindOfClass:[CancelledPopUpView class]] || [view isKindOfClass:[surgePricePopUp class]]) {
            [view removeFromSuperview];
        }
    }
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (panGesture) {
        [panGesture.view removeGestureRecognizer:panGesture];
        panGesture.delegate = nil;
        panGesture = nil;
    }
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    panGesture.delegate = self;
    [frontWindow addGestureRecognizer:panGesture];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (self.isMenuOpened)
        [self closeMenuAnimated];
}


#pragma mark - Gestures

- (void)panGesture:(UIPanGestureRecognizer *)sender {
    if ([Helper isCurrentLanguageRTL]) {
        self.isMenuOnRight = YES;
    } else {
        self.isMenuOnRight = NO;
    }
    if (sender.state == UIGestureRecognizerStateBegan)
        self.startLocation = [sender locationInView:self.view];
    else if (sender.state == UIGestureRecognizerStateEnded
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight)))) {
                     CGFloat dx = self.lastLocation.x - self.startLocation.x;
                     if (self.isMenuOnRight){
                         if ((self.isMenuOpened && dx > 0.f) || self.view.frame.origin.x > 3*self.view.frame.size.width / 4)
                             [self closeMenuAnimated];
                         else
                             [self openMenuAnimated];
                     } else {
                         if ((self.isMenuOpened && dx < 0.f) || self.view.frame.origin.x < self.view.frame.size.width / 4)
                             [self closeMenuAnimated];
                         else
                             [self openMenuAnimated];
                     }
                 }
    else if (sender.state == UIGestureRecognizerStateChanged
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight))))
        [self menuDragging:sender];
}

- (void)menuDragging:(UIPanGestureRecognizer *)sender {
    CGPoint stopLocation = [sender locationInView:self.view];
    self.lastLocation = stopLocation;
    CGFloat dx = stopLocation.x - self.startLocation.x;
    CGFloat distance = dx;
    CGFloat width = (self.isMenuOnRight) ? (-self.view.frame.size.width + self.widthOpened) : (self.view.frame.size.width - self.widthOpened);
    CGFloat scaleController = 1 - ((self.view.frame.origin.x / width) * (1 - self.minScaleController));
    CGFloat scaleTableView = 1 - ((1 - self.minScaleTableView) + ((self.view.frame.origin.x / width) * (-1+self.minScaleTableView)));
    CGFloat alphaTableView = 1 - ((1 - self.minAlphaTableView) + ((self.view.frame.origin.x / width) * (-1+self.minAlphaTableView)));
    if (scaleTableView < self.minScaleTableView)
        scaleTableView = self.minScaleTableView;
    if (scaleController > 1.f)
        scaleController = 1.f;
    self.tableView.transform = CGAffineTransformMakeScale(scaleTableView, scaleTableView);
    self.tableView.alpha = alphaTableView;
    self.currentViewController.view.transform = CGAffineTransformMakeScale(scaleController, scaleController);
    CGRect frame = self.view.frame;
    frame.origin.x = frame.origin.x + distance;
    if ([Helper isCurrentLanguageRTL]) {
        self.isMenuOnRight = YES;
        if (frame.origin.x < -frame.size.width)
            frame.origin.x = -frame.size.width;
        if (frame.origin.x > 0.f)
            frame.origin.x = 0.f;
    } else {
        self.isMenuOnRight = NO;
        if (frame.origin.x < 0.f)
            frame.origin.x = 0.f;
        if (frame.origin.x > (frame.size.width))
            frame.origin.x = (frame.size.width);
    }
    self.view.frame = frame;
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    self.currentViewController.view.frame = frame;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    [self.currentViewController.view endEditing:YES];
    if (self.isMenuOpened)
        return TRUE;
    return FALSE;
}


#pragma mark - Menu

- (void)openViewControllerAtIndexPath:(NSIndexPath*)indexPath {
    if ([self.airDelegate respondsToSelector:@selector(airMenu:viewControllerAtIndexPath:)]) {
        BOOL firstTime = FALSE;
        if (self.currentViewController == nil)
            firstTime = TRUE;
        _currentViewController = [self.airDelegate airMenu:self viewControllerAtIndexPath:indexPath];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMenuAnimated)];
        tapGesture.delegate = self;
        [self.currentViewController.view addGestureRecognizer:tapGesture];
        CGRect frame = self.view.frame;
        frame.origin.x = 0.f;
        frame.origin.y = 0.f;
        self.currentViewController.view.frame = frame;
        frame = self.view.frame;
        frame.origin.x = 0.f;
        frame.origin.y = 0.f;
        self.view.frame = frame;
        self.currentViewController.view.autoresizesSubviews = TRUE;
        self.currentViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        for (UIView *subView in self.view.subviews) {
            if (self.view.subviews.count > 1) {
                [subView removeFromSuperview];
            }
        }
        [self.view addSubview:self.currentViewController.view];
        [self addChildViewController:self.currentViewController];
        if (panGesture1) {
            [panGesture1.view removeGestureRecognizer:panGesture1];
            panGesture1.delegate = nil;
            panGesture1 = nil;
        }
        panGesture1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
        if (!(indexPath.row == 3 || indexPath.row == 7)) {
            [self.view addGestureRecognizer:panGesture1];
        }
        if (!firstTime)
            [self openingAnimation];
    }
}

- (void)openingAnimation {
    self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
    CGRect frame = self.view.frame;
    if ([Helper isCurrentLanguageRTL]) {
        self.isMenuOnRight = YES;
        frame.origin.x = -frame.size.width + self.widthOpened;
        
    } else {
        self.isMenuOnRight = NO;
        frame.origin.x = frame.size.width - self.widthOpened;
    }
    //    if (self.isMenuOnRight)
    //        frame.origin.x = -frame.size.width + self.widthOpened;
    //    else
    //        frame.origin.x = frame.size.width - self.widthOpened;
    
    self.view.frame = frame;
    self.tableView.alpha = 1.f;
    self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    self.currentViewController.view.frame = frame;
    [self closeMenuAnimated];
}


#pragma mark - Actions -

- (void)openMenuAnimated {
    [UIView animateWithDuration:0.3f animations:^{
        self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
        CGRect frame = self.view.frame;
        if ([Helper isCurrentLanguageRTL]) {
            self.isMenuOnRight = YES;
            frame.origin.x = -frame.size.width + self.widthOpened;
            
        } else {
            self.isMenuOnRight = NO;
            frame.origin.x = frame.size.width - self.widthOpened;
        }
        
        //        if (self.isMenuOnRight)
        //        else
        self.view.frame = frame;
        self.tableView.alpha = 1.f;
        self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
        frame = self.currentViewController.view.frame;
        if (self.isMenuOnRight)
            frame.origin.x = self.view.frame.size.width - frame.size.width;
        else
            frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    _isMenuOpened = TRUE;
}

- (void)closeMenuAnimated {
    [UIView animateWithDuration:0.3f animations:^{
        self.currentViewController.view.transform = CGAffineTransformMakeScale(1.f, 1.f);
        CGRect frame = self.view.frame;
        frame.origin.x = 0.f;
        self.view.frame = frame;
        self.tableView.alpha = self.minAlphaTableView;
        self.tableView.transform = CGAffineTransformMakeScale(self.minScaleTableView, self.minScaleTableView);
        frame = self.currentViewController.view.frame;
        frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    _isMenuOpened = FALSE;
}


#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 8) {
    } else {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
        [self openViewControllerAtIndexPath:indexPath];
    }
}

#pragma mark - Setters

- (void)setIsMenuOnRight:(BOOL)isMenuOnRight {
    if (self.view.superview == nil)
        _isMenuOnRight = isMenuOnRight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _menuTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == 0) {
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth - WIDTH_OPENED, 120)];
        headerView.tag = 501;
        headerView.backgroundColor = UIColorFromRGB(0x019F6E);
        UIImageView *profileImageView = [[UIImageView alloc] init];
        UILabel *nameLabel = [[UILabel alloc] init];
        if ([Helper isCurrentLanguageRTL]) {
            profileImageView.frame = CGRectMake(screenWidth - WIDTH_OPENED - 15 - 50, 30, 50, 50);
            nameLabel.frame = CGRectMake(screenWidth - WIDTH_OPENED - 70 - (screenWidth - WIDTH_OPENED - 110), 15, screenWidth - WIDTH_OPENED - 110, 70);
            nameLabel.textAlignment = NSTextAlignmentRight;
        } else {
            profileImageView.frame = CGRectMake(15, 35, 60, 60);
            nameLabel.frame = CGRectMake(90, 30, screenWidth - WIDTH_OPENED - 100, 70);
            nameLabel.textAlignment = NSTextAlignmentLeft;
        }
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
        profileImageView.clipsToBounds = YES;
        [headerView addSubview:profileImageView];
        NSString *profileImgURL = [Utility userProfileImage];
        if(profileImgURL.length) {
            CGRect fra1 = profileImageView.frame;
            fra1.origin.x = profileImageView.frame.size.width/2-10;
            fra1.origin.y = profileImageView.frame.size.height/2-10;
            fra1.size.width = 20;
            fra1.size.height = 20;
            activityIndicator = [[UIActivityIndicatorView alloc]init];
            activityIndicator.frame = fra1;
            [profileImageView addSubview:activityIndicator];
            activityIndicator.backgroundColor=[UIColor clearColor];
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [activityIndicator startAnimating];
            [profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImgURL]
                                placeholderImage:[UIImage imageNamed:@"driverImage"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           if (!error && image)
                                               profileImageView.image = image;
                                           [activityIndicator stopAnimating];
                                           [activityIndicator hidesWhenStopped];
                                       }];
        } else {
            profileImageView.image = [UIImage imageNamed:@"driverImage"];
        }
        
        nameLabel.numberOfLines = 0;
        nameLabel.textColor = [UIColor whiteColor];
        [headerView addSubview:nameLabel];
        NSString *name = flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserName]).uppercaseString;
        NSString *mobileNumber = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserEmailID]);
        NSString *str1 = [name capitalizedString];
        NSString *str2 = mobileNumber;
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentCenter];
        [style setLineBreakMode:NSLineBreakByWordWrapping];
        
        UIFont *font1 = [UIFont fontWithName:fontMedium size:20];
        UIFont *font2 = [UIFont fontWithName:fontNormal  size:15];
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                                NSFontAttributeName:font1,
                                NSForegroundColorAttributeName:UIColorFromRGB(0xFFFFFF),
                                NSParagraphStyleAttributeName:style}; // Added line
        NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                                NSFontAttributeName:font2,
                                NSForegroundColorAttributeName:UIColorFromRGB(0xFFFFFF),
                                NSParagraphStyleAttributeName:style}; // Added line
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",str1] attributes:dict1]];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:str2 attributes:dict2]];
        nameLabel.attributedText = attString;
        if ([Helper isCurrentLanguageRTL]) {
            nameLabel.textAlignment = NSTextAlignmentRight;
        } else {
            nameLabel.textAlignment = NSTextAlignmentLeft;
        }
        [cell addSubview:headerView];
        cell.backgroundColor = UIColorFromRGB(0x019F6E);
        return cell;
    }
    UIImage *cellimageNormal = [UIImage imageNamed:_images_off[indexPath.row]];
    UIImage *cellimageSelected = [UIImage imageNamed:_images_on[indexPath.row]];
    UIButton *setMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [setMenuButton setBackgroundColor:[UIColor clearColor]];
    if (indexPath.row == 1) {
        setMenuButton.frame = CGRectMake(0, 10, screenWidth - WIDTH_OPENED, 50);
    } else {
        setMenuButton.frame = CGRectMake(0, 0, screenWidth - WIDTH_OPENED, 50);
    }
    NSString *title = @"";
    currentWalletAmount = [Utility walletBalance];
    if (indexPath.row == 3 && currentWalletAmount.length) {
        title = [NSString stringWithFormat:@"%@ (%@)", _menuTitle[indexPath.row], currentWalletAmount];
    } else {
        title = _menuTitle[indexPath.row];
    }
    [Helper setButton:setMenuButton Text:title WithFont:fontMedium FSize:15 TitleColor:UIColorFromRGB(0x484848) ShadowColor:nil];
    [setMenuButton setTitleColor:UIColorFromRGB(0x019F6E) forState:UIControlStateHighlighted];
    [setMenuButton addTarget:self action:@selector(buttonPressedAction:) forControlEvents:UIControlEventTouchUpInside];
    setMenuButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    setMenuButton.titleLabel.numberOfLines = 0;
    [setMenuButton setImage:cellimageNormal forState:UIControlStateNormal];
    [setMenuButton setImage:cellimageSelected forState:UIControlStateSelected];
    [setMenuButton setImage:cellimageSelected forState:UIControlStateHighlighted];
    setMenuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    setMenuButton.titleEdgeInsets = UIEdgeInsetsMake(5, 20, 0, 0);
    setMenuButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [setMenuButton setTag:indexPath.row + 100];
    [cell.contentView addSubview:setMenuButton];
    cell.backgroundColor = [UIColor whiteColor];
    if ([Helper isCurrentLanguageRTL]) {
        setMenuButton.transform = CGAffineTransformMakeScale(-1, 1);
        setMenuButton.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
        
    } else {
        setMenuButton.transform = CGAffineTransformMakeScale(1, 1);
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight;
    switch (indexPath.row) {
        case 0:
            cellHeight = 120;
            break;
        case 1:
            cellHeight = 60;
            break;
        default:
            cellHeight = 50;
            break;
    }
    return cellHeight;
}

#pragma mark- ButtonAction

-(void)buttonPressedAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.tag) {
        [self tableView:_tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag - 100 inSection:0]];
    }
}

#pragma mark - UserDelegate
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}

-(void)userDidFailedToLogout:(NSError *)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    User *user = [[User alloc] init];
    [user deleteUserSavedData];
}

@end
