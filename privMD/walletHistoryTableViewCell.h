//
//  walletHistoryTableViewCell.h
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface walletHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *paymentStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *walletBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLable;

@property (weak, nonatomic) IBOutlet UIView *lineView;
-(void)updateCellUI:(NSDictionary *)dict;

@end
