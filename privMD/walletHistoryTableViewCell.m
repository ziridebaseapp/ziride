//
//  walletHistoryTableViewCell.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "walletHistoryTableViewCell.h"

@implementation walletHistoryTableViewCell
@synthesize paymentStatusImageView;
@synthesize walletBalanceLabel;
@synthesize dateLabel;
@synthesize amountLable;

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    self.contentView.frame = CGRectMake(0, 0, screenWidth, self.frame.size.height);
    self.paymentStatusImageView.frame = CGRectMake(5, 10, 20, 20);
    self.walletBalanceLabel.frame = CGRectMake(25, 10, screenWidth-25, 20);
    self.dateLabel.frame = CGRectMake(25, 40, screenWidth-25-95, 20);
    self.amountLable.frame = CGRectMake(screenWidth-25-95, 40, 95, 20);
    if ([Helper isCurrentLanguageRTL]) {
        CGRect frame = self.paymentStatusImageView.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.paymentStatusImageView.frame = frame;
        
        frame = self.walletBalanceLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.walletBalanceLabel.frame = frame;
            
        frame = self.dateLabel.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.dateLabel.frame = frame;
        
        frame = self.amountLable.frame;
        frame.origin.x = screenWidth - frame.origin.x - frame.size.width;
        self.amountLable.frame = frame;
        
        self.walletBalanceLabel.textAlignment = NSTextAlignmentRight;
        self.dateLabel.textAlignment = NSTextAlignmentRight;
        self.amountLable.textAlignment = NSTextAlignmentLeft;
    } else {
        self.walletBalanceLabel.textAlignment = NSTextAlignmentLeft;
        self.dateLabel.textAlignment = NSTextAlignmentLeft;
        self.amountLable.textAlignment = NSTextAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateCellUI:(NSDictionary *)dict {
    NSString *balanceAmount = [Helper getCurrencyLocal:[flStrForObj(dict[@"amount"]) floatValue]];
    if ([flStrForStr(dict[@"type"]) integerValue] == 1) {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"down_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"ZiRide Money Balance", @"ZiRide Money Balance")];
    } else {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"up_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"ZiRide Money Balance", @"ZiRide Money Balance")];
    }
    self.dateLabel.text = [self getDate:flStrForStr(dict[@"date"])];
    self.amountLable.text = balanceAmount;
}

-(NSString*)getDate:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EE, d LLLL yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

@end
