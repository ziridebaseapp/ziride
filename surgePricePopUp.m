//
//  surgePricePopUp.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "surgePricePopUp.h"

@implementation surgePricePopUp
@synthesize onCompletion;
@synthesize messageLabel, vehicleImage, vehicleNameLabel, surgePrice, surgeRoundView, backgroundView;
@synthesize acceptBtn,cancelBtn;

-(id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"surgePricePopUpView" owner:self options:nil] firstObject];
    return self;
}


/**
 Show Surge Price Pop Up view

 @param dict Car Type Details
 @param window Application Window
 */
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    surgeRoundView.layer.masksToBounds = YES;
    surgeRoundView.layer.cornerRadius = surgeRoundView.frame.size.width/2;
    surgeRoundView.layer.borderColor = [UIColor whiteColor].CGColor;
    surgeRoundView.layer.borderWidth = 2.0f;
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
   [Helper setToLabel:messageLabel Text:NSLocalizedString(@"Fares have increased, click on I agree for higher fare to get a taxi or click on cancel to go back.", @"Fares have increased, click on I agree for higher fare to get a taxi or click on cancel to go back.") WithFont:fontNormal FSize:13 Color:[UIColor blackColor]];
    [Helper setToLabel:surgePrice Text:dict[@"surgePrice"] WithFont:fontNormal FSize:28 Color:[UIColor whiteColor]];
    vehicleImage.image = dict[@"vehicleImage"];
    NSString *fare = NSLocalizedString(@"Fares", @"Fares");
    [Helper setToLabel:vehicleNameLabel Text:[NSString stringWithFormat:@"%@ %@", dict[@"vehicleName"], fare] WithFont:fontNormal FSize:12 Color:[UIColor whiteColor]];
    [Helper setButton:cancelBtn Text:NSLocalizedString(@"Cancel", @"Cancel") WithFont:fontNormal FSize:13 TitleColor:[UIColor blackColor] ShadowColor:nil];
    [Helper setButton:acceptBtn Text:NSLocalizedString(@"I accept higher price", @"I accept higher price") WithFont:fontNormal FSize:13 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}


/**
 Cancel Button Action

 @param sender Cancel Buttron
 */
-(IBAction)cancelBtnAction:(id)sender {
    onCompletion(0);
    [self hidePOPup];
}

/**
 Accept Fare Button Action

 @param sender Accept Fare Button
 */
- (IBAction)acceptFareBtnAction:(id)sender {
    onCompletion(1);
    [self hidePOPup];
}


/**
 Hide Pop Up View
 */
-(void)hidePOPup {
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

@end
